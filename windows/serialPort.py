# Open and configure a serial port
from __future__ import print_function
import serial
import sys
import os

if os.name == 'nt':
	from serial.tools.list_ports_windows import comports
elif os.name == 'posix':
	from serial.tools.list_ports_posix import comports
else:
	raise ImportError("Sorry: no implementation for your platform ('{}') available".format(os.name))

for (portName, desc, hwid) in comports():
	sys.stdout.write("{}: ".format(portName))
	try:
		port = serial.Serial(portName, 115200)
		sys.stdout.write("Baud={}, Data={}, Stop={}, Parity={}\n".format(port.baudrate,
																			port.bytesize,
																			port.stopbits,
																			port.parity))
		port.close()
	except:
		sys.stdout.write("SKIPPING\n")
