cmake_minimum_required (VERSION 2.8.12)
project (thruster_config)
set (CMAKE_MODULE_PATH ${CMAKE_MODULE_PATH} "${CMAKE_SOURCE_DIR}/cmake/Modules/")

# Qt cmake options
set (CMAKE_AUTOMOC ON)
set (CMAKE_AUTOUIC ON)
set (CMAKE_AUTORCC ON)
find_package (Qt5 REQUIRED QtCore QtGui)

# Required modules
find_package (Log4cxx REQUIRED)
find_package (Doxygen REQUIRED)

# Generated application
set (EXE thruster_config)

# Include file directories
include_directories (
	${CMAKE_CURRENT_BINARY_DIR}
	${CMAKE_CURRENT_SOURCE_DIR}
	${QT_INCLUDES}
	${LOG4CXX_INCLUDE_DIR}
)

# Include files
set (CPP_HEADER_FILES  
	aboutDialog.h			aboutDialog_p.h
	app_com.h
	cfgUtil.h
	log.h
	main.h
	mainWindow.h			mainWindow_p.h
	portSelectionDialog.h	portSelectionDialog_p.h
	options.h
	programVersions.h
	serialPortUtil.h
	sigUtil.h
	strUtil.h
	testDialg.h				testDialg_.h
	timeUtil.h
)

# C++ source files
set (CPP_SOURCE_FILES
	aboutDialog.cpp
	cfgUtil.cpp
	log.cpp
	main.cpp
	mainWindow.cpp
	portSelectionDialog.cpp
	options.cpp
	programVersion.cpp
	serialPortUtil.cpp
	sigUtil.cpp
	strUtil.cpp
	testDialog.cpp
	timeUtil.cpp
)

# Resource files
set (RESOURCE_FILES
	thruster_config.qrc
)

# Ported Qt c++ source files
set (DOXYGEN_SOURCES
	configuration.dox
	usage.dox
)

# C++ compiler switches
add_definitions (
	-pedantic
	-pedantic-errors
	-Wall
	-Wcast-qual
	-Wconversion
	-Wdisabled-optimization
	-Werror
	-Wextra
	-Wfloat-equal
	-Winvalid-pch
	-Wlogical-op
	-Wmissing-include-dirs
	-Wno-long-long
	-Wpacked
	-Wshadow
	-Wsign-conversion
	-Wuninitialized
	-Wunused
	-Wundef
	-Wvla

#	-Winline
#	-Wmisleading-indentation
#	-Wmissing-declarations
#	-Wpadded
#	-Wredundant-declarations
#	-Wuseless-cast
)

foreach (it ${RESOURCE_FILES})
	QT5_ADD_RESOURCES (QRC_CPP ${it})
endforeach ()

add_executable (${EXE} ${CPP_SOURCE_FILES} ${CPP_HEADER_FILES} ${QRC_CPP})

# Libraries
target_link_libraries (${EXE}
	${QT_QTCORE_LIBRARIES}
	${QT_QTGUI_LIBRARIES}
	${LOG4CXX_LIBRARIES}
)

# Generated documentation
configure_file (${CMAKE_CURRENT_SOURCE_DIR}/Doxyfile.in ${CMAKE_CURRENT_BINARY_DIR}/Doxyfile @ONLY)

add_custom_target(doc ALL
${DOXYGEN_EXECUTABLE} ${CMAKE_CURRENT_BINARY_DIR}/Doxyfile >/dev/null
DEPENDS ${DOXYGEN_SOURCES} ${CPP_SOURCE_FILES} ${CPP_HEADER_FILES}
WORKING_DIRECTORY ${CMAKE_CURRENT_BINARY_DIR}
)

