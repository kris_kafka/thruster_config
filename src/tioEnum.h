#pragma once
#ifndef TIOENUM_H
#ifndef DOXYGEN_SKIP
#define TIOENUM_H
#endif
//######################################################################################################################
/*! @file
@brief Declare the interface to the empty int validator module.
*/
//######################################################################################################################

#include <QtCore/QList>
#include <QtCore/QObject>
#include <QtCore/QScopedPointer>
#include <QtCore/QString>

#include "configCsr.h"
#include "thruster.h"

//######################################################################################################################

class	ThrusterEnumIdPrivate;

//######################################################################################################################
/*! @brief VideoRay CSR protocol enumeration data.
*/
class TioEnumData
{
public:		// Constructors & destructors
	TioEnumData(void);
	TioEnumData(TioEnumData const &other);
	TioEnumData(int nodeId_, int groupId_, int deviceType_, int seenCount_, QString const &serialNumber_);
	TioEnumData(ByteP buffer);

public:		// Functions
	bool	operator==(TioEnumData const &other) const;

public:		// Data
	int		nodeId;			//!< Node ID.
	int		groupId;		//!< Group ID.
	int		deviceType;		//!< Device type.
	int		seenCount;		//!< Number of replies seen.
	QString	serialNumber;	//!< Serial number.
};

#ifndef DOXYGEN_SKIP
Q_DECLARE_TYPEINFO(TioEnumData, Q_PRIMITIVE_TYPE);
#endif

//######################################################################################################################
/*! @brief VideoRay CSR protocol ID broadcast request.
*/
class TioEnum
:
	public	QObject
{
	Q_OBJECT

public:		// Constructors & destructors
	TioEnum(QString const &serialPort, bool const volatile &halt);
	~TioEnum(void);

public:		// Functions
	void				close(void);
	Thruster::Error		enumerate(QList<TioEnumData> &enumData, int &duplicateNodeIdCount,
									int &duplicateSerialCount);
	Thruster::Error		open(void);

private:	// Data
	QScopedPointer<ThrusterEnumIdPrivate>
							d;				//!< Reply handler object.
};

//######################################################################################################################
#endif
