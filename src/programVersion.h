#pragma once
#ifndef PROGRAMVERSION_H
#ifndef DOXYGEN_SKIP
#define PROGRAMVERSION_H
#endif
//######################################################################################################################
/*! @file
@brief Declare the application version information.
*/
//######################################################################################################################

#include "videoray/applib/version.h"

//######################################################################################################################

namespace VersionInfomation
{
	extern software_version_t const	programVersion;
	extern char const				*buildType;
};

//######################################################################################################################
#endif
