/*! @file
@brief Top most applictaion functions.

Initialize the support modules, parse the command line, create and run the main window.
*/
//######################################################################################################################

#include "app_com.h"

#include <iostream>

#include <QtCore/QFile>
#include <QtCore/QRegularExpression>
#include <QtGui/QIcon>
#include <QtWidgets/QStyleFactory>
#include <QtWidgets/QApplication>

#include "iniFile.h"
#include "main.h"
#include "main_log.h"
#include "mainWindow.h"
#include "options.h"
#include "programVersion.h"
#include "timeUtil.h"

//######################################################################################################################

static void	setApplicationName(void);
static bool	setupDefaults(Options &options);

//######################################################################################################################
/*! @brief Top most applictaion function.

@return 0 if no errors else an error code.
*/
int
main(
	int		argc,	//!<[in] Number of arguments.
	char	**argv	//!<[in] Array of arguments.
)
{
	// Initialize the application data.
	QCoreApplication::setOrganizationName("VideoRay");
	QCoreApplication::setOrganizationDomain("videoray.com");
	QCoreApplication::setApplicationVersion(QString("%1 %2")
													.arg(VersionInfomation::programVersion.number)
													.arg(VersionInfomation::buildType));
	QApplication	application(argc, argv);
	setApplicationName();
//	QStyle			*style(QStyleFactory::create("cleanlooks"));
//	application.setStyle(style);
	application.setWindowIcon(QIcon(MainWindow::appIconName()));
	//
	// Parse the command line.
	Options options;
	options.parseCommandLine(argc, argv);
	//
	// Log our starting.
	LOG_Trace(logger, TimeUtil::populate("@{ApplicationName} started at !{DateTime}"));
	//
	// Setup defaults?
	int answer = 0;
	if (!setupDefaults(options)) {
		// Create and show the main window.
		MainWindow	mainWindow;
		mainWindow.show();
		answer = application.exec();
	}
	//
	// Log our ending.
	LOG_Trace(logger, TimeUtil::populate(QString("@{ApplicationName} ended at !{DateTime}, result=%1").arg(answer)));
	return answer;
}

//======================================================================================================================
/*! @brief Set the application name.
*/
void
setApplicationName(void)
{
	// Initialize the application data.
	QString				orgName	(QCoreApplication::applicationName());
	QRegularExpression	regExp	("d?(?:\\.pgm)?$");
	QString				newName	(orgName.replace(regExp, ""));
	QCoreApplication::setApplicationName(newName);
}

//======================================================================================================================
/*! @brief Handle setup modes.

@return true if setup mode.
*/
bool
setupDefaults(
	Options	&options	//!<[in] Options.
)
{
	// Setup defaults?
	bool	answer = false;
	QString filePath;
	if (options.isSetupIni(filePath)) {
		IniFile	iniFile(filePath);
		IniFile::setup(iniFile);
		answer = true;
	}
	if (options.isSetupLog(filePath)) {
		QFile	file(filePath);
		LogModule::setup(file);
		answer = true;
	}
	return answer;
}
