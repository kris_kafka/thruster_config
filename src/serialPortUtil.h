#pragma once
#ifndef SERIALPORTUTIL_H
#ifndef DOXYGEN_SKIP
#define SERIALPORTUTIL_H
#endif
//######################################################################################################################
/*! @file
@brief Declare the interface to the serial port utility module.
*/
//######################################################################################################################

#include <QtCore/QStringList>

//######################################################################################################################
/*! @brief Encapsulates the serial port utility module.
*/
namespace SerialPortUtil
{
	QStringList	availablePorts(void);
	QString		portNameToSystemLocation(const QString &portName);
	QString		portNameFromSystemLocation(const QString &portName);
};

//######################################################################################################################
#endif
