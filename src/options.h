#pragma once
#ifndef OPTIONS_H
#ifndef DOXYGEN_SKIP
#define OPTIONS_H
#endif
//######################################################################################################################
/*! @file
@brief Declare the interface to the program options module.
*/
//######################################################################################################################

#include <QtCore/QString>

//######################################################################################################################
/*! @brief The Options class provides the interface to the program options module.

The program options module provides access to the command line arguments.
*/
class Options
{
public:		// Constructors & destructors
	explicit	Options(void);
				~Options(void);

public:		// Functions
	bool		isEmulation(void);
	bool		isSetupIni(QString &filePath);
	bool		isSetupLog(QString &filePath);
	bool		isShowOnScreenPort(void);
	bool		isShowOnScreenStatus(void);
	QString		serialPort(void);
	void		serialPort(QString const &port);
	void		parseCommandLine(int argc, char **argv);
	QString		userSettingsFileName(void);
};

//######################################################################################################################
#endif
