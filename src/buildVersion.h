#pragma once
#ifndef BUILDVERSION_H
#ifndef DOXYGEN_SKIP
#define BUILDVERSION_H
#endif
//######################################################################################################################
/*! @file
@brief Define the version numbers of the program.
*/
//######################################################################################################################
// Utility macros.

//----------------------------------------------------------------------------------------------------------------------
/*! @hideinitializer Make a string of a symbols value.

@param v	Value to be converted to a string.
*/
#define BV_MK_STR_(v)	#v

//----------------------------------------------------------------------------------------------------------------------
/*! @hideinitializer Make a string of a symbols value.

@param v	Value to be converted to a string.
*/
#define BV_MK_STR(v)	BV_MK_STR_(v)

//######################################################################################################################
// Numeric version numbers.

#define BUILD_NUM_MAJOR		1	//!< @hideinitializer Major version number.
#define BUILD_NUM_MINOR		0	//!< @hideinitializer Minor version number.
#define BUILD_NUM_BUILD		7	//!< @hideinitializer Build number.
#define BUILD_NUM_REVISION	0	//!< @hideinitializer Revision number.

// String version numbers.

#define BUILD_STR_MAJOR		BV_MK_STR(BUILD_NUM_MAJOR)		//!< @hideinitializer Major version number string.
#define BUILD_STR_MINOR		BV_MK_STR(BUILD_NUM_MINOR)		//!< @hideinitializer Minor version number string.
#define BUILD_STR_BUILD		BV_MK_STR(BUILD_NUM_BUILD)		//!< @hideinitializer Build number string.
#define BUILD_STR_REVISION	BV_MK_STR(BUILD_NUM_REVISION)	//!< @hideinitializer Revision number string.

//######################################################################################################################
// Build type.

#if IS_RELEASE_BUILD
	#define BUILD_STR_TYPE	"Release"	//!< @hideinitializer Build type.
#else
	#define BUILD_STR_TYPE	"Debug"		//!< @hideinitializer Build type.
#endif

//######################################################################################################################
#endif
