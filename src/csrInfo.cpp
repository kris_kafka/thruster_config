/*! @file
@brief Define the CSR data utility module.
*/
//######################################################################################################################

#include "app_com.h"

#include <QtCore/QMutex>
#include <QtCore/QMutexLocker>
#include <QtCore/QVector>

#include "csrInfo.h"
#include "csrInfo_log.h"

//######################################################################################################################
// Local functions.

static bool				isTypeOkay(CsrInfo::Type type);
static void				populateData(void);
static int				size(CsrInfo::Type type);
static CsrInfo::Type	type(QString const &name);

//######################################################################################################################
// Inline local functions.

/*! @brief Convert a Type to an integer.

@return the integer corresponding to @a type.
*/
inline constexpr int
operator+(
	CsrInfo::Type	type	//!<[in] type to convert.
)
{
	return static_cast<int>(type);
}

//----------------------------------------------------------
/*! @brief Convert a Name to an integer.

@return the integer corresponding to @a name.
*/
inline constexpr int
operator+(
	CsrInfo::Name	name	//!<[in] name to convert.
)
{
	return static_cast<int>(name);
}

//######################################################################################################################
// Local variables.

static bool					isPopulated = false;	//!< Determine if the other local variables have been populated.

static QVector<int>			typeSizes;				//!< Sizes of the data types.

static QVector<bool>		isTypeFloat;			//!< Determine if a data type is a floating point value.
static QVector<bool>		isTypeInteger;			//!< Determine if a data type is an integer value.
static QVector<bool>		isTypeSigned;			//!< Determine if a data type is a signed value.
static QVector<bool>		isTypeUnsigned;			//!< Determine if a data type is an ussigned value.

static QVector<QString>		cppTypeNames;			//!< C++ data type names.
static QVector<QString>		csrNewTypeNames;		//!< CSR data type names.
static QVector<QString>		csrOldTypeNames;		//!< Alt data type names.
static QVector<QString>		englishTypeNames;		//!< English data type names.
static QVector<QString>		qtTypeNames;			//!< Qt data type names.
static QVector<QString>		stdTypeNames;			//!< std data type names.

//######################################################################################################################
/*! @brief Query properties of a type.

@return true if type is valid, else false.
*/
bool
CsrInfo::check(
	Type	type,		//!<[in]  Data type.
	int		&size,		//!<[out] Size of type.
	bool	&isSigned_,	//!<[out] Is type signed?
	bool	&isFloat_,	//!<[out] Is type floating point?
	QString	&name_		//!<[out] English name of type.
)
{
	// Ensure type is valid.
	if (!isTypeOkay(type)) {
		LOG_Trace(loggerCheck, QString("Invalid type: %1").arg(+type));
		return false;
	}
	// Populate the constants if needed.
	if (!isPopulated) {
		populateData();
	}
	//
	size		= typeSizes[+type];
	isSigned_	= isTypeSigned[+type];
	isFloat_	= isTypeFloat[+type];
	name_		= englishTypeNames[+type];
	LOG_Trace(loggerCheck,
				QString("Check: type=%1, size=%2, isSigned=%3, isFloat=%4, name='%5'")
						.arg(+type).arg(size).arg(isSigned_).arg(isFloat_).arg(name_));
	return true;
}

//======================================================================================================================
/*! @brief Query if a data type corresponds to a floating point value.

@return true if the data corresponding to @a type is floating point, else false (if type is invalid).
*/
bool
CsrInfo::isFloat(
	Type	type	//!<[in]  Data type.
)
{
	// Populate the constants if needed.
	if (!isPopulated) {
		populateData();
	}
	// Ensure type is valid.
	if (!isTypeOkay(type)) {
		LOG_Trace(loggerBadData, QString("Invalid Type: %1").arg(+type));
		return false;
	}
	//
	return isTypeFloat[+type];
}

//======================================================================================================================
/*! @brief Query if a data type corresponds to an integer value.

@return true if the data corresponding to @a type is integer, else false (if type is invalid).
*/
bool
CsrInfo::isInteger(
	Type	type	//!<[in]  Data type.
)
{
	// Populate the constants if needed.
	if (!isPopulated) {
		populateData();
	}
	// Ensure type is valid.
	if (!isTypeOkay(type)) {
		LOG_Trace(loggerBadData, QString("Invalid Type: %1").arg(+type));
		return false;
	}
	//
	return isTypeInteger[+type];
}

//======================================================================================================================
/*! @brief Query if a data type corresponds to a signed value.

@return true if the data corresponding to @a type is signed, else false (if type is invalid).
*/
bool
CsrInfo::isSigned(
	Type	type	//!<[in]  Data type.
)
{
	// Populate the constants if needed.
	if (!isPopulated) {
		populateData();
	}
	// Ensure type is valid.
	if (!isTypeOkay(type)) {
		LOG_Trace(loggerBadData, QString("Invalid Type: %1").arg(+type));
		return false;
	}
	//
	return isTypeSigned[+type];
}

//======================================================================================================================
/*! @brief Query if a data type is valid.

@return true if the data type is valid, else false.
*/
bool
isTypeOkay(
	CsrInfo::Type	type
)
{
	return CsrInfo::Type::Min <= type && type <= CsrInfo::Type::Max;
}

//======================================================================================================================
/*! @brief Query if a data type corresponds to an unsigned value.

@return true if the data corresponding to @a type is unsigned, else false (if type is invalid).
*/
bool
CsrInfo::isUnsigned(
	Type	type	//!<[in]  Data type.
)
{
	// Populate the constants if needed.
	if (!isPopulated) {
		populateData();
	}
	// Ensure type is valid.
	if (!isTypeOkay(type)) {
		LOG_Trace(loggerBadData, QString("Invalid Type: %1").arg(+type));
		return false;
	}
	//
	return isTypeUnsigned[+type];
}

//======================================================================================================================
/*! @brief Query the name of a data type.

@return the @a nameType name corresponding to data type @a type,
else a string beginnng and ending with "**" if either @a type or @a nameType are invalid.

@return the name of a data type.
*/
QString
CsrInfo::name(
	Type	type,	//!<[in] Data type.
	Name	nameType	//!<[in] Name type.
)
{
	LOG_Trace(loggerQueryName, QString("name type=%1, nameType=%2").arg(+type).arg(+nameType));
	// Populate the constants if needed.
	if (!isPopulated) {
		populateData();
	}
	// Ensure type is valid.
	if (!isTypeOkay(type)) {
		LOG_Trace(loggerBadData, QString("Unknown Name=%1").arg(+nameType));
		return QString("** Type=%1 (bad), Name=%2 **").arg(+type).arg(+nameType);
	}//
	switch (nameType) {
	case Name::Cpp:		return cppTypeNames[+type];
	case Name::Csr:		return csrNewTypeNames[+type];
	case Name::Alt:		return csrOldTypeNames[+type];
	case Name::Qt:		return qtTypeNames[+type];
	case Name::Std:		return stdTypeNames[+type];
	case Name::English:	return englishTypeNames[+type];
	default:
		// nameType is not valid.
		LOG_Trace(loggerBadData, QString("Unknown Name=%1").arg(+nameType));
		return QString("** Type=%1, Name=%2 (bad) **").arg(+type).arg(+nameType);
	};
}

//======================================================================================================================
/*! @brief Populate the constants if needed.
*/
void
populateData(void)
{
	// Check if already initialized.
	static QMutex	criticalSection;
	QMutexLocker	locker(&criticalSection);
	if (isPopulated) {
		return;
	}
	LOG_Trace(loggerCreate, "Populating constant data");
	//
	typeSizes		.fill(0, +CsrInfo::Type::End);
	isTypeFloat		.fill(false, +CsrInfo::Type::End);
	isTypeInteger	.fill(true, +CsrInfo::Type::End);
	isTypeSigned	.fill(true, +CsrInfo::Type::End);
	isTypeUnsigned	.fill(false, +CsrInfo::Type::End);
	cppTypeNames	.fill(QString(), +CsrInfo::Type::End);
	csrOldTypeNames	.fill(QString(), +CsrInfo::Type::End);
	csrNewTypeNames	.fill(QString(), +CsrInfo::Type::End);
	englishTypeNames.fill(QString(), +CsrInfo::Type::End);
	qtTypeNames		.fill(QString(), +CsrInfo::Type::End);
	stdTypeNames	.fill(QString(), +CsrInfo::Type::End);
	//
	typeSizes		[+CsrInfo::Type::SI1] = 1;
//	isTypeFloat		[+CsrInfo::Type::SI1] = false;
//	isTypeInteger	[+CsrInfo::Type::SI1] = true;
//	isTypeSigned	[+CsrInfo::Type::SI1] = true;
//	isTypeUnsigned	[+CsrInfo::Type::SI1] = false;
	cppTypeNames	[+CsrInfo::Type::SI1] = "signed char";
	csrNewTypeNames	[+CsrInfo::Type::SI1] = "Sint8";
	csrOldTypeNames	[+CsrInfo::Type::SI1] = "int8_t";
	englishTypeNames[+CsrInfo::Type::SI1] = "1-byte signed integer";
	qtTypeNames		[+CsrInfo::Type::SI1] = "qint8";
	stdTypeNames	[+CsrInfo::Type::SI1] = "int8_t";
	//
	typeSizes		[+CsrInfo::Type::UI1] = 1;
//	isTypeFloat		[+CsrInfo::Type::UI1] = false;
//	isTypeInteger	[+CsrInfo::Type::UI1] = true;
	isTypeSigned	[+CsrInfo::Type::UI1] = false;
	isTypeUnsigned	[+CsrInfo::Type::UI1] = true;
	cppTypeNames	[+CsrInfo::Type::UI1] = "unsigned char";
	csrNewTypeNames	[+CsrInfo::Type::UI1] = "Uint8";
	csrOldTypeNames	[+CsrInfo::Type::UI1] = "uint8_t";
	englishTypeNames[+CsrInfo::Type::UI1] = "1-byte unsigned integer";
	qtTypeNames		[+CsrInfo::Type::UI1] = "quint8";
	stdTypeNames	[+CsrInfo::Type::UI1] = "uint8_t";
	//
	typeSizes		[+CsrInfo::Type::SI2] = 2;
//	isTypeFloat		[+CsrInfo::Type::SI2] = false;
//	isTypeInteger	[+CsrInfo::Type::SI2] = true;
//	isTypeSigned	[+CsrInfo::Type::SI2] = true;
//	isTypeUnsigned	[+CsrInfo::Type::SI2] = false;
	cppTypeNames	[+CsrInfo::Type::SI2] = "signed short";
	csrNewTypeNames	[+CsrInfo::Type::SI2] = "Sint16";
	csrOldTypeNames	[+CsrInfo::Type::SI2] = "int16_t";
	englishTypeNames[+CsrInfo::Type::SI2] = "2-byte signed integer";
	qtTypeNames		[+CsrInfo::Type::SI2] = "qint16";
	stdTypeNames	[+CsrInfo::Type::SI2] = "int16_t";
	//
	typeSizes		[+CsrInfo::Type::UI2] = 2;
//	isTypeFloat		[+CsrInfo::Type::UI2] = false;
//	isTypeInteger	[+CsrInfo::Type::UI2] = true;
	isTypeSigned	[+CsrInfo::Type::UI2] = false;
	isTypeUnsigned	[+CsrInfo::Type::UI2] = true;
	cppTypeNames	[+CsrInfo::Type::UI2] = "unsigned short";
	csrNewTypeNames	[+CsrInfo::Type::UI2] = "Uint16";
	csrOldTypeNames	[+CsrInfo::Type::UI2] = "uint16_t";
	englishTypeNames[+CsrInfo::Type::UI2] = "2-byte unsigned integer";
	qtTypeNames		[+CsrInfo::Type::UI2] = "quint16";
	stdTypeNames	[+CsrInfo::Type::UI2] = "uint16_t";
	//
	typeSizes		[+CsrInfo::Type::SI4] = 4;
//	isTypeFloat		[+CsrInfo::Type::SI4] = false;
//	isTypeInteger	[+CsrInfo::Type::SI4] = true;
//	isTypeSigned	[+CsrInfo::Type::SI4] = true;
//	isTypeUnsigned	[+CsrInfo::Type::SI4] = false;
	cppTypeNames	[+CsrInfo::Type::SI4] = "signed int";
	csrNewTypeNames	[+CsrInfo::Type::SI4] = "Sint32";
	csrOldTypeNames	[+CsrInfo::Type::SI4] = "int32_t";
	englishTypeNames[+CsrInfo::Type::SI4] = "4-byte signed integer";
	qtTypeNames		[+CsrInfo::Type::SI4] = "qint32";
	stdTypeNames	[+CsrInfo::Type::SI4] = "int32_t";
	//
	typeSizes		[+CsrInfo::Type::UI4] = 4;
//	isTypeFloat		[+CsrInfo::Type::UI4] = false;
//	isTypeInteger	[+CsrInfo::Type::UI4] = true;
	isTypeSigned	[+CsrInfo::Type::UI4] = false;
	isTypeUnsigned	[+CsrInfo::Type::UI4] = true;
	cppTypeNames	[+CsrInfo::Type::UI4] = "unsigned int";
	csrNewTypeNames	[+CsrInfo::Type::UI4] = "Uint32";
	csrOldTypeNames	[+CsrInfo::Type::UI4] = "uint32_t";
	englishTypeNames[+CsrInfo::Type::UI4] = "4-byte unsigned integer";
	qtTypeNames		[+CsrInfo::Type::UI4] = "quint32";
	stdTypeNames	[+CsrInfo::Type::UI4] = "uint32_t";
	//
	typeSizes		[+CsrInfo::Type::SI8] = 8;
//	isTypeFloat		[+CsrInfo::Type::SI8] = false;
//	isTypeInteger	[+CsrInfo::Type::SI8] = true;
//	isTypeSigned	[+CsrInfo::Type::SI8] = true;
//	isTypeUnsigned	[+CsrInfo::Type::SI8] = false;
	cppTypeNames	[+CsrInfo::Type::SI8] = "signed long";
	csrNewTypeNames	[+CsrInfo::Type::SI8] = "Sint64";
	csrOldTypeNames	[+CsrInfo::Type::SI8] = "int64_t";
	englishTypeNames[+CsrInfo::Type::SI8] = "8-byte signed integer";
	qtTypeNames		[+CsrInfo::Type::SI8] = "qint64";
	stdTypeNames	[+CsrInfo::Type::SI8] = "int64_t";
	//
	typeSizes		[+CsrInfo::Type::UI8] = 8;
//	isTypeFloat		[+CsrInfo::Type::UI8] = false;
//	isTypeInteger	[+CsrInfo::Type::UI8] = true;
	isTypeSigned	[+CsrInfo::Type::UI8] = false;
	isTypeUnsigned	[+CsrInfo::Type::UI8] = true;
	cppTypeNames	[+CsrInfo::Type::UI8] = "unsigned long";
	csrNewTypeNames	[+CsrInfo::Type::UI8] = "Uint64";
	csrOldTypeNames	[+CsrInfo::Type::UI8] = "uint64_t";
	englishTypeNames[+CsrInfo::Type::UI8] = "8-byte unsigned integer";
	qtTypeNames		[+CsrInfo::Type::UI8] = "quint64";
	stdTypeNames	[+CsrInfo::Type::UI8] = "uint64_t";
	//
	typeSizes		[+CsrInfo::Type::FP4] = 4;
	isTypeFloat		[+CsrInfo::Type::FP4] = true;
	isTypeInteger	[+CsrInfo::Type::FP4] = false;
//	isTypeSigned	[+CsrInfo::Type::FP4] = true;
//	isTypeUnsigned	[+CsrInfo::Type::FP4] = false;
	cppTypeNames	[+CsrInfo::Type::FP4] = "float";
	csrNewTypeNames	[+CsrInfo::Type::FP4] = "Real32";
	csrOldTypeNames	[+CsrInfo::Type::FP4] = "float";
	englishTypeNames[+CsrInfo::Type::FP4] = "4-byte floating point";
	qtTypeNames		[+CsrInfo::Type::FP4] = "float";
	stdTypeNames	[+CsrInfo::Type::FP4] = "float";
	//
	typeSizes		[+CsrInfo::Type::FP8] = 8;
	isTypeFloat		[+CsrInfo::Type::FP8] = true;
	isTypeInteger	[+CsrInfo::Type::FP8] = false;
//	isTypeSigned	[+CsrInfo::Type::FP8] = true;
//	isTypeUnsigned	[+CsrInfo::Type::FP8] = false;
	cppTypeNames	[+CsrInfo::Type::FP8] = "double";
	csrNewTypeNames	[+CsrInfo::Type::FP8] = "Real64";
	csrOldTypeNames	[+CsrInfo::Type::FP8] = "double";
	englishTypeNames[+CsrInfo::Type::FP8] = "8-byte floating point";
	qtTypeNames		[+CsrInfo::Type::FP8] = "double";
	stdTypeNames	[+CsrInfo::Type::FP8] = "double";
	//
	isPopulated = true;
}

//======================================================================================================================
/*! @brief Query the data size of @a type.

@return the size of the data corresponding to @a type, else 0 if type is invalid.
*/
int
CsrInfo::size(
	Type	type	//!<[in]  Data type.
)
{
	LOG_Trace(loggerQuerySize, QString("Get data size of '%1'").arg(+type));
	//
	return ::size(type);
}

//----------------------------------------------------------------------------------------------------------------------
/*! @brief Query the data size of @a type.

@return the size of the data corresponding to @a type, else 0 if type is invalid.
*/
int
size(
	CsrInfo::Type	type	//!<[in]  Data type.
)
{
	// Ensure type is valid.
	if (!isTypeOkay(type)) {
		LOG_Trace(loggerBadData, QString("Invalid Type: %1").arg(+type));
		return 0;
	}
	// Populate the constants if needed.
	if (!isPopulated) {
		populateData();
	}
	return typeSizes[+type];
}

//======================================================================================================================
/*! @brief Query the data size of @a name.

@return the size of the data corresponding to @a name, else 0 if name is invalid.
*/
int
CsrInfo::size(
	QString const	&name	//!<[in] Type name.
)
{
	LOG_Trace(loggerQuerySize, QString("Get data size of '%1'").arg(name));
	//
	return ::size(::type(name));
}

//======================================================================================================================
/*! @brief Convert a human readable type name to a data type (inverse of the name() function).

@return the data type corresponding to @a name, else Bad if @a name is invalid.
*/
CsrInfo::Type
CsrInfo::type(
	QString const	&name	//!<[in] Type name.
)
{
	LOG_Trace(loggerQueryType, QString("Get data type of '%1'").arg(name));
	return ::type(name);
}

//----------------------------------------------------------------------------------------------------------------------
/*! @brief Convert a human readable type name to a data type (inverse of the name() function).

@return the data type corresponding to @a name, else Bad if @a name is invalid.
*/
CsrInfo::Type
type(
	QString const	&name	//!<[in] Type name.
)
{
	// Populate the constants if needed.
	if (!isPopulated) {
		populateData();
	}
	// Standardize the given type name.
	QString	text = name.simplified();
	for (int type=+CsrInfo::Type::Min; type < +CsrInfo::Type::End; ++type) {
		if (	!text.compare(cppTypeNames[type],		Qt::CaseInsensitive)
			||	!text.compare(csrNewTypeNames[type],	Qt::CaseInsensitive)
			||	!text.compare(csrOldTypeNames[type],	Qt::CaseInsensitive)
			||	!text.compare(englishTypeNames[type],	Qt::CaseInsensitive)
			||	!text.compare(qtTypeNames[type],		Qt::CaseInsensitive)
			||	!text.compare(stdTypeNames[type],		Qt::CaseInsensitive)
			) {
			return (CsrInfo::Type)type;
		}
	}
	//
	LOG_Trace(loggerBadData, QString("Invalid TypeName: '%1'").arg(name));
	return CsrInfo::Type::Bad;
}
