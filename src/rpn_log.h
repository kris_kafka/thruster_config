#pragma once
#if !defined(RPN_LOG_H) && !defined(DOXYGEN_SKIP)
#define RPN_LOG_H
//######################################################################################################################
/*! @file
@brief Declare the RPN module loggers.
*/
//######################################################################################################################

#include "log.h"

//######################################################################################################################
// Loggers

LOG_SETUP_START("RPN Module");

// Top level module logger.
LOG_DEF_LOG(logger,			LOG_DL_INFO,	"Rpn");
// Assign symbol logger.
LOG_DEF_LOG(loggerAssign,	LOG_DL_INFO,	"Rpn.Assign");
// Object creation/destruction logger.
LOG_DEF_LOG(loggerCreate,	LOG_DL_INFO,	"Rpn.Create");
// Evaluation logger.
LOG_DEF_LOG(loggerEvaluate,	LOG_DL_INFO,	"Rpn.Evaluate");
// Query logger.
LOG_DEF_LOG(loggerQuery,	LOG_DL_INFO,	"Rpn.Query");

LOG_SETUP_STOP();

//######################################################################################################################
#endif
