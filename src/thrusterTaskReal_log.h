#pragma once
#if !defined(THRUSTERTASKREAL_LOG_H) && !defined(DOXYGEN_SKIP)
#define THRUSTERTASKREAL_LOG_H
//######################################################################################################################
/*! @file
@brief Declare the thruster interface background task real module loggers.
*/
//######################################################################################################################

#include "log.h"

//######################################################################################################################
// Loggers

LOG_SETUP_START("Thruster Interface Background Task Real Module");

// Top level module logger.
LOG_DEF_LOG(logger,							LOG_DL_TRACE,	"ThrusterTaskReal");
// Configuration read background task logger.
LOG_DEF_LOG(loggerConfigurationRead,		LOG_DL_TRACE,	"ThrusterTaskReal.ConfiguratioRead");
// Configuration background task logger.
LOG_DEF_LOG(loggerConfigurationWrite,		LOG_DL_TRACE,	"ThrusterTaskReal.ConfigurationWrite");
// Object creation/destruction logger.
LOG_DEF_LOG(loggerCreate,					LOG_DL_TRACE,	"ThrusterTaskReal.Create");
// Node ID change background task logger.
LOG_DEF_LOG(loggerIdsChange,				LOG_DL_TRACE,	"ThrusterTaskReal.IdsChange");
// Query serial ports available logger.
LOG_DEF_LOG(loggerQuerySerialPorts,			LOG_DL_TRACE,	"ThrusterTaskReal.QuerySerialPorts");
// Scan for thrusters background task logger.
LOG_DEF_LOG(loggerScanForThrusters,			LOG_DL_TRACE,	"ThrusterTaskReal.ScanForThrusters");
// Serial port change background task logger.
LOG_DEF_LOG(loggerSerialPortChange,			LOG_DL_TRACE,	"ThrusterTaskReal.SerialPortChange");
// Module signals logger.
LOG_DEF_LOG(loggerSignals,					LOG_DL_TRACE,	"ThrusterTaskReal.Signals");
// Test thruster logger.
LOG_DEF_LOG(loggerTestingMode,				LOG_DL_TRACE,	"ThrusterTaskReal.TestingMode");

LOG_SETUP_STOP();

//######################################################################################################################
#endif
