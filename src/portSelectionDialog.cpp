/*! @file
@brief Define the port selection dialog module.
*/
//######################################################################################################################

#include "app_com.h"

#include <QtCore/QList>
#include <QtWidgets/QPushButton>

#include "configState.h"
#include "portSelectionDialog.h"
#include "portSelectionDialog_log.h"
#include "portSelectionDialogPrivate.h"
#include "main.h"
#include "strUtil.h"
#include "thruster.h"

//######################################################################################################################
/*! @brief Create a PortSelectionDialog object.
*/
PortSelectionDialog::PortSelectionDialog(
	QString const	&currentPort,	//!<[in] The curently selected port.
	QWidget			*parent			//!<[in] The parent object.
)
:
	d	(Q_NULLPTR)
{
	LOG_Trace(loggerCreate, "Creating PortSelectionDialog");
	//
	d.reset(new (std::nothrow) PortSelectionDialogPrivate(currentPort, parent));
	LOG_CHK_PTR_MSG(loggerCreate, d, "Unable to create PortSelectionDialogPrivate object");
}

//----------------------------------------------------------------------------------------------------------------------
/*! @brief Create a PortSelectionDialogPrivate object.
*/
PortSelectionDialogPrivate::PortSelectionDialogPrivate(
	QString const	&currentPort,	//!<[in] The curently selected port.
	QWidget			*parent			//!<[in] The parent object.
)
:
	QDialog			(parent),
	originalPort	(currentPort)
{
	LOG_Trace(loggerCreate, "Creating PortSelectionDialogPrivate");
	setupUi(this);
	restoreWindowState();
	//
	initializeAvailablePorts();
	//
	LOG_CONNECT(loggerCreate,	portListWidget,	&QListWidget::itemSelectionChanged,
								this,			&PortSelectionDialogPrivate::updateGuiState);
	//
	updateGuiState();
}

//======================================================================================================================
/*! @brief Destroy a PortSelectionDialog object.
*/
PortSelectionDialog::~PortSelectionDialog(void)
{
	LOG_Trace(loggerCreate, "Destroying PortSelectionDialog");
}

//----------------------------------------------------------------------------------------------------------------------
/*! @brief Destroy a PortSelectionDialogPrivate object.
*/
PortSelectionDialogPrivate::~PortSelectionDialogPrivate(void)
{
	LOG_Trace(loggerCreate, "Destroying PortSelectionDialogPrivate");
}

//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
/*! @brief Populate the list of available ports.
*/
void
PortSelectionDialogPrivate::initializeAvailablePorts(void)
{
	LOG_Trace(loggerCreate, "Getting the available ports");
	portListWidget->clear();
	QStringList availablePorts = Thruster::serialPortsAvailable();
	for (QString const &port : availablePorts) {
		portListWidget->addItem(port);
	}
	QList<QListWidgetItem *>	item = portListWidget->findItems(originalPort, Qt::MatchFixedString);
	if (item.empty()) {
		portListWidget->setCurrentRow(0);
	} else {
		portListWidget->setCurrentItem(item.first());
	}
}

//======================================================================================================================
/*! @brief Invoke a PortSelectionDialog object.

@return a QDialog::DialogCode result.
*/
int
PortSelectionDialog::invoke(void)
{
	LOG_Trace(loggerInvoke, "Invoking PortSelectionDialog");
	int answer = d->exec();
	d->saveWindowState();
	return answer;
}

//======================================================================================================================
/*! @brief Return the new serial port.

@return the new serial port.
*/
QString
PortSelectionDialog::newPort(void)
{
	return d->newPort();
}

//----------------------------------------------------------------------------------------------------------------------
/*! @brief Return the new serial port.

@return the new serial port.
*/
QString
PortSelectionDialogPrivate::newPort(void)
{
	QString answer = portListWidget->currentItem()->text();
	LOG_Trace(loggerQuery, QString("Currently selected port: %1").arg(answer));
	return answer;
}

//======================================================================================================================
/*! @brief Restore the window state.
*/
void
PortSelectionDialogPrivate::restoreWindowState(void)
{
	LOG_Trace(loggerWindowState, "Restoring window state");
	//
	IniFile	iniFile;
	IniBytes	savedGeometry	= iniFile.INI_GET(CfgStatePortSelectionDialogGeometry);
	if (!savedGeometry.isEmpty()) {
		restoreGeometry(savedGeometry);
	}
}

//======================================================================================================================
/*! @brief Save the window state.
*/
void
PortSelectionDialogPrivate::saveWindowState(void)
{
	LOG_Trace(loggerWindowState, "Saving window state");
	//
	IniFile	iniFile;
	iniFile.INI_SET(CfgStatePortSelectionDialogGeometry, saveGeometry());
	iniFile.syncAndCheck();
}

//======================================================================================================================
/*! @brief Update the control states.
*/
void
PortSelectionDialogPrivate::updateGuiState(void)
{
	LOG_Trace(loggerGuiState, "Update the control states");
	savePushButton->setEnabled(originalPort != newPort());
}
