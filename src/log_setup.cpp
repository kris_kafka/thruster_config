/*! @file
@brief Define the log setup function.
*/
//######################################################################################################################

#include "app_com.h"

#include <QtCore/QString>

//! @hideinitializer @brief Setup mode control.
#define LOG_SETUP 1
#include "log.h"

//######################################################################################################################
/*! @brief Generate the setup function.
*/
void
LogModule::setup(
	QFile	&logControlFile	//!<[in] File to receive the default log control file.
)
{
	setupStart(logControlFile);

	#ifndef DOXYGEN_SKIP
		#include "log_all.h"
	#endif

	setupStop(logControlFile);
}

