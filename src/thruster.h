#pragma once
#ifndef THRUSTER_H
#ifndef DOXYGEN_SKIP
#define THRUSTER_H
#endif
//######################################################################################################################
/*! @file
@brief Declare the interface to the thruster interface module.
*/
//######################################################################################################################

#include <QtCore/QObject>
#include <QtCore/QStringList>

//######################################################################################################################
/*! @brief The Thruster class provides the interface to the thruster interface module.
*/
class Thruster
:
public	QObject
{
	Q_OBJECT

public:		// Constants
	//! @brief Test Modes.
	typedef enum TestModes {
		TM_Bad = -1,						//!< Bad Type.
		TM_Min = 0,							//!< Minimum of valid TestMode values.
		TM_SinglePropultion = TM_Min,		//!< 0: Node ID, Propulsion command
		TM_SingleCsr,						//!< 1: Node ID, CSR write/read commands
		TM_SingleCsrCheck,					//!< 2: Node ID, CSR write/verify/read commands
		TM_GroupPropulsion,					//!< 3: Group ID, Propulsion command
		TM_GroupCsr,						//!< 4: Group ID, CSR write/read commands
		TM_GroupCsrCheck,					//!< 5: Group ID, CSR write/verify/read commands
		TM_BroadcastPropultion,				//!< 6: Broadcast, Propulsion command
		TM_BroadcastCsr,					//!< 7: Broadcast, CSR write/read commands
		TM_BroadcastCsrCheck,				//!< 8: Broadcast ID, CSR write/verify/read commands
		/* + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + +
		**** NOTE **** Update testModeInfo() when modifying the test modes.
		+ + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + */
		TM_End,								//!< Upper limit of valid TestMode values.
		TM_Max = TM_End - 1					//!< Maximum of valid TestMode values.
	} TestMode;

	//! @brief Thruster error codes.
	typedef enum ThrusterErrors {
		TEC_Bad = -1,				//!< Bad Type.
		TEC_Min = 0,				//!< Minimum of valid Error values.
		TEC_NoError = TEC_Min,		//!<  0: No errors.
		TEC_NotIdle,				//!<  1: Not idle.
		TEC_WrongState,				//!<  2: Wrong state.
		TEC_NodeIdMismatch,			//!<  3: Node ID mismatch.
		TEC_NodeIdNotFound,			//!<  4: Node ID not found.
		TEC_Duplicates,				//!<  5: Duplicate node IDs and/or serial numbers.
		TEC_Stopping,				//!<  6: Background task stopping.
		TEC_RpnFormula,				//!<  7: RPN formula error
		TEC_OpenError,				//!<  8: Serial port open error.
		TEC_PortError,				//!<  9: Serial port error.
		TEC_PacektSize,				//!< 10: Packet size error.
		TEC_PacektError,			//!< 11: Packet error.
		TEC_RetryLimit,				//!< 12: Retry limit.
		TEC_NodeIdDifferent,		//!< 13: Node ID mismatch in ID packet.
		TEC_DeviceType,				//!< 14: Incorrect device type.
		TEC_DeviceTypeDifferent,	//!< 15: Device type mismatch in ID packet.
		TEC_WriteError,				//!< 16: Write error.
		/* + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + +
		**** NOTE **** Update errorMessage() when modifying the error codes.
		+ + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + */
		TEC_End,					//!< Upper limit of valid Error values.
		TEC_Max = TEC_End - 1		//!< Maximum of valid Error values.
	} Error;

	//! @brief Thruster ID Types.
	typedef enum ThrusterIdTypes {
		TIT_Bad = -1,				//!< Bad Type.
		TIT_Min = 0,				//!< Minimum of valid ThrusterIdType values.
		TIT_NodeId = TM_Min,		//!< 0: Destination node is Node ID.
		TIT_GroupId,				//!< 1: Destination node is Group ID.
		TIT_Broadcast,				//!< 2: Destination node is Broadcast.
		/* + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + +
		**** NOTE **** Update testModeInfo() when modifying the test mode ID types.
		+ + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + */
		TIT_End,					//!< Upper limit of valid ThrusterIdType values.
		TIT_Max = TIT_End - 1		//!< Maximum of valid ThrusterIdType values.
	} ThrusterIdType;

	//! @brief States the background task can be in.
	typedef enum class ThrusterStates {
		_Bad = -1,					//!< Bad State.
		_Min = 0,					//!< Minimum of valid State values.
		NotStarted = _Min,			//!< Not started.
		Stopped,					//!< Stopped.
		Idle,						//!< Idle.
		ConfigurationRead,			//!< Reading thruster configuration.
		ConfigurationWrite,			//!< Writting thruster configuration.
		IdsChange,					//!< Changing a node ID.
		ScanForThrusters,			//!< Scanning for thrusters.
		SerialPortChange,			//!< Changing the serial port.
		TestingMode,				//!< Testing a thruster.
		_End,						//!< Upper limit of valid State values.
		_Max = _End - 1				//!< Maximum of valid State values.
	} State;

public:		// Constructors / Destructors
	explicit	Thruster(void);
				~Thruster(void);

public:		// Functions
	bool				configurationRead(int nodeId);
	bool				configurationWrite(int nodeId, int motorId, int maximumPower, bool rotationReverse,
											QByteArray const &csrBlob);
	QString				errorMessage(int errorCode);
	bool				idsChange(QString const &serialNumber, int oldNodeId, int newNodeId, int newGroupId);
	bool				isBusy(void);
	bool				isDuplicateIds(void);
	bool				isDuplicateSerials(void);
	bool				isIdle(void);
	static bool			isSerialPortValid(QString const &port);
	bool				isTestingMode(void);
	QString				lastErrorMessage(void);
	void				moduleClose(void);
	bool				moduleIsOpen(void);
	bool				moduleIsRunning(void);
	bool				moduleOpen(void);
	bool				moduleStart(void);
	void				moduleStop(void);
	bool				scanForThrusters(void);
	bool				serialPortChange(QString const &port);
	static QStringList	serialPortsAvailable(void);
	State				state(void);
	double				testingModeSpeed(void);
	bool				testingModeSpeed(double speed);
	bool				testingModeBegin(int nodeId, int groupId, int motorId, int mode);
	bool				testingModeEnd(void);
	static bool			testingModeInfo(int mode, ThrusterIdType &idType, bool &isCheck, bool &isPropulsion,
										bool &isReply, QString &name);

Q_SIGNALS:
	//! Configuration data read from thruster.
	void	configurationReadNews(
					int			nodeId,				//!< Node ID of the thruster.
					int			groupId,			//!< Group ID read from thruster.
					int			motorId,			//!< Motor ID read from thruster.
					int			maximumPower,		//!< Maximum power read from thruster.
					bool		rotationReverse,	//!< Rotation is reversed read from thruster.
					QString		serialNumber,		//!< Serial number read from thruster.
					QByteArray	csrBlob				//!< CSR blob.
					);

	//! Configuration data read started.
	void	configurationReadStart(
					int		errorCode		//!< Error code (TEC_NoError if no errors).
					);

	//! Configuration data read finished.
	void	configurationReadStop(
					int		errorCode		//!< Error code (TEC_NoError if no errors).
					);

	//! Configuration data write started.
	void	configurationWriteStart(
					int		errorCode		//!< Error code (TEC_NoError if no errors).
					);

	//! Configuration data write finished.
	void	configurationWriteStop(
					int		errorCode		//!< Error code (TEC_NoError if no errors).
					);

	//! Change node ID started.
	void	idsChangeStart(
					int		errorCode		//!< Error code (TEC_NoError if no errors).
					);

	//! Change node ID finished.
	void	idsChangeStop(
					int		errorCode		//!< Error code (TEC_NoError if no errors).
					);

	//! Thruster information found during scan for thrusters.
	void	scanForThrustersNews(
					int		nodeId,			//!< Node ID of thruster.
					int		groupId,		//!< Group ID of thruster.
					QString	serialNumber	//!< Serial number of thruster.
					);

	//! Scanning for thrusters started.
	void	scanForThrustersStart(
					int		errorCode		//!< Error code (TEC_NoError if no errors).
					);

	//! Scanning for thrusters finished.
	void	scanForThrustersStop(
					int		errorCode		//!< Error code (TEC_NoError if no errors).
					);

	//! Serial port change started.
	void	serialPortChangeStart(
					int		errorCode		//!< Error code (TEC_NoError if no errors).
					);

	//! Serial port change finished.
	void	serialPortChangeStop(
					int		errorCode		//!< Error code (TEC_NoError if no errors).
					);

	//! Background task started.
	void	taskStarted(void);

	//! Background task stopped.
	void	taskStopped(void);

	//! Testing mode status data.
	void	testingModeNews(
					int		nodeId,			//!< Node ID.
					double	rpm,			//!< Thruster current RPM.
					double	voltage,		//!< Bus voltage.
					double	current,		//!< Bus current.
					double	temperature,	//!< Thruster temperature.
					int		faults			//!< Thruster fault flags.
					);

	//! Testing mode started.
	void	testingModeStart(
					int		errorCode		//!< Error code (TEC_NoError if no errors).
					);

	//! Testing mode stopped.
	void	testingModeStop(
					int		errorCode		//!< Error code (TEC_NoError if no errors).
					);

private:	// Functions
	Q_DISABLE_COPY(Thruster);
};

//######################################################################################################################
/*! @brief Convert a TestMode to an integer.

@return the integer corresponding to @a mode.
*/
inline constexpr int
operator+(
	Thruster::TestMode	mode	//!<[in] mode to convert.
)
{
	return static_cast<int>(mode);
}

//######################################################################################################################
/*! @brief Convert a State to an integer.

@return the integer corresponding to @a state.
*/
inline constexpr int
operator+(
	Thruster::State	state	//!<[in] type to convert.
)
{
	return static_cast<int>(state);
}

//######################################################################################################################
#endif
