#pragma once
#ifndef CHANGEIDSDIALOG_H
#ifndef DOXYGEN_SKIP
#define CHANGEIDSDIALOG_H
#endif
//######################################################################################################################
/*! @file
@brief Declare the interface to the change node/group IDs dialog module.
*/
//######################################################################################################################

#include <QtCore/QScopedPointer>
#include <QtCore/QString>
#include <QtWidgets/QWidget>

//######################################################################################################################

class	ChangeIdsDialogPrivate;

//######################################################################################################################
/*! @brief The ChangeIdsDialog class provides the interface to the change node/group IDs dialog module.
*/
class ChangeIdsDialog
{
	Q_DISABLE_COPY(ChangeIdsDialog)

public:		// Constructors & destructors
	ChangeIdsDialog(int nodeId, int groupId, QWidget *parent = Q_NULLPTR);
	~ChangeIdsDialog(void);

public:		// Functions
	int		invoke(void);
	int		newNodeId(bool &isOkay);
	int		newGroupId(bool &isOkay);

private:	// Data
	QScopedPointer<ChangeIdsDialogPrivate>	d;	//!< Implementation object.
};

//######################################################################################################################
#endif
