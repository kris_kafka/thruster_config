#pragma once
#ifndef EMPTYINTVALIDATOR_H
#ifndef DOXYGEN_SKIP
#define EMPTYINTVALIDATOR_H
#endif
//######################################################################################################################
/*! @file
@brief Declare the interface to the empty int validator module.
*/
//######################################################################################################################

#include <QtGui/QIntValidator>

//######################################################################################################################
/*! @brief Encapsulates the empty int validator module.
*/
class EmptyIntValidator
:
	public	QIntValidator
{
	Q_OBJECT

public:		// Constructors & destructors
	explicit	EmptyIntValidator(QObject *parent = Q_NULLPTR);
				EmptyIntValidator(int minimum, int maximum, QObject *parent = Q_NULLPTR);

public:		// Functions
	virtual QValidator::State	validate(QString &input, int &pos)	const;
};

//######################################################################################################################
#endif
