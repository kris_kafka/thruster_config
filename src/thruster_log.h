#pragma once
#if !defined(THRUSTER_LOG_H) && !defined(DOXYGEN_SKIP)
#define THRUSTER_LOG_H
//######################################################################################################################
/*! @file
@brief Declare the thruster interface module loggers.
*/
//######################################################################################################################

#include "log.h"

//######################################################################################################################
// Loggers

LOG_SETUP_START("Thruster Interface Module");

// Top level module logger.
LOG_DEF_LOG(logger,						LOG_DL_TRACE,	"Thruster");
// Configuration read interface logger.
LOG_DEF_LOG(loggerConfigurationRead,	LOG_DL_TRACE,	"Thruster.ConfigurationRead");
// Configuration interface logger.
LOG_DEF_LOG(loggerConfigurationWrite,	LOG_DL_TRACE,	"Thruster.ConfigurationWrite");
// Object creation/destruction logger.
LOG_DEF_LOG(loggerCreate,				LOG_DL_TRACE,	"Thruster.Create");
// Node ID change interface logger.
LOG_DEF_LOG(loggerIdsChange,			LOG_DL_TRACE,	"Thruster.IdsChange");
// Module control logger.
LOG_DEF_LOG(loggerModule,				LOG_DL_TRACE,	"Thruster.Module");
// Query logger.
LOG_DEF_LOG(loggerQuery,				LOG_DL_INFO,	"Thruster.Query");
// Query serial ports available logger.
LOG_DEF_LOG(loggerQuerySerialPorts,		LOG_DL_INFO,	"Thruster.QuerySerialPorts");
// Scan for thrusters interface logger.
LOG_DEF_LOG(loggerScanForThrusters,		LOG_DL_TRACE,	"Thruster.ScanForThrusters");
// Serial port change interface logger.
LOG_DEF_LOG(loggerSerialPortChange,		LOG_DL_TRACE,	"Thruster.SerialPortChange");
// Module signals logger.
LOG_DEF_LOG(loggerSignals,				LOG_DL_TRACE,	"Thruster.Signals");
// Test thruster interface logger.
LOG_DEF_LOG(loggerTestingMode,			LOG_DL_TRACE,	"Thruster.TestingMode");

LOG_SETUP_STOP();

//######################################################################################################################
#endif
