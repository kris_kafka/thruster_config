/*! @file
@brief Define the empty int validator module.
*/
//######################################################################################################################

#include "app_com.h"

#include <QtCore/QByteArray>
#include <QtCore/QElapsedTimer>
#include <QtCore/QList>
#include <QtCore/QMutex>
#include <QtCore/QMutexLocker>
#include <QtCore/QScopedPointer>
#include <QtCore/QString>
#include <QtCore/QThread>

#include "videoray/applib/std_device_types.h"
#include "videoray/applib/protocol_vr_csr.h"
#include "videoray/hostlib/bootloader_connection.h"
#include "videoray/hostlib/protocol_vr_csr_handler_state.h"
#include "videoray/hostlib/protocol_vr_csr_parser.h"

#include "configCsr.h"
#include "configEnum.h"
#include "configThruster.h"
#include "packetUtil.h"
#include "tioEnum.h"
#include "tioEnum_log.h"

//######################################################################################################################

static void		getSettings(int &retryLimit, int &sleepDuration, int &timeoutDuration, int &seenMinimum,
							int &commandDataSize);


//######################################################################################################################
/*! @brief VideoRay CSR protocol response handler.
*/
class ThrusterEnumIdPrivate
:
	public	videoray::Protocol_vr_csr_parser,
	public	ProtocolHandlerStateBase
{

public:		// Constructors & destructors
	ThrusterEnumIdPrivate(QString const &serialPort_, bool const volatile &halt_);
	~ThrusterEnumIdPrivate(void);

public:		// Functions
	int		getNewReplies(QList<TioEnumData> &replies);
	qint64	hasTimeoutExpired(qint64 timeout);
	void	restartTimeout(void);

public:		// Data
	QScopedPointer<QList<TioEnumData> >
							allReplies;				//!< All replies.
	QScopedPointer<videoray::GizmoConnection>
							connection;				//!< Connection object.
	int						duplicateNodeIdCount;	//!< Number of duplicate node IDs.
	int						duplicateSerialCount;	//!< Number of duplicate serial numbers.
	QElapsedTimer			elapsedTimer;			//!< Timeout timer.
	bool const volatile		&halt;					//!< Flag to halt I/O.
	int						minSeenCount;			//!< Minimum view count.
	QScopedPointer<QList<TioEnumData> >
							thrusterReplies;		//!< Thruster replies.
	QMutex					replyMutex;			//!< Critical section mutex.
	QString const			serialPort;				//!< Name of the serial port.

private:		// Functions
	void	protocol_vrcsr_handle_custom_command_response(NodeId_t nodeId, Byte flags, int dataLength,
					DeviceType_t deviceType, ByteP buffer)	override;
	void	protocol_vrcsr_handle_response(NodeId_t nodeId, Byte flags, int csrAddress, int dataLength,
					DeviceType_t deviceType, ByteP buffer)	override;
};

//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
/*! @brief Create a default TioEnumData object.
*/
TioEnumData::TioEnumData(void)
:
	nodeId			(0),
	groupId			(0),
	deviceType		(0),
	seenCount		(0),
	serialNumber	()
{
}

//======================================================================================================================
/*! @brief Create a TioEnumData object from another.
*/
TioEnumData::TioEnumData(
	TioEnumData const &other	//!<[in] Source object
)
:
	nodeId			(other.nodeId),
	groupId			(other.groupId),
	deviceType		(other.deviceType),
	seenCount		(other.seenCount),
	serialNumber	(other.serialNumber)
{
}

//======================================================================================================================
/*! @brief Create a TioEnumData object from given values.
*/
TioEnumData::TioEnumData(
	int				nodeId_,		//!<[in] Response packet node ID.
	int				groupId_,		//!<[in] Response packet group ID.
	int				deviceType_,	//!<[in] Response packet device type.
	int				seenCount_,		//!<[in] Seen count.
	QString const	&serialNumber_	//!<[in] Response packet serial number.
)
:
	nodeId			(nodeId_),
	groupId			(groupId_),
	deviceType		(deviceType_),
	seenCount		(seenCount_),
	serialNumber	(serialNumber_)
{
}

//======================================================================================================================
/*! @brief Create a TioEnumData object from a packet buffer.
*/
TioEnumData::TioEnumData(
	ByteP	buffer	//!<[in] Packet buffer.
)
{
	nodeId			= *buffer++;				// Response packet node ID.
	groupId			= *buffer++;				// Response packet group ID.
	deviceType		= *buffer++;				// Response packet device type.
	serialNumber	= QString((char *)buffer);	// Response packet serial number.
	seenCount		= 1;
}

//======================================================================================================================
/*! @brief Compare a TioEnumData object to another.

Ignore the seenCounter.

@return true if the two objects are equal.
*/
bool
TioEnumData::operator==(
	TioEnumData const	&other	//!<[in] Other object to compare with.
)
	const
{
	return	nodeId			== other.nodeId
		&&	groupId			== other.groupId
		&&	deviceType		== other.deviceType
		&&	serialNumber	== other.serialNumber;
}

//######################################################################################################################
/*! @brief Create a TioEnum object.
*/
TioEnum::TioEnum(
	QString const		&serialPort,	//!< Serial port name.
	bool const volatile	&halt			//!< Halt I/O.
)
:
	d	(Q_NULLPTR)
{
	LOG_Trace(loggerCreate, QString("Creating TioEnum object: port='%1'").arg(serialPort));
	//
	d.reset(new (std::nothrow) ThrusterEnumIdPrivate(serialPort, halt));
	LOG_CHK_PTR_MSG(loggerCreate, d, "Unable to create ThrusterEnumIdPrivate object");
}

//======================================================================================================================
/*! @brief Destroy a TioEnum object.
*/
TioEnum::~TioEnum(void)
{
	LOG_Trace(loggerCreate, QString("Destroying TioEnum object: port='%1'").arg(d->serialPort));
}

//======================================================================================================================
/*! @brief Create a ThrusterEnumIdPrivate object.
*/
ThrusterEnumIdPrivate::ThrusterEnumIdPrivate(
	QString const		&serialPort_,	//!< Serial port name.
	bool const volatile	&halt_			//!< Halt I/O.
)
:
	allReplies				(Q_NULLPTR),
	connection				(Q_NULLPTR),
	duplicateNodeIdCount	(0),
	duplicateSerialCount	(0),
	elapsedTimer			(),
	halt					(halt_),
	minSeenCount			(0),
	thrusterReplies			(Q_NULLPTR),
	replyMutex			(),
	serialPort				(serialPort_)
{
	LOG_Trace(loggerCreate, "Creating ThrusterEnumIdPrivate object");
	//
	allReplies.reset(new (std::nothrow) QList<TioEnumData>);
	LOG_CHK_PTR_MSG(loggerCreate, allReplies, "Unable to create ThrusterEnumIdPrivate allReplies");
	thrusterReplies.reset(new (std::nothrow) QList<TioEnumData>);
	LOG_CHK_PTR_MSG(loggerCreate, thrusterReplies, "Unable to create ThrusterEnumIdPrivate thrusterReplies");
}

//======================================================================================================================
/*! @brief Destroy a ThrusterEnumIdPrivate object.
*/
ThrusterEnumIdPrivate::~ThrusterEnumIdPrivate(void)
{
	LOG_Trace(loggerCreate, "Destroying ThrusterEnumIdPrivate object");
}

//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
/*! @brief Close the serial port.
*/
void
TioEnum::close(void)
{
	LOG_Trace(loggerOpen, "Close a connection");
	// Done if already closed.
	if (!d->connection) {
		return;
	}
	// Delete the connection to close the serial port.
	d->connection.reset();
	//
	QThread::msleep(IniFile().INI_GET(CfgThrusterEnumCloseDelay));
}

//======================================================================================================================
/*! @brief Enumerate the nodes.

@return the error code (Thruster::TEC_NoError if no errors).
*/
Thruster::Error
TioEnum::enumerate(
	QList<TioEnumData>	&enumData,				//!<[out] Thruster enumeration data.
	int					&duplicateNodeIdCount,	//!<[out] Number of duplicate node IDs.
	int					&duplicateSerialCount	//!<[out] Number of duplicate serial numbers.
)
{
	LOG_Trace(loggerEnumerate, "Enumerate the nodes");
	//
	LOG_ASSERT_MSG(loggerEnumerate, d->connection, "No open connection");
	// Get our settings.
	int		commandDataSize, retryLimit, seenMinimum, sleepDuration, timeoutDuration;
	getSettings(retryLimit, sleepDuration, timeoutDuration, seenMinimum, commandDataSize);
	// Generate the command.
	int		packetSize = protocol_vrcsr_calc_packet_size(commandDataSize, false, PROTOCOL_VRCSR);
	ByteP	packetBuffer = new (std::nothrow) Byte[packetSize];
	LOG_CHK_PTR_MSG(loggerEnumerate, packetBuffer, "Unable to create TioEnum packetBuffer");
	boost::shared_array<Byte> commandBuffer(packetBuffer);
	int commandSize = protocol_vrcsr_request_id(ID_BROADCAST, true, commandBuffer.get(), packetSize, PROTOCOL_VRCSR);
	if (0 > commandSize) {
		LOG_Error(loggerEnumerate,
					QString("Error building command packet: %1 (%2)").arg(packetSize).arg(commandSize));
		return Thruster::TEC_PacektError;
	}
	//
	bool	isOkay = false;
	try {
		for (int retryCount = 0; !isOkay && retryCount < retryLimit; ++retryCount) {
			// Send the command.
			LOG_Trace(loggerEnumerateSend, QString("Enum send (%1): %2")
						.arg(retryCount).arg(PacketUtil::formatPacket(commandBuffer.get(), packetSize)));
			d->connection->write(commandBuffer, commandSize);
			// Wait until we don't recieve a response for the timeout period
			int minSeenCount = 0;
			d->restartTimeout();
			while (!d->halt && !d->hasTimeoutExpired(timeoutDuration)) {
				QThread::msleep(sleepDuration);
				// Handle any new replies.
				minSeenCount = d->getNewReplies(enumData);
			}
			LOG_Trace(loggerEnumerateStatus, QStringLiteral("Enum status: isOkay=%1, count=%2, seenMinimum=%3, "
										"retryLimit=%4, sleep=%5, timeout=%6").arg(isOkay).arg(enumData.size())
										.arg(seenMinimum).arg(retryLimit).arg(sleepDuration).arg(timeoutDuration));
			//
			if (d->halt) {
				return Thruster::TEC_Stopping;
			}
			// Handle errors.
			if (VR_PROTOCOL_NO_ERR_COMPLETE != d->parser_state.err.flag) {
				// Log the errors.
				LOG_Debug(loggerEnumerateError,
							QString("Enum error: sync1=%1, sync2=%2, headerCrc=%3, payloadOverrun=%4, "
										"payloadCrc=%5, flags=%6")
									.arg(d->parser_state.err.sync1_err_cnt)
									.arg(d->parser_state.err.sync2_err_cnt)
									.arg(d->parser_state.err.headerxsum_err_cnt)
									.arg(d->parser_state.err.payload_overrun_err_cnt)
									.arg(d->parser_state.err.payloadxsum_err_cnt)
									.arg(d->parser_state.err.flag, 0, 16));
				//
				protocol_vrcsr_clear_error(&d->parser_state);
			}
			isOkay = minSeenCount >= seenMinimum;
		}
	}
	catch (boost::system::system_error err) {
		LOG_Error(loggerEnumerate,
					QString("Enum error: port='%1', code=%2, name=%3 what=%4")
							.arg(d->serialPort)
							.arg(err.code().value())
							.arg(err.code().category().name())
							.arg(err.what()));
		return Thruster::TEC_PortError;
	}
	// Done if no errors at least once.
	if (isOkay) {
		
		duplicateNodeIdCount = d->duplicateNodeIdCount;
		duplicateSerialCount = d->duplicateSerialCount;
		return Thruster::TEC_NoError;
	}
	return Thruster::TEC_RetryLimit;
}

//======================================================================================================================
/*! @brief Record signal.

@return minimum seen count.
*/
int
ThrusterEnumIdPrivate::getNewReplies(
	QList<TioEnumData>	&replies	//!<[out] The replies data.
)
{
	QMutexLocker	locker(&replyMutex);
	if (!thrusterReplies->isEmpty()) {
		replies.append(*thrusterReplies);
		thrusterReplies->clear();
	}
	return minSeenCount;
}

//======================================================================================================================
/*! @brief Get our settings.
*/
void
getSettings(
	int		&retryLimit,		//!<[out] Retry limit.
	int		&sleepDuration,		//!<[out] Sleep duration.
	int		&timeoutDuration,	//!<[out] Timeout duration.
	int		&seenMinimum,		//!<[out] seen miminum.
	int		&commandDataSize	//!<[out] Command packet data size.
)
{
	IniFile	iniFile;
	commandDataSize	= iniFile.INI_GET(CfgThrusterRqstIdsPayloadSize);
	retryLimit		= iniFile.INI_GET(CfgEnumRetryLimit);
	sleepDuration	= iniFile.INI_GET(CfgEnumSleepDuration);
	timeoutDuration	= iniFile.INI_GET(CfgEnumTimeoutDuration);
	seenMinimum		= iniFile.INI_GET(CfgEnumSeenMinimum);
}

//======================================================================================================================
/*! @brief Determine if a timeout has occured.

@return true if a timeout has occured.
*/
qint64
ThrusterEnumIdPrivate::hasTimeoutExpired(
	qint64	timeout	//!<[in] Timeout period in milliseconds.
)
{
	bool answer = elapsedTimer.hasExpired(timeout);
	LOG_Trace(loggerQuery, QString("hasTimeoutExpired: %1").arg(answer));
	return answer;
}

//======================================================================================================================
/*! @brief Open the serial port.

@return the error code (Thruster::TEC_NoError if no errors).
*/
Thruster::Error
TioEnum::open(void)
{
	LOG_Trace(loggerOpen, "Open a connection");
	LOG_ASSERT_MSG(loggerOpen, !d->connection, "Connection already open");
	//
	std::string portName = d->serialPort.toStdString();
	try {
		videoray::GizmoConnection	*connection = new (std::nothrow) videoray::GizmoConnection(portName, *d.data());
		LOG_CHK_PTR_MSG(loggerOpen, connection, "Unable to create TioEnum videoray::GizmoConnection object");
		d->connection.reset(connection);
	}
	catch (boost::system::system_error err) {
		LOG_Error(loggerEnumerate,
					QString("Open error: port='%1', code=%2, name=%3 what=%4")
							.arg(d->serialPort)
							.arg(err.code().value())
							.arg(err.code().category().name())
							.arg(err.what()));
		return Thruster::TEC_OpenError;
	}
	// Ensure the connection was created.
	if (!d->connection) {
		LOG_Error(loggerEnumerate, QString("Error creating videoray::GizmoConnection to '%1'").arg(d->serialPort));
		return Thruster::TEC_OpenError;
	}
	// Ensure the port is open.
	if (!d->connection->isOpen()) {
		LOG_Error(loggerEnumerate, QString("Error opening serial port '%1'").arg(d->serialPort));
		return Thruster::TEC_OpenError;
	}
	//
	return Thruster::TEC_NoError;
}

//======================================================================================================================
/*! @brief Handle a VideoRay CSR protocol custom command response.
*/
void
ThrusterEnumIdPrivate::protocol_vrcsr_handle_custom_command_response(
	NodeId_t		nodeId,		//!<[in] Response packet node ID.
	Byte			flags,		//!<[in] Response packet flags.
	int				dataLength,	//!<[in] Response packet data length.
	DeviceType_t	deviceType,	//!<[in] Response packet device type.
	ByteP			buffer		//!<[in] Response packet data buffer.
)
{
	LOG_Trace(loggerCustomReply,
				QString("protocol_vrcsr_handle_custom_command_response: nodeId=%1, flags=%2, dataLength=%3, "
								"deviceType=%4")
						.arg(nodeId).arg(flags).arg(dataLength).arg(deviceType));
	Q_UNUSED(buffer);
	restartTimeout();
}

//======================================================================================================================
/*! @brief Handle a VideoRay CSR protocol response.
*/
void
ThrusterEnumIdPrivate::protocol_vrcsr_handle_response(
	NodeId_t		nodeId,		//!<[in] Response packet node ID.
	Byte			flags,		//!<[in] Response packet flags.
	int				csrAddress,	//!<[in] Response packet CSR address.
	int				dataLength,	//!<[in] Response packet data length.
	DeviceType_t	deviceType,	//!<[in] Response packet device type.
	ByteP			buffer		//!<[in] Response packet data buffer.
)
{
	LOG_Trace(loggerStandardReply,
				QString("protocol_vrcsr_handle_response: nodeId=%1, flags=%2, csrAddress=%3, dataLength=%4, "
								"deviceType=%5, data=%6")
						.arg(nodeId).arg(flags).arg(csrAddress).arg(dataLength).arg(deviceType)
						.arg(PacketUtil::formatData(buffer, dataLength)));
	// Ignore packets that are not ID request replies.
	if (RESPONSE_ENNUMERATION_FLAG != flags || ADDR_UTILITY != csrAddress || dataLength != (3 + BOOTLOADER_SN_SIZE)) {
		return;
	}
	//
	TioEnumData	currentReply(buffer);
	bool	curIsThruster	= DEVICE_M5_THRUSTER == currentReply.deviceType;
	int		priorIndex		= allReplies->indexOf(currentReply);
	LOG_Trace(loggerReplyPackets,
				QStringLiteral("Enum reply: prior=%1, size=%2, nodeId=%3, groupId=%4, deviceType=%5, serialNumber=%6")
								.arg(priorIndex).arg(allReplies->size()).arg(currentReply.nodeId)
								.arg(currentReply.groupId).arg(currentReply.deviceType).arg(currentReply.serialNumber));
	// Duplicate reply?
	if (0 <= priorIndex) {
		int minSeen = ++(*allReplies)[priorIndex].seenCount;
		for (auto reply : *allReplies) {
			if (minSeen > reply.seenCount) {
				minSeen = reply.seenCount;
			}
		}
		QMutexLocker	locker(&replyMutex);
		minSeenCount = minSeen;
	} else {
		// Check for duplicate node IDs and serial numbers.
		int		dupNodeId		= 0;
		int		dupSerNum		= 0;

		for (auto reply : *allReplies) {
			// Ignore duplicates unless at least one of the devices is a thruster.
			bool	isThruster = curIsThruster || DEVICE_M5_THRUSTER == reply.deviceType;
			if (isThruster && reply.nodeId == currentReply.nodeId) {
				dupNodeId = 1;
			}
			if (isThruster && reply.serialNumber == currentReply.serialNumber) {
				dupSerNum = 1;
			}
		}
		// Update counts of duplicates.
		duplicateNodeIdCount	+= dupNodeId;
		duplicateSerialCount	+= dupSerNum;
		// Report the duplicates.
		if (dupNodeId) {
			LOG_Warn(loggerDuplicates, QString("Duplicate node ID: %1").arg(currentReply.nodeId));
		}
		if (dupSerNum) {
			LOG_Warn(loggerDuplicates, QString("Duplicate serial number: %1").arg(currentReply.serialNumber));
		}
		// Remember this device to check for duplicates with other devices.
		allReplies->append(currentReply);
		// Report the device if it is a thruster.
		if (curIsThruster) {
			LOG_Debug(loggerStandardReply,
						QString("New thruster reply: nodeId=%1, serialNumber=%2")
								.arg(currentReply.nodeId).arg(currentReply.serialNumber));
			QMutexLocker	locker(&replyMutex);
			thrusterReplies->append(currentReply);
			minSeenCount = 1;
		}
	}
	got_ack_ = true;
	got_data_ = true;
	restartTimeout();
}

//======================================================================================================================
/*! @brief Start (or restart) timeout timer.
*/
void
ThrusterEnumIdPrivate::restartTimeout(void)
{
	LOG_Trace(loggerTimer, "Starting timeout timer");
	elapsedTimer.start();
}
