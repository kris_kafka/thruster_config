#pragma once
#ifndef TIOTESTINGMODE_H
#ifndef DOXYGEN_SKIP
#define TIOTESTINGMODE_H
#endif
//######################################################################################################################
/*! @file
@brief Declare the interface to the thruster configuration write module.
*/
//######################################################################################################################

#include <QtCore/QScopedPointer>

#include "tioBase.h"

//######################################################################################################################

class	QString;
class	TioImplTestingMode;

//######################################################################################################################
/*! @brief Interface to the thruster configuration write module.
*/
class TioTestingMode
:
	public	TioBase
{
public:		// Constructors & destructors
	TioTestingMode(QString const &serialPort, int nodeId, bool const volatile &halt);
	~TioTestingMode(void);

public:		// Functions
	Thruster::Error		testMode(int groupId, int motorId, int mode, bool sendSpeed, double speed, double &rpm,
									double &voltage, double &current, double &temperature, uint &faults);

protected:	// Functions
	virtual void		doClose(void);
	virtual bool		doOpen(void);

protected:	// Data
	QScopedPointer<TioImplTestingMode>	connection;		//!< Connection to the thruster.
};

//######################################################################################################################
#endif
