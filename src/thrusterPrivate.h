#pragma once
#ifndef THRUSTERPRIVATE_H
#ifndef DOXYGEN_SKIP
#define THRUSTERPRIVATE_H
#endif
//######################################################################################################################
/*! @file
@brief Declare the thruster interface module internals.
*/
//######################################################################################################################

#include <QtCore/QObject>
#include <QtCore/QScopedPointer>

//######################################################################################################################

class	QThread;
class	ThrusterTask;

//######################################################################################################################
/*! @brief The ThrusterPrivate class provides the implementation of the thruster interface module.
*/
class ThrusterPrivate
:
public	QObject
{
	Q_OBJECT
	Q_DISABLE_COPY(ThrusterPrivate);

public:		// Constructors / Destructors
	explicit	ThrusterPrivate(void);
				~ThrusterPrivate(void);

public:		// Functions.
	Q_SLOT void			moduleStarted(void);
	Q_SLOT void			moduleStopped(void);

public:		// Data
	QScopedPointer<ThrusterTask>	bgTask;				//!< Background task.
	QScopedPointer<QThread>			bgThread;			//!< Background thread.
	bool							isOpen;				//!< Is the thruster interface module open?
	QString							lastErrorMessage;	//!< Last error message.

Q_SIGNALS:
	//! Bbackground task started.
	void	taskStarted(void);

	//! Background task stopped.
	void	taskStopped(void);
};

//######################################################################################################################
#endif
