#pragma once
#if !defined(TIOENUM_LOG_H) && !defined(DOXYGEN_SKIP)
#define TIOENUM_LOG_H
//######################################################################################################################
/*! @file
@brief Declare the thruster I/O enumeration module loggers.
*/
//######################################################################################################################

#include "log.h"

//######################################################################################################################
// Loggers

LOG_SETUP_START("Thruster I/O Enumeration Module");

// Enumeration reply handler logger.
LOG_DEF_LOG(logger,					LOG_DL_TRACE,	"TioEnum");
// Enumeration reply handler logger.
LOG_DEF_LOG(loggerCreate,			LOG_DL_TRACE,	"TioEnum.Create");
// Custom command reply logger.
LOG_DEF_LOG(loggerCustomReply,		LOG_DL_TRACE,	"TioEnum.CustomReply");
// Duplicate node ID/serial number logger.
LOG_DEF_LOG(loggerDuplicates,		LOG_DL_TRACE,	"TioEnum.Duplicates");
// Enumerate logger.
LOG_DEF_LOG(loggerEnumerate,		LOG_DL_TRACE,	"TioEnum.Enumerate");
// Enumerate logger - I/O errors.
LOG_DEF_LOG(loggerEnumerateError,	LOG_DL_INFO,	"TioEnum.Enumerate.Error");
// Enumerate logger - Commands sent.
LOG_DEF_LOG(loggerEnumerateSend,	LOG_DL_TRACE,	"TioEnum.Enumerate.Send");
// Enumerate logger - Internal status.
LOG_DEF_LOG(loggerEnumerateStatus,	LOG_DL_INFO,	"TioEnum.Enumerate.Status");
// Open logger.
LOG_DEF_LOG(loggerOpen,				LOG_DL_TRACE,	"TioEnum.Open");
// Query logger.
LOG_DEF_LOG(loggerQuery,			LOG_DL_INFO,	"TioEnum.Query");
// Reply packets logger.
LOG_DEF_LOG(loggerReplyPackets,		LOG_DL_INFO,	"TioEnum.ReplyPackets");
// Standard reply logger.
LOG_DEF_LOG(loggerStandardReply,	LOG_DL_TRACE,	"TioEnum.StandardReply");
// Timer logger.
LOG_DEF_LOG(loggerTimer,			LOG_DL_INFO,	"TioEnum.Timer");

LOG_SETUP_STOP();

//######################################################################################################################
#endif
