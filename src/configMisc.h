#pragma once
#if !defined(CONFIGMISC_H) && !defined(DOXYGEN_SKIP)
#define CONFIGMISC_H
//######################################################################################################################
/*! @file
@brief Configuration file constants - Miscellaneous.
*/
//######################################################################################################################

#include "iniFile.h"

//######################################################################################################################
//! @hideinitializer Settings group prefix.
#define CFG_MISC_GROUP	"miscellaneous/"

//######################################################################################################################
// Background task termination wait values.

//! Maximum time in milliseconds to wait for the background task to terminate.
INI_DEFINE_INTEGER(CfgMiscBackgroundTaskEndTimeout,
					CFG_MISC_GROUP "background_task_termination_timeout",
					5000);

//! Time in milliseonds to sleep while waiting for the background task to terminate.
INI_DEFINE_INTEGER(CfgMiscBackgroundTaskEndSleep,
					CFG_MISC_GROUP "background_task_termination_sleep",
					50);

//######################################################################################################################
// Status message display durations.

//! Time in milliseconds (0=unlimited) to display non-error status messages.
INI_DEFINE_INTEGER(CfgMiscStatusDurationError,
						CFG_MISC_GROUP "status_duration_error",
						60000);

//! Time in milliseconds (0=unlimited) to display non-error status messages.
INI_DEFINE_INTEGER(CfgMiscStatusDurationNotError,
					CFG_MISC_GROUP "status_duration_not_error",
					5000);

//! Time in milliseconds (0=unlimited) to display permanent status messages.
INI_DEFINE_INTEGER(CfgMiscStatusDurationPermanent,
					CFG_MISC_GROUP "status_duration_permanent",
					0);

//######################################################################################################################
// Testing I/O modes values.

//! Testing mode display name format key.
INI_DEFINE_STRING(CfgMiscTestingModeFormat,
					CFG_MISC_GROUP "testing_mode_name_format",
					"{mode}: {name}");

//! Testing mode default key.
INI_DEFINE_INTEGER(CfgMiscTestingModeDefault,
					CFG_MISC_GROUP "testing_mode_default",
					0);

//! Testing speed slider page step.
INI_DEFINE_INTEGER(CfgMiscTestingSpeedSliderPageStep,
					CFG_MISC_GROUP "testing_speed_slider_page_step",
					100);

//! Testing speed slider single step.
INI_DEFINE_INTEGER(CfgMiscTestingSpeedSliderSingleStep,
					CFG_MISC_GROUP "testing_speed_slider_single_step",
					10);

//! Testing speed slider step count.
INI_DEFINE_INTEGER(CfgMiscTestingSpeedSliderSteps,
					CFG_MISC_GROUP "testing_speed_slider_steps",
					1000);

//! Testing speed slider tick interval.
INI_DEFINE_INTEGER(CfgMiscTestingSpeedSliderTickInterval,
					CFG_MISC_GROUP "testing_speed_slider_tick_interval",
					100);

//! Testing speed spin box decimal digits.
INI_DEFINE_INTEGER(CfgMiscTestingSpeedSpinBoxDecimalDigits,
					CFG_MISC_GROUP "testing_speed_spin_box_decimal_digits",
					1);

//! Testing speed spin box (single) step.
INI_DEFINE_FLOAT(CfgMiscTestingSpeedSpinBoxStep,
					CFG_MISC_GROUP "testing_speed_spin_box_step",
					0.1);

//! Testing I/O modes.
INI_DEFINE_STRING(CfgMiscTestingModes,
					CFG_MISC_GROUP "testing_modes",
					"");

//######################################################################################################################
#endif
