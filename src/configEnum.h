#pragma once
#if !defined(CONFIGENUM_H) && !defined(DOXYGEN_SKIP)
#define CONFIGENUM_H
//######################################################################################################################
/*! @file
@brief Configuration file constants - Thruster interface - Enumerate IDs.
*/
//######################################################################################################################

#include "iniFile.h"

//######################################################################################################################

//! @hideinitializer Settings group prefix of the emulation delay values.
#define CFG_ENUM_GROUP	"thruster_enumerate/"

//! Repeat if no errors setting.
INI_DEFINE_INTEGER(CfgEnumRetryLimit,
					CFG_ENUM_GROUP "retry_limit",
					25);

//! Repeat if no errors setting.
INI_DEFINE_INTEGER(CfgEnumSeenMinimum,
					CFG_ENUM_GROUP "seen_minimum",
					2);

//! Repeat if no errors setting.
INI_DEFINE_INTEGER(CfgEnumSleepDuration,
					CFG_ENUM_GROUP "sleep_duration",
					20);

//! Repeat if no errors setting.
INI_DEFINE_INTEGER(CfgEnumTimeoutDuration,
					CFG_ENUM_GROUP "timeout_duration",
					200);

//######################################################################################################################
#endif
