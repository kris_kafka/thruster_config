#pragma once
#if !defined(OPTIONS_LOG_H) && !defined(DOXYGEN_SKIP)
//######################################################################################################################
/*! @file
@brief Declare the options module loggers.
*/
//######################################################################################################################

#include "log.h"

//######################################################################################################################
// Loggers

LOG_SETUP_START("Program Options Module");

// Top level module logger.
LOG_DEF_LOG(logger,				LOG_DL_TRACE,	"Options");
// Emulation logger.
LOG_DEF_LOG(loggerEmulation,	LOG_DL_INFO,	"Options.Emulation");
// Environment information logger.
LOG_DEF_LOG(loggerEnvironment,	LOG_DL_TRACE,	"Options.Environment");
// Setup mode logger.
LOG_DEF_LOG(loggerSetup,		LOG_DL_INFO,	"Options.Setup");
// Serial port logger.
LOG_DEF_LOG(loggerSerialPort,	LOG_DL_INFO,	"Options.SerialPort");

LOG_SETUP_STOP();

//######################################################################################################################
#endif
