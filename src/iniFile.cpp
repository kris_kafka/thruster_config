/*! @file
@brief Define the INI file module.
*/
//######################################################################################################################

#include "app_com.h"

#include <tuple>

#include <QtCore/QCoreApplication>
#include <QtCore/QList>
#include <QtCore/QStringList>

#include "iniFile.h"
#include "iniFile_log.h"
#include "strUtil.h"

//######################################################################################################################

//! Setting information.
typedef std::tuple<IniKey, QVariant, IniKey>	Setting;

//######################################################################################################################

static bool				isSetup		= false;	//! Setup processing mode.
static bool				isStopped	= false;	//! Setup processing finished.
static QStringList		arrays;					//! Current array names.
static QStringList		groups;					//! Current group names.
static QList<Setting>	settings;				//! Settings for array/group settings.

//######################################################################################################################
/*! @brief Create an IniFile object.
*/
IniFile::IniFile(void)
:
	QSettings	(QSettings::IniFormat,
				 QSettings::UserScope,
				 QCoreApplication::organizationName(),
				 QCoreApplication::applicationName())
{
	LOG_Trace(loggerCreate, "Creating an IniFile object");
}

//======================================================================================================================
/*! @brief Create an IniFile object.
*/
IniFile::IniFile(
	QString const	&filePath	//!<[in] File path.
)
:
	QSettings	(filePath, QSettings::IniFormat)
{
	LOG_Trace(loggerCreate, QStringLiteral("Creating an IniFile object for: %1").arg(filePath));
}

//======================================================================================================================
/*! @brief Destroy an IniFile object.
*/
IniFile::~IniFile(void)
{
	LOG_Trace(loggerCreate, "Destroying an IniFile object");
}

//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
/*! @brief Get an INI file setting value.

@return the setting's value.
*/
IniBits
IniFile::get(
	IniBits			def,	//!<[in] Default value of the setting.
	IniKey const	&key	//!<[in] Key of the setting.
)
{
	LOG_ASSERT_MSG(loggerSetup, !isSetup, "Setup mode");
	QVariant var = value(key, def);
	bool isOk = false;
	IniBits answer = var.toULongLong(&isOk);
	LOG_AssertMsg(loggerGet, isOk,
					QStringLiteral("IniFile:update(unsigned) Invalid value for %1: %2").arg(key).arg(var.toString()));
	LOG_Trace(loggerGet, QStringLiteral("IniFile:get(unsigned) %1=%2").arg(key).arg(answer));
	return answer;
}

//======================================================================================================================
/*! @brief Get an INI file setting value.

@return the setting's value.
*/
IniBoolean
IniFile::get(
	IniBoolean		def,	//!<[in] Default value of the setting.
	IniKey const	&key	//!<[in] Key of the setting.
)
{
	LOG_ASSERT_MSG(loggerSetup, !isSetup, "Setup mode");
	IniBoolean answer = value(key, def).toBool();
	LOG_Trace(loggerGet, QStringLiteral("IniFile:get(bool) %1=%2").arg(key).arg(answer));
	return answer;
}

//======================================================================================================================
/*! @brief Get an INI file setting value.

@return the setting's value.
*/
IniBytes
IniFile::get(
	IniBytes const	&def,	//!<[in] Default value of the setting.
	IniKey const	&key	//!<[in] Key of the setting.
)
{
	LOG_ASSERT_MSG(loggerSetup, !isSetup, "Setup mode");
	IniBytes answer = value(key, def).toByteArray();
	LOG_Trace(loggerGetBytes, QStringLiteral("IniFile:get(bytes) %1=%2").arg(key).arg(StrUtil::toString(answer)));
	return answer;
}

//======================================================================================================================
/*! @brief Get an INI file setting value.

@return the setting's value.
*/
IniFloat
IniFile::get(
	IniFloat		def,	//!<[in] Default value of the setting.
	IniKey const	&key	//!<[in] Key of the setting.
)
{
	LOG_ASSERT_MSG(loggerSetup, !isSetup, "Setup mode");
	QVariant var = value(key, def);
	bool isOk = false;
	IniFloat answer = var.toDouble(&isOk);
	LOG_AssertMsg(loggerGet, isOk,
					QStringLiteral("IniFile:update(float) Invalid value for %1: %2").arg(key).arg(var.toString()));
	LOG_Trace(loggerGet, QStringLiteral("IniFile:get(float) %1=%2").arg(key).arg(answer));
	return answer;
}

//======================================================================================================================
/*! @brief Get an INI file setting value.

@return the setting's value.
*/
IniInteger
IniFile::get(
	IniInteger		def,	//!<[in] Default value of the setting.
	IniKey const	&key	//!<[in] Key of the setting.
)
{
	LOG_ASSERT_MSG(loggerSetup, !isSetup, "Setup mode");
	QVariant var = value(key, def);
	bool isOk = false;
	IniInteger answer = var.toLongLong(&isOk);
	LOG_AssertMsg(loggerGet, isOk,
					QStringLiteral("IniFile:update(integer) Invalid value for %1: %2").arg(key).arg(var.toString()));
	LOG_Trace(loggerGet, QStringLiteral("IniFile:get(integer) %1=%2").arg(key).arg(answer));
	return answer;
}

//======================================================================================================================
/*! @brief Get an INI file setting value.

@return the setting's value.
*/
IniString
IniFile::get(
	IniString const	&def,	//!<[in] Default value of the setting.
	IniKey const	&key	//!<[in] Key of the setting.
)
{
	LOG_ASSERT_MSG(loggerSetup, !isSetup, "Setup mode");
	QVariant var = value(key, def);
	IniString answer = var.toString();
	LOG_Trace(loggerGet, QStringLiteral("IniFile:get(string) %1=%2").arg(key).arg(answer));
	return answer;
}

//======================================================================================================================
/*! @brief Set an INI file setting value.

@return a reference to this.
*/
void
IniFile::set(
	IniBits			val,	//!<[in] New value.
	IniKey const	&key	//!<[in] Key of the setting.
)
{
	LOG_ASSERT_MSG(loggerSetup, !isSetup, "Setup mode");
	setValue(key, val);
	LOG_Trace(loggerSet, QStringLiteral("IniFile:set(unsigned) %1=%2").arg(key).arg(val));
}

//======================================================================================================================
/*! @brief Set an INI file setting value.

@return a reference to this.
*/
void
IniFile::set(
	IniBoolean		val,	//!<[in] New value.
	IniKey const	&key	//!<[in] Key of the setting.
)
{
	LOG_ASSERT_MSG(loggerSetup, !isSetup, "Setup mode");
	setValue(key, val);
	LOG_Trace(loggerSet, QStringLiteral("IniFile:set(bool) %1=%2").arg(key).arg(val));
}

//======================================================================================================================
/*! @brief Set an INI file setting value.

@return a reference to this.
*/
void
IniFile::set(
	IniBytes const	&val,	//!<[in] New value.
	IniKey const	&key	//!<[in] Key of the setting.
)
{
	LOG_ASSERT_MSG(loggerSetup, !isSetup, "Setup mode");
	setValue(key, val);
	LOG_Trace(loggerSetBytes, QStringLiteral("IniFile:set(bytes) %1=%2").arg(key).arg(StrUtil::toString(val)));
}

//======================================================================================================================
/*! @brief Set an INI file setting value.

@return a reference to this.
*/
void
IniFile::set(
	IniFloat		val,	//!<[in] New value.
	IniKey const	&key	//!<[in] Key of the setting.
)
{
	LOG_ASSERT_MSG(loggerSetup, !isSetup, "Setup mode");
	setValue(key, val);
	LOG_Trace(loggerSet, QStringLiteral("IniFile:set(float) %1=%2").arg(key).arg(val));
}

//======================================================================================================================
/*! @brief Set an INI file setting value.

@return a reference to this.
*/
void
IniFile::set(
	IniInteger		val,	//!<[in] New value.
	IniKey const	&key	//!<[in] Key of the setting.
)
{
	LOG_ASSERT_MSG(loggerSetup, !isSetup, "Setup mode");
	setValue(key, val);
	LOG_Trace(loggerSet, QStringLiteral("IniFile:set(integer) %1=%2").arg(key).arg(val));
}

//======================================================================================================================
/*! @brief Set an INI file setting value.

@return a reference to this.
*/
void
IniFile::set(
	IniString const	&val,	//!<[in] New value.
	IniKey const	&key	//!<[in] Key of the setting.
)
{
	LOG_ASSERT_MSG(loggerSetup, !isSetup, "Setup mode");
	setValue(key, val);
	LOG_Trace(loggerSet, QStringLiteral("IniFile:set(string) %1=%2").arg(key).arg(val));
}

//======================================================================================================================
/*! @brief Add a new array.
*/
void
IniFile::setupArrayAdd(
	IniKey const	&array	//!<[in] Array name.
)
{
	LOG_ASSERT_MSG(loggerSetup, isSetup, "Not is setup mode");
	LOG_Trace(loggerSetup, QStringLiteral("Define array: %1").arg(array));
	LOG_AssertMsg(loggerSetup,
					settings.isEmpty(),
					QStringLiteral("New array while pending settings: array=%1").arg(array));
	arrays.append(array);
}

//======================================================================================================================
/*! @brief End array processing.
*/
void
IniFile::setupArrayEnd(void)
{
	LOG_ASSERT_MSG(loggerSetup, isSetup, "Not is setup mode");
	for (QString array : arrays) {
		beginWriteArray(array, 0);
		setArrayIndex(0);
		for (Setting setting : settings) {
			IniKey		key		= std::get<0>(setting);
			QVariant	def		= std::get<1>(setting);
			IniKey		type	= std::get<2>(setting);
			LOG_Trace(loggerSetup,
						QStringLiteral("Define %1 setting: key=%2/1/%3, default=%4")
										.arg(type).arg(array).arg(key).arg(def.toString()));
			setValue(key, def);
		}
		endArray();
	}
	//
	arrays.clear();
	setupEndArraysAndGroups();
}

//======================================================================================================================
/*! @brief End array and group processing.
*/
void
IniFile::setupEndArraysAndGroups(void)
{
	LOG_ASSERT_MSG(loggerSetup, isSetup, "Not is setup mode");
	if (arrays.isEmpty() && groups.isEmpty()) {
		settings.clear();
	}
}

//======================================================================================================================
/*! @brief Define a setting.
*/
void
IniFile::setupDefine(
	IniKey const	&key,	//!<[in] Name of the setting.
	QVariant const	&def,	//!<[in] Default value of the setting.
	IniKey const	&type	//!<[in] Type of the setting.
)
{
	LOG_ASSERT_MSG(loggerSetup, isSetup, "Not is setup mode");
	if (!arrays.isEmpty() || !groups.isEmpty()) {
		LOG_Trace(loggerSetup,
					QStringLiteral("Adding %1 setting: key=%2, default=%3").arg(type).arg(key).arg(def.toString()));
		settings.append(Setting(key, def, type));
		return;
	}
	LOG_Trace(loggerSetup, QStringLiteral("Define %1 setting: key=%2, default=%3").arg(type).arg(key).arg(def.toString()));
	setValue(key, def);
}

//======================================================================================================================
/*! @brief Add a new group.
*/
void
IniFile::setupGroupAdd(
	IniKey const	&group	//!<[in] Group name.
)
{
	LOG_ASSERT_MSG(loggerSetup, isSetup, "Not is setup mode");
	LOG_Trace(loggerSetup, QStringLiteral("Define group: %1").arg(group));
	LOG_AssertMsg(loggerSetup,
					settings.isEmpty(),
					QStringLiteral("New group while pending settings: group=%1").arg(group));
	groups.append(group);
}

//======================================================================================================================
/*! @brief End group processing.
*/
void
IniFile::setupGroupEnd(void)
{
	LOG_ASSERT_MSG(loggerSetup, isSetup, "Not is setup mode");
	for (QString group : groups) {
		beginGroup(group);
		for (Setting setting : settings) {
			IniKey		key		= std::get<0>(setting);
			QVariant	def		= std::get<1>(setting);
			IniKey		type	= std::get<2>(setting);
			LOG_Trace(loggerSetup,
						QStringLiteral("Define %1 setting: key=%2/%3, default=%4")
										.arg(type).arg(group).arg(key).arg(def.toString()));
			setValue(key, def);
		}
		endGroup();
	}
	//
	groups.clear();
	setupEndArraysAndGroups();
}

//======================================================================================================================
/*! @brief Start the setup processing.
*/
void
IniFile::setupStart(void)
{
	LOG_ASSERT_MSG(loggerSetup, !isSetup, "Duplicate setupStart");
	isSetup = true;
	//
	clear();
	arrays.clear();
	groups.clear();
	settings.clear();
}

//======================================================================================================================
/*! @brief Stop the setup processing.
*/
void
IniFile::setupStop(void)
{
	LOG_ASSERT_MSG(loggerSetup, isSetup, "setupStop without setupStart");
	LOG_ASSERT_MSG(loggerSetup, !isStopped, "Duplicate setupStop");
	isStopped = true;
	LOG_AssertMsg(loggerSetup, arrays.isEmpty(), QStringLiteral("Pending arrays: %1").arg(arrays.join(", ")));
	LOG_AssertMsg(loggerSetup, groups.isEmpty(), QStringLiteral("Pending groups: %1").arg(groups.join(", ")));
}

//======================================================================================================================
/*! @brief Sync and check.

@return a reference to this.
*/
void
IniFile::syncAndCheck(void)
{
	sync();
	QSettings::Status stat = status();
	if (QSettings::NoError != stat) {
		LOG_Error(loggerSync, QStringLiteral("IniFile:syncAndCheck Error %1").arg(stat));
	}
}
