#pragma once
#ifndef TIOBASE_H
#ifndef DOXYGEN_SKIP
#define TIOBASE_H
#endif
//######################################################################################################################
/*! @file
@brief Declare the interface to the thruster I/O module.
*/
//######################################################################################################################

#include <QtCore/QString>

#include "thruster.h"

//######################################################################################################################

class	TioImpl;

//######################################################################################################################
/*! @brief VideoRay CSR protocol read thruster configuration.
*/
class TioBase
{
public:		// Constructors & destructors
	TioBase(QString const &serialPort_, int nodeId_, bool const volatile &halt_);
	~TioBase(void);

public:		// Functions
	void				close(void);
	Thruster::Error		open(void);

protected:	// Data
	/*!@brief Close connection.
	*/
	virtual void		doClose(void)		= 0;
	/*!@brief Open connection.
	
	@return true if no errors occur else false.
	*/
	virtual bool		doOpen(void)		= 0;

protected:	// Data
	bool const volatile		&halt;		//!< Halt I/O.
	int const				nodeId;		//!< Node ID.
	QString const			serialPort;	//!< Serial port name.
};

//######################################################################################################################
#endif
