#pragma once
#if !defined(TESTDIALOG_LOG_H) && !defined(DOXYGEN_SKIP)
#define TESTDIALOG_LOG_H
//######################################################################################################################
/*! @file
@brief Declare the about dialog loggers.
*/
//######################################################################################################################

#include "log.h"

//######################################################################################################################
// Loggers

LOG_SETUP_START("Test Dialog");

// Top level module logger.
LOG_DEF_LOG(logger,				LOG_DL_INFO,	"TestDialog");
// Object creation/destruction logger
LOG_DEF_LOG(loggerCreate,		LOG_DL_INFO,	"TestDialog.Create");
// GUI state update logger.
LOG_DEF_LOG(loggerGuiState,		LOG_DL_INFO,	"TestDialog.GuiState");
// Invokation logger.
LOG_DEF_LOG(loggerInvoke,		LOG_DL_INFO,	"TestDialog.Invoke");
// Status logger.
LOG_DEF_LOG(loggerStatus,		LOG_DL_INFO,	"TestDialog.Status");
// User input logger.
LOG_DEF_LOG(loggerUserInput,	LOG_DL_INFO,	"TestDialog.UserInput");
// Windows state logger.
LOG_DEF_LOG(loggerWindowState,	LOG_DL_INFO,	"TestDialog.WindowState");

LOG_SETUP_STOP();

//######################################################################################################################
#endif
