#pragma once
#ifndef TIOCONFIGWRITE_H
#ifndef DOXYGEN_SKIP
#define TIOCONFIGWRITE_H
#endif
//######################################################################################################################
/*! @file
@brief Declare the interface to the thruster configuration write module.
*/
//######################################################################################################################

#include <QtCore/QScopedPointer>

#include "tioBase.h"

//######################################################################################################################

class	QByteArray;
class	QString;
class	TioImplConfigWrite;

//######################################################################################################################
/*! @brief Interface to the thruster configuration write module.
*/
class TioConfigWrite
:
	public	TioBase
{
public:		// Constructors & destructors
	TioConfigWrite(QString const &serialPort, int nodeId, bool const volatile &halt);
	~TioConfigWrite(void);

public:		// Functions
	Thruster::Error		configurationWrite(int motorId, int maximumPower, bool rotationReverse,
											QByteArray const &csrBlob);

protected:	// Functions
	virtual void		doClose(void);
	virtual bool		doOpen(void);

protected:	// Data
	QScopedPointer<TioImplConfigWrite>	connection;		//!< Connection to the thruster.
};

//######################################################################################################################
#endif
