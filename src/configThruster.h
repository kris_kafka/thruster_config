#pragma once
#if !defined(CONFIGTHRUSTER_H) && !defined(DOXYGEN_SKIP)
#define CONFIGTHRUSTER_H
//######################################################################################################################
/*! @file
@brief Configuration file constants - Thruster interface.
*/
//######################################################################################################################

#include "iniFile.h"

//######################################################################################################################

//! @hideinitializer Settings group prefix.
#define CFG_THRUSTER_GROUP		"thruster_interface/"

//! Use CDMA in request ID code reply.
INI_DEFINE_BOOLEAN(CfgThrusterConfigReadIdUseCdma,
					CFG_THRUSTER_GROUP "configuration_read_id_use_cdma",
					true);

//! Enhanced serial port test.
INI_DEFINE_BOOLEAN(CfgThrusterEnhancedPortTest,
					CFG_THRUSTER_GROUP "enhanced_serial_port_test",
					false);

//! Serial port close delay after enumerating thrusters.
INI_DEFINE_INTEGER(CfgThrusterEnumCloseDelay,
					CFG_THRUSTER_GROUP "enum_port_close_delay",
					250);

//! Serial port close delay after I/O operations thrusters.
INI_DEFINE_INTEGER(CfgThrusterGioCloseDelay,
					CFG_THRUSTER_GROUP "gio_port_close_delay",
					250);

//! Group ID map mask.
INI_DEFINE_BITS(CfgThrusterGroupIdMask,
					CFG_THRUSTER_GROUP "group_id_mask",
					0x00);

//! Request ID command packet data.
INI_DEFINE_INTEGER(CfgThrusterRqstIdsPayloadSize,
					CFG_THRUSTER_GROUP "request_id_data_size",
					4);

//! Set ID command packet data.
INI_DEFINE_INTEGER(CfgThrusterSetIdsPayloadSize,
					CFG_THRUSTER_GROUP "set_id_data_size",
					2);

//! Testing mode speed send count.
INI_DEFINE_INTEGER(CfgThrusterTestModeSpeedCount,
					CFG_THRUSTER_GROUP "testing_speed_send_count",
					5);

//######################################################################################################################
#endif
