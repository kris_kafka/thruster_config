#pragma once
#ifndef THRUSTERTBASE_H
#ifndef DOXYGEN_SKIP
#define THRUSTERTBASE_H
#endif
//######################################################################################################################
/*! @file
@brief Declare the thruster interface module background task.
*/
//######################################################################################################################

#include <QtCore/QObject>

#include "thruster.h"

//######################################################################################################################
// Signalling macros.

//----------------------------------------------------------------------------------------------------------------------
/*! @hideinitializer @brief Log and emit a signal with an error code without changing the state.
@param signal		Signal to emit.
@param errorCode	Error code to send with the signal.
*/
#define EMIT_SIGNAL(signal, errorCode) \
		do { \
			LOG_Debug(loggerSignals, QString("Emit " #signal ": error=%1").arg(errorCode)); \
			Q_EMIT signal(errorCode); \
		} while (0)

//----------------------------------------------------------------------------------------------------------------------
/*! @hideinitializer @brief Log and emit a signal with an error code without changing the state.
@param signal		Signal to emit.
@param errorCode	Error code to send with the signal.
*/
#define EMIT_IDLE(signal, errorCode) \
		do { \
			state = Thruster::State::Idle; \
			EMIT_SIGNAL(signal, errorCode); \
		} while (0)

//######################################################################################################################

/*! @brief The ThrusterPrivate class provides the implementation of the thruster interface module background task.
*/
class ThrusterTaskBase
:
public	QObject
{
	Q_OBJECT

public:		// Constructors / Destructors
	explicit	ThrusterTaskBase(void);
	virtual		~ThrusterTaskBase(void);

public:		// Virtual functions
	/*!
		@brief Read the configuration of a real thruster.
		@param[in]	nodeId	ID of node to read configuration from.
		@param[out]	nodeIdRead		Node ID read from thruster.
		@param[out]	groupId			Group ID read from thruster.
		@param[out]	motorId			Motor ID read from thruster.
		@param[out]	maximumPower	Maximum power read from thruster.
		@param[out]	rotationReverse	Rotation reversed read from thruster.
		@param[out]	serialNumber	Serial number read from thruster.
		@param[out]	csrBlob			CSR blob.
		@return true if no errors, else false.
	*/
	virtual bool	configurationRead(int nodeId, int &nodeIdRead, int &groupId, int &motorId, int &maximumPower,
										bool &rotationReverse, QString &serialNumber, QByteArray &csrBlob)	= 0;
	/*!
		@brief Write the configuration of a real thruster.
		@param[in]	nodeId			ID of node to write configuraton to.
		@param[in]	motorId			New motor ID.
		@param[in]	maximumPower	New maximum power level.
		@param[in]	rotationReverse	New rotation is reversed.
		@param[in]	csrBlob			CSR blob.
	*/
	virtual void	configurationWrite(int nodeId, int motorId, int maximumPower, bool rotationReverse,
										QByteArray const &csrBlob)	= 0;

	/*!
		@brief Change the node ID of @a currentNode.
		@param[in]	serialNumber	Serial number of the thruster.
		@param[in]	oldNodeId		Old node ID of the thruster.
		@param[in]	newNodeId		New node ID of the thruster.
		@param[in]	newGroupId		New group ID of the thruster.
	*/
	virtual void	idsChange(QString const &serialNumber, int oldNodeId, int newNodeId, int newGroupId)	= 0;
	/*!
		@brief Scan for real thrusters.
		@return true if no errors, else false.
	*/
	virtual bool	scanForThrusters(void)	= 0;
	/*!
		@brief Check if serial port @a is valid.
		@param[in]	portName	Serial port.
		@return true if valid else false.
	*/
	virtual bool	serialPortCheck(QString const &portName)	= 0;
	/*!
		@brief Testing mode.
		@param[in]	nodeId		Node ID of thruster to be tested.
		@param[in]	groupId		Group ID of thruster to be tested.
		@param[in]	motorId		Motor ID of thruster to be tested.
		@param[in]	mode		Testing mode.
	*/
	virtual void	testMode(int nodeId, int groupId, int motorId, int mode)	= 0;

public:		// Functions
	bool			serialPortSet(QString const &portName);
	void			testModeSetSpeed(double speed);


public:		// Data
	int				duplicateNodeIdCount;	//!< Number of duplicate node IDs during last scan for thrusters.
	int				duplicateSerialCount;	//!< Number of duplicate serial numbers during last scan for thrusters.
	QString			serialPort;				//!< Active serial port.
	volatile Thruster::State
					state;					//!< Current state of background task.
	volatile bool	taskHalt;				//!< Set when the background task should stop.
	volatile bool	testModeHalt;			//!< Set when test thruster should be stopped.
	volatile double	testingModeSpeed;		//!< Current testing mode thruster speed.
	volatile bool	testModeSpeedChanged;	//!< Test thruster speed changed.

Q_SIGNALS:
	//! Configuration data read finished.
	void	configurationReadStop(
					int		errorCode		//!< Error code (TEC_NoError if no errors).
					);

	//! Configuration data write finished.
	void	configurationWriteStop(
					int		errorCode		//!< Error code (TEC_NoError if no errors).
					);

	//! Change node ID finished.
	void	idsChangeStop(
					int		errorCode		//!< Error code (TEC_NoError if no errors).
					);

	//! Quit the background thread.
	void	quitThread(void);

	//! Thruster information found during scan for thrusters.
	void	scanForThrustersNews(
					int		nodeId,			//!< Node ID of thruster.
					int		groupId,		//!< Group ID of thruster.
					QString	serialNumber	//!< Serial number of thruster.
					);

	//! Scanning for thrusters finished.
	void	scanForThrustersStop(
					int		errorCode		//!< Error code (TEC_NoError if no errors).
					);

	//! Testing mode status data.
	void	testingModeNews(
					int		nodeId,			//!< Node ID.
					double	rpm,			//!< Thruster current RPM.
					double	voltage,		//!< Bus voltage.
					double	current,		//!< Bus current.
					double	temperature,	//!< Thruster temperature.
					uint	faults			//!< Thruster fault flags.
					);

	//! Testing mode stopped.
	void	testingModeStop(
					int		errorCode		//!< Error code (TEC_NoError if no errors).
					);
};

//######################################################################################################################
#endif
