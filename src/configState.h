#pragma once
#if !defined(CONFIGSTATE_H) && !defined(DOXYGEN_SKIP)
#define CONFIGSTATE_H
//######################################################################################################################
/*! @file
@brief Configuration file constants - Window state.
*/
//######################################################################################################################

#include "iniFile.h"

//######################################################################################################################

//! @hideinitializer Settings group prefix.
#define CFG_STATE_GROUP	"window_state/"

//! Change IDs dialog geometry.
INI_DEFINE_BYTES(CfgStateChangeIdsDialogGeometry,
					CFG_STATE_GROUP "change_ids_dialog_geometry",
					"");

//! Main window geometry.
INI_DEFINE_BYTES(CfgStateMainWindowGeometry,
					CFG_STATE_GROUP "main_window_geometry",
					"");

//! Main window  state.
INI_DEFINE_BYTES(CfgStateMainWindowState,
					CFG_STATE_GROUP "main_window_state",
					"");

//! Port selection dialog geometry.
INI_DEFINE_BYTES(CfgStatePortSelectionDialogGeometry,
					CFG_STATE_GROUP "port_selection_dialog_geometry",
					"");

//! Test mode dialog geometry.
INI_DEFINE_BYTES(CfgStateTestModeDialogGeometry,
					CFG_STATE_GROUP "test_mode_dialog_geometry",
					"");

//######################################################################################################################
#endif
