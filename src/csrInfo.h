#pragma once
#ifndef CSRINFO_H
#ifndef DOXYGEN_SKIP
#define CSRINFO_H
#endif
//######################################################################################################################
/*! @file
@brief Declare the interface to the CSR data info utility module.
*/
//######################################################################################################################

#include <QtCore/QString>

//######################################################################################################################
/*! @brief Encapsulates the CSR data utility module.
*/
namespace CsrInfo
{
	//! Data type codes.
	typedef enum class Type_ {
		Bad = -1,		//<! Bad Type.
		Min = 0,		//!< Minimum of valid Type values.
		SI1 = Min,		//!< 0: Signed 1-byte integer.
		UI1,			//!< 1: Unsigned 1-byte integer.
		SI2,			//!< 2: Signed 2-byte integer.
		UI2,			//!< 3: Unsigned 2-byte integer.
		SI4,			//!< 4: Signed 4-byte integer.
		UI4,			//!< 5: Unsigned 4-byte integer.
		SI8,			//!< 6: Signed 8-byte integer.
		UI8,			//!< 7: Unsigned 8-byte integer.
		FP4,			//!< 8: 4-byte floating point.
		FP8,			//!< 9: 8-byte floating point.
		// Insert new values here.
		End,			//!< Upper limit of valid Type values.
		Max = End - 1	//!< Maximum of valid Type values.
	} Type;

	//! Name type codes.
	typedef enum class Name_ {
		Bad = -1,		//<! Bad Name.
		Min = 0,		//!< Minimum of valid Name values.
		Cpp = Min,		//!< (signed|unsigned) (char|short|int|long) or (float|double)
		Alt,			//!< int(8|16|32|64)_t, uint(8|16|32|64)_t, or (float|double)
		Csr,			//!< Sint(8|16|32|64), Uint(8|16|32|64), or (Real32|Real64)
		English,		//!< (1|2|4|8)-byte (signed|unsigned) integer or (4|8)-byte floating point
		Qt,				//!< qint(8|16|32|64), quint(8|16|32|64), or (float|double)
		Std,			//!< int(8|16|32|64)_t, uint(8|16|32|64)_t, or (float|double)
		// Insert new values here.
		End,			//!< Upper limit of valid Name values.
		Max = End -1	//!< Maximum of valid Name values.
	} Name;

	bool		check(Type type, int &size, bool &isSigned_, bool &isFloat_, QString &name_);
	bool		isFloat(Type type);
	bool		isInteger(Type type);
	bool		isSigned(Type type);
	bool		isUnsigned(Type type);
	QString		name(Type type, Name nameType = Name::Csr);
	int			size(Type type);
	int			size(QString const &name);
	Type		type(QString const &name);
};

//######################################################################################################################
#endif
