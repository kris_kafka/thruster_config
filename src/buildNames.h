#pragma once
#ifndef BUILDNAMES_H
#ifndef DOXYGEN_SKIP
#define BUILDNAMES_H
#endif
//######################################################################################################################
/*! @file
@brief Define the names of the company, product, program, etcetera.
*/
//######################################################################################################################

#define BUILD_STR_COMPANY_NAME	"VideoRay LLC"			//!< Company name.
#define BUILD_STR_PROGRAM_NAME	"thruster_config"		//!< Program name.
#define BUILD_STR_PRODUCT_NAME	"thruster_config"		//!< Product name.
#define BUILD_STR_INTERNAL_NAME	"Thruster Configurator"	//!< Internal program name.

//######################################################################################################################
#endif
