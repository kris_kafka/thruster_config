#pragma once
#ifndef ABOUTDIALOGPRIVATE_H
#ifndef DOXYGEN_SKIP
#define ABOUTDIALOGPRIVATE_H
#endif
//######################################################################################################################
/*! @file
@brief Declare the implementation of the about dialog module.
*/
//######################################################################################################################

#include <QtWidgets/QDialog>

//######################################################################################################################
/*! @brief The AboutDialogPrivate class provides the implementation of the about dialog module.
*/
class AboutDialogPrivate
:
public	QDialog
{
	Q_OBJECT
	Q_DISABLE_COPY(AboutDialogPrivate)

public:		// Constructors & destructors
	explicit	AboutDialogPrivate(QWidget *parent);
				~AboutDialogPrivate(void);
};

//######################################################################################################################
#endif
