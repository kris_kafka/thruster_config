#pragma once
#if !defined(TIOCONFIGWRITE_LOG_H) && !defined(DOXYGEN_SKIP)
#define TIOCONFIGWRITE_LOG_H
//######################################################################################################################
/*! @file
@brief Declare the thruster I/O configuration write module loggers.
*/
//######################################################################################################################

#include "log.h"

//######################################################################################################################
// Loggers

LOG_SETUP_START("Thruster I/O Configuration Write Module");

// Base logger.
LOG_DEF_LOG(logger,							LOG_DL_INFO,	"TioConfigWrite");
// Create/destroy logger.
LOG_DEF_LOG(loggerCreate,					LOG_DL_INFO,	"TioConfigWrite.Create");
// Main logger.
LOG_DEF_LOG(loggerMain,						LOG_DL_INFO,	"TioConfigWrite.Main");
// Write maximum power command logger.
LOG_DEF_LOG(loggerMaxPowerCommand,			LOG_DL_INFO,	"TioConfigWrite.MaxPowerCommand");
// Write maximum power parameters logger.
LOG_DEF_LOG(loggerMaxPowerParameters,		LOG_DL_INFO,	"TioConfigWrite.MaxPowerParameters");
// Write maximum power reply logger.
LOG_DEF_LOG(loggerMaxPowerReply,			LOG_DL_INFO,	"TioConfigWrite.MaxPowerReply");
// Write motor ID command logger.
LOG_DEF_LOG(loggerMotorIdCommand,			LOG_DL_INFO,	"TioConfigWrite.MotorIdCommand");
// Write motor ID parameters logger.
LOG_DEF_LOG(loggerMotorIdParameters,		LOG_DL_INFO,	"TioConfigWrite.MotorIdParameters");
// Write motor ID reply logger.
LOG_DEF_LOG(loggerMotorIdReply,				LOG_DL_INFO,	"TioConfigWrite.MotorIdReply");
// Open/close logger.
LOG_DEF_LOG(loggerOpen,						LOG_DL_INFO,	"TioConfigWrite.Open");
// Write rotation command logger.
LOG_DEF_LOG(loggerRotationCommand,			LOG_DL_INFO,	"TioConfigWrite.RotationCommand");
// Write rotation parameters logger.
LOG_DEF_LOG(loggerRotationParameters,		LOG_DL_INFO,	"TioConfigWrite.RotationParameters");
// Write rotation reply logger.
LOG_DEF_LOG(loggerRotationReply,			LOG_DL_INFO,	"TioConfigWrite.RotationReply");
// Save settings command logger.
LOG_DEF_LOG(loggerSaveSettingsCommand,		LOG_DL_INFO,	"TioConfigWrite.SaveSettingsCommand");
// Save settings parameters logger.
LOG_DEF_LOG(loggerSaveSettingsParameters,	LOG_DL_INFO,	"TioConfigWrite.SaveSettingsParameters");
// Save settings reply logger.
LOG_DEF_LOG(loggerSaveSettingsReply,		LOG_DL_INFO,	"TioConfigWrite.SaveSettingsReply");

LOG_SETUP_STOP();

//######################################################################################################################
#endif
