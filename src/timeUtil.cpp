/*! @file
@brief Define the date/time utility module.
*/
//######################################################################################################################

#include "app_com.h"

#include <QtCore/QString>

#include "strUtil.h"
#include "timeUtil.h"
#include "timeUtil_log.h"

//######################################################################################################################
/*! @brief Populate a template.

The symbolic fields within @a pattern will be replaced with their corresponding values.

@return Formatted string.
*/
QString
TimeUtil::populate(
	QString const	&pattern,	//!<[in] Template.
	QDateTime const	&dateTime	//!<[in] Optional date/time. If omitted, use the current UTC date/time.
)
{
	LOG_Trace(loggerPopulate,
				QString("%1: pattern='%2', dateTime=%3").arg(__func__).arg(pattern).arg(dateTime.toString()));
	QString	answer = StrUtil::populate(pattern);
	answer = STR_REPLACE(answer, "!{Year}",			dateTime.toString("yy"));
	answer = STR_REPLACE(answer, "!{Year4}",		dateTime.toString("yyyy"));
	answer = STR_REPLACE(answer, "!{Month}",		dateTime.toString("M"));
	answer = STR_REPLACE(answer, "!{Month2}",		dateTime.toString("MM"));
	answer = STR_REPLACE(answer, "!{MonthShort}",	dateTime.toString("MMM"));
	answer = STR_REPLACE(answer, "!{MonthLong}",	dateTime.toString("MMMM"));
	answer = STR_REPLACE(answer, "!{Day}",			dateTime.toString("d"));
	answer = STR_REPLACE(answer, "!{Day2}",			dateTime.toString("dd"));
	answer = STR_REPLACE(answer, "!{DayShort}",		dateTime.toString("ddd"));
	answer = STR_REPLACE(answer, "!{DayLong}",		dateTime.toString("dddd"));
	answer = STR_REPLACE(answer, "!{Hour}",			dateTime.toString("h"));
	answer = STR_REPLACE(answer, "!{Hour2}",		dateTime.toString("hh"));
	answer = STR_REPLACE(answer, "!{Minute}",		dateTime.toString("m"));
	answer = STR_REPLACE(answer, "!{Minute2}",		dateTime.toString("mm"));
	answer = STR_REPLACE(answer, "!{Second}",		dateTime.toString("s"));
	answer = STR_REPLACE(answer, "!{Second2}",		dateTime.toString("ss"));
	answer = STR_REPLACE(answer, "!{Millisecond}",	dateTime.toString("z"));
	answer = STR_REPLACE(answer, "!{Millisecond3}",	dateTime.toString("zzz"));
	answer = STR_REPLACE(answer, "!{DateTime}",		dateTime.toString("yyyy/MM/dd hh:mm:ss.zzz"));
	answer = STR_REPLACE(answer, "!{Date}",			dateTime.toString("yyyy/MM/dd"));
	answer = STR_REPLACE(answer, "!{Time}",			dateTime.toString("hh:mm:ss.zzz"));
	return answer;
}
