#pragma once
#ifndef THRUSTERTASKEMULATED_H
#ifndef DOXYGEN_SKIP
#define THRUSTERTASKEMULATED_H
#endif
//######################################################################################################################
/*! @file
@brief Declare the thruster interface module background task.
*/
//######################################################################################################################

#include <QtCore/QElapsedTimer>
#include <QtCore/QObject>
#include <QtCore/QVector>

#include "iniFile.h"
#include "rpn.h"
#include "thrusterTaskBase.h"

//######################################################################################################################

class	QSettings;
class	QThread;
class	TioTestingMode;

//######################################################################################################################
/*! @brief The ThrusterPrivate class provides the implementation of the thruster interface module background task.
*/
class ThrusterTaskEmulated
:
public	ThrusterTaskBase
{
	Q_OBJECT
	Q_DISABLE_COPY(ThrusterTaskEmulated);

public:		// Constructors / Destructors
	explicit	ThrusterTaskEmulated(void);
				~ThrusterTaskEmulated(void);

public:		// Functions
	void				assign(QString const &name, double value);
	virtual bool		configurationRead(int nodeId, int &nodeIdRead, int &groupId, int &motorId, int &maximumPower,
											bool &rotationReverse, QString &serialNumber, QByteArray &csrBlob);
	virtual void		configurationWrite(int nodeId, int motorId, int maximumPower, bool rotationReverse,
											QByteArray const &csrBlob);
	double				evaluate(IniFile &file, IniKey const & grp, IniKey const &key, IniString const &def,
									bool &error);
	virtual void		idsChange(QString const &serialNumber, int oldNodeId, int nodeId, int groupId);
	virtual bool		scanForThrusters(void);
	virtual bool		serialPortCheck(QString const &portName);
	static QStringList	serialPortsAvailable(void);
	virtual void		testMode(int nodeId, int groupId, int motorId, int mode);
	Thruster::ThrusterErrors
						thrusterFindNodeId(int nodeId, int &index, QString &errorMessage);
	Thruster::ThrusterErrors
						thrusterFindSerialNumber(QString const &serialNumber, int &index, QString &errorMessage);
	void				thrusterLoad(bool force=false);
	void				thrusterSave(void);

public:		// Data
	Rpn					rpn;						//!< RPN evaluator.
	QElapsedTimer		testModeTimer;				//!< Test mode elapsed timer.
	QVector<int>		thrusterDelay;				//!< Thruster data - emulation delays.
	QVector<bool>		thrusterFindInScan;			//!< Thruster data - find in scan.
	QVector<int>		thrusterGroupId;			//!< Thruster data - group IDs.
	bool				thrusterIsValid;			//!< Has the thruster data been loaded?
	QVector<int>		thrusterMaximumPower;		//!< Thruster data - maximum power.
	QVector<int>		thrusterMotorId;			//!< Thruster data - motor IDs.
	QVector<int>		thrusterNodeId;				//!< Thruster data - node IDs.
	QVector<bool>		thrusterRotationReversed;	//!< Thruster data - rotation reversed.
	QVector<QString>	thrusterSerialNumber;		//!< Thruster data - serial numbers
};

//######################################################################################################################
#endif
