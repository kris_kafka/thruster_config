#pragma once
#if !defined(THRUSTERTASKBASE_LOG_H) && !defined(DOXYGEN_SKIP)
#define THRUSTERTASKBASE_LOG_H
//######################################################################################################################
/*! @file
@brief Declare the thruster interface background task base module loggers.
*/
//######################################################################################################################

#include "log.h"

//######################################################################################################################
// Loggers

LOG_SETUP_START("Thruster Interface Background Task Base Module");

// Top level module logger.
LOG_DEF_LOG(logger,			LOG_DL_INFO,	"ThrusterTaskBase");
// Object creation/destruction logger.
LOG_DEF_LOG(loggerCreate,	LOG_DL_INFO,	"ThrusterTaskBase.Create");

LOG_SETUP_STOP();

//######################################################################################################################
#endif
