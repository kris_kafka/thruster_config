#pragma once
#ifndef PORTSELECTIONDIALOGPRIVATE_H
#ifndef DOXYGEN_SKIP
#define PORTSELECTIONDIALOGPRIVATE_H
#endif
//######################################################################################################################
/*! @file
@brief Declare the implementation of the port selection dialog module.
*/
//######################################################################################################################

#include <QtCore/QString>

#include "ui_portSelectionDialog.h"

//######################################################################################################################

class	QCloseEvent;

//######################################################################################################################
/*! @brief The PortSelectionDialogPrivate class provides the implementation of the port selection dialog module.
*/
class PortSelectionDialogPrivate
:
	public	QDialog,
	public	Ui_PortSelectionDialog

{
	Q_OBJECT
	Q_DISABLE_COPY(PortSelectionDialogPrivate)

public:		// Constructors & destructors
	PortSelectionDialogPrivate(QString const &currentPort, QWidget *parent);
	~PortSelectionDialogPrivate(void);

public:		// Functions
	void			initializeAvailablePorts(void);
	QString			newPort(void);
	void			restoreWindowState(void);
	void			saveWindowState(void);
	Q_SLOT void		updateGuiState(void);

public:		// Data
	QString const	originalPort;	//!< Originally selected port.
};

//######################################################################################################################
#endif
