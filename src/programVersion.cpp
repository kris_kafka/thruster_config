//######################################################################################################################
/*! @file
@brief Define the application version information.
*/
//######################################################################################################################

#include "app_com.h"

#include "buildDateTime.h"
#include "buildVersion.h"
#include "programVersion.h"

//######################################################################################################################
/*! @hideinitializer @brief String version number.

@param logger	Logger object to create.
@param level	Default logging level in the configuraton file.
@param name		Name of logger in the configuration file.
*/
#if 0 != BUILD_NUM_REVISION
	#define BUILD_VERSION_STR		(BUILD_STR_MAJOR "." BUILD_STR_MINOR "." BUILD_STR_BUILD "." BUILD_STR_REVISION)
#else
	#define BUILD_VERSION_STR		(BUILD_STR_MAJOR "." BUILD_STR_MINOR "." BUILD_STR_BUILD)
#endif

//######################################################################################################################

//! Program version information.
software_version_t const	VersionInfomation::programVersion =
{
	BUILD_STR_YEAR "/" BUILD_STR_MONTH "/" BUILD_STR_DAY,
	BUILD_STR_HOUR ":" BUILD_STR_MINUTE ":" BUILD_STR_SECOND,
	BUILD_NUM_MAJOR, BUILD_NUM_MINOR, BUILD_NUM_BUILD,
	BUILD_VERSION_STR
};

//! How was the application built (debug or release).
char const	*VersionInfomation::buildType = BUILD_STR_TYPE;
