/*! @file
@brief Define the RPN formula evaluator utility module.
*/
//######################################################################################################################

#include "app_com.h"

#include <cmath>

#include <boost/math/constants/constants.hpp>

#include <QtCore/QtGlobal>
#include <QtCore/QHash>
#include <QtCore/QList>
#include <QtCore/QRegularExpression>
#include <QtCore/QStringList>

#include "rpn.h"
#include "rpn_log.h"

//######################################################################################################################
// Determine if a value meets a specific requirement.

//----------------------------------------------------------------------------------------------------------------------
/*! @hideinitializer @brief Determine if @a x is logically true (essentially not zero).

@param[in]	x	Value to check.

@return true if @a x is logically true (essentially not zero).
*/
#define IS_TRUE(x)				!qFuzzyIsNull(x)

//----------------------------------------------------------------------------------------------------------------------
/*! @hideinitializer @brief Determine if @a x is essentially zero.

@param[in]	x	Value to check.

@return true if @a x is essentially zero.
*/
#define IS_ZERO(x)				qFuzzyIsNull(x)

//======================================================================================================================
// Stack operations.

/*! @hideinitializer @brief Pop and return a value from the stack.
*/
#define STACK_POP()				stack.takeLast()

//----------------------------------------------------------------------------------------------------------------------
/*! @hideinitializer @brief Push a single value onto the stack.

@param[in]	x	value to push on the stack.
*/
#define STACK_PUSH(x)			stack.append(x)

//----------------------------------------------------------------------------------------------------------------------
/*! @hideinitializer @brief Push two values onto the stack.

@param[in]	y	first value to push on the stack.
@param[in]	x	second value to push on the stack.
*/
#define STACK_PUSH_2(y, x) \
		do { \
			STACK_PUSH(y); \
			STACK_PUSH(x); \
		} while (0)

//----------------------------------------------------------------------------------------------------------------------
/*! @hideinitializer @brief Push three values onto the stack.

@param[in]	z	first value to push on the stack.
@param[in]	y	second value to push on the stack.
@param[in]	x	third value to push on the stack.
*/
#define STACK_PUSH_3(z, y, x) \
		do { \
			STACK_PUSH(z); \
			STACK_PUSH(y); \
			STACK_PUSH(x); \
		} while (0)

//======================================================================================================================
// Initialize operation macros.

//----------------------------------------------------------------------------------------------------------------------
/*! @hideinitializer @brief Handle error conditions.

@param[in]	test	Determine if an error occured.
@param[in]	type	The type of error.
*/
#define ERROR_IF(test, type) \
		do { \
			if (test) { \
				d->lastError = QString("%1 at %2 %3").arg(type).arg(token).arg(rpn.join(" ")); \
				return false; \
			} \
		} while (0)

//----------------------------------------------------------------------------------------------------------------------
/*! @hideinitializer @brief Handle domain errors.

@param[in]	test	Determine if a domain error occured.
*/
#define DOMAIN_ERROR_IF(test)	ERROR_IF(test, "Domain error")

//----------------------------------------------------------------------------------------------------------------------
/*! @hideinitializer @brief Verify that there are a minimum values on the stack.

@param[in]	args_needed	The minimum number of values needed on the stack.
*/
#define CHECK_SIZE(args_needed) \
		do { \
			if (args_needed > stack.size()) { \
				d->lastError = QString("Not enough arguements for %1").arg(token); \
				return false; \
			} \
		} while (0)

//----------------------------------------------------------------------------------------------------------------------
/*! @hideinitializer @brief Initialize for a unary operation.

CAUTION: Must be invoked within a statement block because
it defines variable x and sets it to the value popped from stack.
*/
#define UNARY_INITIALIZE() \
		CHECK_SIZE(1); \
		double x = STACK_POP()

//----------------------------------------------------------------------------------------------------------------------
/*! @hideinitializer @brief Initialize for a binary operation.

CAUTION: Must be invoked within a statement block because
it defines variable x and y and sets them to the first and second values popped from stack.
*/
#define BINARY_INITIALIZE() \
		CHECK_SIZE(2); \
		double x = STACK_POP(); \
		double y = STACK_POP()

//----------------------------------------------------------------------------------------------------------------------
/*! @hideinitializer @brief Initialize for a ternary operation.

CAUTION: Must be invoked within a statement block because
it defines variable x, y, and z and sets them to the first, second, and thrid values popped from stack.
*/
#define TERNARY_INITIALIZE() \
		CHECK_SIZE(3); \
		double x = STACK_POP(); \
		double y = STACK_POP(); \
		double z = STACK_POP()

//======================================================================================================================
// Perform operations with single results.

//----------------------------------------------------------------------------------------------------------------------
/*! @hideinitializer @brief Perform a unary operation with a single result.

@param[in]	rx	The result of the operation.

CAUTION: Must be invoked within a statement block.
*/
#define UNARY_OPERATOR_1(rx) \
		UNARY_INITIALIZE(); \
		STACK_PUSH(rx)

//----------------------------------------------------------------------------------------------------------------------
/*! @hideinitializer @brief Perform a binary operation with a single result.

@param[in]	rx	The result of the operation.

CAUTION: Must be invoked within a statement block.
*/
#define BINARY_OPERATOR_1(rx) \
		BINARY_INITIALIZE(); \
		STACK_PUSH(rx)

//----------------------------------------------------------------------------------------------------------------------
/*! @hideinitializer @brief Perform a ternary operation with a single result.

@param[in]	rx	The result of the operation.

CAUTION: Must be invoked within a statement block.
*/
#define TERNARY_OPERATOR_1(rx) \
		TERNARY_INITIALIZE(); \
		STACK_PUSH(rx)

//======================================================================================================================
// Perform operations with two results.

//----------------------------------------------------------------------------------------------------------------------
/*! @hideinitializer @brief Perform a unary operation with a two results.

@param[in]	ry	first result to push on the stack.
@param[in]	rx	second result to push on the stack.

CAUTION: Must be invoked within a statement block.
*/
#define UNARY_OPERATOR_2(ry, rx) \
		UNARY_INITIALIZE(); \
		STACK_PUSH_2(ry, rx)

//----------------------------------------------------------------------------------------------------------------------
/*! @hideinitializer @brief Perform a binary operation with a two results.

@param[in]	ry	first result to push on the stack.
@param[in]	rx	second result to push on the stack.

CAUTION: Must be invoked within a statement block.
*/
#define BINARY_OPERATOR_2(ry, rx) \
		BINARY_INITIALIZE(); \
		STACK_PUSH_2(ry, rx)

//----------------------------------------------------------------------------------------------------------------------
/*! @hideinitializer @brief Perform a ternary operation with a two results.

@param[in]	ry	first result to push on the stack.
@param[in]	rx	second result to push on the stack.

CAUTION: Must be invoked within a statement block.
*/
#define TERNARY_OPERATOR_2(ry, rx) \
		TERNARY_INITIALIZE(); \
		STACK_PUSH_2(ry, rx)

//======================================================================================================================
// Perform operations with three results.

//----------------------------------------------------------------------------------------------------------------------
/*! @hideinitializer @brief Perform a unary operation with a three results.

@param[in]	rz	first result to push on the stack.
@param[in]	ry	second result to push on the stack.
@param[in]	rx	third result to push on the stack.

CAUTION: Must be invoked within a statement block.
*/
#define UNARY_OPERATOR_3(rz, ry, rx) \
		UNARY_INITIALIZE(); \
		STACK_PUSH_3(rz, ry, rx)

//----------------------------------------------------------------------------------------------------------------------
/*! @hideinitializer @brief Perform a binary operation with a three results.

@param[in]	rz	first result to push on the stack.
@param[in]	ry	second result to push on the stack.
@param[in]	rx	third result to push on the stack.

CAUTION: Must be invoked within a statement block.
*/
#define BINARY_OPERATOR_3(rz, ry, rx) \
		BINARY_INITIALIZE(); \
		STACK_PUSH_3(rz, ry, rx)

//----------------------------------------------------------------------------------------------------------------------
/*! @hideinitializer @brief Perform a ternary operation with a three results.

@param[in]	rz	first result to push on the stack.
@param[in]	ry	second result to push on the stack.
@param[in]	rx	third result to push on the stack.

CAUTION: Must be invoked within a statement block.
*/
#define TERNARY_OPERATOR_3(rz, ry, rx) \
		TERNARY_INITIALIZE(); \
		STACK_PUSH_3(rz, ry, rx)
 
//######################################################################################################################
/*! @brief Encapsulates the formula evaluator utility module.
*/
class RpnPrivate
{
public:		// Constructors & destructors
	RpnPrivate(void);
	~RpnPrivate(void);

public:		// Data
	QString					lastError;	//!< Error message from last formula evaluation.
	double					lastValue;	//!< Result value from last formula evaluation.
	QHash<QString, double>	symbols;	//!< Symbol table.
};

//######################################################################################################################
/*! @brief Create a Rpn Object.
*/
Rpn::Rpn(void)
:
	d	(Q_NULLPTR)
{
	LOG_Trace(loggerCreate, "Creating a Rpn object");
	//
	d.reset(new (std::nothrow) RpnPrivate);
	LOG_CHK_PTR_MSG(loggerCreate, d, "Unable to create RpnPrivate object");
}

//----------------------------------------------------------------------------------------------------------------------
/*! @brief Create a Rpn Object.
*/
RpnPrivate::RpnPrivate(void)
:
	lastError	(),
	lastValue	(0.0),
	symbols		()
{
	LOG_Trace(loggerCreate, "Creating a RpnPrivate object");
}

//======================================================================================================================
/*! @brief Destroy a Rpn object.
*/
Rpn::~Rpn(void)
{
	LOG_Trace(loggerCreate, "Destroying a Rpn object");
}

//----------------------------------------------------------------------------------------------------------------------
/*! @brief Destroy a RpnPrivate object.
*/
RpnPrivate::~RpnPrivate(void)
{
	LOG_Trace(loggerCreate, "Destroying a RpnPrivate object");
}

//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
/*! @brief Assign symbol @a name to @a value.

If no error occurs, lastError() will be cleared.

@return true if no errors occur.
*/
bool
Rpn::assignSymbol(
	QString const	&name,		//!<[in] Name of the symbol.
	double			value		//!<[in] Value of the symbol.
)
{
	LOG_Trace(loggerAssign, QString("Assigning '%1' to %2").arg(name).arg(value));
	d->symbols[name] = value;
	d->lastError.clear();
	return true;
}

//======================================================================================================================
/*! @brief Declare symbol @a name and assign it to @a value if it does not already exist.

If no error occurs, lastError() will be cleared.

@return true if no errors occur.
*/
bool
Rpn::declareSymbol(
	QString const	&name,		//!<[in] Name of the symbol.
	double			value		//!<[in] Value of the symbol.
)
{
	LOG_Trace(loggerAssign, QString("Declaring '%1' with a default of %2").arg(name).arg(value));
	if (!d->symbols.contains(name)) {
		d->symbols[name] = value;
	}
	d->lastError.clear();
	return true;
}

//======================================================================================================================
/*! @brief Evaluate a formula.

If an error occurs, lastValue() will be unchanged.
If no error occurs, lastError() will be cleared.

@return true if no errors occur.
*/
bool
Rpn::evaluate(
	QString const	&formula	//!<[in] RPN formula to evaluate.
)
{
	LOG_Trace(loggerEvaluate, QString("Evaluating '%1'").arg(formula));
	QList<double>	stack;
	QStringList		rpn = formula.split(QRegularExpression("\\s+"), QString::SkipEmptyParts);
	// Empty formulas are invalid.
	if (rpn.isEmpty()) {
		d->lastError = "Empty formula";
		return false;
	}
	//
	while (!rpn.isEmpty()) {
		// Get the next token.
		QString const	token = rpn.takeFirst();
		// Handle numbers.
		bool	okay = false;
		double	value = token.toDouble(&okay);
		if (okay) {									STACK_PUSH(value);
		// Handle constants.
		} else if (token == "1/2") {				STACK_PUSH(boost::math::constants::half<double>());
		} else if (token == "1/3") {				STACK_PUSH(boost::math::constants::third<double>());
		} else if (token == "2/3") {				STACK_PUSH(boost::math::constants::twothirds<double>());
		} else if (token == "e") {					STACK_PUSH(boost::math::constants::e<double>());
		} else if (token == "euler") {				STACK_PUSH(boost::math::constants::euler<double>());
		} else if (token == "log_2 ") {				STACK_PUSH(boost::math::constants::ln_two <double>());
		} else if (token == "pi") {					STACK_PUSH(boost::math::constants::pi<double>());
		} else if (token == "root_2") 	{			STACK_PUSH(boost::math::constants::root_two<double>());
		} else if (token == "root_2*pi ") {			STACK_PUSH(boost::math::constants::root_two_pi <double>());
		} else if (token == "root_pi") {			STACK_PUSH(boost::math::constants::root_pi<double>());
		} else if (token == "root_pi/2") {			STACK_PUSH(boost::math::constants::root_half_pi<double>());
		// Handle operators.
		} else if (token == "!=") {					BINARY_OPERATOR_1(!qFuzzyCompare(y + 1.0, x + 1.0));
		} else if (token == "*") {					BINARY_OPERATOR_1(y * x);
		} else if (token == "+") {					BINARY_OPERATOR_1(y + x);
		} else if (token == "-") {					BINARY_OPERATOR_1(y - x);
		} else if (token == "/") {					BINARY_INITIALIZE();
													DOMAIN_ERROR_IF(IS_ZERO(x));
													STACK_PUSH(y / x);
		} else if (token == "<") {					BINARY_OPERATOR_1(y < x);
		} else if (token == "<=") {					BINARY_OPERATOR_1(y <= x);
		} else if (token == "=") {					BINARY_OPERATOR_1(qFuzzyCompare(y + 1.0, x + 1.0));
		} else if (token == ">") {					BINARY_OPERATOR_1(y > x);
		} else if (token == ">=") {					BINARY_OPERATOR_1(y >= x);
		} else if (token == "\\") {					BINARY_INITIALIZE();
													DOMAIN_ERROR_IF(IS_ZERO(x));
													STACK_PUSH(trunc(y / x));
		} else if (token == "_rot") {				TERNARY_OPERATOR_3(x, z, y);
		} else if (token == "abs") {				UNARY_OPERATOR_1(qAbs(x));
		} else if (token == "and") {				BINARY_OPERATOR_1(IS_TRUE(y) && IS_TRUE(x));
		} else if (token == "arccos") {				UNARY_INITIALIZE();
													DOMAIN_ERROR_IF(x < -1.0 || 1.0 < x);
													STACK_PUSH(acos(x));
		} else if (token == "arcsin") {				UNARY_INITIALIZE();
													DOMAIN_ERROR_IF(x < -1.0 || 1.0 < x);
													STACK_PUSH(asin(x));
		} else if (token == "arctan") {				UNARY_OPERATOR_1(atan(x));
		} else if (token == "ceil") {				UNARY_OPERATOR_1(ceil(x));
		} else if (token == "chs") {				UNARY_OPERATOR_1(-x);
		} else if (token == "cos") {				UNARY_OPERATOR_1(cos(x));
		} else if (token == "drop") {				UNARY_INITIALIZE();
													Q_UNUSED(x);
		} else if (token == "dup") {				UNARY_OPERATOR_2(x, x);
		} else if (token == "exp") {				UNARY_OPERATOR_1(exp(x));
		} else if (token == "exp10") {				UNARY_OPERATOR_1(pow(10.0, x));
		} else if (token == "floor") {				UNARY_OPERATOR_1(floor(x));
		} else if (token == "if") {					TERNARY_OPERATOR_1(IS_TRUE(x) ? z : y);
		} else if (token == "limit") {				TERNARY_OPERATOR_1(qBound(qMin(x, y), z, qMax(x, y)));
		} else if (token == "ln") {					UNARY_INITIALIZE();
													DOMAIN_ERROR_IF(x < 0.0);
													STACK_PUSH(log(x));
		} else if (token == "log") {				UNARY_INITIALIZE();
													DOMAIN_ERROR_IF(x < 0.0);
													STACK_PUSH(log10(x));
		} else if (token == "max") {				BINARY_OPERATOR_1(qMax(y,x));
		} else if (token == "min") {				BINARY_OPERATOR_1(qMin(y, x));
		} else if (token == "mod") {				BINARY_INITIALIZE();
													DOMAIN_ERROR_IF(IS_ZERO(x));
													STACK_PUSH(fmod(y, x));
		} else if (token == "nip") {				BINARY_OPERATOR_1(x);
													Q_UNUSED(y);
		} else if (token == "not") {				UNARY_OPERATOR_1(IS_ZERO(x));
		} else if (token == "or") {					BINARY_OPERATOR_1(IS_TRUE(y) || IS_TRUE(x));
		} else if (token == "over") {				BINARY_OPERATOR_3(y, x, y);
		} else if (token == "pow") {				BINARY_INITIALIZE();
													DOMAIN_ERROR_IF(IS_ZERO(x) && IS_ZERO(y));
													STACK_PUSH(pow(y, x));
		} else if (token == "rand") {				BINARY_INITIALIZE();
													double	min = qMin(x, y);
													double	max = qMax(x, y);
													double	rnd = (double)qrand() / (double)RAND_MAX;
													STACK_PUSH(min + rnd * (max - min));
		} else if (token == "remainder") {			BINARY_INITIALIZE();
													DOMAIN_ERROR_IF(IS_ZERO(x));
													STACK_PUSH(remainder(y, x));
		} else if (token == "remquo") {				BINARY_INITIALIZE();
													int		quo;
													double	rem = remquo(y, x, &quo);
													STACK_PUSH_2(rem, quo);
		} else if (token == "root") {				BINARY_INITIALIZE();
													DOMAIN_ERROR_IF(IS_ZERO(x));
													STACK_PUSH(pow(y, 1.0 / x));
		} else if (token == "rot") {				TERNARY_OPERATOR_3(y, x, z);
		} else if (token == "round") {				UNARY_OPERATOR_1(round(x));
		} else if (token == "sign") {				UNARY_OPERATOR_1(IS_ZERO(x) ? 0.0 : (x < 0.0) ? -1.0 : 1.0);
		} else if (token == "sin") {				UNARY_OPERATOR_1(sin(x));
		} else if (token == "sqrt") {				UNARY_INITIALIZE();
													DOMAIN_ERROR_IF(x < 0.0);
													STACK_PUSH(sqrt(x));
		} else if (token == "swap") {				BINARY_OPERATOR_2(x, y);
		} else if (token == "tan") {				UNARY_OPERATOR_1(tan(x));
		} else if (token == "trunc") {				UNARY_OPERATOR_1(trunc(x));
		} else if (token == "xor") {				BINARY_OPERATOR_1(IS_TRUE(y) != IS_TRUE(x));
		// Handle symbol references.
		} else if (d->symbols.contains(token)) {	STACK_PUSH(d->symbols.value(token));
		// Everything else is an error.
		} else {
			d->lastError = QString("Unknown token: %1 %2").arg(token).arg(rpn.join(" "));
			return false;
		}
	}
	// Must be only one value left on the stack.
	if (stack.isEmpty()) {
		d->lastError = "No result";
		return false;
	} else if (1 != stack.size()) {
		d->lastError = "Multiple results";
		return false;
	}
	// Save the result.
	d->lastError.clear();
	d->lastValue = STACK_POP();
	return true;
}

//======================================================================================================================
/*! @brief Qeury if @a name is a defined symbol.

@return true if @a name is a defined symbol.
*/
bool
Rpn::isSymbol(
	QString const	&name		//!<[in] Name of the symbol to check.
)
{
	LOG_Trace(loggerQuery, QString("Query if '%1' is a defined symbol").arg(name));
	return d->symbols.contains(name);
}

//======================================================================================================================
/*! @brief Query the last error message.

@return Drror message associated with last formula evaluation or an empty string if no error occured.
*/
QString
Rpn::lastError(void)
{
	LOG_Trace(loggerQuery, QString("Last error: '%1'").arg(d->lastError));
	return d->lastError;
}

//======================================================================================================================
/*! @brief Query the last evaluation value.

@return Value of last formula evaluation.
*/
double
Rpn::lastValue(void)
{
	LOG_Trace(loggerQuery, QString("Last value: '%1'").arg(d->lastValue));
	return d->lastValue;
}
