#pragma once
#if !defined(TIOTESTINGMODE_LOG_H) && !defined(DOXYGEN_SKIP)
#define TIOTESTINGMODE_LOG_H
//######################################################################################################################
/*! @file
@brief Declare the thruster I/O testing mode module loggers.
*/
//######################################################################################################################

#include "log.h"

//######################################################################################################################
// Loggers

LOG_SETUP_START("Thruster I/O Testing Mode Module");

// Base logger.
LOG_DEF_LOG(logger,							LOG_DL_INFO,	"TioTestingMode");
// Create/destroy logger.
LOG_DEF_LOG(loggerCreate,					LOG_DL_INFO,	"TioTestingMode.Create");
// Main logger.
LOG_DEF_LOG(loggerMain,						LOG_DL_INFO,	"TioTestingMode.Main");
// Open/close logger.
LOG_DEF_LOG(loggerOpen,						LOG_DL_INFO,	"TioTestingMode.Open");
// Testing mode propulsion command logger.
LOG_DEF_LOG(loggerPropulsionCommand,		LOG_DL_INFO,	"TioTestingMode.PropulsionCommand");
// Testing mode propulsion parameters logger.
LOG_DEF_LOG(loggerPropulsionParameters,		LOG_DL_INFO,	"TioTestingMode.PropulsionParameters");
// Testing mode propulsion reply logger.
LOG_DEF_LOG(loggerPropulsionReply,			LOG_DL_INFO,	"TioTestingMode.PropulsionReply");
// Testing mode speed check command logger.
LOG_DEF_LOG(loggerSpeedCheckCommand,		LOG_DL_INFO,	"TioTestingMode.SpeedCheckCommand");
// Testing mode speed check parameters logger.
LOG_DEF_LOG(loggerSpeedCheckParameters,		LOG_DL_INFO,	"TioTestingMode.SpeedCheckParameters");
// Testing mode speed check reply logger.
LOG_DEF_LOG(loggerSpeedCheckReply,			LOG_DL_INFO,	"TioTestingMode.SpeedCheckReply");
// Testing mode speed set command logger.
LOG_DEF_LOG(loggerSpeedSetCommand,			LOG_DL_INFO,	"TioTestingMode.SpeedSetCommand");
// Testing mode speed set parameters logger.
LOG_DEF_LOG(loggerSpeedSetParameters,		LOG_DL_INFO,	"TioTestingMode.SpeedSetParameters");
// Testing mode speed set reply logger.
LOG_DEF_LOG(loggerSpeedSetReply,			LOG_DL_INFO,	"TioTestingMode.SpeedSetReply");
// Testing mode status command logger.
LOG_DEF_LOG(loggerStatusCommand,			LOG_DL_INFO,	"TioTestingMode.StatusCommand");
// Testing mode status parameter logger.
LOG_DEF_LOG(loggerStatusParameters,			LOG_DL_INFO,	"TioTestingMode.StatusParameters");
// Testing mode status reply logger.
LOG_DEF_LOG(loggerStatusReply,				LOG_DL_INFO,	"TioTestingMode.StatusReply");

LOG_SETUP_STOP();

//######################################################################################################################
#endif
