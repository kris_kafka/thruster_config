/*! @file
@brief Define the thruster I/O module.
*/
//######################################################################################################################

#include "app_com.h"

#include <cstddef>
#include <string>

#include <QtCore/QSettings>

#include "videoray/applib/std_device_types.h"

#include "configGio.h"
#include "configThruster.h"
#include "thruster.h"
#include "tioIdsChange.h"
#include "tioIdsChange_log.h"
#include "tioImpl.h"
#include "tioParms.h"

//######################################################################################################################
// Local classes.

class	TioImplIdsChange;
class	TioParmsIdsChange;

//######################################################################################################################
// Local functions

static void		idsChangeReply(TioParms *biop, Protocol_VRCSR_Packet const &packet, int deviceTypeSize);

//######################################################################################################################
/*! \brief Read Configuration CSR data implementation.
*/
class TioImplIdsChange
:
	public	TioImpl
{
public:		// Constructors & destructors
				TioImplIdsChange(std::string &portName, int nodeId, bool const volatile &halt);
	virtual		~TioImplIdsChange(void);

public:		// Functions
	Thruster::Error		idsChange(QString const &serialNumber, int newNodeId, int newGroupId);
};

//======================================================================================================================
/*! \brief Read Configuration ID data I/O parameters.
*/
class TioParmsIdsChange
:
	public	TioParms
{
public:		// Constructors & destructors
	TioParmsIdsChange(QString const &serialNumber_, int newNodeId_, int newGroupId_);

public:		// Data
	int				newGroupId;		//!< New group ID of the thruster.
	int				newNodeId;		//!< New node ID of the thruster.
	QString const	serialNumber;	//!< Serial number of the thruster.
};

//######################################################################################################################
/*! @brief Create a TioIdsChange object.
*/
TioIdsChange::TioIdsChange(
	QString const		&serialPort,	//!<[in] Serial port name.
	int					nodeId,			//!<[in] Node ID.
	bool const volatile	&halt			//!<[in] Halt I/O.
)
:
	TioBase		(serialPort, nodeId, halt),
	connection	(Q_NULLPTR)
{
	LOG_Trace(loggerCreate,
				QString("Creating TioIdsChange object: serialPort='%1', nodeId=%2").arg(serialPort).arg(nodeId));
}

//======================================================================================================================
/*! @brief Destroy a TioIdsChange object.
*/
TioIdsChange::~TioIdsChange(void)
{
	LOG_Trace(loggerCreate,
				QString("Destroying TioIdsChange object: serialPort='%1', nodeId=%2").arg(serialPort).arg(nodeId));
}

//======================================================================================================================
/*! @brief Create a TioImplIdsChange object.
*/
TioImplIdsChange::TioImplIdsChange(
	std::string			&portName,	//!<[in] Serial port name.
	int					nodeId,		//!<[in] Node ID.
	bool const volatile	&halt		//!<[in] Halt I/O.
)
:
	TioImpl	(portName, nodeId, halt)
{
	LOG_Trace(loggerCreate,
				QString("Creating TioImplIdsChange object: serialPort='%1', nodeId=%2").arg(serialPort).arg(nodeId));
}

//======================================================================================================================
/*! @brief Destroy a TioImplIdsChange object.
*/
TioImplIdsChange::~TioImplIdsChange(void)
{
	LOG_Trace(loggerCreate,
				QString("Destroying TioImplIdsChange object: serialPort='%1', nodeId=%2").arg(serialPort).arg(nodeId));
}

//======================================================================================================================
/*! @brief Create a TioParmsIdsChange object.
*/
TioParmsIdsChange::TioParmsIdsChange(
	QString const	&serialNumber_,	//!<[in] Serial number of the thruster.
	int				newNodeId_,		//!<[in] New node ID of the thruster.
	int				newGroupId_		//!<[in] New group ID of the thruster.
)
:
	TioParms		(idsChangeReply, "Change IDs"),
	newGroupId		(newGroupId_),
	newNodeId		(newNodeId_),
	serialNumber	(serialNumber_)
{
	LOG_Trace(loggerParameters,
				QString("Creating TioParmsIdsChange object: serialNumber='%1', newNodeId=%2, newGroupId=%3")
						.arg(serialNumber).arg(newNodeId).arg(newGroupId));
	//
	IniFile	iniFile;
	initialize(iniFile, INI_GROUP(CfgGioSetIds));
	//
	replyValidAddress	= ADDR_UTILITY;
	// ToDo: Correct this to handle extended types.
	replyValidLength	= 0;
	replyValidFlags		= RESPONSE_ACK;
	replyValidNodeId	= newNodeId;
	//
	NodeId_t	nodeId = newNodeId;
	GroupId_t	groupId = newGroupId | groupIdMapMask;		// Add group ID flag.
	int	dataSize = iniFile.INI_GET(CfgThrusterSetIdsPayloadSize)
						+ serialNumber.size() + 1 + sizeof(nodeId) + sizeof(groupId);
	commandSize = protocol_vrcsr_calc_packet_size(dataSize, false, PROTOCOL_VRCSR);
	ByteP	packetBuffer = new (std::nothrow) Byte[commandSize];
	LOG_CHK_PTR_MSG(loggerParameters, packetBuffer, "Unable to create TioParmsIdsChange packetBuffer");
	commandBuffer.reset(packetBuffer);
	int	cmdSize = protocol_vrcsr_request_set_id(nodeId,
												groupId,
												serialNumber.toLatin1().constData(),
												commandBuffer.get(),
												commandSize,
												PROTOCOL_VRCSR);
	if (0 > cmdSize) {
		isIoNeeded = false;
		errorCode = Thruster::TEC_PacektError;
		LOG_Error(loggerParameters,
					QString("Error building command packet: %1 (%2)").arg(cmdSize).arg(commandSize));
	}
}

//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
/*! @brief Close the serial port.
*/
void
TioIdsChange::doClose(void)
{
	LOG_Trace(loggerOpen,
				QString("Closing connection: serialPort='%1', nodeId=%2").arg(serialPort).arg(nodeId));
	// Done if already closed.
	if (!connection.isNull()) {
		connection.reset();
	}
}

//======================================================================================================================
/*! @brief Open the serial port.

@return true if no errors occur else false.
*/
bool
TioIdsChange::doOpen(void)
{
	LOG_Trace(loggerOpen,
				QString("Opening connection: serialPort='%1', nodeId=%2").arg(serialPort).arg(nodeId));
	// Done if already open.
	if (!connection.isNull()) {
		return true;
	}
	std::string portName = serialPort.toStdString();
	connection.reset(new TioImplIdsChange(portName, nodeId, halt));
	return !connection.isNull() && connection->isOpen();
}

//======================================================================================================================
/*! @brief Change the IDs.

@return the error code (Thruster::TEC_NoError if no errors).
*/
Thruster::Error
TioIdsChange::idsChange(
	QString const	&serialNumber,	//!< Serial number of the thruster.
	int				newNodeId,		//!< New node ID for the thruster.
	int				newGroupId		//!< New group ID for the thruster.
)
{
	LOG_Trace(loggerMain,
				QString("Change IDs: serialNumber=%1, oldNodeId=%2, newNodeId=%3, newGroupId=%4")
						.arg(serialNumber).arg(nodeId).arg(newNodeId).arg(newGroupId));
	LOG_ASSERT_MSG(loggerMain, connection, "Not open");
	return connection->idsChange(serialNumber, newNodeId, newGroupId);
}

//======================================================================================================================
/*! @brief Get the configuration ID data.

@return the error code (Thruster::TEC_NoError if no errors).
*/
Thruster::Error
TioImplIdsChange::idsChange(
	QString const	&serialNumber,	//!< Serial number of the thruster.
	int				newNodeId,		//!< New node ID for the thruster.
	int				newGroupId		//!< New group ID for the thruster.
)
{
	LOG_Trace(loggerCommand, "Change IDs");
	TioParmsIdsChange		ciop(serialNumber, newNodeId, newGroupId);
	return genericIo(&ciop);
}

//======================================================================================================================
/*! @brief Process a set IDs reply packet.
*/
void
idsChangeReply(
	TioParms					*biop,			//!<[in,out] I/O parameters.
	Protocol_VRCSR_Packet const	&packet,		//!<[in] Reply packet.
	int							deviceTypeSize	//!<[in] Size of device type data prefix.
)
{
	LOG_Trace(loggerReply, "Received change IDs reply");
	Q_UNUSED(packet);
	Q_UNUSED(deviceTypeSize);
	
	TioParmsIdsChange	*iop = dynamic_cast<TioParmsIdsChange *>(biop);
	LOG_ASSERT_MSG(loggerReply, iop, "idsChangeReply: iop is invalid");
	// Update the status.
	iop->errorCode = Thruster::TEC_NoError;
	iop->isReplySeen = true;
}
