/*! @file
@brief Define the thruster interface module.
*/
//######################################################################################################################

#include "app_com.h"

#include <algorithm>
#include <string>

#include <QtCore/QMutex>
#include <QtCore/QMutexLocker>
#include <QtCore/QSettings>
#include <QtCore/QSysInfo>
#include <QtCore/QThread>

#include "serialPortUtil.h"
#include "thruster.h"
#include "thruster_log.h"
#include "thrusterPrivate.h"
#include "thrusterTask.h"

//######################################################################################################################
// Module global variables.

//! Implementation object.
static ThrusterPrivate	*d					= Q_NULLPTR;

//! Implementation object critical section mutex.
static QMutex			dCriticalSection;

//! Implementation object reference count.
static QAtomicInt		dReferenceCount		= 0;

//######################################################################################################################
/*! @brief Create a Thruster object.
*/
Thruster::Thruster(void)
{
	LOG_Trace(loggerCreate, "Creating a Thruster object");
	// Only a single instance is allowed.
	{
		QMutexLocker	locker(&dCriticalSection);
		if (!d)
		{
			d = new (std::nothrow) ThrusterPrivate();
			LOG_CHK_PTR_MSG(logger, d, "Unable to create ThrusterPrivate object");
		}
		++dReferenceCount;
	}
	// Forward the signals.
	LOG_CONNECT(loggerCreate,	d->bgThread.data(),	&QThread::started,
								d,					&ThrusterPrivate::moduleStarted);
	LOG_CONNECT(loggerCreate,	d->bgThread.data(),	&QThread::finished,
								d,					&ThrusterPrivate::moduleStopped);

	LOG_CONNECT(loggerCreate,	d,					&ThrusterPrivate::taskStarted,
								this,				&Thruster::taskStarted);
	LOG_CONNECT(loggerCreate,	d,					&ThrusterPrivate::taskStopped,
								this,				&Thruster::taskStopped);

	LOG_CONNECT(loggerCreate,	d->bgTask.data(),	&ThrusterTask::configurationReadNews,
								this,				&Thruster::configurationReadNews);
	LOG_CONNECT(loggerCreate,	d->bgTask.data(),	&ThrusterTask::configurationReadStart,
								this,				&Thruster::configurationReadStart);
	LOG_CONNECT(loggerCreate,	d->bgTask.data(),	&ThrusterTask::configurationReadStop,
								this,				&Thruster::configurationReadStop);
	LOG_CONNECT(loggerCreate,	d->bgTask.data(),	&ThrusterTask::configurationWriteStart,
								this,				&Thruster::configurationWriteStart);
	LOG_CONNECT(loggerCreate,	d->bgTask.data(),	&ThrusterTask::configurationWriteStop,
								this,				&Thruster::configurationWriteStop);
	LOG_CONNECT(loggerCreate,	d->bgTask.data(),	&ThrusterTask::idsChangeStart,
								this,				&Thruster::idsChangeStart);
	LOG_CONNECT(loggerCreate,	d->bgTask.data(),	&ThrusterTask::idsChangeStop,
								this,				&Thruster::idsChangeStop);
	LOG_CONNECT_Q(loggerCreate,	d->bgTask.data(),	&ThrusterTask::scanForThrustersNews,
								this,				&Thruster::scanForThrustersNews);
	LOG_CONNECT_Q(loggerCreate,	d->bgTask.data(),	&ThrusterTask::scanForThrustersStart,
								this,				&Thruster::scanForThrustersStart);
	LOG_CONNECT_Q(loggerCreate,	d->bgTask.data(),	&ThrusterTask::scanForThrustersStop,
								this,				&Thruster::scanForThrustersStop);
	LOG_CONNECT(loggerCreate,	d->bgTask.data(),	&ThrusterTask::serialPortChangeStart,
								this,				&Thruster::serialPortChangeStart);
	LOG_CONNECT(loggerCreate,	d->bgTask.data(),	&ThrusterTask::serialPortChangeStop,
								this,				&Thruster::serialPortChangeStop);
	LOG_CONNECT(loggerCreate,	d->bgTask.data(),	&ThrusterTask::testingModeNews,
								this,				&Thruster::testingModeNews);
	LOG_CONNECT(loggerCreate,	d->bgTask.data(),	&ThrusterTask::testingModeStart,
								this,				&Thruster::testingModeStart);
	LOG_CONNECT(loggerCreate,	d->bgTask.data(),	&ThrusterTask::testingModeStop,
								this,				&Thruster::testingModeStop);
}

//----------------------------------------------------------------------------------------------------------------------
/*! @brief Create a ThrusterPrivate object.
*/
ThrusterPrivate::ThrusterPrivate(void)
:
	bgTask				(Q_NULLPTR),
	bgThread			(Q_NULLPTR),
	isOpen				(false),
	lastErrorMessage	()
{
	LOG_Trace(loggerCreate, "Creating a ThrusterPrivate object");
	//
	bgThread.reset(new (std::nothrow) QThread(this));
	LOG_CHK_PTR_MSG(logger, bgThread, "Unable to create ThrusterPrivate QThread object");
	bgTask.reset(new (std::nothrow) ThrusterTask);
	LOG_CHK_PTR_MSG(logger, bgTask, "Unable to create ThrusterTask object");
	//
	bgThread->setObjectName("ThrusterThread");
	bgTask->setObjectName("ThrusterTask");
	// Process the background task's signals in the background thread.
	bgTask->moveToThread(bgThread.data());
	//
	LOG_CONNECT(loggerCreate, bgTask.data(), &ThrusterTask::quitThread, bgThread.data(), &QThread::quit);
}

//======================================================================================================================
/*! @brief Destroy a Thruster object.
*/
Thruster::~Thruster(void)
{
	LOG_Trace(loggerCreate, "Destroying a Thruster object");
	// Done if not last instance.
	QMutexLocker	locker(&dCriticalSection);
	//
	if (0 < --dReferenceCount)
	{
		return;
	}
	// Cleanup.
	moduleClose();
	FREE_POINTER(d);
}

//----------------------------------------------------------------------------------------------------------------------
/*! @brief Destroy a ThrusterPrivate object.
*/
ThrusterPrivate::~ThrusterPrivate(void)
{
	LOG_Trace(loggerCreate, "Destroying a ThrusterPrivate object");
}

//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
/*! @brief Read the configuration of @a nodeId.

It it an error to call this function if the thruster interface module is busy.

@return true if okay.
*/
bool
Thruster::configurationRead(
	int	nodeId	//!<[in] ID of node to read configuration from.
)
{
	LOG_Trace(loggerConfigurationRead, QString("Read configuration of thruster: nodeId=%1").arg(nodeId));
	if (!isIdle()) {
		d->lastErrorMessage = QString("Attempting to read thruster configuration when not idle (%1)").arg(+state());
		LOG_Error(loggerConfigurationRead, d->lastErrorMessage);
		return false;
	}
	if (!QMetaObject::invokeMethod(d->bgTask.data(), "configurationRead", Qt::QueuedConnection, Q_ARG(int, nodeId))) {
		d->lastErrorMessage = "Unable to invoke 'configurationRead' in the thruster interface background task";
		LOG_Error(loggerConfigurationRead, d->lastErrorMessage);
		return false;
	}
	d->lastErrorMessage.clear();
	return true;
}

//======================================================================================================================
/*! @brief Write the configuration of @a nodeId.

It it an error to call this function if the thruster interface module is busy.

@return true if okay.
*/
bool
Thruster::configurationWrite(
	int					nodeId,				//!<[in] ID of node to write configuraton to.
	int					motorId,			//!<[in] New motor ID.
	int					maximumPower,		//!<[in] New maximum power level.
	bool				rotationReverse,	//!<[in] New rotation is reversed.
	QByteArray const	&csrBlob			//!<[in] CSR blob.
)
{
	LOG_Trace(loggerConfigurationWrite,
				QString("Write configuration to thruster: nodeId=%1, motorID=%2, maximumPower=%3, rotationReverse=%4")
						.arg(nodeId).arg(motorId).arg(maximumPower).arg(rotationReverse));
	if (!isIdle()) {
		d->lastErrorMessage = QString("Attempting to write thruster configuration when not idle (%1)").arg(+state());
		LOG_Error(loggerConfigurationWrite, d->lastErrorMessage);
		return false;
	}
	if (!QMetaObject::invokeMethod(d->bgTask.data(),
									"configurationWrite",
									Qt::QueuedConnection,
									Q_ARG(int,			nodeId),
									Q_ARG(int,			motorId),
									Q_ARG(int,			maximumPower),
									Q_ARG(bool,			rotationReverse),
									Q_ARG(QByteArray,	csrBlob))) {
		d->lastErrorMessage = "Unable to invoke configurationWrite in the thruster interface background task";
		LOG_Error(loggerConfigurationWrite, d->lastErrorMessage);
		return false;
	}
	d->lastErrorMessage.clear();
	return true;
}

//======================================================================================================================
/*! @brief Get a human readable message for @a errorCode.

@return The result human readable error message.
*/
QString
Thruster::errorMessage(
	int	errorCode	//!<[in] The error code.
)
{
	switch (errorCode) {
	case TEC_NoError:				return "No error";
	case TEC_NotIdle:				return "Not idle";
	case TEC_WrongState:			return "Wrong state";
	case TEC_NodeIdMismatch:		return "Node ID mismatch";
	case TEC_NodeIdNotFound:		return "Node ID not found";
	case TEC_Duplicates:			return "Duplicates encountered";
	case TEC_Stopping:				return "Background task is stopping";
	case TEC_RpnFormula:			return "RPN formula error";
	case TEC_OpenError:				return "Open error";
	case TEC_PortError:				return "Port error";
	case TEC_PacektSize:			return "Packet size error";
	case TEC_PacektError:			return "Packet error";
	case TEC_RetryLimit:			return "Retry limit error";
	case TEC_NodeIdDifferent:		return "Node ID different in IP packet";
	case TEC_DeviceType:			return "Incorrect device type";
	case TEC_DeviceTypeDifferent:	return "Node ID different in IP packet";
	case TEC_WriteError:			return "Write error";
	//
	default:						return QString("Unknown error (%1)").arg(errorCode);
	}
}

//======================================================================================================================
/*! @brief Query if the thruster interface module is busy.

@return true if the thruster interface module is busy.
*/
bool
Thruster::isBusy(void)
{
	return d->bgTask->isBusy();
}

//======================================================================================================================
/*! @brief Query if there are duplicate IDs.

@return true if there are duplicate IDs.
*/
bool
Thruster::isDuplicateIds(void)
{
	return d->bgTask->isDuplicateIds();
}

//======================================================================================================================
/*! @brief Query if there are duplicate serial numbers.

@return true if there are duplicate serial numbers.
*/
bool
Thruster::isDuplicateSerials(void)
{
	return d->bgTask->isDuplicateSerials();
}

//======================================================================================================================
/*! @brief Query if the thruster interface module is idle.

@return true if the thruster interface module is idle.
*/
bool
Thruster::isIdle(void)
{
	return d->bgTask->isIdle();
}

//======================================================================================================================
/*! @brief Query if @a port is a valid serial port.

@return true if @a port is a. valid serial port.
*/
bool
Thruster::isSerialPortValid(
	QString const	&port	//!<[in] Serial port name.
)
{
	LOG_Trace(loggerQuery, QString("Query if '%1' is a valid serial port").arg(port));
	// Port names are case sensitive except under Microsof Windows.
	Qt::CaseSensitivity	cs = QSysInfo::WV_None != QSysInfo::WindowsVersion ? Qt::CaseInsensitive : Qt::CaseSensitive;
	// Ensure the given port is valid.
	return serialPortsAvailable().contains(SerialPortUtil::portNameFromSystemLocation(port), cs);
}

//======================================================================================================================
/*! @brief Query if the thruster interface module is in testing mode.

@return true if the thruster interface module is in testing mode.
*/
bool
Thruster::isTestingMode(void)
{
	bool	answer = Thruster::State::TestingMode == state();
	LOG_Trace(loggerQuery, QString("Query if testing mode: %1").arg(answer));
	return answer;
}

//======================================================================================================================
/*! @brief Query last error message.

@return the last error in human readable format.
*/
QString
Thruster::lastErrorMessage(void)
{
	LOG_Trace(loggerQuery, QString("Query last error message: '%1'").arg(d->lastErrorMessage));
	return d->lastErrorMessage;
}

//======================================================================================================================
/*! @brief Close the thruster interface module.
*/
void
Thruster::moduleClose(void)
{
	LOG_Trace(loggerModule, "Close the thruster interface module");
	// Done if alread closed.
	if (!d->isOpen) {
		LOG_Debug(loggerModule, "Thruster interface module already closed");
	}
	// Terminate the background thread if it is still running.
	if (d->bgThread->isRunning())
	{
		LOG_Error(loggerModule, "Thruster interface background task did not stop normally.");
		d->bgThread->terminate();
	}
	else
	{
		LOG_Trace(loggerModule, "Thruster interface background task has stopped normally.");
	}
	d->isOpen = false;
}

//======================================================================================================================
/*! @brief Query if the thruster interface module is running.

@return true if the thruster interface module is running.
*/
bool
Thruster::moduleIsOpen(void)
{
	LOG_Trace(loggerQuery, "Query if the thruster interface module is open");
	return d->isOpen;
}

//======================================================================================================================
/*! @brief Query if the thruster interface module is running.

@return true if the thruster interface module is running.
*/
bool
Thruster::moduleIsRunning(void)
{
	LOG_Trace(loggerQuery, "Query if the thruster interface module is running");
	
	return d->bgThread->isRunning();
}

//======================================================================================================================
/*! @brief Open the thruster interface module module.

@return true if okay.
*/
bool
Thruster::moduleOpen(void)
{
	LOG_Trace(loggerModule, "Open the thruster interface module");
	//
	// Done if already open.
	if (d->isOpen)
	{
		LOG_Warn(loggerModule, "The thruster interface module is already open");
		return true;
	}
	//
	d->isOpen = true;
	return true;
}

//======================================================================================================================
/*! @brief Start the thruster interface module module.

@return true if okay.
*/
bool
Thruster::moduleStart(void)
{
	LOG_Trace(loggerModule, "Start the thruster interface module");
	// Done if already started.
	if (d->bgThread->isRunning())
	{
		LOG_Warn(loggerModule, "The thruster interface module is already started");
		return true;
	}
	// Start the background task.
	LOG_Trace(logger, "Starting the background thruster interface task.");
	d->bgThread->start();
	return true;
}

//======================================================================================================================
/*! @brief Handle the background task starting.
*/
void
ThrusterPrivate::moduleStarted(void)
{
	LOG_Trace(loggerModule, "The thruster interface module background task has started");
	//
	d->bgTask->moduleStarted();
	//
	LOG_Debug(loggerSignals, "Emit taskStarted"); \
	Q_EMIT taskStarted();
}

//======================================================================================================================
/*! @brief Begin stopping the thruster interface module module.
*/
void
Thruster::moduleStop(void)
{
	LOG_Trace(loggerModule, "Stopping the thruster interface module");
	// Do nothing if not running.
	if (!d->bgThread->isRunning())
	{
		return;
	}
	// Stop the background task.
	d->bgTask->moduleStop();
	if (isIdle()) {
		LOG_Trace(loggerModule, "Quiting the background task");
		d->bgThread->quit();
	}
}

//======================================================================================================================
/*! @brief Handle the background task stopping.
*/
void
ThrusterPrivate::moduleStopped(void)
{
	LOG_Trace(loggerSignals, "The thruster interface module background task has stopped");
	//
	d->bgTask->moduleStopped();
	//
	LOG_Debug(loggerSignals, "Emit taskStopped");
	Q_EMIT taskStopped();
}

//======================================================================================================================
/*! @brief Change the node ID of @a currentNode.

It it an error to call this function if the thruster interface module is busy.

@return true if okay.
*/
bool
Thruster::idsChange(
	QString const	&serialNumber,	//!<[in] Serial number of the thruster.
	int				oldNodeId,		//!<[in] Old node ID of the thruster.
	int				newNodeId,		//!<[in] New node ID for the thruster.
	int				newGroupId		//!<[in] New node ID for the thruster.
)
{
	LOG_Trace(loggerIdsChange,
				QString("Changing the IDs of serialNumber=%1 (oldNodeId=%2) to: node=%3, group=%4")
						.arg(serialNumber).arg(oldNodeId).arg(newNodeId).arg(newGroupId));
	if (!isIdle()) {
		d->lastErrorMessage = QString("Attempting to change the IDs when not idle (%1)").arg(+state());
		LOG_Error(loggerIdsChange, d->lastErrorMessage);
		return false;
	}
	if (!QMetaObject::invokeMethod(d->bgTask.data(),
									"idsChange",
									Qt::QueuedConnection,
									Q_ARG(QString,	serialNumber),
									Q_ARG(int,		oldNodeId),
									Q_ARG(int,		newNodeId),
									Q_ARG(int,		newGroupId))) {
		d->lastErrorMessage = "Unable to invoke idsChange in the thruster interface background task";
		LOG_Error(loggerIdsChange, d->lastErrorMessage);
		return false;
	}
	d->lastErrorMessage.clear();
	return true;
}

//======================================================================================================================
/*! @brief Scan for thrusters.

It it an error to call this function if the thruster interface module is busy.

@return true if okay.
*/
bool
Thruster::scanForThrusters(void)
{
	LOG_Trace(loggerScanForThrusters, "Start a scan for thrusters");
	if (!isIdle()) {
		d->lastErrorMessage = QString("Attempting to scan for thrusters when not idle (%1)").arg(+state());
		LOG_Error(loggerScanForThrusters, d->lastErrorMessage);
		return false;
	}
	if (!QMetaObject::invokeMethod(d->bgTask.data(), "scanForThrusters", Qt::QueuedConnection)) {
		d->lastErrorMessage = "Unable to invoke scanForThrusters in the thruster interface background task";
		LOG_Error(loggerScanForThrusters, d->lastErrorMessage);
		return false;
	}
	d->lastErrorMessage.clear();
	return true;
}

//======================================================================================================================
/*! @brief Change the active serial port to @a port.

It it an error to call this function if the thruster interface module is busy.

@return true if okay.
*/
bool
Thruster::serialPortChange(
	QString const	&port	//!<[in] New serial port.
)
{
	LOG_Trace(loggerSerialPortChange, QString("Change serial port to '%1'").arg(port));
	// Ensure the given port is valid.
	if (!isSerialPortValid(port)) {
		d->lastErrorMessage = QString("Invalid serial port: '%1'").arg(port);
		LOG_Error(loggerSerialPortChange, d->lastErrorMessage);
		return false;
	}
	// Special case if not open or running yet.
	if (!d->isOpen || !d->bgThread->isRunning()) {
		if (!d->bgTask->serialPortSet(port)) {
			d->lastErrorMessage = QString("Unable to initialize the serial port to '%1'").arg(port);
			LOG_Error(loggerSerialPortChange, d->lastErrorMessage);
			return false;
		}
		LOG_Debug(loggerSerialPortChange,
					QString("Initialized the serial port to '%1'").arg(port));
		d->lastErrorMessage.clear();
		return true;
	}
	if (!isIdle()) {
		d->lastErrorMessage = QString("Attempting to change the serial port when not idle (%1)").arg(+state());
		LOG_Error(loggerSerialPortChange, d->lastErrorMessage);
		return false;
	}
	if (!QMetaObject::invokeMethod(d->bgTask.data(), "serialPortChange", Qt::QueuedConnection, Q_ARG(QString, port))) {
		d->lastErrorMessage = "Unable to invoke serialPort in the thruster interface background task";
		LOG_Error(loggerSerialPortChange, d->lastErrorMessage);
		return false;
	}
	d->lastErrorMessage.clear();
	return true;
}

//======================================================================================================================
/*! @brief Query the available serial ports.

We do no error checking on the list of emulated serial ports in the configuration file.

@return list of available serial ports.
*/
QStringList
Thruster::serialPortsAvailable(void)
{
	LOG_Trace(loggerQuerySerialPorts, "Query for the available serial ports");
	// Return the available serial ports if not emulating them.
	return ThrusterTask::serialPortsAvailable();
}

//======================================================================================================================
/*! @brief Query the state the thruster interface module is in.

@return the state the thruster interface module is in.
*/
Thruster::State
Thruster::state(void)
{
	LOG_Trace(loggerQuery, "Query the thruster interface state");
	return d->bgTask->state();
}

//======================================================================================================================
/*! @brief Start testing mode.

It it an error to call this function if the thruster interface module is busy.

@return true if okay.
*/
bool
Thruster::testingModeBegin(
	int	nodeId,		//!<[in] Node ID of thruster to be tested.
	int	groupId,	//!<[in] Group ID of thruster to be tested.
	int	motorId,	//!<[in] Motor ID of thruster to be tested.
	int	mode		//!<[in] Testing mode.
)
{
	LOG_Trace(loggerTestingMode, QString("Starting testing mode: nodeId=%1").arg(nodeId));
	if (!isIdle()) {
		d->lastErrorMessage = QString("Attempting to start testing mode when not idle (%1)").arg(+state());
		LOG_Error(loggerTestingMode, d->lastErrorMessage);
		return false;
	}
	if (!QMetaObject::invokeMethod(d->bgTask.data(),
									"testingModeBegin",
									Qt::QueuedConnection,
									Q_ARG(int, nodeId),
									Q_ARG(int, groupId),
									Q_ARG(int, motorId),
									Q_ARG(int, mode))) {
		d->lastErrorMessage = "Unable to invoke testingModeBegin in the thruster interface background task";
		LOG_Error(loggerTestingMode, d->lastErrorMessage);
		return false;
	}
	d->lastErrorMessage.clear();
	return true;
}

//======================================================================================================================
/*! @brief Stop testing mode.

@return true if okay.
*/
bool
Thruster::testingModeEnd(void)
{
	LOG_Debug(loggerTestingMode, "Stopping testing mode");
	if (!isTestingMode()){
		d->lastErrorMessage = "Attempt to stop testing mode when not in testing mode";
		LOG_Error(loggerTestingMode, d->lastErrorMessage);
		return false;
	}
	d->bgTask->testingModeEnd();
	d->lastErrorMessage.clear();
	return true;
}

//======================================================================================================================
/*! @brief Query information about a testing mode.

@return true if the mode is valid.
*/
bool
Thruster::testingModeInfo(
	int				mode,			//!<[in]  Test mode to query.
	ThrusterIdType	&idType,		//!<[out] Destination node ID.
	bool			&isCheck,		//!<[out] Is the speed checked.
	bool			&isPropulsion,	//!<[out] Propulsion command vs CSR commands
	bool			&isReply,		//!<[out] Is a reply expected.
	QString			&name			//!<[out] Name of the mode.
)
{
	static bool const	Y = true;
	static bool const	N = false;
	switch (mode) {
	case Thruster::TM_SinglePropultion:
		name	= tr("Node, Propulsion", "Thruster");
		idType	= TIT_NodeId;		isPropulsion = Y;	isReply = Y;	isCheck = N;
		break;
	case Thruster::TM_SingleCsr:
		name	= tr("Node, CSR", "Thruster");
		idType	= TIT_NodeId;		isPropulsion = N;	isReply = N;	isCheck = N;
		break;
	case Thruster::TM_SingleCsrCheck:
		name	= tr("Node, CSR (verified)", "Thruster");
		idType	= TIT_NodeId;		isPropulsion = N;	isReply = Y;	isCheck = N;
		break;
	case Thruster::TM_GroupPropulsion:
		name	= tr("Group, Propulsion", "Thruster");
		idType	= TIT_GroupId;		isPropulsion = Y;	isReply = N;	isCheck = N;
		break;
	case Thruster::TM_GroupCsr:
		name	= tr("Group, CSR", "Thruster");
		idType	= TIT_GroupId;		isPropulsion = N;	isReply = N;	isCheck = N;
		break;
	case Thruster::TM_GroupCsrCheck:
		name	= tr("Group, CSR  (verified)", "Thruster");
		idType	= TIT_GroupId;		isPropulsion = N;	isReply = N;	isCheck = Y;
		break;
	case Thruster::TM_BroadcastPropultion:
		name	= tr("All, Propulsion", "Thruster");
		idType	= TIT_Broadcast;	isPropulsion = Y;	isReply = N;	isCheck = N;
		break;
	case Thruster::TM_BroadcastCsr:
		name	= tr("All, CSR ", "Thruster");
		idType	= TIT_Broadcast;	isPropulsion = N;	isReply = N;	isCheck = N;
		break;
	case Thruster::TM_BroadcastCsrCheck:
		name	= tr("All, CSR  (verified)", "Thruster");
		idType	= TIT_Broadcast;	isPropulsion = N;	isReply = N;	isCheck = Y;
		break;
	default:
		LOG_Trace(loggerQuery, QString("Query information about testing mode: mode=%1: INVALID"));
		return false;
	}
	LOG_Trace(loggerQuery,
				QString("Query information about testing mode: mode=%1: name=%2, isCheck=%3, isPropulsion=%4, "
								"isReply=%5, idType=%6")
						.arg(mode).arg(name).arg(isCheck).arg(isPropulsion).arg(isReply).arg(idType));
	return true;
}

//======================================================================================================================
/*! @brief Get the testing mode thruster speed.

@return current testing mode thruster speed.
*/
double
Thruster::testingModeSpeed(void)
{
	LOG_Trace(loggerQuery, "Query the thruster testing mode speed");
	return d->bgTask->testingModeSpeed();
}

//======================================================================================================================
/*! @brief Change the testing mode thruster speed to @a speed.

@return true if okay.
*/
bool
Thruster::testingModeSpeed(
	double	speed	//!<[in] New thruster speed.
)
{
	LOG_Trace(loggerTestingMode, QString("Changing thruster testing mode speed to %1").arg(speed));
	d->bgTask->testingModeSpeed(speed);
	return true;
}
