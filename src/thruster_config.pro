AppName = thruster_config

# Disable the combined building of release and debug versions
# and handle debug/release specific settings.
CONFIG -= debug_and_release
CONFIG -= debug_and_release_target
MY_TARGET_DEBUG_SUFFIX		=
MY_VIDEORAY_DEBUG_SUFFIX	=
MY_APACHE_DEBUG_SUFFIX		=
MY_BOOST_DEBUG_SUFFIX		=
CONFIG (debug, debug|release) {
	CONFIG						-= release
	DEFINES						+= IS_RELEASE_BUILD=0
	#MY_TARGET_DEBUG_SUFFIX		=
	MY_VIDEORAY_DEBUG_SUFFIX	= d
	win* {
	MY_APACHE_DEBUG_SUFFIX		= d
	MY_BOOST_DEBUG_SUFFIX		= -gd
	}
} else {
	CONFIG						-= debug
	CONFIG						+= release
	DEFINES						+= IS_RELEASE_BUILD=1
}

# Qt modules needed.
QT	+= core gui serialport widgets

# Maximize compiler warning messages.
CONFIG	+= warn_on

# The following define makes your compiler emit warnings if you use
# any feature of Qt which as been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000	# disables all the APIs deprecated before Qt 6.0.0

# Disable Qt keywords.
DEFINES += QT_NO_KEYWORDS

# Using static library for log4cxx.
DEFINES += LOG4CXX_STATIC

# Do not have boost generate its auto link library pragmas.
#DEFINES += BOOST_ALL_NO_LIB

# Force compiler warnings to be errors.
*-g++*|*llvm*|*clang* {
	QMAKE_CXXFLAGS	+= -Werror
}
*-msvc* {
	QMAKE_CXXFLAGS	+= /WX
}

# Library type and prefix.
*-g++*|*llvm*|*clang* {
	MY_LIB_TYPE		= .a
	MY_LIB_PREFIX	= lib
}
*-msvc* {
	MY_LIB_TYPE		= .lib
	MY_LIB_PREFIX	=
}

#Define source and build directories.
MY_SOURCE_DIR	= $${_PRO_FILE_PWD_}
MY_BUILD_DIR	= $$OUT_PWD

TARGET		= $${AppName}
TEMPLATE	= app

MY_BIN_DIR		= $${MY_SOURCE_DIR}/../bin
MY_DOC_DIR		= $${MY_BUILD_DIR}/doc
MY_INCLUDE_DIR	= $${MY_BUILD_DIR}/inc

defineReplace(myEnvVal) {
	envVarName		= $$1
	defaultValue	= $$2
	envVarValue		= $$getenv($${envVarName})
	isEmpty(envVarValue) {
		return($${defaultValue})
	}
	return($${envVarValue})
}

# Top level project directory.
win* {
	MY_PROJECTS_DIR	= $$myEnvVal(ProjectsDir, $$(UserProfile)/Projects)
} else {
	MY_PROJECTS_DIR	= $$myEnvVal(ProjectsDir, $$(HOME)/Projects)
}

# Apache files.
win* {
	MY_APACHE_DIR	= $$myEnvVal(ApacheDir, $${MY_PROJECTS_DIR})
	MY_LOG4CXX_DIR	= $$myEnvVal(Log4cxxDir, $${MY_APACHE_DIR})
} else {
	MY_APACHE_DIR	= $$myEnvVal(ApacheDir, /usr/local/apr)
	MY_LOG4CXX_DIR	= $$myEnvVal(Log4cxxDir, /usr/local)
}

MY_APACHE_INC_DIR	= $${MY_APACHE_DIR}/inc
MY_APACHE_LIB_DIR	= $${MY_APACHE_DIR}/lib
MY_LOG4CXX_INC_DIR	= $${MY_LOG4CXX_DIR}/inc
MY_LOG4CXX_LIB_DIR	= $${MY_LOG4CXX_DIR}/lib

MY_APACHE_LIBS		= \
	$${MY_APACHE_LIB_DIR}/$${MY_LIB_PREFIX}apr-1$${MY_APACHE_DEBUG_SUFFIX}$${MY_LIB_TYPE} \
	$${MY_APACHE_LIB_DIR}/$${MY_LIB_PREFIX}aprutil-1$${MY_APACHE_DEBUG_SUFFIX}$${MY_LIB_TYPE}

win*{
	MY_APACHE_LIBS	+= $${MY_APACHE_LIB_DIR}/$${MY_LIB_PREFIX}xml$${MY_LIB_TYPE}
} else {
	MY_APACHE_LIBS	+= /usr/lib/x86_64-linux-gnu/$${MY_LIB_PREFIX}expat$${MY_LIB_TYPE}
}

MY_LOG4CXX_LIBS		= $${MY_LOG4CXX_LIB_DIR}/$${MY_LIB_PREFIX}log4cxx$${MY_APACHE_DEBUG_SUFFIX}$${MY_LIB_TYPE}

# Boost files.
win* {
	mingw {
		MY_BOOST_INC_DIR	= $$myEnvVal(BoostIncDir, /mingw32/include)
		MY_BOOST_LIB_DIR	= $$myEnvVal(BoostLibDir, /mingw32/lib)
		MY_BOOST_LIB_SUFFIX	= $$myEnvVal(BoostLibSuffix, -mt)
	} else {
		MY_BOOST_VERSION	= $$myEnvVal(BoostVersion, 1_65_1)
		MY_BOOST_INC_DIR	= $$myEnvVal(BoostIncDir, $$(SystemDrive)/local/boost/$${MY_BOOST_VERSION})
		MY_BOOST_LIB_DIR	= $$myEnvVal(BoostLibDir, $${MY_BOOST_INC_DIR}/lib32-msvc-14.0)
		MY_BOOST_LIB_SUFFIX	= $$myEnvVal(BoostLibSuffix, -vc140-mt$${MY_BOOST_DEBUG_SUFFIX}-$${MY_BOOST_VERSION})
	}
} else {
	MY_BOOST_INC_DIR		= $$myEnvVal(BoostIncDir, /usr/local/include)
	MY_BOOST_LIB_DIR		= $$myEnvVal(BoostLibDir, /usr/local/lib)
	MY_BOOST_LIB_SUFFIX		= $$myEnvVal(BoostLibSuffix, )
}

msvc {
	MY_BOOST_LIBS	+=	-L$${MY_BOOST_LIB_DIR}
} else {
	MY_BOOST_LIBS	+= \
		$${MY_BOOST_LIB_DIR}/$${MY_LIB_PREFIX}boost_program_options$${MY_BOOST_LIB_SUFFIX}$${MY_LIB_TYPE} \
		$${MY_BOOST_LIB_DIR}/$${MY_LIB_PREFIX}boost_system$${MY_BOOST_LIB_SUFFIX}$${MY_LIB_TYPE} \
		$${MY_BOOST_LIB_DIR}/$${MY_LIB_PREFIX}boost_thread$${MY_BOOST_LIB_SUFFIX}$${MY_LIB_TYPE}
}

# O/S files.
win*:!mingw {
	MY_OS_LIBS		= \
		$${MY_SDK_DIR}/$${MY_LIB_PREFIX}ws2_32$${MY_LIB_TYPE} \
		$${MY_SDK_DIR}/AdvAPI32$${MY_LIB_TYPE} \
		$${MY_SDK_DIR}/odbc32$${MY_LIB_TYPE} \
		$${MY_SDK_DIR}/Rpcrt4$${MY_LIB_TYPE}
}

# Python interpreter (version 3).
win*:!mingw {
	MY_PYTHON3	= $$myEnvVal(Python3Pgm, python)
} else {
	MY_PYTHON3	= $$myEnvVal(Python3Pgm, python3)
}

# Python scripts.
MY_BUILD_DATE_TIME	= $$absolute_path(build_date_time.py, $${MY_BIN_DIR})
MY_MAKE_OLD			= $$absolute_path(mytouch.py, $${MY_BIN_DIR})
MY_MYDOXYGEN		= $$absolute_path(mydoxygen.py, $${MY_BIN_DIR})
MY_UPDATE_HEADER	= $$absolute_path(update_header.py, $${MY_BIN_DIR})

# Windows SDK files.
win* {
	mingw {
		MY_SDK_DIR	= $$myEnvVal(SdkLibDir, /mingw32/i686-w64-mingw32/lib/x86)
	} else {
		MY_SDK_DIR	= $$myEnvVal(SdkLibDir, "$${MY_PGMS_X86}/Windows Kits/8.1/Lib/winv6.3/um/x86")
	}
} else:macx {
	unset(MY_SDK_DIR)
} else {
	unset(MY_SDK_DIR)
}

# VideoRay files.
MY_VR_DIR			= $$myEnvVal(VrDir, $${MY_PROJECTS_DIR})
MY_VR_INC_DIR		= $${MY_VR_DIR}/include
MY_VR_LIB_DIR		= $${MY_VR_DIR}/lib/videoray
MY_VR_LIBS			= \
	$${MY_VR_LIB_DIR}/$${MY_LIB_PREFIX}applib$${MY_VIDEORAY_DEBUG_SUFFIX}$${MY_LIB_TYPE} \
	$${MY_VR_LIB_DIR}/$${MY_LIB_PREFIX}hostlib$${MY_VIDEORAY_DEBUG_SUFFIX}$${MY_LIB_TYPE}

# Define special files.
MY_DATETIME			= $$absolute_path(buildDateTime.h, $${MY_INCLUDE_DIR})
MY_DOXYFILE			= $$absolute_path(doxyfile, $${MY_SOURCE_DIR})
MY_DOX_TARGET		= $$absolute_path(.dummy, $${MY_DOC_DIR})
MY_RC_FILE			= $$absolute_path($${AppName}.rc, $${MY_SOURCE_DIR})
MY_INIFILE_ALL		= $$absolute_path(iniFile_all.h, $${MY_INCLUDE_DIR})
MY_INIFILE_TMP		= $$absolute_path(iniFile_all.tmp, $${MY_INCLUDE_DIR})
MY_LOG_ALL			= $$absolute_path(log_all.h, $${MY_INCLUDE_DIR})
MY_LOG_TMP			= $$absolute_path(log_all.tmp, $${MY_INCLUDE_DIR})

# Define file groups.
ALL_INI_HDR_FILES	= $${MY_SOURCE_DIR}/config*.h
ALL_LOG_HDR_FILES	= $${MY_SOURCE_DIR}/*_log.h
ALL_RESOURCE_FILES	= $$files($${MY_SOURCE_DIR}/resources/*)

DOX_FILES = \
	$${MY_SOURCE_DIR}/build.dox \
	$${MY_SOURCE_DIR}/configuration.dox \
	$${MY_SOURCE_DIR}/rpn.dox \
	$${MY_SOURCE_DIR}/symbols.dox \
	$${MY_SOURCE_DIR}/usage.dox

INCLUDEPATH += \
	$${MY_INCLUDE_DIR} \
	$${MY_BOOST_INC_DIR} \
	$${MY_LOG4CXX_INC_DIR} \
	$${MY_APACHE_INC_DIR} \
	$${MY_VR_INC_DIR}

# Header files.
HEADERS = \
	aboutDialog.h \
	aboutDialog_log.h \
	aboutDialogPrivate.h \
	app_com.h \
	buildNames.h \
	buildVersion.h \
	changeIdsDialog.h \
	changeIdsDialog_log.h \
	changeIdsDialogPrivate.h \
	configCsr.h \
	configEmulation.h \
	configEnum.h \
	configGio.h \
	configMisc.h \
	configOptions.h \
	configState.h \
	configThruster.h \
	configValidators.h \
	csrBlob.h \
	csrBlob_log.h \
	csrInfo.h \
	csrInfo_log.h \
	emptyIntValidator.h \
	iniFile.h \
	iniFile_log.h \
	log.h \
	main.h \
	main_log.h \
	mainWindow.h \
	mainWindow_log.h \
	mainWindowPrivate.h \
	options.h \
	options_log.h \
	packetUtil.h \
	portSelectionDialog.h \
	portSelectionDialog_log.h \
	portSelectionDialogPrivate.h \
	programVersion.h \
	rpn.h \
	rpn_log.h \
	serialPortUtil.h \
	serialPortUtil_log.h \
	strUtil.h \
	strUtil_log.h \
	testDialog.h \
	testDialog_log.h \
	testDialogPrivate.h \
	thruster.h \
	thruster_log.h \
	thrusterPrivate.h \
	thrusterTask.h \
	thrusterTask_log.h \
	thrusterTaskBase.h \
	thrusterTaskBase_log.h \
	thrusterTaskEmulated.h \
	thrusterTaskEmulated_log.h \
	thrusterTaskReal.h \
	thrusterTaskReal_log.h \
	timeUtil.h \
	timeUtil_log.h \
	tioBase.h \
	tioBase_log.h \
	tioConfigRead.h \
	tioConfigRead_log.h \
	tioConfigWrite.h \
	tioConfigWrite_log.h \
	tioEnum.h \
	tioEnum_log.h \
	tioIdsChange.h \
	tioIdsChange_log.h \
	tioTestingMode.h \
	tioTestingMode_log.h \
	tioImpl.h \
	tioParms.h

# Source files.
SOURCES = \
	aboutDialog.cpp \
	changeIdsDialog.cpp \
	csrBlob.cpp \
	csrInfo.cpp \
	emptyIntValidator.cpp \
	iniFile.cpp \
	iniFile_setup.cpp \
	log.cpp \
	log_setup.cpp \
	main.cpp \
	mainWindow.cpp \
	options.cpp \
	packetUtil.cpp \
	portSelectionDialog.cpp \
	rpn.cpp \
	serialPortUtil.cpp \
	strUtil.cpp \
	testDialog.cpp \
	thruster.cpp \
	thrusterTask.cpp \
	thrusterTaskBase.cpp \
	thrusterTaskEmulated.cpp \
	thrusterTaskReal.cpp \
	timeUtil.cpp \
	tioBase.cpp \
	tioConfigRead.cpp \
	tioConfigWrite.cpp \
	tioEnum.cpp \
	tioIdsChange.cpp \
	tioTestingMode.cpp

# Compile the program version information after all other source files have been compiled.
SOURCES += programVersion.cpp

# Windows and dialogs boxes.
FORMS = \
	changeIdsDialog.ui \
	mainWindow.ui \
	portSelectionDialog.ui \
	testDialog.ui

# Libraries.
LIBS += \
	$${MY_VR_LIBS} \
	$${MY_LOG4CXX_LIBS} \
	$${MY_APACHE_LIBS} \
	$${MY_BOOST_LIBS} \
	$${MY_OS_LIBS}

# Resource files.
RESOURCES = \
	$${AppName}.qrc

# Source files with full paths.
MY_FORMS		=
MY_HEADERS		=
MY_RESOURCES	=
MY_SOURCES		=
for(src, $${FORMS})		MY_FORMS		+= $${MY_SOURCE_DIR}/$${src}
for(src, $${HEADERS})	MY_HEADERS		+= $${MY_SOURCE_DIR}/$${src}
for(src, $${RESOURCES})	MY_RESOURCES	+= $${MY_SOURCE_DIR}/$${src}
for(src, $${SOURCES})	MY_SOURCES		+= $${MY_SOURCE_DIR}/$${src}

# Debugging messages.
#message(--------------------------------------------------------------------------------)
#win* {
#	mingw{
#		message(Building for Windows - MinGW)
#	} else {
#		message(Building for Windows - MSVC)
#	}
#} else:macx {
#	message(Building for Mac OS)
#} else {
#	message(Building for Others)
#}
#message("MakeSpec:		$${QMAKESPEC}")
#message("MY_PROJECTS_DIR:	$${MY_PROJECTS_DIR}")
#message("MY_APACHE_DIR:	$${MY_APACHE_DIR}")
#message("MY_LOG4CXX_DIR:	$${MY_LOG4CXX_DIR}")
#message("MY_BOOST_INC_DIR:	$${MY_BOOST_INC_DIR}")
#message("MY_BOOST_LIB_DIR:	$${MY_BOOST_LIB_DIR}")
#message("MY_VR_DIR:		$${MY_VR_DIR}")
#message("QT:		$${QT}")
#message()
#message("DEFINES:	$${DEFINES}")
#message()
#message("CONFIG:	$$unique(CONFIG)")
#message()
#message(INCLUDEPATH:)
#message("	$${INCLUDEPATH}")
#message()
#message(LIBS:)
#message("	$${LIBS}")
#message(--------------------------------------------------------------------------------)

# Generation of the "buildDateTime.h" file in the build destination directory.
DateTimeTarget.target	= $${MY_DATETIME}
DateTimeTarget.depends	= \
	$${ALL_RESOURCE_FILES} \
	$${MY_BUILD_DATE_TIME} \
	$${MY_FORMS} \
	$${MY_HEADERS} \
	$${MY_MAKE_OLD} \
	$${MY_RC_FILE} \
	$${MY_RESOURCES} \
	$${MY_SOURCES} \
	$${MY_UPDATE_HEADER} \
	$${_PRO_FILE_}
DateTimeTarget.commands	= $${MY_PYTHON3} $${MY_BUILD_DATE_TIME} $${MY_DATETIME}
QMAKE_EXTRA_TARGETS		+= DateTimeTarget
PRE_TARGETDEPS			+= $${MY_DATETIME}
HEADERS					+= $$system($${MY_PYTHON3} $${MY_MAKE_OLD} $${MY_DATETIME})

DateTimeClean.commands	= -rm $${MY_DATETIME}
clean.depends			+= DateTimeClean
QMAKE_EXTRA_TARGETS		+= DateTimeClean

#Generation of iniFile_all.h in the build destination directory.
AllIniTarget.target			= $${MY_INIFILE_ALL}
AllIniTarget.depends		= $${MY_UPDATE_HEADER} $${ALL_INI_HDR_FILES}
AllIniTarget.clean_commands	= rm -f $${MY_INIFILE_ALL}
AllIniTarget.commands		= $${MY_PYTHON3} $${MY_UPDATE_HEADER} \
										$${MY_INIFILE_ALL} \
										$${MY_INIFILE_TMP} \
										$${ALL_INI_HDR_FILES}
QMAKE_EXTRA_TARGETS			+= AllIniTarget
PRE_TARGETDEPS				+= $${MY_INIFILE_ALL}
HEADERS						+= $$system($${MY_PYTHON3} $${MY_MAKE_OLD} $${MY_INIFILE_ALL})

AllIniClean.commands	= -rm $${MY_INIFILE_ALL}
clean.depends			+= AllIniClean
QMAKE_EXTRA_TARGETS		+= AllIniClean

#Generation of log_all.h in the build destination directory.
AllLogTarget.target		= $${MY_LOG_ALL}
AllLogTarget.depends	= $${MY_UPDATE_HEADER} $${ALL_LOG_HDR_FILES}
AllLogTarget.commands	= $${MY_PYTHON3} $${MY_UPDATE_HEADER} \
									$${MY_LOG_ALL} \
									$${MY_LOG_TMP} \
									$${ALL_LOG_HDR_FILES}
QMAKE_EXTRA_TARGETS		+= AllLogTarget
PRE_TARGETDEPS			+= $${MY_LOG_ALL}
HEADERS					+= $$system($${MY_PYTHON3} $${MY_MAKE_OLD} $${MY_LOG_ALL})

AllLogClean.commands	= -rm $${MY_LOG_ALL}
clean.depends			+= AllLogClean
QMAKE_EXTRA_TARGETS		+= AllLogClean

# Version information.
win32 {
	mingw {
		# Compile the Windows RC file using the GNU Binutils resource compiler.
		MY_RES_FILE			= $${MY_BUILD_DIR}/win_res.o
		ResTarget.target	= $${MY_RES_FILE}
		ResTarget.depends	= $${MY_DATETIME} $${MY_RC_FILE}
		ResTarget.commands	= windres \
									-i $${MY_RC_FILE} \
									-o $${MY_RES_FILE} \
									-I $${_PRO_FILE_PWD_} \
									-I $${MY_BUILD_DIR} \
									$(DEFINES)
		QMAKE_EXTRA_TARGETS	+= ResTarget
		POST_TARGETDEPS		+= $${MY_RES_FILE}
		QMAKE_CLEAN			+= $${MY_RES_FILE}
		LIBS				+= $${MY_RES_FILE}
	} else {
		# Compile the Windows RC file using the GNU Binutils resource compiler.
		MY_RES_FILE			= $${MY_BUILD_DIR}/$${AppName}.res.
		ResTarget.target	= $${MY_RES_FILE}
		ResTarget.depends	= $${MY_DATETIME} $${MY_RC_FILE}
		ResTarget.commands	= rc \
									/fo $${MY_RES_FILE} \
									/i $$shell_path($${_PRO_FILE_PWD_}) \
									/i $$shell_path($${MY_INCLUDE_DIR}) \
									$$replace($(DEFINES), -D, /d) \
									$${MY_RC_FILE}
		QMAKE_EXTRA_TARGETS	+= ResTarget
		POST_TARGETDEPS		+= $${MY_RES_FILE}
		QMAKE_CLEAN			+= $${MY_RES_FILE}
		RES_FILE			= $${MY_RES_FILE}
	}
} else {
	###################################################################################################################
	# ToDo: Fix this!
	VERSION = 1.0.1
	###################################################################################################################
}

# Generation of the documentation files.
DocTarget.target	= $${MY_DOX_TARGET}
DocTarget.depends	= \
	$${MY_DOXYFILE} \
	$${DOX_FILES} \
	$${_PRO_FILE_} \
	$${MY_HEADERS} \
	$${MY_SOURCES} \
	$${MY_MYDOXYGEN}
DocTarget.commands	= $${MY_PYTHON3} $${MY_MYDOXYGEN} $${MY_SOURCE_DIR} \
							&& touch $${MY_DOX_TARGET}
QMAKE_EXTRA_TARGETS	+= DocTarget
POST_TARGETDEPS		+= $${MY_DOX_TARGET}

DocClean1.commands		= -rm $${MY_DOX_TARGET}
clean.depends			+= DocClean1
QMAKE_EXTRA_TARGETS		+= DocClean1

DocClean2.commands		= -rm -f -r -d $${MY_DOC_DIR}/*
clean.depends			+= DocClean2
QMAKE_EXTRA_TARGETS		+= DocClean2

# Remove directories.
DirClean.commands	= -rmdir $$shell_path($${MY_DOC_DIR}) $$shell_path($${MY_INCLUDE_DIR})
clean.depends		+= DirClean
QMAKE_EXTRA_TARGETS	+= DirClean

QMAKE_EXTRA_TARGETS		+= clean
