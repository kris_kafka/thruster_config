#pragma once
#ifndef PORTSELECTIONDIALOGPRIVATE_H
#ifndef DOXYGEN_SKIP
#define PORTSELECTIONDIALOGPRIVATE_H
#endif
//######################################################################################################################
/*! @file
@brief Declare the implementation of the change node/group IDs dialog module.
*/
//######################################################################################################################

#include <QtCore/QString>
#include <QtGui/QIntValidator>

#include "ui_changeIdsDialog.h"

//######################################################################################################################

class	QCloseEvent;

//######################################################################################################################
/*! @brief The ChangeIdsDialogPrivate class provides the implementation of the change node/group IDs dialog module.
*/
class ChangeIdsDialogPrivate
:
	public	QDialog,
	public	Ui_ChangeIdsDialog

{
	Q_OBJECT
	Q_DISABLE_COPY(ChangeIdsDialogPrivate)

public:		// Constructors & destructors
	ChangeIdsDialogPrivate(int nodeId_, int groupId_, QWidget *parent);
	~ChangeIdsDialogPrivate(void);

public:		// Functions
	int				newGroupId(bool &isOkay);
	int				newNodeId(bool &isOkay);
	void			restoreWindowState(void);
	void			saveWindowState(void);
	void			setValidatorLimits(void);
	Q_SLOT void		updateGuiState(void);

public:		// Data
	int const		groupId;			//!< Original group ID.
	QIntValidator	groupIdValidator;	//!< Validator for the group ID line editor control.
	int const		nodeId;				//!< Original node ID.
	QIntValidator	nodeIdValidator;	//!< Validator for the node ID line editor control.
};

//######################################################################################################################
#endif
