/*! @file
@brief Define the thruster I/O module.
*/
//######################################################################################################################

#include "app_com.h"

#include <QtCore/QMutexLocker>
#include <QtCore/QSettings>
#include <QtCore/QString>
#include <QtCore/QThread>

#include "configGio.h"
#include "configThruster.h"
#include "packetUtil.h"
#include "thruster.h"
#include "tioBase.h"
#include "tioBase_log.h"
#include "tioImpl.h"
#include "tioParms.h"

//######################################################################################################################
// Local classes.

class	DoNothingParser;
class	TioImpl;

//######################################################################################################################
// Local functions

static void		defaultReplyHandler(TioParms *biop, Protocol_VRCSR_Packet const &packet, int deviceTypeSize);

//======================================================================================================================
/*! @brief VideoRay CSR protocol do nothing response parser.
*/
class DoNothingParser
:
	public	videoray::Protocol_vr_csr_parser
{
private:
	void	protocol_vrcsr_handle_custom_command_response(Byte id, Byte flags, int len, Byte device_type,
															ByteP buf) override;
	void	protocol_vrcsr_handle_response(Byte id, Byte flags, int addr, int len, Byte device_type,
											ByteP buf) override;
	bool	protocol_vrcsr_accept_id(Byte id, char isResponse) override;
	bool	protocol_vrcsr_packet_accepted_flag(void) override;
};

//======================================================================================================================
/*! Prevent changes to the I/O parameters.
*/
class	TioImpl::IopLocker
{
public:		// Constructors & destructors
	IopLocker(TioImpl *this_);
	~IopLocker(void);

private:	// Data
	TioImpl	*_this;
};

//======================================================================================================================
/*! Set the I/O parameters and automatically reset.
*/
class	TioImpl::IopValet
{
public:		// Constructors & destructors
	IopValet(TioImpl *this_, TioParms *iop);
	~IopValet(void);

private:	// Data
	TioImpl	*_this;
};

//######################################################################################################################
// Local variables.

//! Default I/O parameters.
static TioParms		defaultIoParameters(defaultReplyHandler, "Default Handler");

//! Do nothing reply packet parser.
static DoNothingParser		doNothingParser;

//######################################################################################################################
/*! @brief Create a TioParms object.
*/
TioParms::TioParms(
	PacketHandler	packetHandler_,	//!<[in] Reply packet handler.
	QString const	&name_			//!<[in] Name of I/O packets.
)
:
	commandBuffer			(),
	commandSize				(0),
	errorCode				(Thruster::TEC_NoError),
	groupIdMapMask			(INI_DEFAULT(CfgThrusterGroupIdMask)),
	isAbortOnError			(INI_DEFAULT(CfgGioAbortOnError)),
	isCheckReplyAddress		(INI_DEFAULT(CfgGioCheckReplyAddress)),
	isCheckReplyFlags		(INI_DEFAULT(CfgGioCheckReplyFlags)),
	isCheckReplyLength		(INI_DEFAULT(CfgGioCheckReplyLength)),
	isCheckReplyNodeId		(INI_DEFAULT(CfgGioCheckReplyNodeId)),
	isImmediateRetryOnError	(INI_DEFAULT(CfgGioImmediateRetryOnError)),
	isIoNeeded				(INI_DEFAULT(CfgGioIoNeeded)),
	isReplyExpected			(INI_DEFAULT(CfgGioNeedReply)),
	isReplyHasDeviceType	(INI_DEFAULT(CfgGioReplyHasDeviceType)),
	isReplySeen				(false),
	isResendEveryRetry		(INI_DEFAULT(CfgGioSendCommandOnRetry)),
	name					(name_),
	packetHandler			(packetHandler_),
	replyValidAddress		(-1),
	replyValidFlags			(-1),
	replyValidLength		(-1),
	replyValidNodeId		(-1),
	retryLimit				(INI_DEFAULT(CfgGioRetryLimit)),
	sleepDuration			(INI_DEFAULT(CfgGioSleepDuration)),
	timeoutDuration			(INI_DEFAULT(CfgGioTimeoutDuration)),
	validReplyDelayDuration	(INI_DEFAULT(CfgGioValidReplyDelay))
{
	LOG_Trace(loggerCreate, QString("Creating TioParms object: name=%1").arg(name));
}

//======================================================================================================================
/*! @brief Destroy a TioParms object.
*/
TioParms::~TioParms(void)
{
	LOG_Trace(loggerCreate, QString("Destroying TioParms object: name=%1").arg(name));
}

//======================================================================================================================
/*! @brief Lock changes to the I/O parameters.
*/
TioImpl::IopLocker::IopLocker(
	TioImpl	*this_	//!<[in] "this" in the caller.
)
:
	_this	(this_)
{
	_this->iopMutex.lock();
}

//======================================================================================================================
/*! @brief Unlock changes to the I/O parameters.
*/
TioImpl::IopLocker::~IopLocker(void)
{
	_this->iopMutex.unlock();
}

//======================================================================================================================
/*! @brief Set the I/O parameters.
*/
TioImpl::IopValet::IopValet(
	TioImpl		*this_,	//!<[in] "this" in the caller.
	TioParms	*iop	//!<[in] New I/O parameters.
)
:
	_this	(this_)
{
	QMutexLocker	locker(&_this->iopMutex);
	_this->iop = iop;
}

//======================================================================================================================
/*! @brief Restore the default I/O parameters.
*/
TioImpl::IopValet::~IopValet(void)
{
	QMutexLocker	locker(&_this->iopMutex);
	_this->iop = &defaultIoParameters;
}

//======================================================================================================================
/*! @brief Create a TioBase object.
*/
TioBase::TioBase(
	QString const		&serialPort_,	//!<[in] Serial port name.
	int					nodeId_,		//!<[in] Node ID.
	bool const volatile	&halt_			//!<[in] Halt I/O.
)
:
	halt		(halt_),
	nodeId		(nodeId_),
	serialPort	(serialPort_)
{
	LOG_Trace(loggerCreate, QString("Creating TioBase object: port='%1', nodeId=%2").arg(serialPort).arg(nodeId));
}

//======================================================================================================================
/*! @brief Destroy a TioBase object.
*/
TioBase::~TioBase(void)
{
	LOG_Trace(loggerCreate,
				QString("Destroying TioBase object: port='%1', nodeId=%2").arg(serialPort).arg(nodeId));
}

//======================================================================================================================
/*! @brief Create a TioImpl object.
*/
TioImpl::TioImpl(
	std::string			&portName,	//!<[in] Serial port name.
	int					nodeId_,	//!<[in] Node ID.
	bool const volatile	&halt_		//!<[in] Halt I/O.
)
:
	videoray::GizmoConnection	(portName, doNothingParser),
	elapsedTimer				(),
	iop							(&defaultIoParameters),
	halt						(halt_),
	nodeId						(nodeId_),
	iopMutex					(),
//	isReplySeen					(false),
	serialPort					(QString::fromStdString(portName))
{
	LOG_Trace(loggerCreate,
				QString("Creating TioImpl object: port='%1', nodeId=%2").arg(serialPort).arg(nodeId));
}

//======================================================================================================================
/*! @brief Destroy a TioImpl object.
*/
TioImpl::~TioImpl(void)
{
	LOG_Trace(loggerCreate,
				QString("Destroying TioImpl object: port='%1', nodeId=%2").arg(serialPort).arg(nodeId));
}

//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
/*! @brief Close the serial port.
*/
void
TioBase::close(void)
{
	LOG_Trace(loggerOpen,
				QString("Close connection: serialPort='%1', nodeId=%2").arg(serialPort).arg(nodeId));
	doClose();
	//
	QThread::msleep(IniFile().INI_GET(CfgThrusterGioCloseDelay));
}

//======================================================================================================================
/*! @brief Process a reply packet.
*/
void
defaultReplyHandler(
	TioParms					*biop,			//!<[in,out] I/O parameters.
	Protocol_VRCSR_Packet const	&packet,		//!<[in] Reply packet.
	int							deviceTypeSize	//!<[in] Size of device type data prefix.
)
{
	LOG_Trace(loggerNullPacket,
				QString("Received unexpected reply (%1): %2").arg(biop->name).arg(PacketUtil::formatPacket(packet)));
	Q_UNUSED(deviceTypeSize);
}

//======================================================================================================================
/*! @brief Perform generic I/O with the thruster.

@return the error code (Thruster::TEC_NoError if no errors).
*/
Thruster::Error
TioImpl::genericIo(
	TioParms	*iop_	//!<[in] New I/O parameters.
)
{
	LOG_Trace(loggerGenericIo, QString("Generic I/O (%1)").arg(iop_->name));
	// Done if no I/O needed.
	if (!iop_->isIoNeeded) {
		return iop_->errorCode;
	}
	// Use the given I/O parameters until we return.
	IopValet	iopReset(this, iop_);
	// Just sent the command once if no reply is expected.
	if (!iop->isReplyExpected) {
		sendCommand(iop->commandBuffer, iop->commandSize);
		return iop->errorCode;
	}
	//
	iop->isReplySeen = false;
	for (int retryCount = 0; !iop->isReplySeen && retryCount < iop->retryLimit; ++retryCount) {
		// Clear the error state.
		iop->errorCode = Thruster::TEC_NoError;
		// Send the command.
		if (0 == retryCount || iop->isResendEveryRetry) {
			sendCommand(iop->commandBuffer, iop->commandSize);
		}
		// Wait for the reply.
		restartTimeout();
		while (!halt && !iop->isReplySeen && !hasTimeoutExpired(iop->timeoutDuration)) {
			QThread::msleep(iop->sleepDuration);
			// Immediate retry on an error if requested.
			if (iop->isImmediateRetryOnError && Thruster::TEC_NoError != iop->errorCode) {
				LOG_Debug(loggerGenericIo,
							QString("GIO (%1): Immediate retry on error: errorCode=%2")
									.arg(iop->name).arg(iop->errorCode));
				break;
			}
		}
		// Abort if halting I/O.
		if (halt) {
			LOG_Debug(loggerGenericIo, QString("GIO (%1): Background task stoppping").arg(iop->name));
			return Thruster::TEC_Stopping;
		}
		// Abort any retries on an error if requested.
		if (iop->isAbortOnError && Thruster::TEC_NoError != iop->errorCode) {
			LOG_Debug(loggerGenericIo,
						QString("GIO (%1): Aborting retries on error: errorCode=%2")
								.arg(iop->name).arg(iop->errorCode));
			break;
		}
	}
	// Report retry limit errors.
	if (!iop->isReplySeen) {
		// Do not overwrite a prior error code.
		if (Thruster::TEC_NoError == iop->errorCode) {
			iop->errorCode = Thruster::TEC_RetryLimit;
		}
		LOG_Error(loggerGenericIo, QString("GIO (%1): Retry limit: errorCode=%2").arg(iop->name).arg(iop->errorCode));
	}
	// Delay if requested.
	if (Thruster::TEC_NoError == iop->errorCode && iop->validReplyDelayDuration) {
		QThread::msleep(iop->validReplyDelayDuration);
	}
	return iop->errorCode;
}

//======================================================================================================================
/*! @brief Determine if a timeout has occured.

@return true if a timeout has occured.
*/
qint64
TioImpl::hasTimeoutExpired(
	qint64	timeout	//!<[in] Timeout period in milliseconds.
)
{
	bool answer = elapsedTimer.hasExpired(timeout);
	LOG_Trace(loggerQuery, QString("hasTimeoutExpired (%1): %2").arg(iop->name).arg(answer));
	return answer;
}

//======================================================================================================================
/*! @brief Intiailize TioParms.
*/
void
TioParms::initialize(
	IniFile			&iniFile,		//!<[in] Settings.
	IniKey const	&group			//!<[in] Group within settings that contains the generic I/O settings.
)
{
	LOG_Trace(loggerInitialize, QString("Initialize (%1): group=%2").arg(name).arg(group));
	isReplySeen				= false;
	groupIdMapMask			= iniFile.INI_GET(CfgThrusterGroupIdMask);
	iniFile.beginGroup(group);
	isAbortOnError			= iniFile.INI_GET(CfgGioAbortOnError);
	isCheckReplyAddress		= iniFile.INI_GET(CfgGioCheckReplyAddress);
	isCheckReplyFlags		= iniFile.INI_GET(CfgGioCheckReplyFlags);
	isCheckReplyLength		= iniFile.INI_GET(CfgGioCheckReplyLength);
	isCheckReplyNodeId		= iniFile.INI_GET(CfgGioCheckReplyNodeId);
	isImmediateRetryOnError	= iniFile.INI_GET(CfgGioImmediateRetryOnError);
	isIoNeeded				= iniFile.INI_GET(CfgGioIoNeeded);
	isReplyExpected			= iniFile.INI_GET(CfgGioNeedReply);
	isReplyHasDeviceType	= iniFile.INI_GET(CfgGioReplyHasDeviceType);
	isResendEveryRetry		= iniFile.INI_GET(CfgGioSendCommandOnRetry);
	retryLimit				= iniFile.INI_GET(CfgGioRetryLimit);
	sleepDuration			= iniFile.INI_GET(CfgGioSleepDuration);
	timeoutDuration			= iniFile.INI_GET(CfgGioTimeoutDuration);
	validReplyDelayDuration	= iniFile.INI_GET(CfgGioValidReplyDelay);
	iniFile.endGroup();
}

//======================================================================================================================
/*! @brief Open the serial port.

@return the error code (Thruster::TEC_NoError if no errors).
*/
Thruster::Error
TioBase::open(void)
{
	LOG_Trace(loggerOpen,
				QString("Open connection: serialPort='%1', nodeId=%2").arg(serialPort).arg(nodeId));
	bool	isOpen = false;
	try {
		isOpen = doOpen();
	}
	catch (boost::system::system_error err) {
		LOG_Error(loggerOpen,
					QString("Open error: port='%1', code=%2, name=%3, what=%4")
							.arg(serialPort)
							.arg(err.code().value())
							.arg(err.code().category().name())
							.arg(err.what()));
		return Thruster::TEC_OpenError;
	}
	// Ensure the port is open.
	if (!isOpen) {
		LOG_Error(loggerOpen, QString("Error opening serial port '%1'").arg(serialPort));
		return Thruster::TEC_OpenError;
	}
	//
	return Thruster::TEC_NoError;
}

//======================================================================================================================
/*! @brief Handle complete reply packets.
*/
void
TioImpl::packet_complete(
	Protocol_VRCSR_Packet	&packet	//!<[in] Reply packet.
)
{
	// Ensure that I/O parameters don't change on us.
	IopLocker	iopLocker(this);
	//
	LOG_Trace(loggerReply, QString("Received reply (%1): %2").arg(iop->name).arg(PacketUtil::formatPacket(packet)));
	// Ignore packets not from the requested node.
	if (iop->isCheckReplyNodeId && iop->replyValidNodeId != packet.header->id) {
		LOG_Debug(loggerReply,
					QString("Unexpected reply (%1): wrong nodeId: %2 (%3)")
							.arg(iop->name).arg(packet.header->id).arg(iop->replyValidNodeId));
		return;
	}
	// Ignore packets with incorrect flags.
	if (iop->isCheckReplyFlags && iop->replyValidFlags != packet.header->flags) {
		LOG_Debug(loggerReply,
					QString("Unexpected reply (%1): flags incorrect: %2 (%3)")
							.arg(iop->name).arg(packet.header->flags).arg(iop->replyValidFlags));
		return;
	}
	// Ignore packets with incorrect address.
	if (iop->isCheckReplyAddress && iop->replyValidAddress != packet.header->address) {
		LOG_Debug(loggerReply,
					QString("Unexpected reply (%1): address incorrect: %2 (%3)")
							.arg(iop->name).arg(packet.header->address).arg(iop->replyValidAddress));
		return;
	}
	// ToDo: Correct this to handle extended types.
	int	deviceTypeSize = iop->isReplyHasDeviceType ? 1 : 0;
	// Ignore packets with incorrect data length.
	if (iop->isCheckReplyLength && (iop->replyValidLength + deviceTypeSize) != packet.header->length) {
		LOG_Debug(loggerReply,
					QString("Unexpected reply (%1): data length incorrect: %2 (%3 + %4)")
							.arg(iop->name).arg(packet.header->length).arg(iop->replyValidLength).arg(deviceTypeSize));
		return;
	}
	//
	(iop->packetHandler)(iop, packet, deviceTypeSize);
}

//======================================================================================================================
/*! \brief Handles response packets sent to the custom command register.
*/
void
DoNothingParser::protocol_vrcsr_handle_custom_command_response(
	Byte	id,				//!<[in] Id of destintion.
	Byte	flags,			//!<[in] Packet flags.
	int		len,			//!<[in] Length of data payload.
	Byte	device_type,	//!<[in] Device type of the responding device.
	ByteP buf				//!<[in] Data payload buffer.
)
{
	Q_UNUSED(id);
	Q_UNUSED(flags);
	Q_UNUSED(len);
	Q_UNUSED(device_type);
	Q_UNUSED(buf);
}

//======================================================================================================================
/*! \brief Handles response packets.
*/
void
DoNothingParser::protocol_vrcsr_handle_response(
	Byte	id,				//!<[in] Node ID of device which sent the response packet.
	Byte	flags,			//!<[in] Packet flags.
	int		addr,			//!<[in] CSR address.
	int		len,			//!<[in] Length of data payload.
	Byte	device_type,	//!<[in] Device type of the responding device.
	ByteP	buf				//!<[in] Packet buffer.
)
{
	Q_UNUSED(id);
	Q_UNUSED(flags);
	Q_UNUSED(addr);
	Q_UNUSED(len);
	Q_UNUSED(device_type);
	Q_UNUSED(buf);
}

//======================================================================================================================
/*! \brief Allows for selective storage and processing of packets.

@return True - packet should be stored and parsed.
*/
bool
DoNothingParser::protocol_vrcsr_accept_id(
	Byte	id,			//!<[in] Node ID of device which sent the response packet.
	char	isResponse	//!<[in] Determine if the packet is a response.
)
{
	Q_UNUSED(id);
	Q_UNUSED(isResponse);
	return true;
}

//======================================================================================================================
/*! \brief Determine if a packet was accepted.

@return True - packet was accepted
*/
bool
DoNothingParser::protocol_vrcsr_packet_accepted_flag(void)
{
	return true;
}

//======================================================================================================================
/*! @brief Start/restart timeout timer.
*/
void
TioImpl::restartTimeout(void)
{
	LOG_Trace(loggerTimer, QString("Starting timeout timer (%1)").arg(iop->name));
	elapsedTimer.start();
}

//======================================================================================================================
/*! @brief Send a command.
*/
void
TioImpl::sendCommand(
	boost::shared_array<Byte>	packetBuffer,	//!<[in] Packet to send.
	int								packetSize		//!<[in] Size of packet.
)
{
	LOG_Trace(loggerSend,
				QString("Sending command (%1): %2")
						.arg(iop->name).arg(PacketUtil::formatPacket(packetBuffer.get(), packetSize)));
	//
	write(packetBuffer, packetSize);
}

//======================================================================================================================
/*! @brief Initilize TioParms.
*/
void
TioParms::setDefaults(void)
{
	LOG_Trace(loggerInitialize, QString("Set defaults (%1)").arg(name));
	isReplySeen				= false;
	groupIdMapMask			= INI_DEFAULT(CfgThrusterGroupIdMask);
	isAbortOnError			= INI_DEFAULT(CfgGioAbortOnError);
	isCheckReplyAddress		= INI_DEFAULT(CfgGioCheckReplyAddress);
	isCheckReplyFlags		= INI_DEFAULT(CfgGioCheckReplyFlags);
	isCheckReplyLength		= INI_DEFAULT(CfgGioCheckReplyLength);
	isCheckReplyNodeId		= INI_DEFAULT(CfgGioCheckReplyNodeId);
	isImmediateRetryOnError	= INI_DEFAULT(CfgGioImmediateRetryOnError);
	isIoNeeded				= INI_DEFAULT(CfgGioIoNeeded);
	isReplyExpected			= INI_DEFAULT(CfgGioNeedReply);
	isReplyHasDeviceType	= INI_DEFAULT(CfgGioReplyHasDeviceType);
	isResendEveryRetry		= INI_DEFAULT(CfgGioSendCommandOnRetry);
	retryLimit				= INI_DEFAULT(CfgGioRetryLimit);
	sleepDuration			= INI_DEFAULT(CfgGioSleepDuration);
	timeoutDuration			= INI_DEFAULT(CfgGioTimeoutDuration);
	validReplyDelayDuration	= INI_DEFAULT(CfgGioValidReplyDelay);
}

//======================================================================================================================
/*! @brief Update TioParms.
*/
void
TioParms::update(
	IniFile			&iniFile,		//!<[in] Settings.
	IniKey const	&group			//!<[in] Group within settings that contains the generic I/O settings.
)
{
	LOG_Trace(loggerInitialize, QString("Update (%1): group=%2").arg(name).arg(group));
	groupIdMapMask			= iniFile.INI_VALUE(CfgThrusterGroupIdMask,			groupIdMapMask);
	iniFile.beginGroup(group);
	isAbortOnError			= iniFile.INI_VALUE(CfgGioAbortOnError,				isAbortOnError);
	isCheckReplyAddress		= iniFile.INI_VALUE(CfgGioCheckReplyAddress,		isCheckReplyAddress);
	isCheckReplyFlags		= iniFile.INI_VALUE(CfgGioCheckReplyFlags,			isCheckReplyFlags);
	isCheckReplyLength		= iniFile.INI_VALUE(CfgGioCheckReplyLength,			isCheckReplyLength);
	isCheckReplyNodeId		= iniFile.INI_VALUE(CfgGioCheckReplyNodeId,			isCheckReplyNodeId);
	isImmediateRetryOnError	= iniFile.INI_VALUE(CfgGioImmediateRetryOnError,	isImmediateRetryOnError);
	isIoNeeded				= iniFile.INI_VALUE(CfgGioIoNeeded,					isIoNeeded);
	isReplyExpected			= iniFile.INI_VALUE(CfgGioNeedReply,				isReplyExpected);
	isReplyHasDeviceType	= iniFile.INI_VALUE(CfgGioReplyHasDeviceType,		isReplyHasDeviceType);
	isResendEveryRetry		= iniFile.INI_VALUE(CfgGioSendCommandOnRetry,		isResendEveryRetry);
	retryLimit				= iniFile.INI_VALUE(CfgGioRetryLimit,				retryLimit);
	sleepDuration			= iniFile.INI_VALUE(CfgGioSleepDuration,			sleepDuration);
	timeoutDuration			= iniFile.INI_VALUE(CfgGioTimeoutDuration,			timeoutDuration);
	validReplyDelayDuration	= iniFile.INI_VALUE(CfgGioValidReplyDelay,			validReplyDelayDuration);
	iniFile.endGroup();
}

