#pragma once
#ifndef TIOIMPL_H
#ifndef DOXYGEN_SKIP
#define TIOIMPL_H
#endif
//######################################################################################################################
/*! @file
@brief Declare the interface to the thruster I/O module.
*/
//######################################################################################################################

#include <stdint.h>

#include <QtCore/QElapsedTimer>
#include <QtCore/QMutex>
#include <QtCore/QString>

#include <boost/shared_array.hpp>

#include "videoray/applib/protocol_vr_csr.h"
#include "videoray/hostlib/bootloader_connection.h"

#include "configCsr.h"
#include "thruster.h"

//######################################################################################################################

class	TioParms;

//######################################################################################################################
/*! @brief VideoRay CSR protocol response handler.
*/
class TioImpl
:
	public	videoray::GizmoConnection
{
private:	// Nested classes
	class	IopLocker;
	class	IopValet;

public:		// Constructors & destructors
				TioImpl(std::string &portName, int nodeId_, bool const volatile &halt_);
	virtual		~TioImpl(void);

protected:	// Functions
	Thruster::Error		genericIo(TioParms *iop_);
	qint64				hasTimeoutExpired(qint64 timeout);
	void				packet_complete(Protocol_VRCSR_Packet &packet) override;
	void				restartTimeout(void);
	void				sendCommand(boost::shared_array<Byte> packetBuffer, int packetSize);

protected:	// Data
	QElapsedTimer			elapsedTimer;	//!< Timeout timer.
	TioParms				*iop;			//!< I/O parameters.
	bool const volatile		&halt;			//!< Flag to halt I/O.
	int const				nodeId;			//!< Node ID.
	QMutex					iopMutex;		//!< Critical section mutex.
	QString const			serialPort;		//!< Serial port.
};

//######################################################################################################################
#endif
