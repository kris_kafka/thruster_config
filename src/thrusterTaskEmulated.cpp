/*! @file
@brief Define the thruster interface module.
*/
//######################################################################################################################

#include "app_com.h"

#include <QtCore/QByteArray>
#include <QtCore/QRegularExpression>
#include <QtCore/QThread>

#include "configEmulation.h"
#include "strUtil.h"
#include "thrusterTaskEmulated.h"
#include "thrusterTaskEmulated_log.h"

//######################################################################################################################
// Macros

//! @hideinitializer Evaluate an emulation formula.
#define EVALUATE(iniFile, settingBase, error) \
						evaluate(iniFile, \
									INI_GROUP(CfgEmulationRpn), \
									INI_KEY(settingBase), \
									INI_DEFAULT(settingBase), \
									error)

//######################################################################################################################
// Inline functions.

//======================================================================================================================
/*! @brief Convert a QElapsedTimer duration @a duration to seconds.

@return duration converted to seconds.
*/
inline double
elapsedTimeToSeconds(
	qint64	duration	//!<[in] Duration.
)
{
	return duration / 1000.0;
}

//######################################################################################################################
/*! @brief Create a ThrusterTaskEmulated object.
*/
ThrusterTaskEmulated::ThrusterTaskEmulated(void)
:
	rpn							(),
	testModeTimer				(),
	thrusterDelay				(0),
	thrusterGroupId				(0),
	thrusterIsValid				(false),
	thrusterMaximumPower		(0),
	thrusterMotorId				(0),
	thrusterNodeId				(0),
	thrusterRotationReversed	(false),
	thrusterSerialNumber		()
{
	LOG_Trace(loggerCreate, "Creating a ThrusterTaskEmulated object");
	testModeTimer.start();
}

//======================================================================================================================
/*! @brief Destroy a ThrusterTaskEmulated object.
*/
ThrusterTaskEmulated::~ThrusterTaskEmulated(void)
{
	LOG_Trace(loggerCreate, "Destroying a ThrusterTaskEmulated object");
}

//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
/*! @brief Assign a RPN symbol @a name to @a value.
*/
void
ThrusterTaskEmulated::assign(
	QString const	&name,	//!<[in] Name of the symbol.
	double			value	//!<[in] New value for the symbol.
)
{
	LOG_Trace(loggerRpn, QString("Assign %1 to %2").arg(name).arg(value));
	LOG_AssertMsg(loggerRpn, rpn.assignSymbol(name, value),
					QString("Unable to assign %1 to %2:\n"
									"\t\tError: %3")
							.arg(name).arg(value).arg(rpn.lastError()));
}

//======================================================================================================================
/*! @brief Read the configuration of an emulated thruster.

@return true if no errors, else false.
*/
bool
ThrusterTaskEmulated::configurationRead(
	int			nodeId,				//!<[in]  ID of node to read configuration from.
	int			&nodeIdRead,		//!<[out] Node ID read from thruster.
	int			&groupId,			//!<[out] Group ID read from thruster.
	int			&motorId,			//!<[out] Motor ID read from thruster.
	int			&maximumPower,		//!<[out] Maximum power read from thruster.
	bool		&rotationReverse,	//!<[out] Rotation reversed read from thruster.
	QString		&serialNumber,		//!<[out] Serial number read from thruster.
	QByteArray	&csrBlob			//!<[out] CSR blob.
)
{
	LOG_Trace(loggerConfigurationRead, QString("Read configuration of thruster: nodeId=%1").arg(nodeId));
	thrusterLoad();
	// Emulation delay.
	QThread::msleep(IniFile().INI_GET(CfgEmulationDelayConfigRead));
	// Find the thruster data.
	int		index;
	QString	errorMessage;
	Thruster::ThrusterErrors	errorCode = thrusterFindNodeId(nodeId, index, errorMessage);
	if (Thruster::TEC_NoError != errorCode) {
		LOG_Error(loggerConfigurationRead, errorMessage);
		// Report the state change.
		EMIT_IDLE(configurationReadStop, errorCode);
		return false;
	}
	nodeIdRead		= thrusterNodeId[index];
	groupId			= thrusterGroupId[index];
	motorId			= thrusterMotorId[index];
	maximumPower	= thrusterMaximumPower[index];
	rotationReverse	= thrusterRotationReversed[index];
	serialNumber	= thrusterSerialNumber[index];
	csrBlob			= QByteArray();
	return true;
}

//======================================================================================================================
/*! @brief Write the configuration of an emulated thruster.
*/
void
ThrusterTaskEmulated::configurationWrite(
	int					nodeId,				//!<[in] ID of node to write configuraton to.
	int					motorId,			//!<[in] New motor ID.
	int					maximumPower,		//!<[in] New maximum power level.
	bool				rotationReverse,	//!<[in] New rotation is reversed.
	QByteArray const	&csrBlob			//!<[in] CSR blob.
)
{
	LOG_Trace(loggerConfigurationWrite,
				QString("Write configuration to thruster: nodeId=%1, motorID=%2, maximumPower=%3, rotationReverse=%4")
						.arg(nodeId).arg(motorId).arg(maximumPower).arg(rotationReverse));
	Q_UNUSED(csrBlob);
	thrusterLoad();
	// Emulation delay.
	QThread::msleep(IniFile().INI_GET(CfgEmulationDelayConfigWrite));
	// Find the thruster data.
	int		index;
	QString	errorMessage;
	Thruster::ThrusterErrors	errorCode = thrusterFindNodeId(nodeId, index, errorMessage);
	if (Thruster::TEC_NoError != errorCode) {
		LOG_Error(loggerConfigurationWrite, errorMessage);
		// Report the state change.
		EMIT_IDLE(configurationWriteStop, errorCode);
		return;
	}
	// Write the emulated configuration values.
	thrusterMotorId[index]			= motorId;
	thrusterMaximumPower[index]		= maximumPower;
	thrusterRotationReversed[index]	= rotationReverse;
	thrusterSave();
	//
	LOG_Debug(loggerConfigurationWrite,
				QString("Write configuration to thruster finished: nodeId=%1, motorID=%2, maximumPower=%3, "
								"rotationReverse=%4")
						.arg(nodeId).arg(motorId).arg(maximumPower).arg(rotationReverse));
	// Report successful completion.
	EMIT_IDLE(configurationWriteStop, Thruster::TEC_NoError);
}

//======================================================================================================================
/*! @brief Evaluate a RPN formula.

@return The result of the evaluation of the RPN formula.
*/
double
ThrusterTaskEmulated::evaluate(
	IniFile			&file,		//!<[in]	IniFile.
	IniKey const	&grp,		//!<[in]	Setting group of the RPN formula.
	IniKey const	&key,		//!<[in]	Setting key of RPN formula.
	IniString const	&def,		//!<[in]	Default RPN formula.
	bool			&error		//!<[out]	Set to true if an error occurs.
)
{
	LOG_Trace(loggerRpn, QString("Evaluate RPN formula for '%1'").arg(key));
	QString	str = file.get(def, grp + key);
	LOG_Debug(loggerRpn, QString("Evaluate: %1").arg(str));
	if (!rpn.evaluate(str)) {
		error = true;
		LOG_Error(loggerRpn, LOG_TO_STR(QString("Evaluation of RPN formula failed:\n"
												"\t\tName:     %1\n"
												"\t\tFormula:  %2\n"
												"\t\tError:    %3")
										.arg(key).arg(str).arg(rpn.lastError())));
	}
	return rpn.lastValue();
}

//======================================================================================================================
/*! @brief Change the node ID of @a currentNode.
*/
void
ThrusterTaskEmulated::idsChange(
	QString const	&serialNumber,	//!<[in] Serial number of the thruster.
	int				oldNodeId,		//!<[in] Old node ID of the thruster.
	int				newNodeId,		//!<[in] New node ID of the thruster.
	int				newGroupId		//!<[in] New group ID of the thruster.
)
{
	LOG_Trace(loggerIdsChange,
				QString("Changing the IDs of %1 (%2) to: node=%3, group=%4")
						.arg(oldNodeId).arg(serialNumber).arg(newNodeId).arg(newGroupId));
	// Delay.
	QThread::msleep(IniFile().INI_GET(CfgEmulationDelayIdsChange));
	// Should we stop.
	if (taskHalt) {
		LOG_Info(loggerIdsChange, "Background task stopping");
		EMIT_IDLE(idsChangeStop, Thruster::TEC_Stopping);
		LOG_Trace(loggerIdsChange, "Quiting the background task");
		Q_EMIT quitThread();
		return;
	}
	// Find the thruster data.
	thrusterLoad();
	int		index;
	QString	errorMessage;
	Thruster::ThrusterErrors	errorCode = thrusterFindSerialNumber(serialNumber, index, errorMessage);
	if (Thruster::TEC_NoError != errorCode) {
		LOG_Error(loggerIdsChange, errorMessage);
		// Report the state change.
		EMIT_IDLE(idsChangeStop, errorCode);
		return;
	}
	// Write the emulated node ID.
	thrusterNodeId[index] = newNodeId;
	thrusterGroupId[index] = newGroupId;
	thrusterSave();
	//
	EMIT_IDLE(idsChangeStop, Thruster::TEC_NoError);
}

//======================================================================================================================
/*! @brief Scan for emulated thrusters.

@return true if no errors, else false.
*/
bool
ThrusterTaskEmulated::scanForThrusters(void)
{
	LOG_Trace(loggerScanForThrusters, "Scan for thrusters");
	thrusterLoad(true);
	int	count = thrusterDelay.size();
	for (int index = 0; index < count; ++index) {
		// Get the thruster's data.
		int		delay			= thrusterDelay[index];
		bool	findInScan		= thrusterFindInScan[index];
		int		nodeId			= thrusterNodeId[index];
		int		groupId			= thrusterGroupId[index];
		QString	serialNumber	= thrusterSerialNumber[index];
		if (findInScan) {
			// Delay before reporting found thruster.
			QThread::msleep(delay);
			// Report the found thruster.
			LOG_Debug(loggerSignals,
						QString("Emit scanForThrustersNews: nodeId=%1, groupId=%2, serialNumber=%3")
								.arg(nodeId).arg(groupId).arg(serialNumber));
			Q_EMIT scanForThrustersNews(nodeId, groupId, serialNumber);
		}
		// Should we stop.
		if (taskHalt) {
			LOG_Info(loggerScanForThrusters, "Background task stopping");
			EMIT_IDLE(scanForThrustersStop, Thruster::TEC_Stopping);
			LOG_Trace(loggerScanForThrusters, "Quiting the background task");
			Q_EMIT quitThread();
			return false;
		}
	}
	// Final delay.
	QThread::msleep(IniFile().INI_GET(CfgEmulationDelayScanForThrusters));
	return true;
}

//======================================================================================================================
/*! @brief Check if serial port @a is valid.

@return true if valid else false.
*/
bool
ThrusterTaskEmulated::serialPortCheck(
	QString const	&portName	//!<[in] Serial port.
)
{
	Q_UNUSED(portName);
	return true;
}

//======================================================================================================================
/*! @brief Query the available serial ports.

We do no error checking on the list of emulated serial ports in the configuration file.

@return list of available serial ports.
*/
QStringList
ThrusterTaskEmulated::serialPortsAvailable(void)
{
	LOG_Trace(loggerQuerySerialPorts, "Query for the available serial ports");
	QStringList	list;
	// Emulate the serial ports.
	IniFile	iniFile;
	int		count = iniFile.beginReadArray(INI_GROUP(CfgEmulationPorts));
	QString	defaultTemplate = QSysInfo::WV_None != QSysInfo::WindowsVersion ? "COM%1" : "/dev/com%1";
	// Get the serial ports from the configuration file.
	for (int index = 0; index < count; ++index) {
		iniFile.setArrayIndex(index);
		list += iniFile.INI_VALUE(CfgEmulationPortsName, defaultTemplate.arg(1 + index));
	}
	std::sort(list.begin(), list.end(), StrUtil::alphaNumericLess);
	return list;
}

//======================================================================================================================
/*! @brief Testing mode emulation.
*/
void
ThrusterTaskEmulated::testMode(
	int	nodeId,		//!<[in] Node ID of thruster to be tested.
	int	groupId,	//!<[in] Group ID of thruster to be tested.
	int	motorId,	//!<[in] Motor ID of thruster to be tested.
	int	mode		//!<[in] Testing mode.
)
{
	LOG_Trace(loggerTestingMode,
				QString("Testing mode: nodeId=%1, groupId=%2, motorId=%3, mode=%4")
						.arg(nodeId).arg(groupId).arg(motorId).arg(mode));
	//
	double	testModeTimeOff = elapsedTimeToSeconds(testModeTimer.restart());
	int		testModeCount = 0;
	while (!testModeHalt) {
		// Handle speed changes.
		if (testModeSpeedChanged) {
			testModeSpeedChanged = false;
			LOG_Debug(loggerTestingMode, QString("The testing mode speed changed to %1").arg(testingModeSpeed));
		}
		IniFile	iniFile;
		QThread::msleep(iniFile.INI_GET(CfgEmulationDelayTestingMode));
		/* Calculate the status values.
		*/
		// Update the predefined symbols.
		assign("count",		testModeCount);
		assign("node_id",	nodeId);
		assign("speed",		testingModeSpeed);
		assign("time_off",	testModeTimeOff);
		assign("time_on",	elapsedTimeToSeconds(testModeTimer.elapsed()));
		++testModeCount;
		testModeTimeOff = 0.0;
		// Declare all of the user variables.
		QStringList	userSymbols = iniFile.INI_GET(CfgEmulationRpnSymbols)
										.split(QRegularExpression("\\s+"), QString::SkipEmptyParts);
		for (QString symbol : userSymbols) {
			rpn.declareSymbol(symbol);
		}
		// Assign all of the user variables.
		bool	error = false;
		for (QString symbol : userSymbols) {
			rpn.assignSymbol(symbol, evaluate(iniFile, INI_GROUP(CfgEmulationRpn), symbol, "0.0", error));
		}
		// Evaluate the required formulas.
		double	current		= EVALUATE(iniFile, CfgEmulationRpnBusAmps, error);
		double	faults		= EVALUATE(iniFile, CfgEmulationRpnFaultFlags, error);
		double	rpm			= EVALUATE(iniFile, CfgEmulationRpnRpm, error);
		double	temperature	= EVALUATE(iniFile, CfgEmulationRpnTemperature, error);
		double	voltage		= EVALUATE(iniFile, CfgEmulationRpnBusVolts, error);
		// Handle errors in any of the formulas.
		if (error) {
			EMIT_IDLE(testingModeStop, Thruster::TEC_RpnFormula);
			return;
		}
		// Emit the status values.
		LOG_Debug(loggerSignals,
					QString("Emit testingModeNews: nodeId=%1, rpm=%2, voltage=%3, current=%4, temperature=%5, "
									"faults=%6")
							.arg(nodeId).arg(rpm).arg(voltage).arg(current)
							.arg(temperature).arg((int)faults, 2, 16, QChar('0')));
		Q_EMIT testingModeNews(nodeId, rpm, voltage, current, temperature, (uint)faults);
		// Should we stop.
		if (taskHalt) {
			LOG_Info(loggerTestingMode, "Background task stopping");
			EMIT_IDLE(testingModeStop, Thruster::TEC_Stopping);
			LOG_Trace(loggerTestingMode, "Quiting the background task");
			Q_EMIT quitThread();
			return;
		}
	}
	//
	EMIT_IDLE(testingModeStop, Thruster::TEC_NoError);
}

//======================================================================================================================
/*! @brief Find the thruster data for @a nodeId.

If an error occurs:
	- @a index will be -1.
	- @a errorMessage will contain a human readable error message describing the error.
	- Return error code.

If no error occurs:
	- @a index will be the index of the thruster's data.
	- @a errorMesssage will be cleared.
	- Return Thruster::TEC_NoError.

@return Error code for errors, TEC_NoError if no error.
*/
Thruster::ThrusterErrors
ThrusterTaskEmulated::thrusterFindNodeId(
	int			nodeId,			//!<[in]  Node ID of thruster to find.
	int			&index,			//!<[out] Index where found or -1 if an error occurs.
	QString		&errorMessage	//!<[out] Error message if an error occurs.
)
{
	LOG_Trace(loggerEmulationFind, QString("Find thruster with nodeID=%1").arg(nodeId));
	// Find all thrusters with the given node ID.
	index = -1;
	int	count = thrusterDelay.size();
	for (int idx = 0; idx < count; ++idx) {
		if (nodeId == thrusterNodeId[idx]) {
			// Duplicates are an error.
			if (-1 == index) {
				index = idx;
			} else {
				index = -1;
				errorMessage = QString("Multiple thrusters with nodeID=%1").arg(nodeId);
				return Thruster::TEC_Duplicates;
			}
		}
	}
	// Error if no thruster has the given node ID.
	if (-1 == index) {
		errorMessage = QString("No thruster with nodeID=%1").arg(nodeId);
		return Thruster::TEC_NodeIdNotFound;
	}
	// No error.
	errorMessage.clear();
	return Thruster::TEC_NoError;
}

//======================================================================================================================
/*! @brief Find the thruster data for @a serialNumber.

If an error occurs:
	- @a index will be -1.
	- @a errorMessage will contain a human readable error message describing the error.
	- Return error code.

If no error occurs:
	- @a index will be the index of the thruster's data.
	- @a errorMesssage will be cleared.
	- Return Thruster::TEC_NoError.

@return Error code for errors, TEC_NoError if no error.
*/
Thruster::ThrusterErrors
ThrusterTaskEmulated::thrusterFindSerialNumber(
	QString const	&serialNumber,	//!<[in] Serial number of thruster to find.
	int				&index,			//!<[out]	Index where found or -1 if an error occurs.
	QString			&errorMessage	//!<[out]	Error message if an error occurs.
)
{
	LOG_Trace(loggerEmulationFind, QString("Find thruster with serial number=%1").arg(serialNumber));
	// Find all thrusters with the given serial number.
	index = -1;
	int	count = thrusterDelay.size();
	for (int idx = 0; idx < count; ++idx) {
		if (serialNumber == thrusterSerialNumber[idx]) {
			// Duplicates are an error.
			if (-1 == index) {
				index = idx;
			} else {
				index = -1;
				errorMessage = QString("Multiple thrusters with serial number of %1").arg(serialNumber);
				return Thruster::TEC_Duplicates;
			}
		}
	}
	// Error if no thruster has the given serial number.
	if (-1 == index) {
		errorMessage = QString("No thruster with serial number of %1").arg(serialNumber);
		return Thruster::TEC_NodeIdNotFound;
	}
	// No error.
	errorMessage.clear();
	return Thruster::TEC_NoError;
}

//======================================================================================================================
/*! @brief Load the emulated thruster data.
*/
void
ThrusterTaskEmulated::thrusterLoad(
	bool	force	//!<[in] Force reload.
)
{
	LOG_Trace(loggerEmulationData, QString("Loading thruster data, force=%1").arg(force));
	//
	if (force) {
		thrusterIsValid = false;
		thrusterDelay			.clear();
		thrusterGroupId			.clear();
		thrusterMaximumPower	.clear();
		thrusterMotorId			.clear();
		thrusterNodeId			.clear();
		thrusterRotationReversed.clear();
		thrusterSerialNumber	.clear();
	}
	// Done if the data has already been loaded.
	if (thrusterIsValid) {
		return;
	}
	//
	duplicateNodeIdCount	= 0;
	duplicateSerialCount	= 0;
	//
	IniFile		iniFile;
	int			count = iniFile.beginReadArray(INI_GROUP(CfgEmulationThruster));
	thrusterDelay				.reserve(count);
	thrusterGroupId				.reserve(count);
	thrusterMaximumPower		.reserve(count);
	thrusterMotorId				.reserve(count);
	thrusterNodeId				.reserve(count);
	thrusterRotationReversed	.reserve(count);
	thrusterSerialNumber		.reserve(count);
	//
	for (int index = 0; index < count; ++index) {
		// Load the thruster's data.
		iniFile.setArrayIndex(index);
		int		index1			= 1 + index;
		bool	findInScan		= iniFile.INI_GET(CfgEmulationThrusterFindInScan);
		bool	rotationReverse	= iniFile.INI_GET(CfgEmulationThrusterRotationReverse);
		int		delay			= iniFile.INI_GET(CfgEmulationThrusterDelay);
		int		groupId			= iniFile.INI_GET(CfgEmulationThrusterGroupId);
		int		maximumPower	= iniFile.INI_GET(CfgEmulationThrusterMaximumPower);
		int		motorId			= iniFile.INI_GET(CfgEmulationThrusterMotorId);
		int		nodeId			= iniFile.INI_GET(CfgEmulationThrusterNodeId);
		QString	serialNumber	= iniFile.INI_VALUE(CfgEmulationThrusterSerialNumber,
													INI_DEFAULT(CfgEmulationThrusterSerialNumber).arg(index1))
										.trimmed();
		LOG_Debug(loggerEmulationData, QString("%1: delay=%2, findInScan=%3, groupId=%4, maximumPower=%5, motorId=%6, "
													"nodeId=%7, rotationReverse=%8, serialNumber=%9")
											.arg(index1).arg(delay).arg(findInScan).arg(groupId).arg(maximumPower)
											.arg(motorId).arg(nodeId).arg(rotationReverse).arg(serialNumber));
		// Check for duplicates.
		if (thrusterNodeId.contains(nodeId)) {
			++duplicateNodeIdCount;
			LOG_Debug(loggerEmulationData, QString("Duplicate nodeID=%1").arg(nodeId));
		}
		if (thrusterSerialNumber.contains(serialNumber)) {
			++duplicateSerialCount;
			LOG_Debug(loggerEmulationData, QString("Duplicate serial number %1").arg(serialNumber));
		}
		//
		thrusterDelay			.append(delay);
		thrusterFindInScan		.append(findInScan);
		thrusterGroupId			.append(groupId);
		thrusterMaximumPower	.append(maximumPower);
		thrusterMotorId			.append(motorId);
		thrusterNodeId			.append(nodeId);
		thrusterRotationReversed.append(rotationReverse);
		thrusterSerialNumber	.append(serialNumber);
	}
	iniFile.endArray();
	//
	thrusterIsValid = true;
}

//======================================================================================================================
/*! @brief Save the emulated thruster data.
*/
void
ThrusterTaskEmulated::thrusterSave(void)
{
	LOG_Trace(loggerEmulationData, "Saving thruster data");
	//
	// Done if the data has not been loaded.
	if (!thrusterIsValid) {
		return;
	}
	//
	IniFile	iniFile;
	int		count = thrusterDelay.size();
	iniFile.beginWriteArray(CfgEmulationThruster_Group, count);
	//
	for (int index = 0; index < count; ++index) {
		int		index1			= 1 + index;
		int		delay			= thrusterDelay[index];
		bool	findInScan		= thrusterFindInScan[index];
		int		groupId			= thrusterGroupId[index];
		int		maximumPower	= thrusterMaximumPower[index];
		int		motorId			= thrusterMotorId[index];
		int		nodeId			= thrusterNodeId[index];
		bool	rotationReverse	= thrusterRotationReversed[index];
		QString	serialNumber	= thrusterSerialNumber[index];
		LOG_Debug(loggerEmulationData, QString("%1: delay=%2, findInScan=%3, groupId=%4, maximumPower=%5, motorId=%6, "
													"nodeId=%7, rotationReverse=%8, serialNumber=%9")
											.arg(index1).arg(delay).arg(findInScan).arg(groupId).arg(maximumPower)
											.arg(motorId).arg(nodeId).arg(rotationReverse).arg(serialNumber));
		// Save the thruster's data.
		iniFile.setArrayIndex(index);
		iniFile	.INI_SET(CfgEmulationThrusterDelay,				delay);
		iniFile	.INI_SET(CfgEmulationThrusterFindInScan,		findInScan);
		iniFile	.INI_SET(CfgEmulationThrusterGroupId,			groupId);
		iniFile	.INI_SET(CfgEmulationThrusterMaximumPower,		maximumPower);
		iniFile	.INI_SET(CfgEmulationThrusterMotorId,			motorId);
		iniFile	.INI_SET(CfgEmulationThrusterNodeId,			nodeId);
		iniFile	.INI_SET(CfgEmulationThrusterRotationReverse,	rotationReverse);
		iniFile	.INI_SET(CfgEmulationThrusterSerialNumber,		serialNumber);
	}
	iniFile.endArray();
	iniFile.syncAndCheck();
	QSettings::Status	status = iniFile.status();
	if (QSettings::NoError != status) {
		LOG_Error(loggerEmulationData, QString("Error writing to configuration file: %1").arg(status));
	}
}
