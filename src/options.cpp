/*! @file
@brief Define the program options module.

@note We can not use the normal logging macros because the logging module has not been configured yet.
*/
//######################################################################################################################

#include "app_com.h"

#include <iostream>
#include <string>

#include <boost/program_options.hpp>
#include <boost/program_options/parsers.hpp>

#include <log4cxx/basicconfigurator.h>
#include <log4cxx/consoleappender.h>
#include <log4cxx/fileappender.h>
#include <log4cxx/logger.h>
#include <log4cxx/patternlayout.h>
#include <log4cxx/propertyconfigurator.h>
#include <log4cxx/helpers/pool.h>
#include <log4cxx/xml/domconfigurator.h>

#include <QtCore/QAtomicInt>
#include <QtCore/QDebug>
#include <QtCore/QFileInfo>
#include <QtCore/QMutex>
#include <QtCore/QMutexLocker>
#include <QtCore/QStandardPaths>
#include <QtCore/QString>
#include <QtCore/QtDebug>
#include <QtWidgets/QApplication>

#include "configOptions.h"
#include "iniFile.h"
#include "options.h"
#include "options_log.h"
#include "programVersion.h"
#include "serialPortUtil.h"
#include "strUtil.h"
#include "thruster.h"
#include "timeUtil.h"

//######################################################################################################################

#ifndef OPTIONS_TRACE
	#define OPTIONS_TRACE	0	//!< @hideinitializer Enable trace messages.
#endif
#ifndef OPTIONS_DEBUG
	#define OPTIONS_DEBUG	0	//!< @hideinitializer Enable debug messages.
#endif
#ifndef OPTIONS_INFO
	#define OPTIONS_INFO	0	//!< @hideinitializer Enable informational messages.
#endif

//######################################################################################################################
// Special loggers.

LOG_DEF_ROOT(rootLogger);		//!< @hideinitializer Root logger.

//######################################################################################################################

namespace po = boost::program_options;

//######################################################################################################################
// Formatting Constants.

// Usage message.
static int const		UsageShortOptionsWidth	= 4;		//!< Width of short options text.
static int const		UsageLongOptionsWidth	= 21;		//!< Width of long options text.
static int const		UsageArgsWidth			= 7;		//!< Width of arguments text.

// Version message.
static int const		VersionCaptionWidth		= 10;		//!< Width of caption text.

static QString const	ApplicationNameFmt			= "Application name:    @{ApplicationName}";
static QString const	VersionFmt					= "Version:             @{VersionNumber} - @{VersionType}";
static QString const	BuiltFmt					= "Built:               @{VersionDate} @{VersionTime}";
static QString const	LastModifiedFmt				= "Last modified:       !{DateTime}";
static QString const	PathFmt						= "Path:                '@{ApplicationFilePath}'";
static QString const	ArgumentsFmt				= "Arguments:           %1";
static QString const	OsInformationFmt			= "OS information:      %1";
static QString const	KernelTypeFmt				= "Kernel type:         %1";
static QString const	KernelVersionFmt			= "Kernel version:      %1";
static QString const	AbiFmt						= "ABI:                 %1";
static QString const	OptionsHdr					= "Options:";
static QString const	OptionEmulateFmt			= "  Emulate:           %1";
static QString const	OptionLogConfigurationFmt	= "  log_configuration: '%1'";
static QString const	OptionMinimumLevelFmt		= "  minimum_level:     %1";
static QString const	OptionPortFmt				= "  port:              '%1'";
static QString const	OptionShowPortFmt			= "  Show Port:         %1";
static QString const	OptionShowStatusFmt			= "  Show Status:       %1";
static QString const	OptionSetupIniFmt			= "  setup_ini:         '%1'";
static QString const	OptionSetupLogFmt			= "  setup_log:         '%1'";
static QString const	SettingsFileFmt				= "Settings file:       '%1'";
static QString const	HomeLocationFmt				= "HomeLocation:        '%1'";
static QString const	TempLocationFmt				= "TempLocation:        '%1'";
static QString const	DocumentsLocationFmt		= "DocumentsLocation:   '%1'";
static QString const	RuntimeLocationFmt			= "RuntimeLocation:     '%1'";
static QString const	MultiLineDataFmt			= "    '%1'";
static QString const	AppConfigLocationHdr		= "AppConfigLocation:";
static QString const	AppLocalDataLocationHdr		= "AppLocalDataLocation:";
static QString const	AppDataLocationHdr			= "AppDataLocation:";
static QString const	ConfigLocationHdr			= "ConfigLocation:";
static QString const	GenericConfigLocationHdr	= "GenericConfigLocation:";
static QString const	GenericDataLocationHdr		= "GenericDataLocation:";
static QString const	ConfigLocationsHdr			= "Log configuration directories:";

//######################################################################################################################
// Debugging marcos

//----------------------------------------------------------------------------------------------------------------------
/*! @hideinitializer Log a trace message.

@param message	Message to log.
*/
#if OPTIONS_TRACE
	#define OPTIONS_TRACE_MESSAGE(message)	qDebug().noquote() << (message)
#else
	#define OPTIONS_TRACE_MESSAGE(message)	// Nothing
#endif

//----------------------------------------------------------------------------------------------------------------------
/*! @hideinitializer Log a debug message.

@param message	Message to log.
*/
#if OPTIONS_DEBUG
	#define OPTIONS_DEBUG_MESSAGE(message)	qDebug().noquote() << (message)
#else
	#define OPTIONS_DEBUG_MESSAGE(message)	// Nothing
#endif

//----------------------------------------------------------------------------------------------------------------------
/*! @hideinitializer Log a informational message.

@param message	Message to log.
*/
#if OPTIONS_INFO
	#define OPTIONS_INFO_MESSAGE(message)	qDebug().noquote() << (message)
#else
	#define OPTIONS_INFO_MESSAGE(message)	// Nothing
#endif

//######################################################################################################################
/*! @brief The OptionsPrivate class provides the program options module implementation.
*/
class OptionsPrivate
{
public:		// Constructors & destructors
	OptionsPrivate(void);
	~OptionsPrivate(void);

public:		// Functions
	void		checkForConflictingOptions(po::variables_map const &vm, char const *opt1, char const *opt2);
	void		configureConsoleLogging(void);
	void		configureLogging(void);
	QStringList	getConfigurationLocations(void);
	bool		isMinimumMessageLevel(QString const &level);
	QString		locateConfigurationFile(QString const &name);
	void		logInformation(void);
	void		logSetupInformation(void);
	void		parseCommandLine(int argc, char **argv);
	void		saveSerialPort(void);
	void		setSerialPort(void);
	void		showUsage(bool isVerbose);
	void		showVersion(void);

public:		// Data
	bool	isCommandLineParsed;		//!< Has the the command line been parsed?
	bool	isEmulation;				//!< Emulate the hardware.
	bool	isMinimumLevelFromCmdLine;	//!< Was the minimum message level set on the command line?
	bool	isSerialPortFromCmdLine;	//!< Was the serial port set on the command line?
	bool	isSetupMode;				//!< Is setup mode?
	bool	isShowOnScreenPort;			//!< Show on screen serial port?
	bool	isShowOnScreenStatus;		//!< Show on screen status?
	QString	logConfiguration;			//!< Template for the logging configuration file.
	QString	minimumLevel;				//!< Minimum message log level.
	QString	serialPort;					//!< Serial port name.
	QString	setupIniFile;				//!< INI file setup mode file path.
	QString	setupLogFile;				//!< Log control file setup mode file path.
	QString	userSettingsFileName;		//!< File/path of user's settings file.
};

//######################################################################################################################
// Command line and configuration file constants.

//! Default log configuration file name.
static QString const	DefaultLogConfigurationFile = "@{ApplicationName}.xml";

//######################################################################################################################
// log4cxx module values

//! Console logger appender object.
log4cxx::ConsoleAppender	*consoleAppender	= Q_NULLPTR;

//! Logger module pool object.
log4cxx::helpers::Pool		*pool				= Q_NULLPTR;

//######################################################################################################################
// Module global variables.

//! Implementation object.
static OptionsPrivate	*d						= Q_NULLPTR;

//! Implementation object critical section mutex.
static QMutex			dCriticalSection;

//! Implementation object reference count.
static QAtomicInt		dReferenceCount			= 0;

//######################################################################################################################
/*! @brief Create a Options object.
*/
Options::Options(void)
{
	OPTIONS_TRACE_MESSAGE("Creating Options");
	// Only a single instance is allowed.
	QMutexLocker	locker(&dCriticalSection);
	if (!d)
	{
		d = new (std::nothrow) OptionsPrivate;
		if (!d) {
			qCritical() << "Unable to allocate a OptionsPrivate object";
			exit(EXIT_FAILURE);
		}
	}
	++dReferenceCount;
}

//----------------------------------------------------------------------------------------------------------------------
/*! @brief Create a OptionsPrivate object.
*/
OptionsPrivate::OptionsPrivate(void)
:
	isCommandLineParsed			(false),
	isEmulation					(false),
	isMinimumLevelFromCmdLine	(false),
	isSerialPortFromCmdLine		(false),
	isSetupMode					(false),
	isShowOnScreenStatus		(false),
	logConfiguration			(),
	minimumLevel				(),
	serialPort					(),
	setupIniFile				(),
	setupLogFile				(),
	userSettingsFileName		()
{
	OPTIONS_TRACE_MESSAGE("Creating OptionsPrivate");
	// Load the default settings.
	IniFile	iniFile;
	userSettingsFileName = iniFile.fileName();
	isEmulation				= iniFile.INI_GET(CfgOptionsEmulate);
	isShowOnScreenPort		= iniFile.INI_GET(CfgOptionsShowPort);
	isShowOnScreenStatus	= iniFile.INI_GET(CfgOptionsShowStatus);
	logConfiguration		= iniFile.INI_GET(CfgOptionsConfigFile);
	minimumLevel			= iniFile.INI_GET(CfgOptionsMinMessageLvl);
	QString portName		= iniFile.INI_GET(CfgOptionsSerialPort);
	serialPort				= SerialPortUtil::portNameFromSystemLocation(portName);
	// Update the INI file with the simple serial port name if needed.
	if (serialPort != portName) {
		saveSerialPort();
	}
}

//======================================================================================================================
/*! @brief Destroy a Options object.
*/
Options::~Options(void)
{
	OPTIONS_TRACE_MESSAGE("Destroying Options");
	// Done if not last instance.
	QMutexLocker	locker(&dCriticalSection);
	if (0 < --dReferenceCount)
	{
		return;
	}
	// Cleanup.
	FREE_POINTER(d);
}

//----------------------------------------------------------------------------------------------------------------------
/*! @brief Destroy a OptionsPrivate object.
*/
OptionsPrivate::~OptionsPrivate(void)
{
	OPTIONS_TRACE_MESSAGE("Destroying OptionsPrivate");
}

//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
/*! @brief Check for conflicting command line options.
*/
void
OptionsPrivate::checkForConflictingOptions(
	po::variables_map const		&vm,	//!<[in] Variable map object.
	char const					*opt1,	//!<[in] First option name.
	char const					*opt2	//!<[in] Second option name.
)
{
	if (vm.count(opt1) && !vm[opt1].defaulted() && vm.count(opt2) && !vm[opt2].defaulted()) {
		throw std::logic_error(std::string("Conflicting options '") + opt1 + "' and '" + opt2 + "'.");
	}
}

//======================================================================================================================
/*! @brief Configure the logging module for logging to the console.
*/
void
OptionsPrivate::configureConsoleLogging(void)
{
	OPTIONS_TRACE_MESSAGE(__func__);
	OPTIONS_INFO_MESSAGE("Logging to the console");
	log4cxx::PatternLayout	*layout = new (std::nothrow) log4cxx::PatternLayout(LOG4CXX_STR("%m%n"));
	if (!layout) {
		qCritical() << "Unable to allocate a log4cxx::PatternLayout object";
		exit(EXIT_FAILURE);
	}
	consoleAppender = new (std::nothrow) log4cxx::ConsoleAppender(log4cxx::LayoutPtr(layout));
	if (!consoleAppender) {
		qCritical() << "Unable to allocate a log4cxx::ConsoleAppender object";
		exit(EXIT_FAILURE);
	}
	log4cxx::BasicConfigurator::configure(log4cxx::AppenderPtr(consoleAppender));
	//
	if (minimumLevel.isEmpty()) {
		if (isSetupMode) {
			minimumLevel = OptionValueLevelInfo;
		} else {
			minimumLevel = OptionValueLevelError;
		}
	}
	// Set the minimum log message level.
	if (isMinimumMessageLevel(OptionValueLevelTrace)) {
		rootLogger->setLevel(log4cxx::Level::getTrace());
	} else if (isMinimumMessageLevel(OptionValueLevelDebug)) {
		rootLogger->setLevel(log4cxx::Level::getDebug());
	} else if (isMinimumMessageLevel(OptionValueLevelInfo)) {
		rootLogger->setLevel(log4cxx::Level::getInfo());
	} else if (isMinimumMessageLevel(OptionValueLevelWarn)) {
		rootLogger->setLevel(log4cxx::Level::getWarn());
	} else if (isMinimumMessageLevel(OptionValueLevelError)) {
		rootLogger->setLevel(log4cxx::Level::getError());
	} else if (isMinimumMessageLevel(OptionValueLevelFatal)) {
		rootLogger->setLevel(log4cxx::Level::getFatal());
	} else {
		qCritical().noquote() << QString("Invalid minimum log message level: '%1'").arg(minimumLevel);
		exit(EXIT_FAILURE);
	}
	OPTIONS_DEBUG_MESSAGE(QString("Minimum log message level: %1").arg(minimumLevel));
}

//======================================================================================================================
/*! @brief Configure the logging module.
*/
void
OptionsPrivate::configureLogging(void)
{
	OPTIONS_TRACE_MESSAGE(__func__);
	// Locate the logging configuraton file if needed.
	if (!logConfiguration.isEmpty()) {
		QString		filePath = TimeUtil::populate(logConfiguration);
		QFileInfo	fileInfo(filePath);
		if (!fileInfo.exists() && !fileInfo.isAbsolute()) {
			QString foundFile = locateConfigurationFile(filePath);
			if (foundFile.isEmpty()) {
				qCritical().noquote() << QString("Unable to find log configuration file '%1'").arg(logConfiguration);
				exit(EXIT_FAILURE);
			}
			logConfiguration = foundFile;
		}
	}
	// If no logging arguments, attempt to locate the logging configuration file in standard locations.
	if (logConfiguration.isEmpty() && !isMinimumLevelFromCmdLine) {
		logConfiguration = locateConfigurationFile(TimeUtil::populate(DefaultLogConfigurationFile));
	}
	// Handle a log configuration file.
	if (!logConfiguration.isEmpty()) {
		if (!QFileInfo(logConfiguration).exists()) {
			qCritical().noquote() << QString("Log configuration file is missing (%1): '%2'")
											.arg(logConfiguration).arg(logConfiguration);
			exit(EXIT_FAILURE);
		}
		OPTIONS_INFO_MESSAGE(QString("Configuring logger from XML file: %1").arg(logConfiguration));
		log4cxx::xml::DOMConfigurator::configure(logConfiguration.toStdString());
		return;
	}
	// Log to the console if no log configuration file.
	configureConsoleLogging();
}

//======================================================================================================================
/*! @brief Get configuration locations.

@return List of configuration locations.
*/
QStringList
OptionsPrivate::getConfigurationLocations(void)
{
	QStringList answer;
	QStringList appConfigLocs = QStandardPaths::standardLocations(QStandardPaths::AppConfigLocation);
	answer.append(appConfigLocs);
	QStringList appLocalDataLocs = QStandardPaths::standardLocations(QStandardPaths::AppLocalDataLocation);
	answer.append(appLocalDataLocs);
	QStringList appDataLocs = QStandardPaths::standardLocations(QStandardPaths::AppDataLocation);
	answer.append(appDataLocs);
	QStringList configLocs = QStandardPaths::standardLocations(QStandardPaths::ConfigLocation);
	answer.append(configLocs);
	QStringList genericConfigLocs = QStandardPaths::standardLocations(QStandardPaths::GenericConfigLocation);
	answer.append(genericConfigLocs);
	QStringList genericDataLocs = QStandardPaths::standardLocations(QStandardPaths::GenericDataLocation);
	answer.append(genericDataLocs);
	// Ignore the number of removed duplicate items.
	(void)answer.removeDuplicates();
	return answer;
}

//======================================================================================================================
/*! @brief Query if the hardware should be emulated.

@return true if the hardware should be emulated.
*/
bool
Options::isEmulation(void)
{
	LOG_Trace(loggerEmulation, QString("Query if emulating hardware: %1").arg(d->isEmulation));
	return d->isEmulation;
}

//======================================================================================================================
/*! @brief Query if INI file setup mode is enabled.

@return true if INI file setup mode enabled.
*/
bool
Options::isSetupIni(
	QString	&filePath	//!<[out] File path.
)
{
	LOG_Trace(loggerSetup, QString("Query if INI file setup mode is enabled: '%1'").arg(d->setupIniFile));
	filePath = d->setupIniFile;
	return !d->setupIniFile.isEmpty();
}

//======================================================================================================================
/*! @brief Query if log control file setup mode is enabled.

@return true if log control file setup mode enabled.
*/
bool
Options::isSetupLog(
	QString	&filePath	//!<[out] File path.
)
{
	LOG_Trace(loggerSetup, QString("Query if log control file setup mode is enabled: '%1'").arg(d->setupLogFile));
	filePath = d->setupLogFile;
	return !d->setupLogFile.isEmpty();
}

//======================================================================================================================
/*! @brief Determine if @a minimumLevel is equal to @a level.

The comparision is case insensitive.

@return true if @a minimumLevel is equal to @a level.
*/
bool
OptionsPrivate::isMinimumMessageLevel(
	QString const	&level	//!<[in] Name of level.
)
{
	return !QString::compare(minimumLevel, level, Qt::CaseInsensitive);
}

//======================================================================================================================
/*! @brief Query if showing on screen serial port.

@return true if showing on screen status.
*/
bool
Options::isShowOnScreenPort(void)
{
	LOG_Trace(loggerEmulation, QString("Query if showing on screen serial port: %1").arg(d->isShowOnScreenStatus));
	return d->isShowOnScreenPort;
}

//======================================================================================================================
/*! @brief Query if showing on screen status.

@return true if showing on screen status.
*/
bool
Options::isShowOnScreenStatus(void)
{
	LOG_Trace(loggerEmulation, QString("Query if showing on screen status: %1").arg(d->isShowOnScreenStatus));
	return d->isShowOnScreenStatus;
}

//======================================================================================================================
/*! @brief Attempt to locate a configuration file.

@return Path/name of configuration file if found else an empty string.
*/
QString
OptionsPrivate::locateConfigurationFile(
	QString const	&name	//!<[in] Name of confirgation file.
)
{
	OPTIONS_TRACE_MESSAGE(QString("%1: %2").arg(__func__).arg(name));
	QString answer = QStandardPaths::locate(QStandardPaths::AppConfigLocation, name, QStandardPaths::LocateFile);
	if (answer.isEmpty()) {
		answer = QStandardPaths::locate(QStandardPaths::AppLocalDataLocation, name, QStandardPaths::LocateFile);
	}
	if (answer.isEmpty()) {
		answer = QStandardPaths::locate(QStandardPaths::AppDataLocation, name, QStandardPaths::LocateFile);
	}
	if (answer.isEmpty()) {
		answer = QStandardPaths::locate(QStandardPaths::ConfigLocation, name, QStandardPaths::LocateFile);
	}
	if (answer.isEmpty()) {
		answer = QStandardPaths::locate(QStandardPaths::GenericConfigLocation, name, QStandardPaths::LocateFile);
	}
	if (answer.isEmpty()) {
		answer = QStandardPaths::locate(QStandardPaths::GenericDataLocation, name, QStandardPaths::LocateFile);
	}
	return answer;
}

//======================================================================================================================
/*! @brief Log useful debugging information.
*/
void
OptionsPrivate::logInformation(void)
{
	LOG_Trace(loggerEnvironment, StrUtil::populate(ApplicationNameFmt));
	LOG_Trace(loggerEnvironment, StrUtil::populate(VersionFmt));
	LOG_Trace(loggerEnvironment, StrUtil::populate(BuiltFmt));
	LOG_Trace(loggerEnvironment, TimeUtil::populate(LastModifiedFmt,
													QFileInfo(QCoreApplication::applicationFilePath())
															.lastModified().toUTC()));
	LOG_Trace(loggerEnvironment, StrUtil::populate(PathFmt));
	LOG_Trace(loggerEnvironment, QString(ArgumentsFmt).arg(QCoreApplication::arguments().mid(1).join(" ")));
	LOG_Trace(loggerEnvironment, QString(OsInformationFmt).arg(QSysInfo::prettyProductName()));
	LOG_Trace(loggerEnvironment, QString(KernelTypeFmt).arg(QSysInfo::kernelType()));
	LOG_Trace(loggerEnvironment, QString(KernelVersionFmt).arg(QSysInfo::kernelVersion()));
	LOG_Trace(loggerEnvironment, QString(AbiFmt).arg(QSysInfo::buildAbi()));

	LOG_Trace(loggerEnvironment, OptionsHdr);
	LOG_Trace(loggerEnvironment, QString(OptionEmulateFmt).arg(isEmulation));
	LOG_Trace(loggerEnvironment, QString(OptionLogConfigurationFmt).arg(logConfiguration));
	LOG_Trace(loggerEnvironment, QString(OptionMinimumLevelFmt).arg(logConfiguration.isEmpty() ? minimumLevel : "N/A"));
	LOG_Trace(loggerEnvironment, QString(OptionPortFmt).arg(serialPort));
	LOG_Trace(loggerEnvironment, QString(OptionShowPortFmt).arg(isShowOnScreenPort));
	LOG_Trace(loggerEnvironment, QString(OptionShowStatusFmt).arg(isShowOnScreenStatus));
	LOG_Trace(loggerEnvironment, QString(OptionSetupIniFmt).arg(setupIniFile));
	LOG_Trace(loggerEnvironment, QString(OptionSetupLogFmt).arg(setupLogFile));

	LOG_Trace(loggerEnvironment, QString(SettingsFileFmt).arg(userSettingsFileName));

	QStringList pathList;
	pathList = QStandardPaths::standardLocations(QStandardPaths::HomeLocation);
	LOG_Trace(loggerEnvironment, QString(HomeLocationFmt).arg(pathList.join("', '")));
	pathList = QStandardPaths::standardLocations(QStandardPaths::TempLocation);
	LOG_Trace(loggerEnvironment, QString(TempLocationFmt).arg(pathList.join("', '")));
	pathList = QStandardPaths::standardLocations(QStandardPaths::DocumentsLocation);
	LOG_Trace(loggerEnvironment, QString(DocumentsLocationFmt).arg(pathList.join("', '")));
	pathList = QStandardPaths::standardLocations(QStandardPaths::RuntimeLocation);
	LOG_Trace(loggerEnvironment, QString(RuntimeLocationFmt).arg(pathList.join("', '")));

	QStringList logConfigLocs;
	QStringList appConfigLocs = QStandardPaths::standardLocations(QStandardPaths::AppConfigLocation);
	logConfigLocs.append(appConfigLocs);
	QStringList appLocalDataLocs = QStandardPaths::standardLocations(QStandardPaths::AppLocalDataLocation);
	logConfigLocs.append(appLocalDataLocs);
	QStringList appDataLocs = QStandardPaths::standardLocations(QStandardPaths::AppDataLocation);
	logConfigLocs.append(appDataLocs);
	QStringList configLocs = QStandardPaths::standardLocations(QStandardPaths::ConfigLocation);
	logConfigLocs.append(configLocs);
	QStringList genericConfigLocs = QStandardPaths::standardLocations(QStandardPaths::GenericConfigLocation);
	logConfigLocs.append(genericConfigLocs);
	QStringList genericDataLocs = QStandardPaths::standardLocations(QStandardPaths::GenericDataLocation);
	logConfigLocs.append(genericDataLocs);
	// Ignore the number of removed items.
	(void)logConfigLocs.removeDuplicates();
	LOG_Trace(loggerEnvironment, ConfigLocationsHdr);
	for (QString path : logConfigLocs) {
		LOG_Trace(loggerEnvironment, QString(MultiLineDataFmt).arg(path));
	}
	//
	LOG_Trace(loggerEnvironment, AppConfigLocationHdr);
	for (QString path : appConfigLocs) {
		LOG_Trace(loggerEnvironment, QString(MultiLineDataFmt).arg(path));
	}
	LOG_Trace(loggerEnvironment, AppDataLocationHdr);
	for (QString path : appLocalDataLocs) {
		LOG_Trace(loggerEnvironment, QString(MultiLineDataFmt).arg(path));
	}
	LOG_Trace(loggerEnvironment, AppDataLocationHdr);
	for (QString path : appDataLocs) {
		LOG_Trace(loggerEnvironment, QString(MultiLineDataFmt).arg(path));
	}
	LOG_Trace(loggerEnvironment, ConfigLocationHdr);
	for (QString path : configLocs) {
		LOG_Trace(loggerEnvironment, QString(MultiLineDataFmt).arg(path));
	}
	LOG_Trace(loggerEnvironment, GenericConfigLocationHdr);
	for (QString path : genericConfigLocs) {
		LOG_Trace(loggerEnvironment, QString(MultiLineDataFmt).arg(path));
	}
	LOG_Trace(loggerEnvironment, GenericDataLocationHdr);
	for (QString path : genericDataLocs) {
		LOG_Trace(loggerEnvironment, QString(MultiLineDataFmt).arg(path));
	}
}

//======================================================================================================================
/*! @brief Log useful setup information.
*/
void
OptionsPrivate::logSetupInformation(void)
{
	LOG_Info(loggerEnvironment, StrUtil::populate(VersionFmt));
	LOG_Info(loggerEnvironment, StrUtil::populate(BuiltFmt));
	LOG_Info(loggerEnvironment, QString(SettingsFileFmt).arg(userSettingsFileName));
	//
	LOG_Info(loggerEnvironment, ConfigLocationsHdr);
	QStringList logConfigLocs = getConfigurationLocations();
	for (QString path : logConfigLocs) {
		LOG_Info(loggerEnvironment, QString(MultiLineDataFmt).arg(path));
	}
}

//======================================================================================================================
/*! @brief Parse the command line arguements.
*/
void
Options::parseCommandLine(
	int		argc,	//!<[in] Number of arguments.
	char	**argv	//!<[in] Array of arguments.
)
{
	OPTIONS_TRACE_MESSAGE("Parsing the command line");
	// Error if the command line has already been parsed.
	if (d->isCommandLineParsed) {
		LOG_FATAL(logger, "Command line already parsed");
	}
	//
	d->parseCommandLine(argc, argv);
	d->isCommandLineParsed = true;
	if (d->isSetupMode) {
		d->configureConsoleLogging();
		d->logSetupInformation();
	} else {
		d->configureLogging();
		d->logInformation();
		d->setSerialPort();
	}
}

//----------------------------------------------------------------------------------------------------------------------
/*! @brief Parse the command line arguements.
*/
void
OptionsPrivate::parseCommandLine(
	int		argc,	//!<[in] Number of arguments.
	char	**argv	//!<[in] Array of arguments.
)
{
	OPTIONS_TRACE_MESSAGE(__func__);
	// Define the parameters.
	std::string					emulateValue;
	std::string					logConfiguratonValue;
	std::string					minimumLevelValue;
	std::string					portValue;
	std::string					setupIniValue;
	std::string					setupLogValue;
	po::options_description		desc("Options");
	desc.add_options()
		(CFG_OPT_LONG_NAME_HELP					"," CFG_OPT_SHORT_NAME_HELP,
				"show this help message")
		(CFG_OPT_LONG_NAME_VERSION				"," CFG_OPT_SHORT_NAME_VERSION,
				"show version information")
		(CFG_OPT_LONG_NAME_EMULATION			"," CFG_OPT_SHORT_NAME_EMULATION,
				po::value(&emulateValue)->implicit_value("true"),
				"emulate hardware")
		(CFG_OPT_LONG_NAME_LOG_CONFIGURATION	"," CFG_OPT_SHORT_NAME_LOG_CONFIGURATION,
				po::value(&logConfiguratonValue),
				"log configuration file")
		(CFG_OPT_LONG_NAME_MINIMUM_LEVEL		"," CFG_OPT_SHORT_NAME_MINIMUM_LEVEL,
				po::value(&minimumLevelValue),
				"minimum log message level")
		(CFG_OPT_LONG_NAME_PORT					"," CFG_OPT_SHORT_NAME_PORT,
				po::value(&portValue),
				"serial port")
		(CFG_OPT_LONG_NAME_SETUP_INI,
				po::value(&setupIniValue),
				"generate tempate INI file")
		(CFG_OPT_LONG_NAME_SETUP_LOG,
				po::value(&setupLogValue),
				"generate template log control file")
		(CFG_OPT_LONG_SHOW_PORT,
				"show on screen status")
		(CFG_OPT_LONG_SHOW_STATUS,
				"show on screen status")
		;

	// Parse the coomand line.
	try {
		po::variables_map	vm;
		po::parsed_options	parsed = po::basic_command_line_parser<char>(argc, argv).options(desc).run();
		po::store(parsed, vm);
		po::notify(vm);
		/* Special options.
		*/
		bool isDone = false;
		if (vm.count(CFG_OPT_LONG_NAME_VERSION)) {
			isDone = true;
			showVersion();
		}
		if (vm.count(CFG_OPT_LONG_NAME_HELP)) {
			isDone = true;
			showUsage(true);
		}
		if (isDone) {
			exit(EXIT_SUCCESS);
		}
		/* Log options.
		*/
		checkForConflictingOptions(vm, CFG_OPT_LONG_NAME_LOG_CONFIGURATION, CFG_OPT_LONG_NAME_MINIMUM_LEVEL);
		if (vm.count(CFG_OPT_LONG_NAME_LOG_CONFIGURATION)) {
			logConfiguration = QString::fromStdString(logConfiguratonValue);
			OPTIONS_DEBUG_MESSAGE(QString(CFG_OPT_LONG_NAME_LOG_CONFIGURATION ": '%1'").arg(logConfiguration));
		}
		if (vm.count(CFG_OPT_LONG_NAME_MINIMUM_LEVEL)) {
			isMinimumLevelFromCmdLine = true;
			minimumLevel = QString::fromStdString(minimumLevelValue);
			OPTIONS_DEBUG_MESSAGE(QString(CFG_OPT_LONG_NAME_MINIMUM_LEVEL ": %1").arg(minimumLevel));
		}
		/* Setup options.
		*/
		if (vm.count(CFG_OPT_LONG_NAME_SETUP_INI)) {
			isSetupMode = true;
			setupIniFile = QString::fromStdString(setupIniValue);
			OPTIONS_DEBUG_MESSAGE(QString(CFG_OPT_LONG_NAME_SETUP_INI ": %1").arg(setupIniFile));
		}
		if (vm.count(CFG_OPT_LONG_NAME_SETUP_LOG)) {
			isSetupMode = true;
			setupLogFile = QString::fromStdString(setupLogValue);
			OPTIONS_DEBUG_MESSAGE(QString(CFG_OPT_LONG_NAME_SETUP_LOG ": %1").arg(setupLogFile));
		}
		if (isSetupMode) {
			return;
		}
		/* Miscellaneous options.
		*/
		if (vm.count(CFG_OPT_LONG_NAME_EMULATION)) {
			isEmulation = QVariant(QString::fromStdString(emulateValue)).toBool();
			OPTIONS_DEBUG_MESSAGE(QString(CFG_OPT_LONG_NAME_EMULATION ": %1").arg(isEmulation));
		}
		if (vm.count(CFG_OPT_LONG_NAME_PORT)) {
			isSerialPortFromCmdLine = true;
			serialPort = SerialPortUtil::portNameFromSystemLocation(QString::fromStdString(portValue));
			OPTIONS_DEBUG_MESSAGE(QString(CFG_OPT_LONG_NAME_PORT ": %1").arg(serialPort));
		}
		if (vm.count(CFG_OPT_LONG_SHOW_PORT)) {
			isShowOnScreenPort = !isShowOnScreenPort;
		}
		if (vm.count(CFG_OPT_LONG_SHOW_STATUS)) {
			isShowOnScreenStatus = !isShowOnScreenStatus;
		}
	}
	catch(std::exception &e) {
		std::cerr << e.what() << std::endl;
		showUsage(false);
		exit(EXIT_FAILURE);
	}
}

//======================================================================================================================
/*! @brief Save the current serial port name.
*/
void
OptionsPrivate::saveSerialPort(void)
{
	LOG_Trace(loggerSerialPort, QString("Saving serial port as '%1'").arg(serialPort));
	IniFile	iniFile;
	iniFile.INI_SET(CfgOptionsSerialPort, serialPort);
	iniFile.syncAndCheck();
}

//======================================================================================================================
/*! @brief Obtain the current serial port name.

@return the current serial port name.
*/
QString
Options::serialPort(void)
{
	LOG_Trace(loggerSerialPort, QString("Query serial port: '%1'").arg(d->serialPort));
	return d->serialPort;
}

//======================================================================================================================
/*! @brief Set the current serial port name.
*/
void
Options::serialPort(
	QString const	&port	//!<[in] New serial port name.
)
{
	LOG_Trace(loggerSerialPort, QString("Set serial port to '%1'").arg(port));
	d->serialPort = port;
	d->saveSerialPort();
}

//======================================================================================================================
/*! @brief Set the serial port.
*/
void
OptionsPrivate::setSerialPort(void)
{
	LOG_Trace(loggerSerialPort, "Setting the active serial port");
	// Handle serial port from the command line.
	if (d->isSerialPortFromCmdLine) {
		// Ensure the serial port is valid.
		if (!Thruster::isSerialPortValid(d->serialPort)) {
			LOG_Fatal(logger, QString("Invalid serial port: %1").arg(d->serialPort));
			exit(EXIT_FAILURE);
		}
		// Update the saved value.
		d->saveSerialPort();
	}
	// Use the default serial port if none given on the command line and no valid prior one.
	if (serialPort.isEmpty() || !Thruster::isSerialPortValid(serialPort)) {
		QStringList availablePorts = Thruster::serialPortsAvailable();
		if (availablePorts.empty()) {
			LOG_FATAL(logger, "No available serial ports");
		}
		serialPort = availablePorts.first();
		saveSerialPort();
	}
}

//======================================================================================================================
/*! @brief Display the usage instructions.
*/
void
OptionsPrivate::showUsage(
	bool	isVerbose	//!<[in] Should the message be verbose?
)
{
	OPTIONS_TRACE_MESSAGE(__func__);
	std::cout	<<	"\n"
					"Usage: " << QCoreApplication::applicationName().toStdString() << " [options]\n"
					"\n";
	//
	std::cout.width(UsageShortOptionsWidth + UsageLongOptionsWidth);
	std::cout << std::left << "Option";
	std::cout.width(UsageArgsWidth);
	std::cout << std::left << "Arg" << "Description\n";
	//
	std::cout.width(UsageShortOptionsWidth);
	std::cout << "-" CFG_OPT_SHORT_NAME_HELP ",";
	std::cout.width(UsageLongOptionsWidth);
	std::cout << "--" CFG_OPT_LONG_NAME_HELP;
	std::cout.width(UsageArgsWidth);
	std::cout << "" << "Show this help message\n";
	//
	std::cout.width(UsageShortOptionsWidth);
	std::cout << "-" CFG_OPT_SHORT_NAME_VERSION ",";
	std::cout.width(UsageLongOptionsWidth);
	std::cout << "--" CFG_OPT_LONG_NAME_VERSION;
	std::cout.width(UsageArgsWidth);
	std::cout << "" << "Show version information\n";
	//
	std::cout.width(UsageShortOptionsWidth);
	std::cout << "-" CFG_OPT_SHORT_NAME_PORT ",";
	std::cout.width(UsageLongOptionsWidth);
	std::cout << "--" CFG_OPT_LONG_NAME_PORT;
	std::cout.width(UsageArgsWidth);
	std::cout << "PORT" << "Name of serial port\n";
	//
	std::cout.width(UsageShortOptionsWidth);
	std::cout << "-" CFG_OPT_SHORT_NAME_LOG_CONFIGURATION ",";
	std::cout.width(UsageLongOptionsWidth);
	std::cout << "--" CFG_OPT_LONG_NAME_LOG_CONFIGURATION;
	std::cout.width(UsageArgsWidth);
	std::cout << "FILE" << "Log configuration\n";
	//
	std::cout.width(UsageShortOptionsWidth);
	std::cout << "-" CFG_OPT_SHORT_NAME_MINIMUM_LEVEL ",";
	std::cout.width(UsageLongOptionsWidth);
	std::cout << "--" CFG_OPT_LONG_NAME_MINIMUM_LEVEL;
	std::cout.width(UsageArgsWidth);
	std::cout << "LEVEL" << "Minimum log message level\n";
	//
	std::cout.width(UsageShortOptionsWidth);
	std::cout << "";
	std::cout.width(UsageLongOptionsWidth);
	std::cout << "--" CFG_OPT_LONG_NAME_SETUP_INI;
	std::cout.width(UsageArgsWidth);
	std::cout << "FILE" << "Generate template INI file\n";
	//
	std::cout.width(UsageShortOptionsWidth);
	std::cout << "";
	std::cout.width(UsageLongOptionsWidth);
	std::cout << "--" CFG_OPT_LONG_NAME_SETUP_LOG;
	std::cout.width(UsageArgsWidth);
	std::cout << "FILE" << "Generate template log configuration\n";
	//
	std::cout.width(UsageShortOptionsWidth);
	std::cout << "";
	std::cout.width(UsageLongOptionsWidth);
	std::cout << "--" CFG_OPT_LONG_SHOW_PORT;
	std::cout.width(UsageArgsWidth);
	std::cout << "" << "Show on screen serial port\n";
	//
	std::cout.width(UsageShortOptionsWidth);
	std::cout << "";
	std::cout.width(UsageLongOptionsWidth);
	std::cout << "--" CFG_OPT_LONG_SHOW_STATUS;
	std::cout.width(UsageArgsWidth);
	std::cout << "" << "Show on screen status\n";
	//
	std::cout.width(UsageShortOptionsWidth);
	std::cout << "-" CFG_OPT_SHORT_NAME_EMULATION ",";
	std::cout.width(UsageLongOptionsWidth);
	std::cout << "--" CFG_OPT_LONG_NAME_EMULATION;
	std::cout.width(UsageArgsWidth);
	std::cout << "BOOL" << "Emulate hardware\n";
	//
	if (isVerbose) {
		std::cout	<<	"\n"
						"Locations searched for log configuration file:\n";
		//
		QStringList logConfigLocs = getConfigurationLocations();
		for (QString path : logConfigLocs) {
			std::cout << path.toLatin1().constData() << "\n";
		}
	}
	std::cout << std::endl;
}

//======================================================================================================================
/*! @brief Display the program version information.
*/
void
OptionsPrivate::showVersion(void)
{
	OPTIONS_TRACE_MESSAGE(__func__);
	std::cout.width(VersionCaptionWidth);
	std::cout	<< std::left << "Program:"	<< QCoreApplication::applicationName().toStdString() << '\n';
	std::cout.width(VersionCaptionWidth);
	std::cout	<< std::left << "Version:"	<< QCoreApplication::applicationVersion().toStdString() << '\n';
	std::cout.width(VersionCaptionWidth);
	std::cout	<< std::left << "Built:"	<< VersionInfomation::programVersion.date << ' '
											<< VersionInfomation::programVersion.time << '\n';
	std::cout.width(VersionCaptionWidth);
	std::cout	<< std::left << "Path:"		<< QCoreApplication::applicationFilePath().toStdString()
											<< std::endl;
}

//======================================================================================================================
/*! @brief Obtain the path/name of the user's settings file.

@return the path/name of the user's settings file.
*/
QString
Options::userSettingsFileName(void)
{
	return d->userSettingsFileName;
}
