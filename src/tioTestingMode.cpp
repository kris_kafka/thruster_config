/*! @file
@brief Define the thruster I/O module.
*/
//######################################################################################################################

#include "app_com.h"

#include <cstddef>
#include <string>

#include <QtCore/QSettings>

#include "videoray/applib/std_device_types.h"
#include "videoray/applib/gizmos/motor_controller/specific_response.h"

#include "configCsr.h"
#include "configGio.h"
#include "configThruster.h"
#include "csrBlob.h"
#include "thruster.h"
#include "tioTestingMode.h"
#include "tioTestingMode_log.h"
#include "tioImpl.h"
#include "tioParms.h"

//######################################################################################################################
// Local classes.

class	TioImplTestingMode;
class	TioParmsPropulsion;
class	TioParmsSpeedCheck;
class	TioParmsSpeedSet;
class	TioParmsStatus;

//######################################################################################################################
// Local functions

static void		propulsionReply(TioParms *biop, Protocol_VRCSR_Packet const &packet, int deviceTypeSize);
static void		speedCheckReply(TioParms *biop, Protocol_VRCSR_Packet const &packet, int deviceTypeSize);
static void		speedSetReply(TioParms *biop, Protocol_VRCSR_Packet const &packet, int deviceTypeSize);
static void		statusReply(TioParms *biop, Protocol_VRCSR_Packet const &packet, int deviceTypeSize);

//######################################################################################################################
/*! \brief Read Configuration CSR data implementation.
*/
class TioImplTestingMode
:
	public	TioImpl
{
public:		// Constructors & destructors
				TioImplTestingMode(std::string &portName, int nodeId, bool const volatile &halt);
	virtual		~TioImplTestingMode(void);

public:		// Functions
	Thruster::Error		propulsionSet(int nodeId, int motorId, int id, double speed, double &rpm,
										double &voltage,
										double &current, double &temperature, uint &faults);
	Thruster::Error		speedCheck(int nodeId, double speed);
	Thruster::Error		speedSet(int nodeId, bool isReply, double speed);
	Thruster::Error		statusGet(int nodeId, double &rpm, double &voltage, double &current, double &temperature,
									uint &faults);
};

//======================================================================================================================
/*! \brief Testing mode (propulsion) I/O parameters.
*/
class TioParmsPropulsion
:
	public	TioParms
{
public:		// Constructors & destructors
	TioParmsPropulsion(int nodeId, int motorId, int id, double speed, double &rpm_, double &voltage_,
					double &current_, double &temperature_, uint &faults_);

public:		// Data
	double	&rpm;			//!< RPM from the thruster.
	double	&voltage;		//!< Bus voltage from the thruster.
	double	&current;		//!< Bus current from the thruster.
	double	&temperature;	//!< Temperature from the thruster.
	uint	&faults;		//!< Fault flags from the thruster.
};

//======================================================================================================================
/*! \brief Testing mode (CSR) speed check I/O parameters.
*/
class TioParmsSpeedCheck
:
	public	TioParms
{
public:		// Constructors & destructors
	TioParmsSpeedCheck(int nodeId, double speed);

public:		// Data
	PowerTarget_t	powerTarget;		//!< Power target.
	int				powerTargetOffset;	//!< CSR offset of powerTarget.
	CsrInfo::Type	powerTargetType;	//!< CSR type of powerTarget.
};

//======================================================================================================================
/*! \brief Testing mode (CSR) speed set I/O parameters.
*/
class TioParmsSpeedSet
:
	public	TioParms
{
public:		// Constructors & destructors
	TioParmsSpeedSet(int nodeId, bool isReply, double speed);

public:		// Data
	PowerTarget_t	powerTarget;		//!< Power target.
	int				powerTargetOffset;	//!< CSR offset of powerTarget.
	CsrInfo::Type	powerTargetType;	//!< CSR type of powerTarget.
};

//======================================================================================================================
/*! \brief Testing mode (CSR) status I/O parameters.
*/
class TioParmsStatus
:
	public	TioParms
{
public:		// Constructors & destructors
	TioParmsStatus(int nodeId, double &rpm_, double &voltage, double &current, double &temperature_, uint &faults);

public:		// Data
	double			&busCurrent;		//!< Bus current from the thruster.
	int				busCurrentOffset;	//!< CSR offset of current.
	CsrInfo::Type	busCurrentType;		//!< CSR type of current.
	double			&busVoltage;		//!< Bus voltage from the thruster.
	int				busVoltageOffset;	//!< CSR offset of voltage.
	CsrInfo::Type	busVoltageType;		//!< CSR type of voltage.
	uint			&faultFlags;		//!< Fault flags from the thruster.
	int				faultFlagsOffset;	//!< CSR offset of faults.
	CsrInfo::Type	faultFlagsType;		//!< CSR type of faults.
	double			&rpm;				//!< RPM from the thruster.
	int				rpmOffset;			//!< CSR offset of rpm.
	CsrInfo::Type	rpmType;			//!< CSR type of rpm.
	double			&temperature;		//!< Temperature from the thruster.
	int				temperatureOffset;	//!< CSR offset of temperature.
	CsrInfo::Type	temperatureType;	//!< CSR type of temperature.
};

//######################################################################################################################
/*! @brief Create a TioTestingMode object.
*/
TioTestingMode::TioTestingMode(
	QString const		&serialPort,	//!<[in] Serial port name.
	int					nodeId,			//!<[in] Node ID.
	bool const volatile	&halt			//!<[in] Halt I/O.
)
:
	TioBase		(serialPort, nodeId, halt),
	connection	(Q_NULLPTR)
{
	LOG_Trace(loggerCreate,
				QString("Creating TioTestingMode object: serialPort='%1', nodeId=%2").arg(serialPort).arg(nodeId));
}

//======================================================================================================================
/*! @brief Destroy a TioTestingMode object.
*/
TioTestingMode::~TioTestingMode(void)
{
	LOG_Trace(loggerCreate,
				QString("Destroying TioTestingMode object: serialPort='%1', nodeId=%2").arg(serialPort).arg(nodeId));
}

//======================================================================================================================
/*! @brief Create a TioImplTestingMode object.
*/
TioImplTestingMode::TioImplTestingMode(
	std::string			&portName,	//!<[in] Serial port name.
	int					nodeId,		//!<[in] Node ID.
	bool const volatile	&halt		//!<[in] Halt I/O.
)
:
	TioImpl	(portName, nodeId, halt)
{
	LOG_Trace(loggerCreate,
				QString("Creating TioImplTestingMode object: serialPort='%1', nodeId=%2").arg(serialPort).arg(nodeId));
}

//======================================================================================================================
/*! @brief Destroy a TioImplTestingMode object.
*/
TioImplTestingMode::~TioImplTestingMode(void)
{
	LOG_Trace(loggerCreate,
				QString("Destroying TioImplTestingMode object: serialPort='%1', nodeId=%2").arg(serialPort).arg(nodeId));
}

//======================================================================================================================
/*! @brief Create a TioParmsPropulsion object.
*/
TioParmsPropulsion::TioParmsPropulsion(
	int		nodeId,			//!<[in]  Node ID of thruster.
	int		motorId,		//!<[in]  Motor ID of thruster.
	int		id,				//!<[in]  Command ID of thruster.
	double	speed,			//!<[in]  Speed of the thruster.
	double	&rpm_,			//!<[out] RPM from the thruster.
	double	&voltage_,		//!<[out] Bus voltage from the thruster.
	double	&current_,		//!<[out] Bus current from the thruster.
	double	&temperature_,	//!<[out] Temperature from the thruster.
	uint	&faults_		//!<[out] Faults from the thruster.
)
:
	TioParms		(propulsionReply, "TestMode Propulsion"),
	rpm				(rpm_),
	voltage			(voltage_),
	current			(current_),
	temperature		(temperature_),
	faults			(faults_)
{
	LOG_Trace(loggerPropulsionParameters,
				QString("Creating TioParmsPropulsion object: nodeId=%1, motorId=%2, id=%3, speed=%4")
						.arg(nodeId).arg(motorId).arg(id).arg(speed));
	//
	IniFile	iniFile;
	initialize(iniFile, INI_GROUP(CfgGioTstMdPropulsion));
	//
	replyValidAddress	= ADDR_CUSTOM_COMMAND;
	replyValidLength	= sizeof(BusAmps_t) + sizeof(BusVolts_t) + sizeof(Rpm_t) + sizeof(Temperature_t)
						+ sizeof(FaultFlags1_t);
	replyValidFlags		= RESPONSE_THRUSTER_STANDARD;
	replyValidNodeId	= nodeId;
	//
	PowerTarget_t	cmdSpeed = speed;
	QByteArray	payload(2 + motorId * sizeof(cmdSpeed), '\0');
	payload[0] = (Byte)PROPULSION_COMMAND;
	payload[1] = nodeId;
	payload.append((char *)&cmdSpeed, sizeof(cmdSpeed));
	commandSize = protocol_vrcsr_calc_packet_size(payload.size(), false, PROTOCOL_VRCSR);
	ByteP	packetBuffer = new (std::nothrow) Byte[commandSize];
	LOG_CHK_PTR_MSG(loggerPropulsionParameters, packetBuffer, "Unable to create TioParmsPropulsion packetBuffer");
	commandBuffer.reset(packetBuffer);
	(void)protocol_vrcsr_insert_data((ByteP)payload.data(),
										payload.size(),
										false,
										commandBuffer.get(),
										PROTOCOL_VRCSR);
	(void)protocol_vrcsr_build_request_inplace(id,
												0,
												replyValidAddress,
												payload.size(),
												commandBuffer.get(),
												PROTOCOL_VRCSR);
}

//======================================================================================================================
/*! @brief Create a TioParmsSpeedCheck object.
*/
TioParmsSpeedCheck::TioParmsSpeedCheck(
	int		nodeId,		//!<[in] Node ID of thruster.
	double	speed		//!<[in] Speed of thruster.
)
:
	TioParms			(speedCheckReply, "TestMode SpeedCheck"),
	powerTarget			(speed),
	powerTargetOffset	(0),
	powerTargetType		(CsrInfo::Type::Bad)
{
	LOG_Trace(loggerSpeedCheckParameters,
				QString("Creating TioParmsSpeedCheck object: nodeId=%1, speed=%2").arg(nodeId).arg(speed));
	//
	IniFile	iniFile;
	initialize(iniFile, INI_GROUP(CfgGioTstMdChkSpeed));
	//
	powerTargetOffset	= iniFile.INI_GET(CfgCsrPowerTargetOffset);
	powerTargetType		= CsrInfo::type(iniFile.INI_GET(CfgCsrPowerTargetType));
	//
	replyValidAddress	= powerTargetOffset;
	int lastCsrAddress	= powerTargetOffset + CsrInfo::size(powerTargetType);
	replyValidLength	= lastCsrAddress - replyValidAddress;
	replyValidFlags		= RESPONSE_TYPE_FLAG;
	replyValidNodeId	= nodeId;
	//
	LOG_AssertMsg(loggerSpeedSetParameters, 0 < replyValidLength,
					QString("Invalid reply length").arg(replyValidLength));
	//
	commandSize = protocol_vrcsr_calc_packet_size(0, false, PROTOCOL_VRCSR);
	ByteP	packetBuffer = new (std::nothrow) Byte[commandSize];
	LOG_CHK_PTR_MSG(loggerSpeedCheckParameters, packetBuffer, "Unable to create  TioParmsSpeedCheck packetBuffer");
	commandBuffer.reset(packetBuffer);
	(void)protocol_vrcsr_build_request_inplace(nodeId,
												REQUEST_LEN(replyValidLength),
												replyValidAddress,
												0,
												commandBuffer.get(),
												PROTOCOL_VRCSR);
}

//======================================================================================================================
/*! @brief Create a TioParmsSpeedSet object.
*/
TioParmsSpeedSet::TioParmsSpeedSet(
	int		nodeId,		//!<[in] Node ID of thruster.
	bool	isReply,	//!<[in] Is a reply expected.
	double	speed		//!<[in] Speed of thruster.
)
:
	TioParms			(speedSetReply, "TestMode SpeedSet"),
	powerTarget			(speed),
	powerTargetOffset	(0),
	powerTargetType		(CsrInfo::Type::Bad)
{
	LOG_Trace(loggerSpeedSetParameters,
				QString("Creating TioParmsSpeedSet object: nodeId=%1, isReply=%2, speed=%3")
						.arg(nodeId).arg(isReply).arg(speed));
	//
	IniFile	iniFile;
	initialize(iniFile, INI_GROUP(CfgGioTstMdSetSpeed));
	isReplyExpected = isReplyExpected && isReply;
	//
	powerTargetOffset	= iniFile.INI_GET(CfgCsrPowerTargetOffset);
	powerTargetType		= CsrInfo::type(iniFile.INI_GET(CfgCsrPowerTargetType));
	//
	replyValidAddress	= powerTargetOffset;
	int lastCsrAddress	= powerTargetOffset + CsrInfo::size(powerTargetType);
	replyValidLength	= lastCsrAddress - replyValidAddress;
	replyValidFlags		= RESPONSE_TYPE_FLAG;
	replyValidNodeId	= nodeId;
	//
	LOG_AssertMsg(loggerSpeedSetParameters, 0 < replyValidLength,
					QString("Invalid reply length").arg(replyValidLength));
	//
	QByteArray	payload((char *)&powerTarget, sizeof(powerTarget));
	commandSize = protocol_vrcsr_calc_packet_size(payload.size(), false, PROTOCOL_VRCSR);
	ByteP	packetBuffer = new (std::nothrow) Byte[commandSize];
	LOG_CHK_PTR_MSG(loggerSpeedSetParameters, packetBuffer, "Unable to create TioParmsSpeedSet packetBuffer");
	commandBuffer.reset(packetBuffer);
	(void)protocol_vrcsr_insert_data((ByteP)payload.data(),
										payload.size(),
										false,
										commandBuffer.get(),
										PROTOCOL_VRCSR);
	(void)protocol_vrcsr_build_request_inplace(nodeId,
												isReplyExpected ? REQUEST_LEN(replyValidLength) : 0,
												replyValidAddress,
												payload.size(),
												commandBuffer.get(),
												PROTOCOL_VRCSR);
}

//======================================================================================================================
/*! @brief Create a TioParmsStatus object.
*/
TioParmsStatus::TioParmsStatus(
	int		nodeId,			//!<[in]  Node ID of thruster.
	double	&rpm_,			//!<[out] RPM from the thruster.
	double	&voltage,		//!<[out] Bus voltage from the thruster.
	double	&current,		//!<[out] Bus current from the thruster.
	double	&temperature_,	//!<[out] Temperature from the thruster.
	uint	&faults			//!<[out] Faults from the thruster.
)
:
	TioParms			(statusReply, "TestMode Status"),
	busCurrent			(current),
	busCurrentOffset	(0),
	busCurrentType		(CsrInfo::Type::Bad),
	busVoltage			(voltage),
	busVoltageOffset	(0),
	busVoltageType		(CsrInfo::Type::Bad),
	faultFlags			(faults),
	faultFlagsOffset	(0),
	faultFlagsType		(CsrInfo::Type::Bad),
	rpm					(rpm_),
	rpmOffset			(0),
	rpmType				(CsrInfo::Type::Bad),
	temperature			(temperature_),
	temperatureOffset	(0),
	temperatureType		(CsrInfo::Type::Bad)
{
	LOG_Trace(loggerStatusParameters,
				QString("Creating TioParmsStatus object: nodeId=%1").arg(nodeId));
	//
	IniFile	iniFile;
	initialize(iniFile, INI_GROUP(CfgGioTstMdGetStatus));
	//
	busCurrentOffset	= iniFile.INI_GET(CfgCsrBusAmpsOffset);
	busVoltageOffset	= iniFile.INI_GET(CfgCsrBusVoltsOffset);
	faultFlagsOffset	= iniFile.INI_GET(CfgCsrFaultFlagsOffset);
	rpmOffset			= iniFile.INI_GET(CfgCsrRpmOffset);
	temperatureOffset	= iniFile.INI_GET(CfgCsrTemperatureOffset);
	busCurrentType		= CsrInfo::type(iniFile.INI_GET(CfgCsrBusAmpsType));
	busVoltageType		= CsrInfo::type(iniFile.INI_GET(CfgCsrBusVoltsType));
	faultFlagsType		= CsrInfo::type(iniFile.INI_GET(CfgCsrFaultFlagsType));
	rpmType				= CsrInfo::type(iniFile.INI_GET(CfgCsrRpmType));
	temperatureType		= CsrInfo::type(iniFile.INI_GET(CfgCsrTemperatureType));
	//
	replyValidAddress	= qMin(busCurrentOffset, busVoltageOffset);
	replyValidAddress	= qMin(replyValidAddress, faultFlagsOffset);
	replyValidAddress	= qMin(replyValidAddress, rpmOffset);
	replyValidAddress	= qMin(replyValidAddress, temperatureOffset);
	//
	int lastCsrAddress	= qMax(busCurrentOffset + CsrInfo::size(busCurrentType),
								busVoltageOffset + CsrInfo::size(busVoltageType));
	lastCsrAddress		= qMax(lastCsrAddress, faultFlagsOffset + CsrInfo::size(faultFlagsType));
	lastCsrAddress		= qMax(lastCsrAddress, rpmOffset + CsrInfo::size(rpmType));
	lastCsrAddress		= qMax(lastCsrAddress, temperatureOffset + CsrInfo::size(temperatureType));
	replyValidLength	= lastCsrAddress - replyValidAddress;
	//
	replyValidFlags		= RESPONSE_TYPE_FLAG;
	replyValidNodeId	= nodeId;
	//
	LOG_AssertMsg(loggerStatusParameters, 0 < replyValidLength,
					QString("Invalid reply length").arg(replyValidLength));
	//
	commandSize = protocol_vrcsr_calc_packet_size(0, false, PROTOCOL_VRCSR);
	ByteP	packetBuffer = new (std::nothrow) Byte[commandSize];
	LOG_CHK_PTR_MSG(loggerStatusParameters, packetBuffer, "Unable to create TioParmsStatus packetBuffer");
	commandBuffer.reset(packetBuffer);
	(void)protocol_vrcsr_build_request_inplace(nodeId,
												REQUEST_LEN(replyValidLength),
												replyValidAddress,
												0,
												commandBuffer.get(),
												PROTOCOL_VRCSR);
}

//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
/*! @brief Close the serial port.
*/
void
TioTestingMode::doClose(void)
{
	LOG_Trace(loggerOpen,
				QString("Closing connection: serialPort='%1', nodeId=%2").arg(serialPort).arg(nodeId));
	// Done if already closed.
	if (!connection.isNull()) {
		connection.reset();
	}
}

//======================================================================================================================
/*! @brief Open the serial port.

@return true if no errors occur else false.
*/
bool
TioTestingMode::doOpen(void)
{
	LOG_Trace(loggerOpen,
				QString("Opening connection: serialPort='%1', nodeId=%2").arg(serialPort).arg(nodeId));
	// Done if already open.
	if (!connection.isNull()) {
		return true;
	}
	std::string portName = serialPort.toStdString();
	connection.reset(new TioImplTestingMode(portName, nodeId, halt));
	return !connection.isNull() && connection->isOpen();
}

//======================================================================================================================
/*! @brief Testing mode (propulsion) transaction.

@return the error code (Thruster::TEC_NoError if no errors).
*/
Thruster::Error
TioImplTestingMode::propulsionSet(
	int		nodeId,			//!<[in]  Node ID of thruster.
	int		motorId,		//!<[in]  Motor ID of thruster.
	int		id,				//!<[in]  ID of destination thruster.
	double	speed,			//!<[in]  Speed of thruster.
	double	&rpm,			//!<[out] RPM from the thruster.
	double	&voltage,		//!<[out] Bus voltage from the thruster.
	double	&current,		//!<[out] Bus current from the thruster.
	double	&temperature,	//!<[out] Temperature from the thruster.
	uint	&faults			//!<[out] Fault flags from the thruster.
)
{
	LOG_Trace(loggerPropulsionCommand,
				QString("Testing mode propulsion: nodeId=%1, motorId=%2, id=%3, speed=%4")
						.arg(nodeId).arg(motorId).arg(id).arg(speed));
	TioParmsPropulsion		ciop(nodeId, motorId, id, speed, rpm, voltage, current, temperature, faults);
	return genericIo(&ciop);
}

//======================================================================================================================
/*! @brief Process a testing mode (propulsion) reply packet.
*/
void
propulsionReply(
	TioParms					*biop,			//!<[in,out] I/O parameters.
	Protocol_VRCSR_Packet const	&packet,		//!<[in] Reply packet.
	int							deviceTypeSize	//!<[in] Size of device type data prefix.
)
{
	LOG_Trace(loggerPropulsionReply, "Received testing mode propulsion reply");
	Q_UNUSED(deviceTypeSize);
	//
	TioParmsPropulsion	*iop = dynamic_cast<TioParmsPropulsion *>(biop);
	LOG_ASSERT_MSG(loggerPropulsionReply, iop, "propulsionReply: iop is invalid");
	// Extract the status data from the packet.
	ByteP	payload		= ByteP(deviceTypeSize + packet.payload);
	iop->rpm			= *(Rpm_t *)payload;
	payload				+= sizeof(Rpm_t);
	iop->voltage		= *(BusVolts_t *)payload;
	payload				+= sizeof(BusVolts_t);
	iop->current		= *(BusAmps_t *)payload;
	payload				+= sizeof(BusAmps_t);
	iop->temperature	= *(Temperature_t *)payload;
	payload				+= sizeof(Temperature_t);
	iop->faults			= *(FaultFlags1_t *)payload;
	// Update the status.
	iop->errorCode		= Thruster::TEC_NoError;
	iop->isReplySeen	= true;
}

//======================================================================================================================
/*! @brief Testing mode (CSR) check speed.

@return the error code (Thruster::TEC_NoError if no errors).
*/
Thruster::Error
TioImplTestingMode::speedCheck(
	int		nodeId,		//!<[in] Node ID of thruster.
	double	speed		//!<[in] Speed of thruster.
)
{
	LOG_Trace(loggerSpeedSetCommand,
				QString("Testing mode speed: nodeId=%1, speed=%2").arg(nodeId).arg(speed));
	TioParmsSpeedCheck		ciop(nodeId, speed);
	return genericIo(&ciop);
}

//======================================================================================================================
/*! @brief Testing mode (CSR) set speed.

@return the error code (Thruster::TEC_NoError if no errors).
*/
Thruster::Error
TioImplTestingMode::speedSet(
	int		nodeId,		//!<[in] Node ID of thruster.
	bool	isReply,	//!<[in] Is a reply expected.
	double	speed		//!<[in] Speed of thruster.
)
{
	LOG_Trace(loggerSpeedSetCommand,
				QString("Testing mode speed: nodeId=%1, isreply=%2, speed=%3").arg(nodeId).arg(isReply).arg(speed));
	TioParmsSpeedSet		ciop(nodeId, isReply, speed);
	return genericIo(&ciop);
}

//======================================================================================================================
/*! @brief Process a testing mode (CSR) speed check reply packet.
*/
void
speedCheckReply(
	TioParms					*biop,			//!<[in,out] I/O parameters.
	Protocol_VRCSR_Packet const	&packet,		//!<[in] Reply packet.
	int							deviceTypeSize	//!<[in] Size of device type data prefix.
)
{
	LOG_Trace(loggerSpeedCheckReply, "Received testing mode speed reply");
	Q_UNUSED(deviceTypeSize);
	//
	TioParmsSpeedCheck	*iop = dynamic_cast<TioParmsSpeedCheck *>(biop);
	LOG_ASSERT_MSG(loggerSpeedCheckReply, iop, "speedCheckReply: iop is invalid");
	// Extract the data from the payload.
	PowerTarget_t	csrPowerTarget;
	CsrBlob(packet.payload, packet.header->length, iop->replyValidAddress - deviceTypeSize)
			.move(iop->powerTargetOffset)
			.get(csrPowerTarget, iop->powerTargetType);
	// Update the status.
	if (iop->powerTarget == csrPowerTarget) {
		iop->errorCode = Thruster::TEC_NoError;
		iop->isReplySeen = true;
	} else {
		iop->errorCode = Thruster::TEC_WriteError;
		LOG_Error(loggerSpeedCheckReply,
					QString("Unexpected reply: Data mismatch (check speed): expected=%1, payload=%2")
							.arg(iop->powerTarget).arg(csrPowerTarget));
	}
}

//======================================================================================================================
/*! @brief Process a testing mode (CSR) speed set reply packet.
*/
void
speedSetReply(
	TioParms					*biop,			//!<[in,out] I/O parameters.
	Protocol_VRCSR_Packet const	&packet,		//!<[in] Reply packet.
	int							deviceTypeSize	//!<[in] Size of device type data prefix.
)
{
	LOG_Trace(loggerSpeedSetReply, "Received testing mode speed reply");
	Q_UNUSED(deviceTypeSize);
	//
	TioParmsSpeedSet	*iop = dynamic_cast<TioParmsSpeedSet *>(biop);
	LOG_ASSERT_MSG(loggerSpeedSetReply, iop, "speedSetReply: iop is invalid");
	// Extract the data from the payload.
	PowerTarget_t	csrPowerTarget;
	CsrBlob(packet.payload, packet.header->length, iop->replyValidAddress - deviceTypeSize)
			.move(iop->powerTargetOffset)
			.get(csrPowerTarget, iop->powerTargetType);
	// Update the status.
	if (iop->powerTarget == csrPowerTarget) {
		iop->errorCode = Thruster::TEC_NoError;
		iop->isReplySeen = true;
	} else {
		iop->errorCode = Thruster::TEC_WriteError;
		LOG_Error(loggerSpeedSetReply,
					QString("Unexpected reply: Data mismatch (set speed): expected=%1, payload=%2")
							.arg(iop->powerTarget).arg(csrPowerTarget));
	}
}

//======================================================================================================================
/*! @brief Testing mode (CSR) get status.

@return the error code (Thruster::TEC_NoError if no errors).
*/
Thruster::Error
TioImplTestingMode::statusGet(
	int		nodeId,			//!<[in]  Node ID of thruster.
	double	&rpm,			//!<[out] RPM from the thruster.
	double	&voltage,		//!<[out] Bus voltage from the thruster.
	double	&current,		//!<[out] Bus current from the thruster.
	double	&temperature,	//!<[out] Temperature from the thruster.
	uint	&faults			//!<[out] Fault flags from the thruster.
)
{
	LOG_Trace(loggerStatusCommand, "Testing mode status");
	TioParmsStatus		ciop(nodeId, rpm, voltage, current, temperature, faults);
	return genericIo(&ciop);
}

//======================================================================================================================
/*! @brief Process a testing mode (CSR) status reply packet.
*/
void
statusReply(
	TioParms					*biop,			//!<[in,out] I/O parameters.
	Protocol_VRCSR_Packet const	&packet,		//!<[in] Reply packet.
	int							deviceTypeSize	//!<[in] Size of device type data prefix.
)
{
	LOG_Trace(loggerStatusReply, "Received testing mode status reply");
	Q_UNUSED(deviceTypeSize);
	//
	TioParmsStatus	*iop = dynamic_cast<TioParmsStatus *>(biop);
	LOG_ASSERT_MSG(loggerStatusReply, iop, "statusReply: iop is invalid");
	// Extract the data from the payload.
	BusAmps_t		csrBusCurrent;
	BusVolts_t		csrBusVoltage;
	FaultFlags_t	csrFaultFlags;
	Rpm_t			csrRpm;
	Temperature_t	csrTemperature;
	CsrBlob	csr(packet.payload, packet.header->length, iop->replyValidAddress - deviceTypeSize);
	csr	
		.move(iop->busCurrentOffset)	.get(csrBusCurrent,		iop->busCurrentType)
		.move(iop->busVoltageOffset)	.get(csrBusVoltage,		iop->busVoltageType)
		.move(iop->faultFlagsOffset)	.get(csrFaultFlags,		iop->faultFlagsType)
		.move(iop->rpmOffset)			.get(csrRpm,			iop->rpmType)
		.move(iop->temperatureOffset)	.get(csrTemperature,	iop->temperatureType);
	iop->busCurrent		= csrBusCurrent;
	iop->busVoltage		= csrBusVoltage;
	iop->faultFlags		= csrFaultFlags;
	iop->rpm			= csrRpm;
	iop->temperature	= csrTemperature;
	// Update the status.
	iop->errorCode		= Thruster::TEC_NoError;
	iop->isReplySeen	= true;
}

//======================================================================================================================
/*! @brief Testing mode transation.

@return the error code (Thruster::TEC_NoError if no errors).
*/
Thruster::Error
TioTestingMode::testMode(
	int		groupId,		//!<[in]  Group ID.
	int		motorId,		//!<[in]  Motor ID.
	int		mode,			//!<[in]  Testing mode.
	bool	sendSpeed,		//!<[in]  Send speed (single mode only).
	double	speed,			//!<[in]  New speed.
	double	&rpm,			//!<[out] RPM read from the thruster.
	double	&voltage,		//!<[out] Bus voltage read from the thruster.
	double	&current,		//!<[out] Bus current read from the thruster.
	double	&temperature,	//!<[out] Temperature read from the thruster.
	uint	&faults			//!<[out] Fault flags read from the thruster.
)
{
	LOG_Trace(loggerMain,
				QString("Testing mode: nodeId=%1, motorId=%2, speed=%3").arg(nodeId).arg(motorId).arg(speed));
	LOG_ASSERT_MSG(loggerMain, connection, "Not open");
	//
	rpm				= 0.0;
	voltage			= 0.0;
	current			= 0.0;
	temperature		= 0.0;
	faults			= 0;
	//
	Thruster::ThrusterIdType	idType;
	bool						isCheck, isPropulsion, isReply;
	QString						name;
	bool isOk = Thruster::testingModeInfo(mode, idType, isCheck, isPropulsion, isReply, name);
	LOG_AssertMsg(loggerMain, isOk, QString("Invalid testing mode: %1").arg(mode));
	//
	int id = ID_BROADCAST;
	switch (idType) {
	case Thruster::TIT_NodeId:
		id = nodeId;
		break;
	case Thruster::TIT_GroupId:
		id = groupId | IniFile().INI_GET(CfgThrusterGroupIdMask);
		break;
	case Thruster::TIT_Broadcast:
		id = ID_BROADCAST;
		break;
	default:
		LOG_Fatal(loggerMain, QString("Invalid ThrusterIdType: %1").arg(idType));
		return Thruster::TEC_NoError;
	}
	LOG_Trace(loggerMain,
				QString("testingModeInfo: mode=%1, idType=%2, isCheck=%3, isPropulsion=%4, isReply=%5, id=%6")
				.arg(mode).arg(idType).arg(isCheck).arg(isPropulsion).arg(isReply).arg(id));
	//
	if (isPropulsion) {
		return connection->propulsionSet(nodeId, motorId, id, speed, rpm, voltage, current, temperature, faults);
	}
	// CSR commands.
	if (sendSpeed) {
		Thruster::Error	errorCode;
		errorCode = connection->speedSet(id, isReply, speed);
		if (Thruster::TEC_NoError != errorCode) {
			return errorCode;
		}
		if (isCheck) {
			errorCode = connection->speedCheck(nodeId, speed);
			if (Thruster::TEC_NoError != errorCode) {
				return errorCode;
			}
		}
	}
	return connection->statusGet(nodeId, rpm, voltage, current, temperature, faults);
}
