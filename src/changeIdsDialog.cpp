/*! @file
@brief Define the change node/group IDs dialog module.
*/
//######################################################################################################################

#include "app_com.h"

#include <QtCore/QList>
#include <QtWidgets/QPushButton>

#include "changeIdsDialog.h"
#include "changeIdsDialog_log.h"
#include "changeIdsDialogPrivate.h"
#include "configState.h"
#include "configValidators.h"
#include "main.h"
#include "serialPortUtil.h"

//######################################################################################################################
/*! @brief Create a ChangeIdsDialog object.
*/
ChangeIdsDialog::ChangeIdsDialog(
	int		nodeId,		//!<[in] The curent node ID.
	int		groupId,	//!<[in] The curent group ID.
	QWidget	*parent		//!<[in] The parent object.
)
:
	d	(Q_NULLPTR)
{
	LOG_Trace(loggerCreate, "Creating ChangeIdsDialog");
	//
	d.reset(new (std::nothrow) ChangeIdsDialogPrivate(nodeId, groupId, parent));
	LOG_CHK_PTR_MSG(loggerCreate, d, "Unable to create ChangeIdsDialogPrivate object");
}

//----------------------------------------------------------------------------------------------------------------------
/*! @brief Create a ChangeIdsDialogPrivate object.
*/
ChangeIdsDialogPrivate::ChangeIdsDialogPrivate(
	int		nodeId_,	//!<[in] The curent node ID.
	int		groupId_,	//!<[in] The curent group ID.
	QWidget	*parent		//!<[in] The parent object.
)
:
	QDialog				(parent),
	groupId				(groupId_),
	groupIdValidator	(),
	nodeId				(nodeId_),
	nodeIdValidator		()
{
	LOG_Trace(loggerCreate, "Creating ChangeIdsDialogPrivate");
	setupUi(this);
	restoreWindowState();
	//
	setValidatorLimits();
	groupIdLineEdit	->setValidator(&groupIdValidator);
	nodeIdLineEdit	->setValidator(&nodeIdValidator);
	groupIdLineEdit	->setText(QString::number(groupId));
	nodeIdLineEdit	->setText(QString::number(nodeId));
	//
	LOG_CONNECT(loggerCreate, groupIdLineEdit, &QLineEdit::textEdited, this, &ChangeIdsDialogPrivate::updateGuiState);
	LOG_CONNECT(loggerCreate, nodeIdLineEdit, &QLineEdit::textEdited, this, &ChangeIdsDialogPrivate::updateGuiState);
	//
	updateGuiState();
}

//======================================================================================================================
/*! @brief Destroy a ChangeIdsDialog object.
*/
ChangeIdsDialog::~ChangeIdsDialog(void)
{
	LOG_Trace(loggerCreate, "Destroying ChangeIdsDialog");
}

//----------------------------------------------------------------------------------------------------------------------
/*! @brief Destroy a ChangeIdsDialogPrivate object.
*/
ChangeIdsDialogPrivate::~ChangeIdsDialogPrivate(void)
{
	LOG_Trace(loggerCreate, "Destroying ChangeIdsDialogPrivate");
}

//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
/*! @brief Invoke a ChangeIdsDialog object.

@return a QDialog::DialogCode result.
*/
int
ChangeIdsDialog::invoke(void)
{
	LOG_Trace(loggerInvoke, "Invoking ChangeIdsDialog");
	int	answer = d->exec();
	d->saveWindowState();
	return answer;
}

//======================================================================================================================
/*! @brief Return the current group ID.

@return the current group ID.
*/
int
ChangeIdsDialog::newGroupId(
	bool	&isOkay		//!<[out] Is the ID valid.
)
{
	return d->newGroupId(isOkay);
}

//----------------------------------------------------------------------------------------------------------------------
/*! @brief Return the current group ID.

@return the current group ID.
*/
int
ChangeIdsDialogPrivate::newGroupId(
	bool	&isOkay		//!<[out] Is the ID valid.
)
{
	QString text = groupIdLineEdit->text();
	int pos = 0;
	isOkay = QValidator::Acceptable == groupIdLineEdit->validator()->validate(text, pos);
	int answer = text.toInt(isOkay ? &isOkay : Q_NULLPTR);
	LOG_Trace(loggerQuery, QString("New group ID: %1, isOkay=%2").arg(answer).arg(isOkay));
	return answer;
}

//======================================================================================================================
/*! @brief Return the current node ID.

@return the current node ID.
*/
int
ChangeIdsDialog::newNodeId(
	bool	&isOkay		//!<[out] Is the ID valid.
)
{
	return d->newNodeId(isOkay);
}

//----------------------------------------------------------------------------------------------------------------------
/*! @brief Return the current node ID.

@return the current node ID.
*/
int
ChangeIdsDialogPrivate::newNodeId(
	bool	&isOkay		//!<[out] Is the ID valid.
)
{
	QString text = nodeIdLineEdit->text();
	int pos = 0;
	isOkay = QValidator::Acceptable == nodeIdLineEdit->validator()->validate(text, pos);
	int answer = text.toInt(isOkay ? &isOkay : Q_NULLPTR);
	LOG_Trace(loggerQuery, QString("New node ID: %1, isOkay=%2").arg(answer).arg(isOkay));
	return answer;
}

//======================================================================================================================
/*! @brief Restore the window state.
*/
void
ChangeIdsDialogPrivate::restoreWindowState(void)
{
	LOG_Trace(loggerWindowState, "Restoring window state");
	//
	IniFile	iniFile;
	IniBytes	savedGeometry	= iniFile.INI_GET(CfgStateChangeIdsDialogGeometry);
	if (!savedGeometry.isEmpty()) {
		restoreGeometry(savedGeometry);
	}
}

//======================================================================================================================
/*! @brief Save the window state.
*/
void
ChangeIdsDialogPrivate::saveWindowState(void)
{
	LOG_Trace(loggerWindowState, "Saving window state");
	//
	IniFile	iniFile;
	iniFile.INI_SET(CfgStateChangeIdsDialogGeometry, saveGeometry());
	iniFile.syncAndCheck();
}

//======================================================================================================================
/*! @brief Set the limits of the input validators.
*/
void
ChangeIdsDialogPrivate::setValidatorLimits(void)
{
	LOG_Trace(loggerCreate, "Setting the input validator limits");
	//
	IniFile		iniFile;
	int	groupIdMax	= iniFile.INI_GET(CfgValidatorsGroupIdMax);
	int	groupIdMin	= iniFile.INI_GET(CfgValidatorsGroupIdMin);
	int	nodeIdMax	= iniFile.INI_GET(CfgValidatorsNodeIdMax);
	int	nodeIdMin	= iniFile.INI_GET(CfgValidatorsNodeIdMin);
	//
	groupIdValidator.setRange(groupIdMin, groupIdMax);
	nodeIdValidator.setRange(nodeIdMin, nodeIdMax);
	//
	groupIdLimitsLabel	->setText(QStringLiteral("%1-%2").arg(groupIdMin).arg(groupIdMax));
	nodeIdLimitsLabel	->setText(QStringLiteral("%1-%2").arg(nodeIdMin).arg(nodeIdMax));
}

//======================================================================================================================
/*! @brief Update the control states.
*/
void
ChangeIdsDialogPrivate::updateGuiState(void)
{
	LOG_Trace(loggerGuiState, "Update the control states");
	bool isGroupIdOkay = false;
	bool isNodeIdOkay = false;
	bool isGroupIdChanged = groupId != newGroupId(isGroupIdOkay);
	bool isNodeIdChanged = nodeId != newNodeId(isNodeIdOkay);
	savePushButton->setEnabled(isGroupIdOkay && isNodeIdOkay && (isGroupIdChanged || isNodeIdChanged));
}
