#pragma once
#ifndef TIOCONFIGREAD_H
#ifndef DOXYGEN_SKIP
#define TIOCONFIGREAD_H
#endif
//######################################################################################################################
/*! @file
@brief Declare the interface to the thruster configuration read module.
*/
//######################################################################################################################

#include <QtCore/QScopedPointer>

#include "tioBase.h"

//######################################################################################################################

class	TioImplConfigRead;

//######################################################################################################################
/*! @brief Interface to the thruster configuration read module.
*/
class TioConfigRead
:
	public	TioBase
{
public:		// Constructors & destructors
	TioConfigRead(QString const &serialPort, int nodeId, bool const volatile &halt);
	~TioConfigRead(void);

public:		// Functions
	Thruster::Error		configurationRead(int &cfgNodeId, int &groupId, QString &serialNumber, int &motorId,
											int &maximumPower, bool &rotationReverse, QByteArray &csrBlob);

protected:	// Functions
	virtual void		doClose(void);
	virtual bool		doOpen(void);

protected:	// Data
	QScopedPointer<TioImplConfigRead>	connection;		//!< Connection to the thruster.
};

//######################################################################################################################
#endif
