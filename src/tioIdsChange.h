#pragma once
#ifndef TIOIDSCHANGE_H
#ifndef DOXYGEN_SKIP
#define TIOIDSCHANGE_H
#endif
//######################################################################################################################
/*! @file
@brief Declare the interface to the thruster configuration write module.
*/
//######################################################################################################################

#include <QtCore/QScopedPointer>

#include "tioBase.h"

//######################################################################################################################

class	QString;
class	TioImplIdsChange;

//######################################################################################################################
/*! @brief Interface to the thruster configuration write module.
*/
class TioIdsChange
:
	public	TioBase
{
public:		// Constructors & destructors
	TioIdsChange(QString const &serialPort, int nodeId, bool const volatile &halt);
	~TioIdsChange(void);

public:		// Functions
	Thruster::Error		idsChange(QString const &serialNumber, int newNodeId, int newGroupId);

protected:	// Functions
	virtual void		doClose(void);
	virtual bool		doOpen(void);

protected:	// Data
	QScopedPointer<TioImplIdsChange>	connection;		//!< Connection to the thruster.
};

//######################################################################################################################
#endif
