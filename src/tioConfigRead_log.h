#pragma once
#if !defined(TIOCONFIGREAD_LOG_H) && !defined(DOXYGEN_SKIP)
#define TIOCONFIGREAD_LOG_H
//######################################################################################################################
/*! @file
@brief Declare the thruster I/O configuration read module loggers.
*/
//######################################################################################################################

#include "log.h"

//######################################################################################################################
// Loggers

LOG_SETUP_START("Thruster I/O Configuration Read Module");

// Base logger.
LOG_DEF_LOG(logger,					LOG_DL_INFO,	"TioConfigRead");
// Create/destroy logger.
LOG_DEF_LOG(loggerCreate,			LOG_DL_INFO,	"TioConfigRead.Create");
// Read CSR data command logger.
LOG_DEF_LOG(loggerCsrCommand,		LOG_DL_INFO,	"TioConfigRead.CsrCommand");
// Read CSR data parameters logger.
LOG_DEF_LOG(loggerCsrParameters,	LOG_DL_INFO,	"TioConfigRead.CsrParameters");
// Read CSR dat reply logger.
LOG_DEF_LOG(loggerCsrReply,			LOG_DL_INFO,	"TioConfigRead.CsrReply");
// Read IDs command logger.
LOG_DEF_LOG(loggerIdsCommand,		LOG_DL_INFO,	"TioConfigRead.IdsCommand");
// Read IDs parameters logger.
LOG_DEF_LOG(loggerIdsParameters,	LOG_DL_INFO,	"TioConfigRead.IdsParameters");
// Read IDs reply logger.
LOG_DEF_LOG(loggerIdsReply,			LOG_DL_INFO,	"TioConfigRead.IdsReply");
// Main logger.
LOG_DEF_LOG(loggerMain,				LOG_DL_INFO,	"TioConfigRead.Main");
// Open/close logger.
LOG_DEF_LOG(loggerOpen,				LOG_DL_INFO,	"TioConfigRead.Open");

LOG_SETUP_STOP();

//######################################################################################################################
#endif
