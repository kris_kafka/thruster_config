#pragma once
#ifndef PACKETUTIL_H
#ifndef DOXYGEN_SKIP
#define PACKETUTIL_H
#endif
//######################################################################################################################
/*! @file
@brief Declare the interface to the empty int validator module.
*/
//######################################################################################################################

#include <QtCore/QByteArray>
#include <QtCore/QString>

#include "videoray/applib/protocol_vr_csr.h"

//######################################################################################################################
/*! @brief VideoRay CSR protocol read thruster configuration.
*/
namespace PacketUtil
{
	QString		formatByte(QByteArray const &packet, int index);
	QString		formatData(QByteArray const &data);
	QString		formatData(void const *data, int size);
	QString		formatHeader(QByteArray const &packet);
	QString		formatHeader(Protocol_VRCSR_Packet const &packet);
	QString		formatHeader(void const *data, int size);
	QString		formatPacket(QByteArray const &packet);
	QString		formatPacket(Protocol_VRCSR_Packet const &packet);
	QString		formatPacket(void const *data, int size);
	QString		formatPayload(QByteArray const &packet);
	QString		formatPayload(Protocol_VRCSR_Packet const &packet);
	QString		formatPayload(void const *data, int size);
};

//######################################################################################################################
#endif
