/*! @file
@brief Define the thruster interface module.
*/
//######################################################################################################################

#include "app_com.h"

#include <QtCore/QByteArray>
#include <QtCore/QList>
#include <QtCore/QSettings>
#include <QtCore/QThread>
#include <QtSerialPort/QSerialPort>

#include "configThruster.h"
#include "tioEnum.h"
#include "tioConfigRead.h"
#include "tioConfigWrite.h"
#include "tioIdsChange.h"
#include "tioTestingMode.h"
#include "thrusterTaskReal.h"
#include "thrusterTaskReal_log.h"
#include "serialPortUtil.h"
#include "strUtil.h"

//######################################################################################################################
/*! @brief Create a ThrusterTaskReal object.
*/
ThrusterTaskReal::ThrusterTaskReal(void)
:
	ThrusterTaskBase	()
{
	LOG_Trace(loggerCreate, "Creating a ThrusterTaskReal object");
}

//======================================================================================================================
/*! @brief Destroy a ThrusterTaskReal object.
*/
ThrusterTaskReal::~ThrusterTaskReal(void)
{
	LOG_Trace(loggerCreate, "Destroying a ThrusterTaskReal object");
}

//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
/*! @brief Read the configuration of a real thruster.

@return true if no errors, else false.
*/
bool
ThrusterTaskReal::configurationRead(
	int			nodeId,				//!<[in]  ID of node to read configuration from.
	int			&nodeIdRead,		//!<[out] Node ID read from thruster.
	int			&groupId,			//!<[out] Group ID read from thruster.
	int			&motorId,			//!<[out] Motor ID read from thruster.
	int			&maximumPower,		//!<[out] Maximum power read from thruster.
	bool		&rotationReverse,	//!<[out] Rotation reversed read from thruster.
	QString		&serialNumber,		//!<[out] Serial number read from thruster.
	QByteArray	&csrBlob			//!<[out] CSR blob.
)
{
	LOG_Trace(loggerConfigurationRead, QString("Read configuration of thruster: nodeId=%1").arg(nodeId));
	//
	TioConfigRead	tio(SerialPortUtil::portNameToSystemLocation(serialPort), nodeId, taskHalt);
	Thruster::Error	errorCode;
	// Open the connection.
	errorCode = tio.open();
	if (Thruster::TEC_NoError != errorCode) {
		LOG_Error(loggerConfigurationRead,
					QString("Unable to open TioConfigRead connection: seriaPort='%1', nodeId=%2, errorCode =%3")
							.arg(serialPort).arg(nodeId).arg(errorCode));
		EMIT_IDLE(configurationReadStop, Thruster::TEC_OpenError);
		return false;
	}
	// Read the configuration.
	errorCode = tio.configurationRead(nodeIdRead, groupId, serialNumber, motorId, maximumPower, rotationReverse,
										csrBlob);
	if (Thruster::TEC_NoError != errorCode) {
		LOG_Error(loggerConfigurationRead,
					QString("Unable to read thruster configuration: seriaPort='%1', nodeId=%2, errorCode=%3")
							.arg(serialPort).arg(nodeId).arg(errorCode));
		EMIT_IDLE(configurationReadStop, errorCode);
		return false;
	}
	//
	tio.close();
	return true;
}

//======================================================================================================================
/*! @brief Write the configuration of a real thruster.
*/
void
ThrusterTaskReal::configurationWrite(
	int					nodeId,				//!<[in] ID of node to write configuraton to.
	int					motorId,			//!<[in] New motor ID.
	int					maximumPower,		//!<[in] New maximum power level.
	bool				rotationReverse,	//!<[in] New rotation is reversed.
	QByteArray const	&csrBlob			//!<[in] CSR blob.
)
{
	LOG_Trace(loggerConfigurationWrite,
				QString("Write configuration to thruster: nodeId=%1, motorID=%2, maximumPower=%3, rotationReverse=%4")
						.arg(nodeId).arg(motorId).arg(maximumPower).arg(rotationReverse));
	//
	TioConfigWrite	tio(SerialPortUtil::portNameToSystemLocation(serialPort), nodeId, taskHalt);
	Thruster::Error	errorCode;
	// Open the connection.
	errorCode = tio.open();
	if (Thruster::TEC_NoError != errorCode) {
		LOG_Error(loggerConfigurationWrite,
					QString("Unable to open TioConfigWrite connection: seriaPort='%1', nodeId=%2, errorCode =%3")
							.arg(serialPort).arg(nodeId).arg(errorCode));
		EMIT_IDLE(configurationWriteStop, Thruster::TEC_OpenError);
		return;
	}
	// Write the configuration.
	errorCode = tio.configurationWrite(motorId, maximumPower, rotationReverse, csrBlob);
	if (Thruster::TEC_NoError != errorCode) {
		LOG_Error(loggerConfigurationWrite,
					QString("Unable to write thruster configuration: seriaPort='%1', nodeId=%2, errorCode=%3, "
									"motorId=%4, maximumPower=%5, rotationReverse=%6")
							.arg(serialPort).arg(nodeId).arg(errorCode)
							.arg(motorId).arg(maximumPower).arg(rotationReverse));
		EMIT_IDLE(configurationWriteStop, errorCode);
		return;
	}
	//
	tio.close();
	LOG_Debug(loggerConfigurationWrite,
				QString("Write configuration to thruster finished: nodeId=%1, motorID=%3, maximumPower=%4, "
								"rotationReverse=%5")
						.arg(nodeId).arg(motorId).arg(maximumPower).arg(rotationReverse));
	// Report successful completion.
	EMIT_IDLE(configurationWriteStop, Thruster::TEC_NoError);
}

//======================================================================================================================
/*! @brief Change the node ID of @a currentNode.
*/
void
ThrusterTaskReal::idsChange(
	QString const	&serialNumber,	//!<[in] Serial number of the thruster.
	int				oldNodeId,		//!<[in] Old node ID of the thruster.
	int				newNodeId,		//!<[in] New node ID of the thruster.
	int				newGroupId		//!<[in] New group ID of the thruster.
)
{
	LOG_Trace(loggerIdsChange,
				QString("Changing the IDs of %1 (%2) to: node=%3, group=%4")
						.arg(oldNodeId).arg(serialNumber).arg(newNodeId).arg(newGroupId));
	//
	TioIdsChange	tio(SerialPortUtil::portNameToSystemLocation(serialPort), oldNodeId, taskHalt);
	Thruster::Error	errorCode;
	// Open the connection.
	errorCode = tio.open();
	if (Thruster::TEC_NoError != errorCode) {
		LOG_Error(loggerIdsChange,
					QString("Unable to open TioIdsChange connection: seriaPort='%1', nodeId=%2, errorCode =%3")
							.arg(serialPort).arg(oldNodeId).arg(errorCode));
		EMIT_IDLE(idsChangeStop, Thruster::TEC_OpenError);
		return;
	}
	// Change the IDs.
	errorCode = tio.idsChange(serialNumber, newNodeId, newGroupId);
	if (Thruster::TEC_NoError != errorCode) {
		LOG_Error(loggerIdsChange,
					QString("Unable to change IDs: seriaPort='%1', nodeId=%2, errorCode=%3, newNodeId=%4, newGroupId=%5")
							.arg(serialPort).arg(oldNodeId).arg(errorCode).arg(newNodeId).arg(newGroupId));
		EMIT_IDLE(idsChangeStop, errorCode);
		return;
	}
	//
	tio.close();
	EMIT_IDLE(idsChangeStop, Thruster::TEC_NoError);
}

//======================================================================================================================
/*! @brief Scan for real thrusters.

@return true if no errors, else false.
*/
bool
ThrusterTaskReal::scanForThrusters(void)
{
	LOG_Trace(loggerScanForThrusters, "Scan for thrusters");
	//
	TioEnum	tei(SerialPortUtil::portNameToSystemLocation(serialPort), taskHalt);
	Thruster::Error	errorCode;
	errorCode = tei.open();
	if (Thruster::TEC_NoError != errorCode) {
		LOG_Error(loggerScanForThrusters,
					QString("Unable to open connection: serialPort='%1', errorCode=%2").arg(serialPort).arg(errorCode));
		EMIT_IDLE(scanForThrustersStop, errorCode);
		return false;
	}
	QList<TioEnumData>	enumData;
	errorCode = tei.enumerate(enumData, duplicateNodeIdCount, duplicateSerialCount);
	// Sort the replies by the node IDs.
	std::sort(enumData.begin(),
				enumData.end(),
				[](const TioEnumData &a, const TioEnumData &b) -> bool { return a.nodeId < b.nodeId; });
	// Announce the thrusters.
	for (TioEnumData data : enumData) {
		LOG_Debug(loggerSignals,
					QString("Emit scanForThrustersNews: nodeId=%1, groupId=%2, serialNumber=%3")
							.arg(data.nodeId).arg(data.groupId).arg(data.serialNumber));
		Q_EMIT scanForThrustersNews(data.nodeId, data.groupId, data.serialNumber);
	}
	//
	if (Thruster::TEC_NoError != errorCode) {
		LOG_Error(loggerScanForThrusters,
					QString("Thruster enumeration error: serialPort='%1', errorCode=%2").arg(serialPort).arg(errorCode));
		EMIT_IDLE(scanForThrustersStop, errorCode);
		return false;
	}
	//
	tei.close();
	return true;
}
//======================================================================================================================
/*! @brief Check if serial port @a is valid.

@return true if valid else false.
*/
bool
ThrusterTaskReal::serialPortCheck(
	QString const	&portName	//!<[in] Serial port.
)
{
	// Make sure we can open the serial port.
	static QChar const		pad = '0';
	static QString const	format = "Status of '%1': error=%2, pins=0x%3, baud=%4, data=%5, stop=%6, parity=%7, "
										"flow=%8, break=%9";
	bool	enhancedSerialPortTest = IniFile().INI_GET(CfgThrusterEnhancedPortTest);
	QSerialPort	port;
	port.setPortName(SerialPortUtil::portNameToSystemLocation(portName));
	if (!port.setBaudRate(QSerialPort::Baud115200)) {
		LOG_Error(loggerSerialPortChange,
					QString("setBaudRate failed for serial port '%1': %2")
							.arg(portName).arg(port.errorString()));
		return false;
	}
	if (!port.setDataBits(QSerialPort::Data8)) {
		LOG_Error(loggerSerialPortChange,
					QString("setDataBits failed for serial port '%1': %2")
							.arg(portName).arg(port.errorString()));
		return false;
	}
	if (!port.setParity(QSerialPort::NoParity)) {
		LOG_Error(loggerSerialPortChange,
					QString("setParity failed for serial port '%1': %2")
							.arg(portName).arg(port.errorString()));
		return false;
	}
	if (!port.setStopBits(QSerialPort::OneStop)) {
		LOG_Error(loggerSerialPortChange,
					QString("setStopBits failed for serial port '%1': %2")
							.arg(portName).arg(port.errorString()));
		return false;
	}
	if (!port.setFlowControl(QSerialPort::NoFlowControl)) {
		LOG_Error(loggerSerialPortChange,
					QString("setFlowControl failed for serial port '%1': %2")
							.arg(portName).arg(port.errorString()));
		return false;
	}
	if (!port.open(QIODevice::ReadWrite)) {
		LOG_Error(loggerSerialPortChange,
					QString("Unable to open serial port '%1': %2")
							.arg(portName).arg(port.errorString()));
		return false;
	}
	if (enhancedSerialPortTest) {
		if (!port.setDataTerminalReady(true)) {
			LOG_Error(loggerSerialPortChange,
						QString("setDataTerminalReady(true) failed for serial port '%1': %2")
								.arg(portName).arg(port.errorString()));
			return false;
		}
		if (!port.setRequestToSend(true)) {
			LOG_Error(loggerSerialPortChange,
						QString("setRequestToSend(true) failed for serial port '%1': %2")
								.arg(portName).arg(port.errorString()));
			return false;
		}
		if (!port.setBreakEnabled(true)) {
			LOG_Error(loggerSerialPortChange,
						QString("setBreakEnabled(true) failed for serial port '%1': %2")
								.arg(portName).arg(port.errorString()));
			return false;
		}
		LOG_Trace(loggerSerialPortChange,
					format	.arg(portName)			.arg(port.error())			.arg(port.pinoutSignals(), 3, 16, pad)
							.arg(port.baudRate())	.arg(port.dataBits())		.arg(port.stopBits())
							.arg(port.parity())		.arg(port.flowControl())	.arg(port.isBreakEnabled()));
		QThread::msleep(100);
	}
	if (!port.setBreakEnabled(false)) {
		LOG_Error(loggerSerialPortChange,
					QString("setBreakEnabled(false) failed for serial port '%1': %2")
							.arg(portName).arg(port.errorString()));
		return false;
	}
	if (!port.clear()) {
		LOG_Error(loggerSerialPortChange,
					QString("clear failed for serial port '%1': %2")
							.arg(portName).arg(port.errorString()));
		return false;
	}
	if (enhancedSerialPortTest) {
		LOG_Trace(loggerSerialPortChange,
					format	.arg(portName)			.arg(port.error())			.arg(port.pinoutSignals(), 3, 16, pad)
							.arg(port.baudRate())	.arg(port.dataBits())		.arg(port.stopBits())
							.arg(port.parity())		.arg(port.flowControl())	.arg(port.isBreakEnabled()));
		if (0 > port.write("\r\r\r\r")) {
			LOG_Error(loggerSerialPortChange,
						QString("write failed for serial port '%1': %2")
								.arg(portName).arg(port.errorString()));
			return false;
		}
		if (!port.waitForBytesWritten(100)) {
			LOG_Error(loggerSerialPortChange,
						QString("waitForBytesWritten failed for serial port '%1': %2")
								.arg(portName).arg(port.errorString()));
			return false;
		}
		LOG_Trace(loggerSerialPortChange,
					QString("waitForReadyRead for '%1': %2").arg(portName).arg(port.waitForReadyRead(100)));
	}
	if (!port.setRequestToSend(false)) {
		LOG_Error(loggerSerialPortChange,
					QString("setRequestToSend(false) failed for serial port '%1': %2")
							.arg(portName).arg(port.errorString()));
		return false;
	}
	if (!port.setDataTerminalReady(false)) {
		LOG_Error(loggerSerialPortChange,
					QString("setDataTerminalReady(false) failed for serial port '%1': %2")
							.arg(portName).arg(port.errorString()));
		return false;
	}
	LOG_Trace(loggerSerialPortChange,
				format	.arg(portName)			.arg(port.error())			.arg(port.pinoutSignals(), 3, 16, pad)
						.arg(port.baudRate())	.arg(port.dataBits())		.arg(port.stopBits())
						.arg(port.parity())		.arg(port.flowControl())	.arg(port.isBreakEnabled()));
	port.close();
	return true;
}

//======================================================================================================================
/*! @brief Query the available serial ports.

We do no error checking on the list of emulated serial ports in the configuration file.

@return list of available serial ports.
*/
QStringList
ThrusterTaskReal::serialPortsAvailable(void)
{
	LOG_Trace(loggerQuerySerialPorts, "Query for the available serial ports");
	// Get the available serial ports.
	QStringList	list = SerialPortUtil::availablePorts();
	// Sort the list.
	std::sort(list.begin(), list.end(), StrUtil::alphaNumericLess);
	return list;
}

//======================================================================================================================
/*! @brief Testing mode.
*/
void
ThrusterTaskReal::testMode(
	int	nodeId,		//!<[in] Node ID of thruster to be tested.
	int	groupId,	//!<[in] Group ID of thruster to be tested.
	int	motorId,	//!<[in] Motor ID of thruster to be tested.
	int	mode		//!<[in] Testing mode.
)
{
	LOG_Trace(loggerTestingMode,
				QString("Testing mode: nodeId=%1, groupId=%2, motorId=%3, mode=%4")
						.arg(nodeId).arg(groupId).arg(motorId).arg(mode));
	//
	TioTestingMode		tio(SerialPortUtil::portNameToSystemLocation(serialPort), nodeId, taskHalt);
	Thruster::Error	errorCode;
	// Open the connection.
	errorCode = tio.open();
	if (Thruster::TEC_NoError != errorCode) {
		LOG_Error(loggerTestingMode,
					QString("Unable to open TioTestingMode connection: seriaPort='%1', nodeId=%2, errorCode=%3")
							.arg(serialPort).arg(nodeId).arg(errorCode));
		EMIT_IDLE(testingModeStop, Thruster::TEC_OpenError);
		return;
	}
	//
	int	sendSpeedCount = IniFile().INI_GET(CfgThrusterTestModeSpeedCount);
	while (!testModeHalt) {
		// Handle speed changes.
		if (testModeSpeedChanged) {
			sendSpeedCount = IniFile().INI_GET(CfgThrusterTestModeSpeedCount);
			testModeSpeedChanged = false;
			LOG_Debug(loggerTestingMode, QString("The testing mode speed changed to %1").arg(testingModeSpeed));
		}
		// Set the speed and get the status.
		double	speed = testingModeSpeed;
				speed = qBound(-100.0, speed, 100.0) / 100.0;
		double	current, rpm, temperature, voltage;
		uint	faults;
		errorCode = tio.testMode(groupId, motorId, mode, !!sendSpeedCount, speed, rpm, voltage, current, temperature,
									faults);
		if (Thruster::TEC_NoError != errorCode && Thruster::TEC_Stopping != errorCode) {
			testModeTerminate(tio, nodeId, groupId, motorId, mode);
			LOG_Error(loggerTestingMode,
						QString("Testing mode failed: "
										"seriaPort='%1', nodeId=%2, groupId=%3, motorId=%4, mode=%5, speed=%6, "
										"errorCode=%7")
								.arg(serialPort).arg(nodeId).arg(groupId).arg(motorId).arg(mode).arg(testingModeSpeed)
								.arg(errorCode));
			EMIT_IDLE(testingModeStop, errorCode);
			return;
		}
		if (0 < sendSpeedCount) {
			--sendSpeedCount;
		}
		// Emit the status values.
		LOG_Debug(loggerSignals,
					QString("Emit testingModeNews: nodeId=%1, rpm=%2, voltage=%3, current=%4, temperature=%5, "
									"faults=%6")
							.arg(nodeId).arg(rpm).arg(voltage).arg(current)
							.arg(temperature).arg((int)faults, 2, 16, QChar('0')));
		Q_EMIT testingModeNews(nodeId, rpm, voltage, current, temperature, (int)faults);
		// Should we stop.
		if (taskHalt) {
			testModeTerminate(tio, nodeId, groupId, motorId, mode);
			LOG_Info(loggerTestingMode, "Background task stopping");
			EMIT_IDLE(testingModeStop, Thruster::TEC_Stopping);
			LOG_Trace(loggerSignals, "Quiting the background task");
			Q_EMIT quitThread();
			return;
		}
	}
	// Stop the thruster.
	testModeTerminate(tio, nodeId, groupId, motorId, mode);
	//
	tio.close();
	EMIT_IDLE(testingModeStop, Thruster::TEC_NoError);
}

//======================================================================================================================
/*! @brief Stop the thruster to stop testing mode.
*/
void
ThrusterTaskReal::testModeTerminate(
	TioTestingMode	&tio,		//!<[in] Thruster IO object.
	int	nodeId,		//!<[in] Node ID of thruster to be tested.
	int	groupId,	//!<[in] Group ID of thruster to be tested.
	int	motorId,	//!<[in] Motor ID of thruster to be tested.
	int	mode		//!<[in] Testing mode.
)
{
	LOG_Trace(loggerTestingMode,
				QString("Testing mode stop: nodeId=%1, groupId=%2, motorId=%3, mode=%4")
						.arg(nodeId).arg(groupId).arg(motorId).arg(mode));
	//
	double			current, rpm, temperature, voltage;
	uint			faults;
	double			speed = 0.0;
	Thruster::Error	errorCode = tio.testMode(groupId, motorId, mode, true, speed, rpm, voltage, current, temperature,
												faults);
	if (Thruster::TEC_NoError != errorCode && Thruster::TEC_Stopping != errorCode) {
		LOG_Error(loggerTestingMode,
					QString("Unable to stop the thruster: "
									"seriaPort='%1', nodeId=%2, motorId=%3, mode=%4, errorCode=%5")
							.arg(serialPort).arg(nodeId).arg(motorId).arg(mode).arg(errorCode));
	}
}
