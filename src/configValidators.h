#pragma once
#if !defined(CONFIGVALIDATOR_H) && !defined(DOXYGEN_SKIP)
#define CONFIGVALIDATOR_H
//######################################################################################################################
/*! @file
@brief Configuration file constants - Input validators.
*/
//######################################################################################################################

#include "iniFile.h"

//######################################################################################################################

//! @hideinitializer Settings group prefix.
#define CFG_VALIDATORS_GROUP	"validator_limits/"

//! Group ID input validator minimum setting.
INI_DEFINE_INTEGER(CfgValidatorsGroupIdMin,
					CFG_VALIDATORS_GROUP "group_id_minimum",
					128);

//! Group ID input validator makimum setting.
INI_DEFINE_INTEGER(CfgValidatorsGroupIdMax,
					CFG_VALIDATORS_GROUP "group_id_maximum",
					254);

//! Maximum power input validator minimum setting.
INI_DEFINE_INTEGER(CfgValidatorsMaximumPowerMin,
					CFG_VALIDATORS_GROUP "maximum_power_minimum",
					25);

//! Maximum power input validator maximum setting.
INI_DEFINE_INTEGER(CfgValidatorsMaximumPowerMax,
					CFG_VALIDATORS_GROUP "maximum_power_maximum",
					800);

//! Motor ID input validator minimum setting.
INI_DEFINE_INTEGER(CfgValidatorsMotorIdMin,
					CFG_VALIDATORS_GROUP "motor_id_minimum",
					0);

//! Motor ID input validator maximum setting.
INI_DEFINE_INTEGER(CfgValidatorsMotorIdMax,
					CFG_VALIDATORS_GROUP "motor_id_maximum",
					59);

//! Node ID input validator minimum setting.
INI_DEFINE_INTEGER(CfgValidatorsNodeIdMin,
					CFG_VALIDATORS_GROUP "node_id_minimum",
					0);

//! Node ID input validator maximum setting.
INI_DEFINE_INTEGER(CfgValidatorsNodeIdMax,
					CFG_VALIDATORS_GROUP "node_id_maximum",
					127);

//######################################################################################################################
#endif
