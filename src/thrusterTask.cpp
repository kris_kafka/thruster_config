/*! @file
@brief Define the thruster interface module.
*/
//######################################################################################################################

#include "app_com.h"

#include <QtCore/QByteArray>

#include "options.h"
#include "thruster.h"
#include "thrusterTask.h"
#include "thrusterTask_log.h"
#include "thrusterTaskEmulated.h"
#include "thrusterTaskReal.h"

//######################################################################################################################
// Signalling macros.

//----------------------------------------------------------------------------------------------------------------------
/*! @hideinitializer @brief Log and emit a signal with an error code without changing the state.
@param signal		Signal to emit.
@param newState		New state.
*/
#define EMIT_START(signal, newState) \
		do { \
			handler->state = newState; \
			EMIT_SIGNAL(signal, Thruster::TEC_NoError); \
		} while (0)

//----------------------------------------------------------------------------------------------------------------------
/*! @hideinitializer @brief Log and emit a signal with an error code without changing the state.
@param signal		Signal to emit.
@param errorCode	Error code to send with the signal.
*/
#define EMIT_STOP(signal, errorCode) \
		do { \
			handler->state = Thruster::State::Idle; \
			EMIT_SIGNAL(signal, errorCode); \
		} while (0)

//######################################################################################################################
/*! @brief Create a ThrusterTask object.
*/
ThrusterTask::ThrusterTask(void)
:
	handler		(Q_NULLPTR)
{
	LOG_Trace(loggerCreate, "Creating a ThrusterTask object");
	//
	ThrusterTaskBase	*h = Q_NULLPTR;
	if (Options().isEmulation()) {
		h = new (std::nothrow) ThrusterTaskEmulated();
		LOG_CHK_PTR_MSG(logger, h, "Unable to create ThrusterTaskEmulated object");
	} else {
		h = new (std::nothrow) ThrusterTaskReal();
		LOG_CHK_PTR_MSG(logger, h, "Unable to create ThrusterTaskReal object");
	}
	handler.reset(h);
	LOG_CONNECT(loggerCreate,	handler.data(),	&ThrusterTaskBase::configurationReadStop,
								this,			&ThrusterTask::configurationReadStop);
	LOG_CONNECT(loggerCreate,	handler.data(),	&ThrusterTaskBase::configurationWriteStop,
								this,			&ThrusterTask::configurationWriteStop);
	LOG_CONNECT(loggerCreate,	handler.data(),	&ThrusterTaskBase::idsChangeStop,
								this,			&ThrusterTask::idsChangeStop);
	LOG_CONNECT(loggerCreate,	handler.data(),	&ThrusterTaskBase::quitThread,
								this,			&ThrusterTask::quitThread);
	LOG_CONNECT_Q(loggerCreate,	handler.data(),	&ThrusterTaskBase::scanForThrustersNews,
								this,			&ThrusterTask::scanForThrustersNews);
	LOG_CONNECT_Q(loggerCreate,	handler.data(),	&ThrusterTaskBase::scanForThrustersStop,
								this,			&ThrusterTask::scanForThrustersStop);
	LOG_CONNECT(loggerCreate,	handler.data(),	&ThrusterTaskBase::testingModeNews,
								this,			&ThrusterTask::testingModeNews);
	LOG_CONNECT(loggerCreate,	handler.data(),	&ThrusterTaskBase::testingModeStop,
								this,			&ThrusterTask::testingModeStop);
}

//======================================================================================================================
/*! @brief Destroy a ThrusterTask object.
*/
ThrusterTask::~ThrusterTask(void)
{
	LOG_Trace(loggerCreate, "Destroying a ThrusterTask object");
}

//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
/*! @brief Read the configuration of @a nodeId.
*/
void
ThrusterTask::configurationRead(
	int	nodeId	//!<[in] ID of node to read configuration from.
)
{
	LOG_Trace(loggerConfigurationRead, QString("Read configuration of thruster: nodeId=%1").arg(nodeId));
	// Ensure we are idle.
	if (!isIdle()) {
		LOG_Error(loggerConfigurationRead,
					QString("Not idle (%1) when attempting to read configuration").arg(+state()));
		EMIT_SIGNAL(configurationReadStart, Thruster::TEC_NotIdle);
		return;
	}
	// Should we stop.
	if (handler->taskHalt) {
		LOG_Info(loggerConfigurationRead, "Background task stopping");
		EMIT_SIGNAL(configurationReadStart, Thruster::TEC_Stopping);
		LOG_Trace(loggerConfigurationRead, "Quiting the background task");
		Q_EMIT quitThread();
		return;
	}
	// Report the state change.
	EMIT_START(configurationReadStart, Thruster::State::ConfigurationRead);
	// Perform the operation.
	int			groupId, maximumPower, motorId, nodeIdRead;
	bool		rotationReverse;
	QString		serialNumber;
	QByteArray	csrBlob;
	if (!handler->configurationRead(nodeId, nodeIdRead, groupId, motorId, maximumPower, rotationReverse, serialNumber,
										csrBlob)) {
		return;
	}
	//
	LOG_Trace(loggerConfigurationRead,
				QString("Read the configuration of thruster finished: nodeId=%1, nodeIdRead=%2, groupId=%3, "
								"motorID=%4, maximumPower=%5, rotationReverse=%6, SerialNumber=%7")
						.arg(nodeId).arg(nodeIdRead).arg(groupId).arg(motorId).arg(maximumPower).arg(rotationReverse)
						.arg(serialNumber));
	// Ensure the node ID read from the thruster matches the original node ID.
	if (nodeId != nodeIdRead) {
		LOG_Error(loggerConfigurationRead,
					QString("The node ID read from the thruster (%1) does not match the requested node ID (%2)")
							.arg(nodeIdRead).arg(nodeId));
		EMIT_STOP(configurationReadStop, Thruster::TEC_NodeIdMismatch);
		return;
	}
	// Report the result.
	LOG_Debug(loggerConfigurationRead,
				QString("Emit configurationReadNews: nodeId=%1, groupId=%2, motorId=%3, maximumPower=%4, "
								"rotationReverse=%5, serialNumber=%6")
						.arg(nodeId).arg(groupId).arg(motorId).arg(maximumPower)
						.arg(rotationReverse).arg(serialNumber));
	Q_EMIT configurationReadNews(nodeId, groupId, motorId, maximumPower, rotationReverse, serialNumber, csrBlob);
	// Report successful completion.
	EMIT_STOP(configurationReadStop, Thruster::TEC_NoError);
}

//======================================================================================================================
/*! @brief Write the configuration of @a nodeId.
*/
void
ThrusterTask::configurationWrite(
	int					nodeId,				//!<[in] ID of node to write configuraton to.
	int					motorId,			//!<[in] New motor ID.
	int					maximumPower,		//!<[in] New maximum power level.
	bool				rotationReverse,	//!<[in] New rotation is reversed.
	QByteArray const	&csrBlob			//!<[in] CSR blob.
)
{
	LOG_Trace(loggerConfigurationWrite,
				QString("Write configuration to thruster: nodeId=%1, motorID=%2, maximumPower=%3, rotationReverse=%4")
						.arg(nodeId).arg(motorId).arg(maximumPower).arg(rotationReverse));
	// Ensure we are idle.
	if (!isIdle()) {
		LOG_Error(loggerConfigurationWrite,
					QString("Not idle (%1) when attempting to write configuration to nodeId=%2")
							.arg(+state()).arg(nodeId));
		EMIT_SIGNAL(configurationWriteStart, Thruster::TEC_NotIdle);
		return;
	}
	// Should we stop.
	if (handler->taskHalt) {
		LOG_Info(loggerConfigurationWrite, "Background task stopping");
		EMIT_SIGNAL(configurationWriteStart, Thruster::TEC_Stopping);
		LOG_Trace(loggerConfigurationWrite, "Quiting the background task");
		Q_EMIT quitThread();
		return;
	}
	// Report the state change.
	EMIT_START(configurationWriteStart, Thruster::State::ConfigurationWrite);
	// Perform the operation.
	handler->configurationWrite(nodeId, motorId, maximumPower, rotationReverse, csrBlob);
}

//======================================================================================================================
/*! @brief Change the node ID of @a currentNode.
*/
void
ThrusterTask::idsChange(
	QString const	&serialNumber,	//!<[in] Serial number of the thruster.
	int				oldNodeId,		//!<[in] Old node ID of the thruster.
	int				newNodeId,		//!<[in] New node ID for the thruster.
	int				newGroupId		//!<[in] New group ID for the thruster.
)
{
	LOG_Trace(loggerIdsChange,
				QString("Changing the IDs of %1 (%2) to: node=%3, group=%4")
						.arg(oldNodeId).arg(serialNumber).arg(newNodeId).arg(newGroupId));
	//
	if (!isIdle()) {
		LOG_Error(loggerIdsChange,
					QString("Not idle (%1) when attempting to the change the IDs of %2 (%3) to: node=%4, group=%5")
							.arg(+state()).arg(oldNodeId).arg(serialNumber).arg(newNodeId).arg(newGroupId));
		EMIT_SIGNAL(idsChangeStart, Thruster::TEC_NotIdle);
		return;
	}
	// Should we stop.
	if (handler->taskHalt) {
		LOG_Info(loggerIdsChange, "Background task stopping");
		EMIT_SIGNAL(idsChangeStart, Thruster::TEC_Stopping);
		LOG_Trace(loggerIdsChange, "Quiting the background task");
		Q_EMIT quitThread();
		return;
	}
	// Report the state change.
	EMIT_START(idsChangeStart, Thruster::State::IdsChange);
	// Perform the operation.
	handler->idsChange(serialNumber, oldNodeId, newNodeId, newGroupId);
}

//======================================================================================================================
/*! @brief Query if the thruster interface module is busy.

@return true if the thruster interface module is busy.
*/
bool
ThrusterTask::isBusy(void)
{
	Thruster::State s = state();
	bool			answer = Thruster::State::Idle != s
							&& Thruster::State::NotStarted != s
							&& Thruster::State::Stopped != s;
	LOG_Trace(loggerQuery, QString("Query if the thruster interface is busy: %1").arg(answer));
	return answer;
}

//======================================================================================================================
/*! @brief Query if there are duplicate IDs.

@return true if there are duplicate IDs.
*/
bool
ThrusterTask::isDuplicateIds(void)
{
	bool	answer = 0 != handler->duplicateNodeIdCount;
	LOG_Trace(loggerQuery, QString("Query if duplicate IDs: %1").arg(answer));
	return answer;
}

//======================================================================================================================
/*! @brief Query if there are duplicate serial numbers.

@return true if there are duplicate serial numbers.
*/
bool
ThrusterTask::isDuplicateSerials(void)
{
	bool	answer = 0 != handler->duplicateSerialCount;
	LOG_Trace(loggerQuery, QString("Query if duplicate serial numbers: %1").arg(answer));
	return answer;
}

//======================================================================================================================
/*! @brief Query if the thruster interface module is idle.

@return true if the thruster interface module is idle.
*/
bool
ThrusterTask::isIdle(void)
{
	bool	answer = Thruster::State::Idle == state();
	LOG_Trace(loggerQuery, QString("Query if idle: %1").arg(answer));
	return answer;
}

//======================================================================================================================
/*! @brief Handle the background task starting.
*/
void
ThrusterTask::moduleStarted(void)
{
	handler->state = Thruster::State::Idle;
}

//======================================================================================================================
/*! @brief Begin stopping the thruster interface module module.
*/
void
ThrusterTask::moduleStop(void)
{
	// Stop the background task.
	handler->taskHalt = true;
	handler->testModeHalt = true;
}

//======================================================================================================================
/*! @brief Handle the background task stopping.
*/
void
ThrusterTask::moduleStopped(void)
{
	handler->state = Thruster::State::Stopped;
}

//======================================================================================================================
/*! @brief Scan for thrusters.
*/
void
ThrusterTask::scanForThrusters(void)
{
	LOG_Trace(loggerScanForThrusters, "Start a scan for thrusters");
	// Ensure we are idle.
	if (!isIdle()) {
		LOG_Error(loggerScanForThrusters,
					QString("Not idle (%1) when attempting to scan for thrusters").arg(+state()));
		EMIT_SIGNAL(scanForThrustersStart, Thruster::TEC_NotIdle);
		return;
	}
	// Should we stop.
	if (handler->taskHalt) {
		LOG_Info(loggerScanForThrusters, "Background task stopping");
		EMIT_SIGNAL(scanForThrustersStart, Thruster::TEC_Stopping);
		LOG_Trace(loggerScanForThrusters, "Quiting the background task");
		Q_EMIT quitThread();
		return;
	}
	// Report the state change.
	EMIT_START(scanForThrustersStart, Thruster::State::ScanForThrusters);
	// Perform the operation.
	if (!handler->scanForThrusters()) {
		return;
	}
	// Record the number of duplicate node IDs.
	int	errorCode = Thruster::TEC_NoError;
	if (handler->duplicateNodeIdCount) {
		LOG_Info(loggerScanForThrusters, QString("%1 duplicate node IDs").arg(handler->duplicateNodeIdCount));
	}
	// Record the number of duplicate serial numbers.
	if (handler->duplicateSerialCount) {
		LOG_Warn(loggerScanForThrusters, QString("%1 duplicate serial numbers").arg(handler->duplicateSerialCount));
	}
	// Report the state change.
	EMIT_STOP(scanForThrustersStop, errorCode);
}

//======================================================================================================================
/*! @brief Change the active serial port to @a port.
*/
void
ThrusterTask::serialPortChange(
	QString const	&portName	//!<[in] New serial port.
)
{
	LOG_Trace(loggerSerialPortChange, QString("Change serial port to '%1'").arg(portName));
	// Ensure we are idle.
	if (!isIdle()) {
		LOG_Error(loggerSerialPortChange,
					QString("Not idle (%1) when attempting to the serical port").arg(+state()));
		EMIT_SIGNAL(serialPortChangeStart, Thruster::TEC_NotIdle);
		return;
	}
	// Should we stop.
	if (handler->taskHalt) {
		LOG_Info(loggerSerialPortChange, "Background task stopping");
		EMIT_SIGNAL(serialPortChangeStart, Thruster::TEC_Stopping);
		LOG_Trace(loggerSerialPortChange, "Quiting the background task");
		Q_EMIT quitThread();
		return;
	}
	// Report the state change.
	EMIT_START(serialPortChangeStart, Thruster::State::SerialPortChange);
	// Check the serial port and select it if it is valid.
	if (!handler->serialPortSet(portName)) {
		EMIT_STOP(serialPortChangeStop, Thruster::TEC_OpenError);
		return;
	}
	// Report the state change.
	LOG_Debug(loggerSerialPortChange, QString("Changed serial port to '%1'").arg(portName));
	EMIT_STOP(serialPortChangeStop, Thruster::TEC_NoError);
}

//======================================================================================================================
/*! @brief Query the available serial ports.

We do no error checking on the list of emulated serial ports in the configuration file.

@return list of available serial ports.
*/
QStringList
ThrusterTask::serialPortsAvailable(void)
{
	// Return the available serial ports if not emulating them.
	return Options().isEmulation()
			? ThrusterTaskEmulated::serialPortsAvailable()
			: ThrusterTaskReal::serialPortsAvailable();
}

//======================================================================================================================
/*! @brief Check a serial port and select it if valid.

@return true if valid else false.
*/
bool
ThrusterTask::serialPortSet(
	QString const	&portName	//!<[in] New serial port.
)
{
	return handler->serialPortSet(portName);
}

//======================================================================================================================
/*! @brief Query the state the thruster interface module is in.

@return the state the thruster interface module is in.
*/
Thruster::State
ThrusterTask::state(void)
{
	Thruster::State	answer = handler->state;
	return answer;
}

//======================================================================================================================
/*! @brief Start testing mode.
*/
void
ThrusterTask::testingModeBegin(
	int	nodeId,		//!<[in] Node ID of thruster to be tested.
	int	groupId,	//!<[in] Group ID of thruster to be tested.
	int	motorId,	//!<[in] Motor ID of thruster to be tested.
	int	mode		//!<[in] Testing mode.
)
{
	LOG_Trace(loggerTestingMode, QString("Starting testing mode: nodeId=%1").arg(nodeId));
	// Ensure we are idle.
	if (!isIdle()) {
		LOG_Error(loggerTestingMode,
					QString("Not idle (%1) when attempting to start testing thruster: nodeId=%2")
							.arg(+state()).arg(nodeId));
		EMIT_SIGNAL(testingModeStart, Thruster::TEC_NotIdle);
		return;
	}
	// Should we stop.
	if (handler->taskHalt) {
		LOG_Info(loggerTestingMode, "Background task stopping");
		EMIT_SIGNAL(testingModeStart, Thruster::TEC_Stopping);
		LOG_Trace(loggerTestingMode, "Quiting the background task");
		Q_EMIT quitThread();
		return;
	}
	//
	handler->testModeHalt = false;
	// Report the state change.
	EMIT_START(testingModeStart, Thruster::State::TestingMode);
	// Perform the operation.
	handler->testMode(nodeId, groupId, motorId, mode);
}

//======================================================================================================================
/*! @brief Stop testing mode.
*/
void
ThrusterTask::testingModeEnd(void)
{
	handler->testModeHalt = true;
}

//======================================================================================================================
/*! @brief Query the testing mode thruster speed.

@return current testing mode thruster speed.
*/
double
ThrusterTask::testingModeSpeed(void)
{
	LOG_Trace(loggerQuery, "Query the thruster testing mode speed");
	return handler->testingModeSpeed;
}

//======================================================================================================================
/*! @brief Change the testing mode thruster speed to @a speed.

@return true if okay.
*/
bool
ThrusterTask::testingModeSpeed(
	double	speed	//!<[in] New thruster speed.
)
{
	LOG_Trace(loggerTestingMode, QString("Changing thruster testing mode speed to %1").arg(speed));
	handler->testingModeSpeed = speed;
	handler->testModeSpeedChanged = true;
	return true;
}
