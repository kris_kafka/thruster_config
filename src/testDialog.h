#pragma once
#ifndef TESTDIALOG_H
#ifndef DOXYGEN_SKIP
#define TESTDIALOG_H
#endif
//######################################################################################################################
/*! @file
@brief Declare the interface to the test dialog module.
*/
//######################################################################################################################

#include <QtCore/QScopedPointer>
#include <QtGui/QCloseEvent>
#include <QtWidgets/QWidget>

//######################################################################################################################

class	TestDialogPrivate;

//######################################################################################################################
/*! @brief The TestDialog class provides the interface to the test dialog module.
*/
class TestDialog
{
	Q_DISABLE_COPY(TestDialog)

public:		// Constructors & destructors
	TestDialog(int nodeId, int groupId, int motorId, QWidget *parent = Q_NULLPTR);
	~TestDialog(void);

public:		// Functions
	void	invoke(void);

private:	// Data
	QScopedPointer<TestDialogPrivate>	d;	//!< Implementation object.
};

//######################################################################################################################
#endif
