#pragma once
#if !defined(THRUSTERTASK_LOG_H) && !defined(DOXYGEN_SKIP)
#define THRUSTERTASK_LOG_H
//######################################################################################################################
/*! @file
@brief Declare the thruster interface background task module loggers.
*/
//######################################################################################################################

#include "log.h"

//######################################################################################################################
// Loggers

LOG_SETUP_START("Thruster Interface Background Task Module");

// Top level module logger.
LOG_DEF_LOG(logger,							LOG_DL_TRACE,	"ThrusterTask");
// Configuration read background task logger.
LOG_DEF_LOG(loggerConfigurationRead,		LOG_DL_TRACE,	"ThrusterTask.ConfiguratioRead");
// Configuration background task logger.
LOG_DEF_LOG(loggerConfigurationWrite,		LOG_DL_TRACE,	"ThrusterTask.ConfigurationWrite");
// Object creation/destruction logger.
LOG_DEF_LOG(loggerCreate,					LOG_DL_TRACE,	"ThrusterTask.Create");
// Node ID change background task logger.
LOG_DEF_LOG(loggerIdsChange,				LOG_DL_TRACE,	"ThrusterTask.IdsChange");
// Query logger.
LOG_DEF_LOG(loggerQuery,					LOG_DL_INFO,	"ThrusterTask.Query");
// Scan for thrusters background task logger.
LOG_DEF_LOG(loggerScanForThrusters,			LOG_DL_TRACE,	"ThrusterTask.ScanForThrusters");
// Serial port change background task logger.
LOG_DEF_LOG(loggerSerialPortChange,			LOG_DL_DEBUG,	"ThrusterTask.SerialPortChange");
// Module signals logger.
LOG_DEF_LOG(loggerSignals,					LOG_DL_TRACE,	"ThrusterTask.Signals");
// Test thruster logger.
LOG_DEF_LOG(loggerTestingMode,				LOG_DL_TRACE,	"ThrusterTask.TestingMode");

LOG_SETUP_STOP();

//######################################################################################################################
#endif
