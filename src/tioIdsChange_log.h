#pragma once
#if !defined(TIOIDSCHANGE_LOG_H) && !defined(DOXYGEN_SKIP)
#define TIOIDSCHANGE_LOG_H
//######################################################################################################################
/*! @file
@brief Declare the thruster I/O IDs change module loggers.
*/
//######################################################################################################################

#include "log.h"

//######################################################################################################################
// Loggers

LOG_SETUP_START("Thruster I/O IDs Change Module");

// Base logger.
LOG_DEF_LOG(logger,				LOG_DL_INFO,	"TioIdsChange");
// Create/destroy logger.
LOG_DEF_LOG(loggerCreate,		LOG_DL_INFO,	"TioIdsChange.Create");
// Node ID change command logger.
LOG_DEF_LOG(loggerCommand,		LOG_DL_INFO,	"TioIdsChange.Command");
// Main logger.
LOG_DEF_LOG(loggerMain,			LOG_DL_INFO,	"TioIdsChange.Main");
// Open/close logger.
LOG_DEF_LOG(loggerOpen,			LOG_DL_INFO,	"TioIdsChange.Open");
// Node ID change parameters logger.
LOG_DEF_LOG(loggerParameters,	LOG_DL_INFO,	"TioIdsChange.Parameters");
// Node ID change reply logger.
LOG_DEF_LOG(loggerReply,		LOG_DL_INFO,	"TioIdsChange.Reply");

LOG_SETUP_STOP();

//######################################################################################################################
#endif
