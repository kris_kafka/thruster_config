#pragma once
#if !defined(CSRINFO_LOG_H) && !defined(DOXYGEN_SKIP)
#define CSRINFO_LOG_H
//######################################################################################################################
/*! @file
@brief Declare the CSR info module loggers.
*/
//######################################################################################################################

#include "log.h"

//######################################################################################################################
// Loggers

LOG_SETUP_START("CSR Info Module");

// Top level module logger.
LOG_DEF_LOG(logger,				LOG_DL_INFO,	"CsrInfo");
// Bad data logger.
LOG_DEF_LOG(loggerBadData,		LOG_DL_INFO,	"CsrInfo.BadData");
// Check logger.
LOG_DEF_LOG(loggerCheck,		LOG_DL_INFO,	"CsrInfo.Check");
// Object creation/destruction logger.
LOG_DEF_LOG(loggerCreate,		LOG_DL_INFO,	"CsrInfo.Create");
// Query name logger.
LOG_DEF_LOG(loggerQueryName,	LOG_DL_INFO,	"CsrInfo.QueryName");
// Query size logger.
LOG_DEF_LOG(loggerQuerySize,	LOG_DL_INFO,	"CsrInfo.QuerySize");
// Query type logger.
LOG_DEF_LOG(loggerQueryType,	LOG_DL_INFO,	"CsrInfo.QueryType");

LOG_SETUP_STOP();

//######################################################################################################################
#endif
