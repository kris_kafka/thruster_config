#pragma once
#if !defined(CONFIGEMULATION_H) && !defined(DOXYGEN_SKIP)
#define CONFIGEMULATION_H
//######################################################################################################################
/*! @file
@brief Configuration file constants - Emulation.
*/
//######################################################################################################################

#include "iniFile.h"

//######################################################################################################################
// Delays.

//! @hideinitializer Settings group prefix of the emulation delay values.
#define CFG_EMUL_DLY_GROUP	"emulation_delays/"

//! Read configuration emulation delay setting.
INI_DEFINE_INTEGER(CfgEmulationDelayConfigRead,
					CFG_EMUL_DLY_GROUP "configuration_read",
					100);

//! Write configuration emulation delay Setting.
INI_DEFINE_INTEGER(CfgEmulationDelayConfigWrite,
					CFG_EMUL_DLY_GROUP "configuration_write",
					100);

//! Change node ID emulation delay Setting.
INI_DEFINE_INTEGER(CfgEmulationDelayIdsChange,
					CFG_EMUL_DLY_GROUP "ids_change",
					100);

//! Scan for thrusters emulation delay Setting.
INI_DEFINE_INTEGER(CfgEmulationDelayScanForThrusters,
					CFG_EMUL_DLY_GROUP "scan_for_thrusters",
					100);

//! Test thruster emulation delay Setting.
INI_DEFINE_INTEGER(CfgEmulationDelayTestingMode,
					CFG_EMUL_DLY_GROUP "testing_mode",
					100);

//######################################################################################################################
// Serial ports.

//! Emulated serial ports group.
INI_DEFINE_ARRAY(CfgEmulationPorts, "emulation_serial_ports");

//! Emulated serial port setting.
INI_DEFINE_STRING(CfgEmulationPortsName,
					"port",
					"");

//! End of emulated ports array.
INI_END_ARRAY();

//######################################################################################################################
// Testing mode emulation formulas.

//! @hideinitializer Settings group prefix of the emulation delay values.
#define CFG_EMUL_RPN_GROUP	"emulation_formulas/"

//! Emulation formulas group.
INI_DEFINE_GROUP(CfgEmulationRpn, "emulation_formulas");
//! End of emulation formulas group.
INI_END_GROUP();

//! Bus current emulation formula.
INI_DEFINE_STRING(CfgEmulationRpnBusAmps,
					CFG_EMUL_RPN_GROUP "current",
					"speed abs 0.1 * 0.08 +");

//! Bus voltage emulation formula.
INI_DEFINE_STRING(CfgEmulationRpnBusVolts,
					CFG_EMUL_RPN_GROUP "voltage",
					"48.0 speed abs 0.01 * -");

//! Fault flags emulation formula.
INI_DEFINE_STRING(CfgEmulationRpnFaultFlags,
					CFG_EMUL_RPN_GROUP "faults",
					"0");

//! RPM emulation formula.
INI_DEFINE_STRING(CfgEmulationRpnRpm,
					CFG_EMUL_RPN_GROUP "rpm",
					"speed 40.0 *");

//! Temperature emulation formula.
INI_DEFINE_STRING(CfgEmulationRpnTemperature,
					CFG_EMUL_RPN_GROUP "temperature",
					"20.0 speed abs 0.0015 * +");

//! User defined symbols.
INI_DEFINE_STRING(CfgEmulationRpnSymbols,
					CFG_EMUL_RPN_GROUP "symbols",
					"");

//######################################################################################################################
// Thrusters.

//! Emulated thrusters group.
INI_DEFINE_ARRAY(CfgEmulationThruster, "emulation_thrusters");

//! Delay before finding emulated thruster during scan for thrusters.
INI_DEFINE_INTEGER(CfgEmulationThrusterDelay,
					"delay",
					250);

//! Emulated thruster find in scan.
INI_DEFINE_BOOLEAN(CfgEmulationThrusterFindInScan,
					"find_in_scan",
					true);

//! Emulated thruster group ID configuration.
INI_DEFINE_INTEGER(CfgEmulationThrusterGroupId,
					"group_id",
					128);

//! Emulated thruster maximum power configuration.
INI_DEFINE_INTEGER(CfgEmulationThrusterMaximumPower,
					"maximum_power",
					500);

//! Emulated thruster motor ID configuration key.
INI_DEFINE_INTEGER(CfgEmulationThrusterMotorId,
					"motor_id",
					0);

//! Emulated thruster node ID configuration key.
INI_DEFINE_INTEGER(CfgEmulationThrusterNodeId,
					"node_id",
					0);

//! Emulated thruster rotation reversed configuration key.
INI_DEFINE_BOOLEAN(CfgEmulationThrusterRotationReverse,
					"rotation_reverse",
					false);

//! Emulated thruster serial number key.
INI_DEFINE_STRING(CfgEmulationThrusterSerialNumber,
					"serial_number",
					"Emulated_%1");

//! End of emulated thrusters array.
INI_END_ARRAY();

//######################################################################################################################
#endif
