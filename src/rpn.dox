/*! @page rpnFormula														RPN Formulas
@section rpnFormulaIntrodution												Introduction
This section documents the reverse polish notation (RPN) formulas.

RPN formulas consist of space delimited sequence of tokens.
The tokens are evaluated one at time in a left to right order.

Each token is one of the following types:
Type		| Description
----		| -----------
Number		| A number. The number is pushed on the stack.
Constant	| A constant. The value of the constant is pushed on the stack.
Operator	| A name documented in @ref rpnFormulaOperators "RPN Operators".
Others		| A user/system defined symbol. The value of the symbol is pushed on the stack.
<!----------------------------------------------------------------------------->
<hr>
@section rpnFormulaConstants												RPN Constants
This section documents the RPN formula constants available.

Name		| Description			| Value
--------	| -----------			| ------
1/2			| 1/2					| 0.5
1/3			| 1/3					| 0.33333333333....
2/3			| 2/3					| 0.66666666666....
e			| e						| 2.71828182845...
euler		| euler's constant		| 0.57721566490...
ln_2		| ln_2					| 0.69314718055...
pi			| pi					| 3.14159265358...
root_2		| square root of 2		| 1.41421356237...
root_2*pi	| square root of 2*pi	| 2.50662827463...
root_pi		| square root of pi		| 1.77245385090...
root_pi/2	| square root of pi/2	| 1.25331413731...
<!----------------------------------------------------------------------------->
<hr>
@section rpnFormulaOperators												RPN Operators
This section documents the RPN formula operators available.

Each operator requires a certain number of values (see Args in the table) on the stack.
It is an error if the required values are not available which causes the evaluation of the formula halts.

After popping the required values from the stack, the result of the operation (see Results in the table) is pushed on the stack.

| Operator	| Description			| Args	| Results	| Example							| Stack				| Notes |
| --------	| -----------			| :--:	| :-----:	| -------							| ------			| ----- |
| !=		| not equal				| 2		| 1			| 7 2 !=							| 1					| |
| *			| multiplication		| 2		| 1			| 7 2 *								| 14				| |
| +			| addition				| 2		| 1			| 7 2 +								| 9					| |
| -			| subtraction			| 2		| 1			| 7 2 -								| 5					| |
| /			| division				| 2		| 1			| 7 2 /								| 3.5				| |
| \<		| less than				| 2		| 1			| 7 2 <								| 0					| |
| \<=		| less than or equal	| 2		| 1			| 7 2 <=							| 0					| |
| =			| equal					| 2		| 1			| 7 2 = 3 3 =						| 0 1				| |
| \>		| greater than			| 2		| 1			| 7 2 >								| 1					| |
| \>=		| greater than or equal	| 2		| 1			| 7 2 >=							| 1					| |
| \\		| integer division		| 2		| 1			| 7 2 \\							| 3					| |
| _rot		| rotate 3 values		| 3		| 3			| 7 2 5 _rot						| 5 7 2				| |
| abs		| absolute value		| 1		| 1			| -123 abs 456 abs 0 abs			| 123 456 0			| |
| and		| logical and			| 2		| 1			| 0 0 and 0 1 and 1 0 and 2 3 and	| 0 0 0 1			| |
| arccos	| arc cosine			| 1		| 1			| 1 arccos							| 0					| |
| arcsin	| arc sine				| 1		| 1			| 0 arcsin							| 0					| |
| arctan	| arc sine				| 1		| 1			| 0 arctan							| 0					| |
| ceil		| round upward			| 1		| 1			| 7 2 / ceil						| 4					| Smallest integral value that is not less than x. |
| chs		| change sign			| 1		| 1			| 7 chs								| -7				| |
| cos		| cosine				| 1		| 1			| 0 cos								| 1					| |
| drop		| discard value			| 1		| 0			| 7 2 drop							| 7					| |
| dup		| duplicate value		| 1		| 2			| 7 2 dup							| 7 2 2				| |
| exp		| base-e exponential	| 1		| 1			| 1 exp								| 2.71828182845...	| |
| exp10		| base-10 exponential	| 1		| 1			| 2 exp10							| 100				| |
| floor		| round downward		| 1		| 1			| 7 2 / floor						| 3					| Largest integral value that is not greater than x. |
| if		| conditional			| 3		| 1			| 7 2 1 if 7 2 0 if					| 7 2				| '@a X @a Y @a Z if' = @a X if @a Z not zero else @a Y |
| limit		| restrict value		| 3		| 1			| 7 2 5 limit						| 5					| Assuming @a Y < @a Z, '@a X @a Y @a Z limit' = @a Y if @a X is less than @a Y, @a Z if @a X is greater than @a Z, else @a X |
| ln		| natural logarithm		| 1		| 1			| e ln								| 1					| |
| log		| common logarithm		| 1		| 1			| 10 log							| 1					| |
| max		| maximum				| 2		| 1			| 7 2 max							| 7					| |
| min		| minimum				| 2		| 1			| 7 2 min							| 2					| |
| mod		| modulo				| 2		| 1			| 7 2 mod							| 1					| |
| nip		| discard value			| 2		| 1			| 7 2 nip							| 2					| |
| not		| logical negation		| 2		| 1			| 0 not 1 not 2 not 3 not			| 1 0 0 0			| |
| or		| logical or			| 2		| 1			| 0 0 or 0 1 or 1 0 or 2 3 or		| 0 1 1 1			| |
| over		| duplicate value		| 2		| 3			| 7 2 over							| 7 2 7				| |
| pow		| exponentiation		| 2		| 1			| 7 2 pow							| 49				| |
| rand		| generate random		| 2		| 1			| 7 2 rand							| 5.2315739			| '@a X @a Y rand' = a random value between @a X and @a Y |
| remainder	| remainder				| 2		| 1			| 7 2 remainder						| 0.5				| Remainder of y/x (rounded to nearest). |
| remquo	| remainder & quotient	| 2		| 2			| 7 2 remquo						| 0.5 3				| Same as remainder but additionally stores the quotient. |
| root		| root					| 2		| 1			| 64 3 root							| 4					| Same as: 1.0 swap / pow |
| rot		| rotate 3 values		| 3		| 3			| 7 2 5 rot							| 2 5 7				| |
| round		| round					| 1		| 1			| pi round							| 3					| Integral value that is nearest to x, with halfway cases rounded away from zero. |
| sign		| determine sign		| 1		| 1			| -7 sign 2 sign 0 sign				| -1 1 0			| '@a X sign' = 1 if @a X is greater than 0, -1 if @a X is less than 0, else 0 |
| sin		| sine					| 1		| 1			| 0 sin								| 0					| |
| sqrt		| square root			| 1		| 1			| 16 sqrt							| 4					| |
| swap		| swap 2 values			| 2		| 2			| 7 2 swap							| 2 7				| |
| tan		| tangent				| 1		| 1			| 0 tan								| 0					| |
| trunc		| round toward zero		| 1		| 1			| pi trunc							| 3					| Nearest integral value that is not larger in magnitude than x. |
| xor		| logical xor			| 2		| 1			| 0 0 xor 0 1 xor 1 1 xor 2 3 xor	| 0 1 1 0			| |
*/
