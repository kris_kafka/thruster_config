#pragma once
#ifndef THRUSTERTASKREAL_H
#ifndef DOXYGEN_SKIP
#define THRUSTERTASKREAL_H
#endif
//######################################################################################################################
/*! @file
@brief Declare the thruster interface module background task.
*/
//######################################################################################################################

#include <QtCore/QObject>

#include "thrusterTaskBase.h"

//######################################################################################################################

class	QByteArray;
class	TioTestingMode;

//######################################################################################################################
/*! @brief The ThrusterPrivate class provides the implementation of the thruster interface module background task.
*/
class ThrusterTaskReal
:
public	ThrusterTaskBase
{
	Q_OBJECT
	Q_DISABLE_COPY(ThrusterTaskReal);

public:		// Constructors / Destructors
	explicit	ThrusterTaskReal(void);
	virtual		~ThrusterTaskReal(void);

public:		// Functions
	virtual bool		configurationRead(int nodeId, int &nodeIdRead, int &groupId, int &motorId, int &maximumPower,
											bool &rotationReverse, QString &serialNumber, QByteArray &csrBlob);
	virtual void		configurationWrite(int nodeId, int motorId, int maximumPower, bool rotationReverse,
											QByteArray const &csrBlob);
	virtual void		idsChange(QString const &serialNumber, int oldNodeId, int nodeId, int groupId);
	virtual bool		scanForThrusters(void);
	virtual bool		serialPortCheck(QString const &portName);
	static QStringList	serialPortsAvailable(void);
	virtual void		testMode(int nodeId, int groupId, int motorId, int mode);
	void				testModeTerminate(TioTestingMode &tio, int nodeId, int groupId, int motorId, int mode);
};

//######################################################################################################################
#endif
