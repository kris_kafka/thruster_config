/*! @file
@brief Define the empty int validator module.
*/
//######################################################################################################################

#include "app_com.h"

#include "packetUtil.h"

//######################################################################################################################
// Constants

//! Packet header size.
static QString const	BadIndex		= "--";
static QString const	HeaderFormat	= "sync=%1%2, ID=%3, flags=%4, addr=%5, len=%6, crc=%7";
static int const		HeaderSize		= PROTOCOL_VRCSR_HEADER_SIZE + PROTOCOL_VRCSR_XSUM_SIZE;
static QChar const		ZeroPad			= '0';

//######################################################################################################################
/*! @brief Format a packet header.

@return the formatted byte.
*/
QString
PacketUtil::formatByte(
	QByteArray const	&packet,	//!<[in] Packet to format.
	int					index		//!<[in] Index of byte in packet.
)
{
	return (0 <= index && index < packet.size())
			? QString("%1").arg(0xff & packet.at(index), 2, 16, ZeroPad)
			: BadIndex;
}

//======================================================================================================================
/*! @brief Format a data buffer.

@return the formatted data.
*/
QString
PacketUtil::formatData(
	QByteArray const	&data	//!<[in] Data to format.
)
{
	int	size = data.size();
	QString answer;
	answer.reserve(3 * size);
	for (int index = 0; index < size; ++index) {
		if (0 != index) {
			answer += " ";
		}
		answer += formatByte(data, index);
	}
	return answer;
}

//======================================================================================================================
/*! @brief Format a data buffer.

@return the formatted data.
*/
QString
PacketUtil::formatData(
	void const	*data,	//!<[in] Data to format.
	int			size	//!<[in] Size of data.
)
{
	size = qMax(size, 0);
	QString answer;
	answer.reserve(3 * size);
	for (int index = 0; index < size; ++index) {
		answer += QString(0 == index ? "%1" : " %1").arg(((uchar const *)data)[index], 2, 16, ZeroPad);
	}
	return answer;
}

//======================================================================================================================
/*! @brief Format a packet header.

@return the formatted header.
*/
QString
PacketUtil::formatHeader(
	QByteArray const	&packet	//!<[in] Packet to format.
)
{
	return HeaderFormat
			.arg(formatByte(packet, 0))
			.arg(formatByte(packet, 1))
			.arg(formatByte(packet, 2))
			.arg(formatByte(packet, 3))
			.arg(formatByte(packet, 4))
			.arg(formatByte(packet, 5))
			.arg(formatByte(packet, 6) + formatByte(packet, 7) + formatByte(packet, 8) + formatByte(packet, 9));
}

//======================================================================================================================
/*! @brief Format a packet header.

@return the formatted header.
*/
QString
PacketUtil::formatHeader(
	Protocol_VRCSR_Packet const	&packet	//!<[in] Packet to format.
)
{
	return HeaderFormat
			.arg(packet.header->sync[0],	2, 16, ZeroPad)
			.arg(packet.header->sync[1],	2, 16, ZeroPad)
			.arg(packet.header->id,			2, 16, ZeroPad)
			.arg(packet.header->flags,		2, 16, ZeroPad)
			.arg(packet.header->address,	2, 16, ZeroPad)
			.arg(packet.header->length,		2, 16, ZeroPad)
			.arg(packet.header_xsum,		8, 16, ZeroPad);
}

//======================================================================================================================
/*! @brief Format a packet header.

@return the formatted packet.
*/
QString
PacketUtil::formatHeader(
	void const	*data,	//!<[in] Packet to format.
	int			size	//!<[in] Size of packet.
)
{
	return formatHeader(QByteArray((char const *)data, 0 < size ? size : 0));
}

//======================================================================================================================
/*! @brief Format a packet.

@return the formatted packet.
*/
QString
PacketUtil::formatPacket(
	QByteArray const	&packet	//!<[in] Packet to format.
)
{
	QString	header = formatHeader(packet);
	QString	payload = formatPayload(packet);
	return payload.isEmpty() ? header : QString("%1, %2").arg(header).arg(payload);
}

//======================================================================================================================
/*! @brief Format a packet.

@return the formatted packet.
*/
QString
PacketUtil::formatPacket(
	Protocol_VRCSR_Packet const	&packet	//!<[in] Packet to format.
)
{
	QString	header = formatHeader(packet);
	QString	payload = formatPayload(packet);
	return payload.isEmpty() ? header : QString("%1, %2").arg(header).arg(payload);
}

//======================================================================================================================
/*! @brief Format a packet.

@return the formatted packet.
*/
QString
PacketUtil::formatPacket(
	void const	*data,	//!<[in] Packet to format.
	int			size	//!<[in] Size of packet.
)
{
	return formatPacket(QByteArray((char const *)data, 0 < size ? size : 0));
}

//======================================================================================================================
/*! @brief Format a packet payload.

@return the formatted payload.
*/
QString
PacketUtil::formatPayload(
	QByteArray const	&packet	//!<[in] Packet to format.
)
{
	int	size = packet.size();
	int	paySize = size - HeaderSize;
	QString answer;
	if (0 < paySize) {
		int	datSize = paySize - PROTOCOL_VRCSR_XSUM_SIZE;
		int	index = HeaderSize;
		if (0 < datSize) {
			int datEnd = packet.size() - PROTOCOL_VRCSR_XSUM_SIZE;
			for ( ; index < datEnd; ++index) {
				answer += HeaderSize == index ? "data=" : " ";
				answer += formatByte(packet, index);
			}
		} else {
			answer = "data=__";
		}
		answer += ", crc=";
		for ( ; index < size; ++index) {
			answer += formatByte(packet, index);
		}
	}
	return answer;
}

//======================================================================================================================
/*! @brief Format a packet payload.

@return the formatted payload.
*/
QString
PacketUtil::formatPayload(
	Protocol_VRCSR_Packet const	&packet	//!<[in] Packet to format.
)
{
	int	len = packet.header->length;
	QString answer;
	if (len) {
		answer.reserve(32 + 3 * len);
		for (int i = 0; i < len; ++i) {
			answer += QString(0 == i ? "data=%1" : " %1").arg(packet.payload[i], 2, 16, ZeroPad);
		}
		answer += QString(", crc=%1").arg(packet.payload_xsum, 8, 16, ZeroPad);
	}
	return answer;
}

//======================================================================================================================
/*! @brief Format a packet payload.

@return the formatted packet.
*/
QString
PacketUtil::formatPayload(
	void const	*data,	//!<[in] Packet to format.
	int			size	//!<[in] Size of packet.
)
{
	return formatPayload(QByteArray((char const *)data, 0 < size ? size : 0));
}
