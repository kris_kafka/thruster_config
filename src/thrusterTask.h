#pragma once
#ifndef THRUSTERTASK_H
#ifndef DOXYGEN_SKIP
#define THRUSTERTASK_H
#endif
//######################################################################################################################
/*! @file
@brief Declare the thruster interface module background task.
*/
//######################################################################################################################

#include <QtCore/QElapsedTimer>
#include <QtCore/QObject>
#include <QtCore/QScopedPointer>
#include <QtCore/QTimer>
#include <QtCore/QVector>

#include "thruster.h"
#include "thrusterTaskBase.h"

//######################################################################################################################

class	QSettings;
class	QThread;
class	TioTestingMode;

//######################################################################################################################
/*! @brief The ThrusterPrivate class provides the implementation of the thruster interface module background task.
*/
class ThrusterTask
:
public	QObject
{
	Q_OBJECT
	Q_DISABLE_COPY(ThrusterTask);

public:		// Constructors / Destructors
	explicit	ThrusterTask(void);
				~ThrusterTask(void);

public:		// Functions
	Q_SLOT void			configurationRead(int nodeId);
	Q_SLOT void			configurationWrite(int nodeId, int motorId, int maximumPower, bool rotationReverse,
											QByteArray const &csrBlob);
	Q_SLOT void			idsChange(QString const &serialNumber, int oldNodeId, int newNodeId, int newGroupId);
	bool				isBusy(void);
	bool				isDuplicateIds(void);
	bool				isDuplicateSerials(void);
	bool				isIdle(void);
	void				moduleStarted(void);
	void				moduleStop(void);
	void				moduleStopped(void);
	Q_SLOT void			scanForThrusters(void);
	Q_SLOT void			serialPortChange(QString const &portName);
	static QStringList	serialPortsAvailable(void);
	bool				serialPortSet(QString const &portName);
	Thruster::State		state(void);
	Q_SLOT void			testingModeBegin(int nodeId, int groupId, int motorId, int mode);
	void				testingModeEnd(void);
	double				testingModeSpeed(void);
	bool				testingModeSpeed(double speed);

public:		// Data
	QScopedPointer<ThrusterTaskBase>
					handler;				//!< Thruster task handler.

Q_SIGNALS:
	//! Configuration data read from thruster.
	void	configurationReadNews(
					int			nodeId,				//!< Node ID of the thruster.
					int			groupId,			//!< Group ID read from thruster.
					int			motorId,			//!< Motor ID read from thruster.
					int			maximumPower,		//!< Maximum power read from thruster.
					bool		rotationReverse,	//!< Rotation is reversed read from thruster.
					QString		serialNumber,		//!< Serial number read from thruster.
					QByteArray	csrBlob				//!< CSR blob.
					);

	//! Configuration data read started.
	void	configurationReadStart(
					int		errorCode		//!< Error code (TEC_NoError if no errors).
					);

	//! Configuration data read finished.
	void	configurationReadStop(
					int		errorCode		//!< Error code (TEC_NoError if no errors).
					);

	//! Configuration data write started.
	void	configurationWriteStart(
					int		errorCode		//!< Error code (TEC_NoError if no errors).
					);

	//! Configuration data write finished.
	void	configurationWriteStop(
					int		errorCode		//!< Error code (TEC_NoError if no errors).
					);

	//! Change node ID started.
	void	idsChangeStart(
					int		errorCode		//!< Error code (TEC_NoError if no errors).
					);

	//! Change node ID finished.
	void	idsChangeStop(
					int		errorCode		//!< Error code (TEC_NoError if no errors).
					);

	//! Quit the background thread.
	void	quitThread(void);

	//! Thruster information found during scan for thrusters.
	void	scanForThrustersNews(
					int		nodeId,			//!< Node ID of thruster.
					int		groupId,		//!< Group ID of thruster.
					QString	serialNumber	//!< Serial number of thruster.
					);

	//! Scanning for thrusters started.
	void	scanForThrustersStart(
					int		errorCode		//!< Error code (TEC_NoError if no errors).
					);

	//! Scanning for thrusters finished.
	void	scanForThrustersStop(
					int		errorCode		//!< Error code (TEC_NoError if no errors).
					);

	//! Serial port change started.
	void	serialPortChangeStart(
					int		errorCode		//!< Error code (TEC_NoError if no errors).
					);

	//! Serial port change finished.
	void	serialPortChangeStop(
					int		errorCode		//!< Error code (TEC_NoError if no errors).
					);

	//! Testing mode status data.
	void	testingModeNews(
					int		nodeId,			//!< Node ID.
					double	rpm,			//!< Thruster current RPM.
					double	voltage,		//!< Bus voltage.
					double	current,		//!< Bus current.
					double	temperature,	//!< Thruster temperature.
					uint	faults			//!< Thruster fault flags.
					);

	//! Testing mode started.
	void	testingModeStart(
					int		errorCode		//!< Error code (TEC_NoError if no errors).
					);

	//! Testing mode stopped.
	void	testingModeStop(
					int		errorCode		//!< Error code (TEC_NoError if no errors).
					);
};

//######################################################################################################################
#endif
