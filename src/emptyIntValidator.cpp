/*! @file
@brief Define the empty int validator module.
*/
//######################################################################################################################

#include "app_com.h"

#include "emptyIntValidator.h"

//######################################################################################################################
/*! @brief Create a EmptyIntValidator object.
*/
EmptyIntValidator::EmptyIntValidator(
	QObject	*parent	//!<[in] The parent.
)
:
	QIntValidator	(parent)
{
}

//----------------------------------------------------------------------------------------------------------------------
/*! @brief Create a EmptyIntValidator object.
*/
EmptyIntValidator::EmptyIntValidator(
	int		minimum,	//!<[in] Minimum valid value.
	int		maximum,	//!<[in] Maximum valid value.
	QObject	*parent		//!<[in] The parent.
)
:
	QIntValidator(minimum, maximum,parent)
{
}

//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
/*! @brief Validate an integer.

@return QValidator::Acceptable if @a input is empty, else QIntValidator::validate(input, pos).
*/
QValidator::State
EmptyIntValidator::validate(
	QString	&input,	//!<[in,out] Input text.
	int		&pos	//!<[in,out] Cursor position within input.
) const
{
	return input.isEmpty() ? QValidator::Acceptable : QIntValidator::validate(input, pos);
}
