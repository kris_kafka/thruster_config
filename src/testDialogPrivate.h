#pragma once
#ifndef TESTDIALOGPRIVATE_H
#ifndef DOXYGEN_SKIP
#define TESTDIALOGPRIVATE_H
#endif
//######################################################################################################################
/*! @file
@brief Declare the implementation of the test dialog module.
*/
//######################################################################################################################

#include <QtCore/QScopedPointer>
#include <QtCore/QStringList>
#include <QtCore/QVariant>
#include <QtCore/QVector>

#include "ui_testDialog.h"

//######################################################################################################################

class	QCloseEvent;
class	Thruster;

//######################################################################################################################
/*! @brief The TestDialogPrivate class provides the implementation of the test dialog module.
*/
class TestDialogPrivate
:
	public	QDialog,
	public	Ui_TestDialog

{
	Q_OBJECT
	Q_DISABLE_COPY(TestDialogPrivate)

public:		// Constructors & destructors
	TestDialogPrivate(int nodeId_, int groupId_, int motorId_, QWidget *parent);
	~TestDialogPrivate(void);

public:		// Functions
	Q_SLOT void		at_thruster_testModeNews(int nodeId_, double rpm, double voltage, double current,
												double temperature, uint faults);
	Q_SLOT void		at_thruster_testModeStart(int errorCode);
	Q_SLOT void		at_thruster_testModeStop(int errorCode);
	void			initModes(void);
	Q_SLOT void		on_modeComboBox_currentIndexChanged(int newValue);
	Q_SLOT void		on_speedSlider_valueChanged(int newValue);
	Q_SLOT void		on_speedSpinBox_valueChanged(double newValue);
	Q_SLOT void		on_startPushButton_clicked(void);
	Q_SLOT void		on_stopPushButton_clicked(void);
	void			restoreWindowState(void);
	void			saveWindowState(void);
	void			setSpeed(double newSpeed);
	void			setupSpeedControls(void);
	void			showErrorMessage(QString const &message);
	void			updateGuiState(void);

protected:	// Functions
	virtual void	closeEvent(QCloseEvent *event)	override;

public:		// Data
	int							currentMode;	//!< Current I/O mode.
	int							groupId;		//!< Group ID of thruster.
	QVector<bool>				isCsr;			//!< CSR read/write commands or propulsion command.
	int							nodeId;			//!< Node ID of thruster.
	int							motorId;		//!< Motor ID of thruster.
	double						sliderFactor;	//!< Speed slider limit factor.
	QScopedPointer<Thruster>	thruster;		//!< Thruster interface object.
};

//######################################################################################################################
#endif
