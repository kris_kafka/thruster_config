#pragma once
#if !defined(MAINWINDOW_LOG_H) && !defined(DOXYGEN_SKIP)
#define MAINWINDOW_LOG_H
//######################################################################################################################
/*! @file
@brief Declare the main window loggers.
*/
//######################################################################################################################

#include "log.h"

//######################################################################################################################
// Loggers

LOG_SETUP_START("Main Window");

// Top level module logger.
LOG_DEF_LOG(logger,						LOG_DL_TRACE,	"MainWindow");
// Thruster configuration read logger.
LOG_DEF_LOG(loggerConfigurationRead,	LOG_DL_TRACE,	"MainWindow.ConfigurationRead");
// Thruster configuration write logger.
LOG_DEF_LOG(loggerConfigurationWrite,	LOG_DL_TRACE,	"MainWindow.ConfigurationWrite");
// Object creation logger.
LOG_DEF_LOG(loggerCreate,				LOG_DL_TRACE,	"MainWindow.Create");
// Object destruction logger.
LOG_DEF_LOG(loggerDestroy,				LOG_DL_TRACE,	"MainWindow.Destroy");
// GUI state update logger.
LOG_DEF_LOG(loggerGuiState,				LOG_DL_INFO,	"MainWindow.GuiState");
// Thruster configuration logger.
LOG_DEF_LOG(loggerHaveNodeId,			LOG_DL_INFO,	"MainWindow.HaveNodeId");
// IDs change logger.
LOG_DEF_LOG(loggerIdsChange,			LOG_DL_TRACE,	"MainWindow.IdsChange");
// Invoke logger.
LOG_DEF_LOG(loggerInvoke,				LOG_DL_INFO,	"MainWindow.Invoke");
// Limits logger.
LOG_DEF_LOG(loggerLimits,				LOG_DL_INFO,	"MainWindow.Limits");
// Node ID user input logger.
LOG_DEF_LOG(loggerNodeIdUserInput,		LOG_DL_TRACE,	"MainWindow.NodeIdUserInput");
// Query logger.
LOG_DEF_LOG(loggerQuery,				LOG_DL_INFO,	"MainWindow.Query");
// Scan for thrusters logger.
LOG_DEF_LOG(loggerScanForThrusters,		LOG_DL_TRACE,	"MainWindow.ScanForThrusters");
// Serial port logger.
LOG_DEF_LOG(loggerSerialPort,			LOG_DL_TRACE,	"MainWindow.SerialPort");
// Set GUI controls logger.
LOG_DEF_LOG(loggerSetGuiControls,		LOG_DL_INFO,	"MainWindow.SetGuiControls");
// Show logger.
LOG_DEF_LOG(loggerShow,					LOG_DL_INFO,	"MainWindow.Show");
// Status message logger.
LOG_DEF_LOG(loggerStatus,				LOG_DL_TRACE,	"MainWindow.Status");
// Thruster background task logger.
LOG_DEF_LOG(loggerTask,					LOG_DL_TRACE,	"MainWindow.Task");
// Testing mode logger.
LOG_DEF_LOG(loggerTesting,				LOG_DL_TRACE,	"MainWindow.Testing");
// Window state logger.
LOG_DEF_LOG(loggerWindowState,			LOG_DL_TRACE,	"MainWindow.WindowState");

LOG_SETUP_STOP();

//######################################################################################################################
#endif
