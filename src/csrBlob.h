#pragma once
#ifndef CSRBLOB_H
#ifndef DOXYGEN_SKIP
#define CSRBLOB_H
#endif
//######################################################################################################################
/*! @file
@brief Declare the interface to the CSR byte array utility module.
*/
//######################################################################################################################

#include <QtCore/QScopedPointer>
#include <QtCore/QByteArray>

#include "csrInfo.h"

//######################################################################################################################

class	CsrByteArrayPrivate;

//######################################################################################################################
/*! @brief Encapsulates the CSR byte array utility module.
*/
class CsrBlob
{
public:		// Constructors & destructors
	explicit CsrBlob(void);
	CsrBlob(void const *buffer, int size, int base = 0);
	CsrBlob(QByteArray const &buffer, int base = 0);
	~CsrBlob(void);

public:		// Functions
	QByteArray	blob(void);
	CsrBlob		&blob(QByteArray const &buffer);
	QByteArray	buffer(void);
	CsrBlob		&buffer(void const *buffer, int size, int base = 0);
	CsrBlob		&buffer(QByteArray const &buffer, int base = 0);
	CsrBlob		&get(qint8 &value, CsrInfo::Type type);
	CsrBlob		&get(quint8 &value, CsrInfo::Type type);
	CsrBlob		&get(qint16 &value, CsrInfo::Type type);
	CsrBlob		&get(quint16 &value, CsrInfo::Type type);
	CsrBlob		&get(qint32 &value, CsrInfo::Type type);
	CsrBlob		&get(quint32 &value, CsrInfo::Type type);
	CsrBlob		&get(qint64 &value, CsrInfo::Type type);
	CsrBlob		&get(quint64 &value, CsrInfo::Type type);
	CsrBlob		&get(float &value, CsrInfo::Type type);
	CsrBlob		&get(double &value, CsrInfo::Type type);
	CsrBlob		&move(int offset);
	CsrBlob		&seek(int offset);
	CsrBlob		&set(qint8 value, CsrInfo::Type type);
	CsrBlob		&set(quint8 value, CsrInfo::Type type);
	CsrBlob		&set(qint16 value, CsrInfo::Type type);
	CsrBlob		&set(quint16 value, CsrInfo::Type type);
	CsrBlob		&set(qint32 value, CsrInfo::Type type);
	CsrBlob		&set(quint32 value, CsrInfo::Type type);
	CsrBlob		&set(qint64 value, CsrInfo::Type type);
	CsrBlob		&set(quint64 value, CsrInfo::Type type);
	CsrBlob		&set(float value, CsrInfo::Type type);
	CsrBlob		&set(double value, CsrInfo::Type type);

private:	// Data
	QScopedPointer<CsrByteArrayPrivate>	d;	//!< Implementation object.
};

//######################################################################################################################
#endif
