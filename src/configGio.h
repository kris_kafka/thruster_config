#pragma once
#if !defined(CONFIGGIO_H) && !defined(DOXYGEN_SKIP)
#define CONFIGGIO_H
//######################################################################################################################
/*! @file
@brief Configuration file constants - Thruster interface - Generic I/O.
*/
//######################################################################################################################

#include "iniFile.h"

//######################################################################################################################
// Generic I/O groups.

//! Configuration Read - get CSR data group.
INI_DEFINE_GROUP(CfgGioConfRdCsr,			"thruster_gio_config_read_csr");
//! Configuration Read - get ID group.
INI_DEFINE_GROUP(CfgGioConfRdIds,			"thruster_gio_config_read_ids");
//! Configuration Write - set maximum power group.
INI_DEFINE_GROUP(CfgGioConfWrMaxPower,		"thruster_gio_config_write_maximum_power");
//! Configuration Write - set motor ID group.
INI_DEFINE_GROUP(CfgGioConfWrMotorId,		"thruster_gio_config_write_motor_id");
//! Configuration Write - set rotation group.
INI_DEFINE_GROUP(CfgGioConfWrRoration,		"thruster_gio_config_write_rotation");
//! Save settings check group.
INI_DEFINE_GROUP(CfgGioSaveSettingCheck,	"thruster_gio_save_settings_check");
//! Save ettings start group.
INI_DEFINE_GROUP(CfgGioSaveSettingStart,	"thruster_gio_save_settings_start");
//! Set IDs group.
INI_DEFINE_GROUP(CfgGioSetIds,				"thruster_gio_set_ids");
//! Testing mode group (multiple) group.
INI_DEFINE_GROUP(CfgGioTstMdPropulsion,		"thruster_gio_testing_propulsion");
//! Testing mode check speed group.
INI_DEFINE_GROUP(CfgGioTstMdChkSpeed,		"thruster_gio_testing_speed_check");
//! Testing mode set speed group.
INI_DEFINE_GROUP(CfgGioTstMdSetSpeed,		"thruster_gio_testing_speed_set");
//! Testing mode get status group.
INI_DEFINE_GROUP(CfgGioTstMdGetStatus,		"thruster_gio_testing_status");

//######################################################################################################################
// Generic I/O settings.

//! Abort on error setting.
INI_DEFINE_BOOLEAN(CfgGioAbortOnError,
					"abort_on_error",
					false);

//! Check reply packet header address field setting.
INI_DEFINE_BOOLEAN(CfgGioCheckReplyAddress,
					"check_reply_address_field",
					true);

//! Check reply packet header flags field setting.
INI_DEFINE_BOOLEAN(CfgGioCheckReplyFlags,
					"check_reply_flags_field",
					true);

//! Check reply packet header length field setting.
INI_DEFINE_BOOLEAN(CfgGioCheckReplyLength,
					"check_reply_length_field",
					true);

//! Check reply packet header address setting.
INI_DEFINE_BOOLEAN(CfgGioCheckReplyNodeId,
					"check_reply_node_id_field",
					true);

//! Immediate retry on error setting.
INI_DEFINE_BOOLEAN(CfgGioImmediateRetryOnError,
					"immediate_retry_on_error",
					true);

//! Immediate retry on error setting.
INI_DEFINE_BOOLEAN(CfgGioIoNeeded,
					"io_needed",
					true);

//! Is a reply packet expected setting.
INI_DEFINE_BOOLEAN(CfgGioNeedReply,
					"need_reply",
					true);

//! Does the reply packet payload have a device type prefix setting.
INI_DEFINE_BOOLEAN(CfgGioReplyHasDeviceType,
					"reply_has_device_type_prefix",
					true);

//! Resend command on every retry setting.
INI_DEFINE_BOOLEAN(CfgGioSendCommandOnRetry,
					"resend_command_every_retry",
					true);

//! Retry limit setting.
INI_DEFINE_INTEGER(CfgGioRetryLimit,
					"retry_limit",
					5);

//! Sleep duration setting.
INI_DEFINE_INTEGER(CfgGioSleepDuration,
					"sleep_duration",
					20);

//! Timeout duration setting.
INI_DEFINE_INTEGER(CfgGioTimeoutDuration,
					"timeout_duration",
					1000);

//! Delay duration after valid reply.
INI_DEFINE_INTEGER(CfgGioValidReplyDelay,
					"valid_reply_delay_duration",
					0);

//######################################################################################################################

//! End of generic I/O groups.
INI_END_GROUP();

//######################################################################################################################
#endif
