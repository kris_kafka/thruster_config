#pragma once
#ifndef PORTSELECTIONDIALOG_H
#ifndef DOXYGEN_SKIP
#define PORTSELECTIONDIALOG_H
#endif
//######################################################################################################################
/*! @file
@brief Declare the interface to the port selection dialog module.
*/
//######################################################################################################################

#include <QtCore/QScopedPointer>
#include <QtCore/QString>
#include <QtWidgets/QWidget>

//######################################################################################################################

class	PortSelectionDialogPrivate;

//######################################################################################################################
/*! @brief The PortSelectionDialog class provides the interface to the port selection dialog module.
*/
class PortSelectionDialog
{
	Q_DISABLE_COPY(PortSelectionDialog)

public:		// Constructors & destructors
	PortSelectionDialog(QString const &currentPort, QWidget *parent = Q_NULLPTR);
	~PortSelectionDialog(void);

public:		// Functions
	int			invoke(void);
	QString		newPort(void);

private:	// Data
	QScopedPointer<PortSelectionDialogPrivate>	d;	//!< Implementation object.
};

//######################################################################################################################
#endif
