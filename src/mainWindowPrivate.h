#pragma once
#ifndef MAINWINDOWPRIVATE_H
#ifndef DOXYGEN_SKIP
#define MAINWINDOWPRIVATE_H
#endif
//######################################################################################################################
/*! @file
@brief Declare the implementation of the main window module.
*/

//######################################################################################################################

#include <QtGui/QIntValidator>

#include "emptyIntValidator.h"
#include "thruster.h"

#include "ui_mainWindow.h"

//######################################################################################################################

class	QCloseEvent;
class	Options;
class	QTimer;
class	QString;
class	QWidget;

//######################################################################################################################
/*! @brief The MainWindowPrivate class provides the implementation of the main window module.
*/
class MainWindowPrivate
:
	public	QMainWindow,
	public	Ui_MainWindow
{
	Q_OBJECT
	Q_DISABLE_COPY(MainWindowPrivate)

public:		// Constructors & destructors
	MainWindowPrivate(QWidget *parent);
	~MainWindowPrivate(void);

public:		// Functions
	void			at_statusTimer_timeout(void);
	Q_SLOT void		at_thruster_configurationReadNews(int nodeId, int groupId, int motorId, int	 maximumPower,
														bool rotationReverse, QString const &serialNumber,
														QByteArray const &csrBlob);
	Q_SLOT void		at_thruster_configurationReadStart(int errorCode);
	Q_SLOT void		at_thruster_configurationReadStop(int errorCode);
	Q_SLOT void		at_thruster_configurationWriteStart(int errorCode);
	Q_SLOT void		at_thruster_configurationWriteStop(int errorCode);
	Q_SLOT void		at_thruster_idsChangeStart(int errorCode);
	Q_SLOT void		at_thruster_idsChangeStop(int errorCode);
	Q_SLOT void		at_thruster_scanForThrustersNews(int nodeId, int groupId, QString const &serialNumber);
	Q_SLOT void		at_thruster_scanForThrustersStart(int errorCode);
	Q_SLOT void		at_thruster_scanForThrustersStop(int errorCode);
	Q_SLOT void		at_thruster_serialPortChangeStart(int errorCode);
	Q_SLOT void		at_thruster_serialPortChangeStop(int errorCode);
	Q_SLOT void		at_thruster_taskStarted(void);
	Q_SLOT void		at_thruster_taskStopped(void);
	void			configurationReadEnd(int errorCode);
	void			configurationWriteEnd(int errorCode);
	uint			displayErrorTime(int errorCode);
	uint			displayTimeError(void);
	uint			displayTimeNotError(void);
	uint			displayTimePermanent(void);
	void			focusRestore(void);
	void			focusSave(QWidget *widget = Q_NULLPTR);
	int				newMaximumPower(bool &isOkay);
	int				newMotorId(bool &isOkay);
	bool			haveNodeId(void);
	void			invokeTestDialog(void);
	Q_SLOT void		on_aboutAction_triggered(void);
	Q_SLOT void		on_changeIdsPushButton_clicked(void);
	Q_SLOT void		on_changeSerialPortAction_triggered(void);
	Q_SLOT void		on_maximumPowerLineEdit_textEdited(void);
	Q_SLOT void		on_motorIdLineEdit_textEdited(void);
	Q_SLOT void		on_nodeIdComboBox_currentIndexChanged(int index);
	Q_SLOT void		on_nodeIdEnterRadioButton_clicked(bool checked);
	Q_SLOT void		on_nodeIdLineEdit_editingFinished(void);
	Q_SLOT void		on_nodeIdSelectRadioButton_clicked(bool checked);
	Q_SLOT void		on_readPushButton_clicked(void);
	Q_SLOT void		on_rotationReversedCheckBox_stateChanged(void);
	Q_SLOT void		on_scanPushButton_clicked(void);
	Q_SLOT void		on_testAction_triggered(void);
	Q_SLOT void		on_testPushButton_clicked(void);
	Q_SLOT void		on_writePushButton_clicked(void);
	void			idsChangeEnd(int errorCode);
	void			readThrusterConfiguraton(void);
	void			restoreWindowState(void);
	void			saveWindowState(void);
	void			scanForThrusters(void);
	void			scanForThrustersEnd(int errorCode);
	void			serialPortChangeEnd(int errorCode);
	void			setConfigurationControls(void);
	void			statusMessage(QString const &message, uint displayTime);
	void			setValidatorLimits(void);
	void			showWarning(QString const &message);
	void			updateGuiState(void);
	void			writeThrusterConfiguraton(void);

public:		// Data
	int							groupIdMaximum;			//!< Maxmimum valid group ID.
	int							groupIdMinimum;			//!< Minmimum valid group ID.
	bool						isConfigurationDataOk;	//!< Is the configuration data data valid?
	QByteArray					lastCsrBlob;			//!< Configutation CSR blob.
	bool						isLastOk;				//!< Is the 'last...' data okay?
	bool						isNodeIdNotFound;		//!< Was the node ID not found.
	bool						isScanForThrustersOk;	//!< Has the scan for thrusters completed without errors?
	bool						isScanningForThrusters;	//!< Are we scanning for thrusters?
	int							lastBadNodeId;			//!< Last bad node ID.
	int							lastGroupId;			//!< Configuration group ID.
	int							lastMaximumPower;		//!< Configuration maximum power value.
	int							lastMotorId;			//!< Configuration motor ID.
	int							lastNodeId;				//!< Configuration node ID.
	bool						lastRotationReversed;	//!< Configuration rotation reversed value.
	QString						lastSerialNumber;		//!< Configuration Serial number.
	QIntValidator				maximumPowerValidator;	//!< Validator for the maximum power line editor control.
	QIntValidator				motorIdValidator;		//!< Validator for the motor ID line editor control.
	QString						newSerialPort;			//!< New serial port;
	int							nodeIdMaximum;			//!< Maximum valid node ID.
	int							nodeIdMinimum;			//!< Minimum valid node ID.
	EmptyIntValidator			nodeIdValidator;		//!< Validator for the node ID line editor control.
	QVector<int>				nodesFound;				//!< Node IDs found during the last scan.
	QScopedPointer<Options>		options;				//!< Program options object.
	QWidget						*savedFocus;			//!< Widget that had focus.
	int							selectedNodeId;			//!< Currently selected node ID (after haveNodeId()).
	int							selectedGroupId;		//!< Currently selected group ID (after haveNodeId()).
	QString						selectedSerialNumber;	//!< Currently selected serialNumber (after haveNodeId()).
	QScopedPointer<QTimer>		statusTimer;			//!< Clear status message timer.
	QScopedPointer<Thruster>	thruster;				//!< Thruster interface object.

protected:	// Functions
	virtual void	closeEvent(QCloseEvent *event)	override;
};

//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
/*! @brief Determine the status message display time based upon @a errorCode.

@return the status message display time.
*/
inline uint
MainWindowPrivate::displayErrorTime(
	int	errorCode)	//!<[in] Error code.
{
	return Thruster::TEC_NoError != errorCode ? displayTimeError() : displayTimeNotError();
}

//######################################################################################################################
#endif
