#pragma once
#if !defined(CONFIGOPTIONS_H) && !defined(DOXYGEN_SKIP)
#define CONFIGOPTIONS_H
//######################################################################################################################
/*! @file
@brief Configuration file constants - Default options.
*/
//######################################################################################################################

#include "iniFile.h"

//######################################################################################################################
//! @hideinitializer Default options settings group name prefix.
#define CFG_OPTIONS_GROUP	"default_options/"

// Long form of command line options, not available in configuration files.
//! @hideinitializer Help option (long).
#define CFG_OPT_LONG_NAME_HELP				"help"
//! @hideinitializer Help option (short).
#define CFG_OPT_SHORT_NAME_HELP				"h"
//! @hideinitializer Version option (long).
#define CFG_OPT_LONG_NAME_VERSION			"version"
//! @hideinitializer Version option (short).
#define CFG_OPT_SHORT_NAME_VERSION			"v"

// Long form of command line options, available in configuration files.
//! @hideinitializer Hardware emulation option (long).
#define CFG_OPT_LONG_NAME_EMULATION				"emulate"
//! @hideinitializer Hardware emulation option (short).
#define CFG_OPT_SHORT_NAME_EMULATION			"e"
//! @hideinitializer Log configuration file option (long).
#define CFG_OPT_LONG_NAME_LOG_CONFIGURATION		"log_configuration"
//! @hideinitializer Log configuration file option (short).
#define CFG_OPT_SHORT_NAME_LOG_CONFIGURATION	"c"
//! @hideinitializer Minimum log level option (long).
#define CFG_OPT_LONG_NAME_MINIMUM_LEVEL			"minimum_level"
//! @hideinitializer Minimum log level option (short).
#define CFG_OPT_SHORT_NAME_MINIMUM_LEVEL		"l"
//! @hideinitializer Port selection (long).
#define CFG_OPT_LONG_NAME_PORT					"port"
//! @hideinitializer Port selection (short).
#define CFG_OPT_SHORT_NAME_PORT					"p"
//! @hideinitializer INI file setup mode option (long).
#define CFG_OPT_LONG_NAME_SETUP_INI				"setup_ini"
//! @hideinitializer Log control file setup mode option (long).
#define CFG_OPT_LONG_NAME_SETUP_LOG				"setup_log"
//! @hideinitializer Show on screen serial port (long).
#define CFG_OPT_LONG_SHOW_PORT					"show_port"
//! @hideinitializer Show on screen status (long).
#define CFG_OPT_LONG_SHOW_STATUS				"show_status"

// Names of log message levels.
static QString const	OptionValueLevelDebug	= "debug";	//!< Name of debug log level.
static QString const	OptionValueLevelError	= "error";	//!< Name of error log level.
static QString const	OptionValueLevelFatal	= "fatal";	//!< Name of fatal log level.
static QString const	OptionValueLevelInfo	= "info";	//!< Name of info log level.
static QString const	OptionValueLevelTrace	= "trace";	//!< Name of trace log level.
static QString const	OptionValueLevelWarn	= "warn";	//!< Name of warning log level.

#ifndef DOXYGEN_SKIP
//! Key name of hardware emulation control value.
INI_DEFINE_BOOLEAN(CfgOptionsEmulate,
					CFG_OPTIONS_GROUP CFG_OPT_LONG_NAME_EMULATION,
					false);

//! Key name of default configuration log configuration file setting value.
INI_DEFINE_STRING(CfgOptionsConfigFile,
					CFG_OPTIONS_GROUP CFG_OPT_LONG_NAME_LOG_CONFIGURATION,
					"");

//! Key name of default configuration minimum log message level setting value.
INI_DEFINE_STRING(CfgOptionsMinMessageLvl,
					CFG_OPTIONS_GROUP CFG_OPT_LONG_NAME_MINIMUM_LEVEL,
					OptionValueLevelWarn);

//! Key name of last (default) serial port value.
INI_DEFINE_STRING(CfgOptionsSerialPort,
					CFG_OPTIONS_GROUP CFG_OPT_LONG_NAME_PORT,
					"");

//! Key name of show on screen serial port control value.
INI_DEFINE_BOOLEAN(CfgOptionsShowPort,
					CFG_OPTIONS_GROUP CFG_OPT_LONG_SHOW_PORT,
					false);

//! Key name of show on screen status control value.
INI_DEFINE_BOOLEAN(CfgOptionsShowStatus,
					CFG_OPTIONS_GROUP CFG_OPT_LONG_SHOW_STATUS,
					false);
#endif

//######################################################################################################################
#endif
