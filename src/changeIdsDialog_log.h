#pragma once
#if !defined(CHANGEIDSDIALOG_LOG_H) && !defined(DOXYGEN_SKIP)
#define CHANGEIDSDIALOG_LOG_H
//######################################################################################################################
/*! @file
@brief Declare the change IDs dialog loggers.
*/
//######################################################################################################################

#include "log.h"

//######################################################################################################################
// Loggers

LOG_SETUP_START("Change IDs Dialog");

// Top level module logger.
LOG_DEF_LOG(logger,				LOG_DL_TRACE,	"ChangeIdsDialog");
// Creation/destruction logger.
LOG_DEF_LOG(loggerCreate,		LOG_DL_TRACE,	"ChangeIdsDialog.Create");
// GUI state update logger.
LOG_DEF_LOG(loggerGuiState,		LOG_DL_TRACE,	"ChangeIdsDialog.GuiState");
// Invokation logger.
LOG_DEF_LOG(loggerInvoke,		LOG_DL_TRACE,	"ChangeIdsDialog.Invoke");
// Query logger.
LOG_DEF_LOG(loggerQuery,		LOG_DL_INFO,	"ChangeIdsDialog.Query");
// Windows state logger.
LOG_DEF_LOG(loggerWindowState,	LOG_DL_INFO,	"ChangeIdsDialog.WindowState");

LOG_SETUP_STOP();

//######################################################################################################################
#endif
