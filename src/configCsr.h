#pragma once
#if !defined(CONFIGURATIONCSR_H) && !defined(DOXYGEN_SKIP)
#define CONFIGURATIONCSR_H
//######################################################################################################################
/*! @file
@brief Configuration file constants - CSR related.
*/
//######################################################################################################################

#include "iniFile.h"

#if !INI_SETUP
	#include "videoray/applib/gizmos/motor_controller/CSR.h"
#endif

//######################################################################################################################

//! A byte.
INI_TYPEDEF(Byte, uint8_t)

//! Pointer to an array of bytes.
INI_TYPEDEF(ByteP, Byte *)

//######################################################################################################################

//! @hideinitializer Settings group prefix.
#define CFG_CFG_GROUP		"thruster_csr/"

//! Bus current CSR offset.
INI_DEFINE_INTEGER(CfgCsrBusAmpsOffset,
					CFG_CFG_GROUP "bus_current_offset",
					offsetof(CSR_Motor_Controller, bus_i));
//! Bus current CSR type.
INI_DEFINE_STRING(CfgCsrBusAmpsType,
					CFG_CFG_GROUP "bus_current_type",
					"float");
//! Bus current data type.
INI_TYPEDEF(BusAmps_t, decltype(CSR_Motor_Controller::bus_i))

//! Bus voltage CSR offset.
INI_DEFINE_INTEGER(CfgCsrBusVoltsOffset,
					CFG_CFG_GROUP "bus_voltage_offset",
					offsetof(CSR_Motor_Controller, bus_v));
//! Bus voltage CSR type.
INI_DEFINE_STRING(CfgCsrBusVoltsType,
					CFG_CFG_GROUP "bus_voltage_type",
					"float");
//! Bus voltage data type.
INI_TYPEDEF(BusVolts_t, decltype(CSR_Motor_Controller::bus_v))

//! Device type data type.
INI_TYPEDEF(DeviceType_t, Byte)

//! Fault flags CSR offset.
INI_DEFINE_INTEGER(CfgCsrFaultFlagsOffset,
					CFG_CFG_GROUP "fault_flags_offset",
					offsetof(CSR_Motor_Controller, fault));
//! Fault flags CSR type.
INI_DEFINE_STRING(CfgCsrFaultFlagsType,
					CFG_CFG_GROUP "fault_flags_type",
					"uint32");
//! Fault flags data type.
INI_TYPEDEF(FaultFlags_t, decltype(CSR_Motor_Controller::fault))
//! Fault flags data type (in motor controller propulsion command reply).
INI_TYPEDEF(FaultFlags1_t, Byte)

//! Group ID data type.
INI_TYPEDEF(GroupId_t, decltype(CSR_Motor_Controller::GROUP_ID))

//! Maximum power CSR offset.
INI_DEFINE_INTEGER(CfgCsrMaximumPowerOffset,
					CFG_CFG_GROUP "maximum_power_offset",
					offsetof(CSR_Motor_Controller, max_allowable_power));
//! Motor ID CSR type.
INI_DEFINE_STRING(CfgCsrMaximumPowerType,
					CFG_CFG_GROUP "maximum_power_type",
					"uint32");
//! Maximum power data type.
INI_TYPEDEF(MaximumPower_t, decltype(CSR_Motor_Controller::max_allowable_power))

//!  Motor control flags CSR offset.
INI_DEFINE_INTEGER(CfgCsrMotorControlFlagsOffset,
					CFG_CFG_GROUP "motor_control_flags_offset",
					offsetof(CSR_Motor_Controller, motor_control_flags));
//!  Motor control flags CSR type.
INI_DEFINE_STRING(CfgCsrMotorControlFlagsType,
					CFG_CFG_GROUP "motor_control_flags_type",
					"uint8");
//! Motor control flags data type.
typedef decltype(CSR_Motor_Controller::motor_control_flags)	MotorControlFlags_t;

//! Motor ID CSR offset.
INI_DEFINE_INTEGER(CfgCsrMotorIdOffset,
					CFG_CFG_GROUP "motor_id_offset",
					offsetof(CSR_Motor_Controller, motor_ID));
//! Motor ID CSR type.
INI_DEFINE_STRING(CfgCsrMotorIdType,
					CFG_CFG_GROUP "motor_id_type",
					"uint16");
//! Motor ID data type.
INI_TYPEDEF(MotorId_t, decltype(CSR_Motor_Controller::motor_ID))

//! Node ID data type.
INI_TYPEDEF(NodeId_t, decltype(CSR_Motor_Controller::NODE_ID))

//! Power target CSR offset.
INI_DEFINE_INTEGER(CfgCsrPowerTargetOffset,
					CFG_CFG_GROUP "power_target_offset",
					offsetof(CSR_Motor_Controller, pwr_target));
//! Power target CSR type.
INI_DEFINE_STRING(CfgCsrPowerTargetType,
					CFG_CFG_GROUP "power_target_type",
					"float");
//! Power target data type.
INI_TYPEDEF(PowerTarget_t, decltype(CSR_Motor_Controller::pwr_target))

//! Rotation reversed false bit value.
INI_DEFINE_BITS(CfgCsrRotationReversedFalse,
					CFG_CFG_GROUP "rotation_reversed_false",
					(reverse_rotation_MASK & ~reverse_rotation_TRUE));
//! Rotation reversed keep bit mask in motor control flags.
INI_DEFINE_BITS(CfgCsrRotationReversedKeep,
					CFG_CFG_GROUP "rotation_reversed_keep",
					((MotorControlFlags_t)~reverse_rotation_MASK));
//! Rotation reversed bit mask in motor control flags.
INI_DEFINE_BITS(CfgCsrRotationReversedMask,
					CFG_CFG_GROUP "rotation_reversed_mask",
					reverse_rotation_MASK);
//! Rotation reversed true bit value.
INI_DEFINE_BITS(CfgCsrRotationReversedTrue,
					CFG_CFG_GROUP "rotation_reversed_true",
					reverse_rotation_TRUE);

//! RPM CSR offset.
INI_DEFINE_INTEGER(CfgCsrRpmOffset,
					CFG_CFG_GROUP "rpm_offset",
					offsetof(CSR_Motor_Controller, rpm));
//! RPM CSR type.
INI_DEFINE_STRING(CfgCsrRpmType,
					CFG_CFG_GROUP "rpm_type",
					"float");
//! RPM data type.
INI_TYPEDEF(Rpm_t, decltype(CSR_Motor_Controller::rpm))

//! Save settings CSR offset.
INI_DEFINE_INTEGER(CfgCsrSaveSettingsOffset,
					CFG_CFG_GROUP "save_settings_offset",
					offsetof(CSR_Motor_Controller, save_settings));
//! Save settings CSR type.
INI_DEFINE_STRING(CfgCsrSaveSettingsType,
					CFG_CFG_GROUP "save_settings_type",
					"uint16");
//! Save settings control field data type.
INI_TYPEDEF(SaveSettings_t, decltype(CSR_Motor_Controller::save_settings))

//! Temperature CSR offset.
INI_DEFINE_INTEGER(CfgCsrTemperatureOffset,
					CFG_CFG_GROUP "temperature_offset",
					offsetof(CSR_Motor_Controller, temp));
//! Temperature CSR type.
INI_DEFINE_STRING(CfgCsrTemperatureType,
					CFG_CFG_GROUP "temperature_type",
					"float");
//! Temperature data type.
INI_TYPEDEF(Temperature_t, decltype(CSR_Motor_Controller::temp))

//######################################################################################################################
#endif
