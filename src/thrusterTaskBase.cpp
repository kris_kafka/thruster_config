/*! @file
@brief Define the thruster interface module.
*/
//######################################################################################################################

#include "app_com.h"

#include "thrusterTaskBase.h"
#include "thrusterTaskBase_log.h"

//######################################################################################################################
/*! @brief Create a ThrusterTaskBase object.
*/
ThrusterTaskBase::ThrusterTaskBase(void)
:
	duplicateNodeIdCount	(0),
	duplicateSerialCount	(0),
	serialPort				(),
	state					(Thruster::State::NotStarted),
	taskHalt				(false),
	testModeHalt			(false),
	testingModeSpeed		(0.0),
	testModeSpeedChanged	(false)
{
	LOG_Trace(loggerCreate, "Creating a ThrusterTaskBase object");
}

//======================================================================================================================
/*! @brief Destroy a ThrusterTaskBase object.
*/
ThrusterTaskBase::~ThrusterTaskBase(void)
{
	LOG_Trace(loggerCreate, "Destroying a ThrusterTaskBase object");
}

//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
/*! @brief Check a serial port and select it if valid.

@return true if valid else false.
*/
bool
ThrusterTaskBase::serialPortSet(
	QString const	&portName	//!<[in] New serial port.
)
{
	bool	isOk = serialPortCheck(portName);
	if (isOk) {
		serialPort = portName;
	}
	return isOk;
}

//======================================================================================================================
/*! @brief Change the testing mode thruster speed.
*/
void
ThrusterTaskBase::testModeSetSpeed(
	double	speed	//!<[in] New thruster speed.
)
{
	testingModeSpeed = speed;
	testModeSpeedChanged = true;
}
