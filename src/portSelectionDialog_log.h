#pragma once
#if !defined(PORTSELECTIONDIALOG_LOG_H) && !defined(DOXYGEN_SKIP)
#define PORTSELECTIONDIALOG_LOG_H
//######################################################################################################################
/*! @file
@brief Declare the port selection dialog loggers.
*/
//######################################################################################################################

#include "log.h"

//######################################################################################################################
// Loggers

LOG_SETUP_START("Port Selection Dialog");

// Top level module logger.
LOG_DEF_LOG(logger,				LOG_DL_INFO,	"PortSelectionDialog");
// Creation/destruction logger.
LOG_DEF_LOG(loggerCreate,		LOG_DL_INFO,	"PortSelectionDialog.Create");
// GUI state update logger.
LOG_DEF_LOG(loggerGuiState,		LOG_DL_INFO,	"PortSelectionDialog.GuiState");
// Invokation logger.
LOG_DEF_LOG(loggerInvoke,		LOG_DL_INFO,	"PortSelectionDialog.Invoke");
// Query logger.
LOG_DEF_LOG(loggerQuery,		LOG_DL_INFO,	"PortSelectionDialog.Query");
// Windows state logger.
LOG_DEF_LOG(loggerWindowState,	LOG_DL_INFO,	"PortSelectionDialog.WindowState");

LOG_SETUP_STOP();

//######################################################################################################################
#endif
