/*! @file
@brief Define the help/about dialog module.
*/
//######################################################################################################################

#include "app_com.h"

#include <QtCore/QElapsedTimer>
#include <QtCore/QThread>
#include <QtCore/QTimer>
#include <QtGui/QCloseEvent>
#include <QtWidgets/QDialog>
#include <QtWidgets/QMessageBox>

#include "aboutDialog.h"
#include "changeIdsDialog.h"
#include "configMisc.h"
#include "configState.h"
#include "configValidators.h"
#include "mainWindow.h"
#include "mainWindow_log.h"
#include "mainWindowPrivate.h"
#include "options.h"
#include "portSelectionDialog.h"
#include "testDialog.h"
#include "thruster.h"

//######################################################################################################################
// Constants

//! Name of the application's icon resource.
static QString const	AppIconName				= ":/thruster_config";

static int const		OldFactoryDefaultMotorId	= 65535;
static int const		OldFactoryDefaultNodeId		= 255;
static int const		OldFactoryDefaultGroupId	= 255;
static int const		GroupIdWidth				= 3;
static int const		NodeIdWidth					= 3;

//######################################################################################################################
/*! @brief Create the main window.
*/
MainWindow::MainWindow(
	QWidget	*parent	//!<[in] Parent.
)
:
	d	(Q_NULLPTR)
{
	LOG_Trace(loggerCreate, "Creating MainWindow");
	//
	d.reset(new (std::nothrow) MainWindowPrivate(parent));
	LOG_CHK_PTR_MSG(loggerCreate, d, "Unable to create MainWindowPrivate object");
}

//----------------------------------------------------------------------------------------------------------------------
/*! @brief Create the main window.
*/
MainWindowPrivate::MainWindowPrivate(
	QWidget	*parent	//!<[in] Parent.
)
:
	QMainWindow				(parent),
	isConfigurationDataOk	(false),
	isLastOk				(false),
	isNodeIdNotFound		(false),
	isScanForThrustersOk	(false),
	isScanningForThrusters	(false),
	lastBadNodeId			(-1),
	lastGroupId				(0),
	lastMaximumPower		(0),
	lastMotorId				(0),
	lastNodeId				(0),
	lastRotationReversed	(false),
	lastSerialNumber		(),
	maximumPowerValidator	(INI_DEFAULT(CfgValidatorsMaximumPowerMin), INI_DEFAULT(CfgValidatorsMaximumPowerMax)),
	motorIdValidator		(INI_DEFAULT(CfgValidatorsMotorIdMin), INI_DEFAULT(CfgValidatorsMotorIdMax)),
	newSerialPort			(),
	nodeIdValidator			(INI_DEFAULT(CfgValidatorsNodeIdMin), INI_DEFAULT(CfgValidatorsNodeIdMax)),
	options					(Q_NULLPTR),
	savedFocus				(Q_NULLPTR),
	selectedNodeId			(-1),
	selectedSerialNumber	(),
	statusTimer				(Q_NULLPTR),
	thruster				(Q_NULLPTR)
{
	LOG_Trace(loggerCreate, "Creating MainWindowPrivate");
	options.reset(new (std::nothrow) Options);
	LOG_CHK_PTR_MSG(loggerCreate, options, "Unable to create Options object");
	statusTimer.reset(new (std::nothrow) QTimer);
	LOG_CHK_PTR_MSG(loggerCreate, statusTimer, "Unable to create QTimer object");
	thruster.reset(new (std::nothrow) Thruster);
	LOG_CHK_PTR_MSG(loggerCreate, thruster, "Unable to create Thruster object");
	setupUi(this);
	serialPortLabel	->setVisible(options->isShowOnScreenPort());
	statusLabel		->setVisible(options->isShowOnScreenStatus());
	restoreWindowState();
	// First widget to get focus.
	nodeIdComboBox->setFocus();
	focusSave();
	//
	setValidatorLimits();
	nodeIdLineEdit		->setValidator(&nodeIdValidator);
	motorIdLineEdit		->setValidator(&motorIdValidator);
	maximumPowerLineEdit->setValidator(&maximumPowerValidator);
	//
	statusLabel->clear();
	nodeIdLineEdit->setModified(false);
	setConfigurationControls();
	//
	statusTimer->setSingleShot(true);
	//
	LOG_CONNECT(loggerCreate,	statusTimer.data(),	&QTimer::timeout,
								this,				&MainWindowPrivate::at_statusTimer_timeout);
	LOG_CONNECT(loggerCreate,	thruster.data(),	&Thruster::configurationReadNews,
								this,				&MainWindowPrivate::at_thruster_configurationReadNews);
	LOG_CONNECT(loggerCreate,	thruster.data(),	&Thruster::configurationReadStart,
								this,				&MainWindowPrivate::at_thruster_configurationReadStart);
	LOG_CONNECT(loggerCreate,	thruster.data(),	&Thruster::configurationReadStop,
								this,				&MainWindowPrivate::at_thruster_configurationReadStop);
	LOG_CONNECT(loggerCreate,	thruster.data(),	&Thruster::configurationWriteStart,
								this,				&MainWindowPrivate::at_thruster_configurationWriteStart);
	LOG_CONNECT(loggerCreate,	thruster.data(),	&Thruster::configurationWriteStop,
								this,				&MainWindowPrivate::at_thruster_configurationWriteStop);
	LOG_CONNECT(loggerCreate,	thruster.data(),	&Thruster::idsChangeStart,
								this,				&MainWindowPrivate::at_thruster_idsChangeStart);
	LOG_CONNECT(loggerCreate,	thruster.data(),	&Thruster::idsChangeStop,
								this,				&MainWindowPrivate::at_thruster_idsChangeStop);
	LOG_CONNECT_Q(loggerCreate,	thruster.data(),	&Thruster::scanForThrustersNews,
								this,				&MainWindowPrivate::at_thruster_scanForThrustersNews);
	LOG_CONNECT_Q(loggerCreate,	thruster.data(),	&Thruster::scanForThrustersStart,
								this,				&MainWindowPrivate::at_thruster_scanForThrustersStart);
	LOG_CONNECT_Q(loggerCreate,	thruster.data(),	&Thruster::scanForThrustersStop,
								this,				&MainWindowPrivate::at_thruster_scanForThrustersStop);
	LOG_CONNECT(loggerCreate,	thruster.data(),	&Thruster::serialPortChangeStart,
								this,				&MainWindowPrivate::at_thruster_serialPortChangeStart);
	LOG_CONNECT(loggerCreate,	thruster.data(),	&Thruster::serialPortChangeStop,
								this,				&MainWindowPrivate::at_thruster_serialPortChangeStop);
	LOG_CONNECT(loggerCreate,	thruster.data(),	&Thruster::taskStarted,
								this,				&MainWindowPrivate::at_thruster_taskStarted);
	LOG_CONNECT(loggerCreate,	thruster.data(),	&Thruster::taskStopped,
								this,				&MainWindowPrivate::at_thruster_taskStopped);
	//
	QString port = options->serialPort();
	serialPortLabel->setText(QString("Serial port: %1%2")
									.arg(port)
									.arg(options->isEmulation() ? " (Emulated)" : ""));
	LOG_ASSERT_MSG(loggerCreate, thruster->serialPortChange(port),	"Unable to initialize the serial port");
	LOG_ASSERT_MSG(loggerCreate, thruster->moduleOpen(), 			"Unable to open the thruster interface module");
	LOG_ASSERT_MSG(loggerCreate, thruster->moduleStart(),			"Unable to start the thruster interface module");
	//
	updateGuiState();
}

//======================================================================================================================
/*! @brief Destroy the main window.
*/
MainWindow::~MainWindow(void)
{
	LOG_Trace(loggerDestroy, "Destroying MainWindow");
}

//----------------------------------------------------------------------------------------------------------------------
/*! @brief Destroy the main window.
*/
MainWindowPrivate::~MainWindowPrivate(void)
{
	LOG_Trace(loggerDestroy, "Destroying MainWindowPrivate");
	thruster->moduleStop();
	IniFile	iniFile;
	qint64	timeoutDuration	= iniFile.INI_GET(CfgMiscBackgroundTaskEndTimeout);
	qint64	timeoutSleep	= iniFile.INI_GET(CfgMiscBackgroundTaskEndSleep);
	QElapsedTimer	exitTimer;
	exitTimer.start();
	while (thruster->moduleIsRunning() && !exitTimer.hasExpired(timeoutDuration)) {
		QThread::msleep(timeoutSleep);
	}
	thruster->moduleClose();
	//
	statusTimer->stop();
}

//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
/*! @brief Query the application's icon name.

@return Name of the application's icon.
*/
QString
MainWindow::appIconName(void)
{
	LOG_Trace(loggerQuery, "Querying application icon name");
	return AppIconName;
}

//======================================================================================================================
/*! @brief Handle status message timeout signals.
*/
void
MainWindowPrivate::at_statusTimer_timeout(void)
{
	statusLabel->clear();
}

//======================================================================================================================
/*! @brief Handle configuration read data signals.
*/
void
MainWindowPrivate::at_thruster_configurationReadNews(
	int					nodeId,				//!< Node ID of the thruster.
	int					groupId,			//!< Group ID read from thruster.
	int					motorId,			//!< Motor ID read from thruster.
	int					maximumPower,		//!< Maximum power read from thruster.
	bool				rotationReverse,	//!< Rotation is reversed read from thruster.
	QString const		&serialNumber,		//!< Serial number read from thruster.
	QByteArray const	&csrBlob			//!< CSR blob.
)
{
	LOG_Trace(loggerConfigurationRead,
				QString("at_thruster_configurationReadNews: nodeId=%1, groupId=%2, motorId=%3, maximumPower=%4, "
								"rotationReverse=%5, serialNumber=%6")
						.arg(nodeId).arg(groupId).arg(motorId).arg(maximumPower).arg(rotationReverse).arg(serialNumber));
	statusMessage(QString("Configuration read data: ni=%1, gi=%2, mi=%3, mp=%4, rr=%5, sn='%6'")
						.arg(nodeId).arg(groupId).arg(motorId).arg(maximumPower).arg(rotationReverse)
						.arg(serialNumber), displayTimeNotError());
	// Error if the node IDs are different.
	if (nodeId != selectedNodeId) {
		LOG_Error(loggerConfigurationRead,
					QString("Received configuration data a differnt node: nodeId=%1, groupId=%2, motorId=%3, "
									"maximumPower=%4, rotationReverse=%5, serialNumber=%6")
						.arg(nodeId).arg(groupId).arg(motorId).arg(maximumPower).arg(rotationReverse).arg(serialNumber));
		return;
	}
	//
	isLastOk				= true;
	lastGroupId				= groupId;
	lastMotorId				= motorId;
	lastMaximumPower		= maximumPower;
	lastNodeId				= nodeId;
	lastRotationReversed	= rotationReverse;
	lastSerialNumber		= serialNumber;
	lastCsrBlob				= csrBlob;
}

//======================================================================================================================
/*! @brief Handle configuraton read start signals.
*/
void
MainWindowPrivate::at_thruster_configurationReadStart(
	int	errorCode	//!< Error code.
)
{
	LOG_Trace(loggerConfigurationRead, QString("at_thruster_configurationReadStart: %1").arg(errorCode));
	statusMessage(QString("Configuration read start: %1").arg(errorCode), displayErrorTime(errorCode));
	//
	if (Thruster::TEC_NoError != errorCode) {
		configurationReadEnd(errorCode);
	} else {
		updateGuiState();
	}
}

//======================================================================================================================
/*! @brief Handle configuraton read stop signals.
*/
void
MainWindowPrivate::at_thruster_configurationReadStop(
	int	errorCode	//!< Error code.
)
{
	LOG_Trace(loggerConfigurationRead, QString("at_thruster_configurationReadStop: %1").arg(errorCode));
	statusMessage(QString("Configuration read stop: %1").arg(errorCode), displayErrorTime(errorCode));
	//
	configurationReadEnd(errorCode);
}

//======================================================================================================================
/*! @brief Handle configuration write start signals.
*/
void
MainWindowPrivate::at_thruster_configurationWriteStart(
	int	errorCode	//!< Error code.
)
{
	LOG_Trace(loggerConfigurationWrite, QString("at_thruster_configurationWriteStart: %1").arg(errorCode));
	statusMessage(QString("Configuration write start: %1").arg(errorCode), displayErrorTime(errorCode));
	//
	if (Thruster::TEC_NoError != errorCode) {
		configurationWriteEnd(errorCode);
	} else {
		updateGuiState();
	}
}

//======================================================================================================================
/*! @brief Handle configuration write stop signals.
*/
void
MainWindowPrivate::at_thruster_configurationWriteStop(
	int	errorCode	//!< Error code.
)
{
	LOG_Trace(loggerConfigurationWrite, QString("at_thruster_configurationWriteStop: %1").arg(errorCode));
	statusMessage(QString("Configuration write stop: %1").arg(errorCode), displayErrorTime(errorCode));
	//
	configurationWriteEnd(errorCode);
}

//======================================================================================================================
/*! @brief Handle node ID change start signals.
*/
void
MainWindowPrivate::at_thruster_idsChangeStart(
	int	errorCode	//!< Error code.
)
{
	LOG_Trace(loggerIdsChange, QString("at_thruster_idsChangeStart: %1").arg(errorCode));
	statusMessage(QString("Node ID change start: %1").arg(errorCode), displayErrorTime(errorCode));
	//
	if (Thruster::TEC_NoError != errorCode) {
		idsChangeEnd(errorCode);
	} else {
		updateGuiState();
	}
}

//======================================================================================================================
/*! @brief Handle node ID change stop signals.
*/
void
MainWindowPrivate::at_thruster_idsChangeStop(
	int	errorCode	//!< Error code.
)
{
	LOG_Trace(loggerIdsChange, QString("at_thruster_idsChangeStop: %1").arg(errorCode));
	statusMessage(QString("Node ID change stop: %1").arg(errorCode), displayErrorTime(errorCode));
	//
	idsChangeEnd(errorCode);
}

//======================================================================================================================
/*! @brief Handle scan for thrusters data signals.
*/
void
MainWindowPrivate::at_thruster_scanForThrustersNews(
	int				nodeId,			//!<[in] Node ID of the found thruster.
	int				groupId,		//!<[in] Group ID of the found thruster.
	QString const	&serialNumber	//!<[in] Serial number of the found thruster.
)
{
	LOG_Trace(loggerScanForThrusters,
				QString("at_thruster_scanForThrustersNews: nodeId=%1, groupId=%2, serialNumber=%3")
						.arg(nodeId).arg(groupId).arg(serialNumber));
	statusMessage(QString("Scan for thrusters found: ni=%1, gi=%2, sn='%3'")
					.arg(nodeId).arg(groupId).arg(serialNumber), displayTimeNotError());
	nodeIdComboBox->addItem(QStringLiteral("%1").arg(nodeId),
							QVariant(QStringLiteral("%1 %2 %3")
										.arg(nodeId, NodeIdWidth).arg(groupId, GroupIdWidth).arg(serialNumber)));
	//
	updateGuiState();
}

//======================================================================================================================
/*! @brief Handle scan for thrusters start signals.
*/
void
MainWindowPrivate::at_thruster_scanForThrustersStart(
	int	errorCode	//!< Error code.
)
{
	LOG_Trace(loggerScanForThrusters, QString("at_thruster_scanForThrustersStart: %1").arg(errorCode));
	statusMessage(QString("Scan for thrusters start: %1").arg(errorCode), displayErrorTime(errorCode));
	//
	if (Thruster::TEC_NoError != errorCode) {
		scanForThrustersEnd(errorCode);
	} else {
		updateGuiState();
	}
}

//======================================================================================================================
/*! @brief Handle scan for thrusters stop signals.
*/
void
MainWindowPrivate::at_thruster_scanForThrustersStop(
	int	errorCode	//!< Error code.
)
{
	LOG_Trace(loggerScanForThrusters, QString("at_thruster_scanForThrustersStop: %1").arg(errorCode));
	statusMessage(QString("Scan for thrusters stop: %1").arg(errorCode), displayErrorTime(errorCode));
	//
	scanForThrustersEnd(errorCode);
}

//======================================================================================================================
/*! @brief Handle change serial port start signals.
*/
void
MainWindowPrivate::at_thruster_serialPortChangeStart(
	int	errorCode	//!< Error code.
)
{
	LOG_Trace(loggerSerialPort, QString("at_thruster_serialPortChangeStart: %1").arg(errorCode));
	statusMessage(QString("Serial port change start: %1").arg(errorCode), displayErrorTime(errorCode));
	//
	if (Thruster::TEC_NoError != errorCode) {
		serialPortChangeEnd(errorCode);
	} else {
		updateGuiState();
	}
}

//======================================================================================================================
/*! @brief Handle change serial port stop signals.
*/
void
MainWindowPrivate::at_thruster_serialPortChangeStop(
	int	errorCode	//!< Error code.
)
{
	LOG_Trace(loggerSerialPort, QString("at_thruster_serialPortChangeStop: %1").arg(errorCode));
	statusMessage(QString("Serial port change stop: %1").arg(errorCode), displayErrorTime(errorCode));
	//
	if (Thruster::TEC_NoError == errorCode) {
		serialPortLabel->setText(QString("Serial port: %1 %2")
									.arg(newSerialPort)
									.arg(options->isEmulation() ? "Emulated" : "Real"));
		options->serialPort(newSerialPort);
		newSerialPort.clear();
	}
	//
	serialPortChangeEnd(errorCode);
}

//======================================================================================================================
/*! @brief Handle thruster interface module background task started signal.
*/
void
MainWindowPrivate::at_thruster_taskStarted(void)
{
	LOG_Trace(loggerTask, "at_thruster_taskStarted");
	statusMessage("Thruster background task started", displayTimeNotError());
	//
	scanForThrusters();
}

//======================================================================================================================
/*! @brief Handle thruster interface module background task stopped signal.
*/
void
MainWindowPrivate::at_thruster_taskStopped(void)
{
	LOG_Trace(loggerTask, "at_thruster_taskStopped");
	statusMessage("Thruster background task stopped", displayTimeNotError());
	//
	updateGuiState();
}

//======================================================================================================================
/*! @brief Restore the window state.
*/
void
MainWindowPrivate::closeEvent(
	QCloseEvent		*event	//![in,out] Event information.
)
{
	LOG_Trace(loggerDestroy, "Closing window");
	//
	saveWindowState();
	QMainWindow::closeEvent(event);
}

//======================================================================================================================
/*! @brief Handle configuraton read ending.
*/
void
MainWindowPrivate::configurationReadEnd(
	int	errorCode	//!< Error code.
)
{
	LOG_Trace(loggerConfigurationRead, QString("configurationReadEnd: %1").arg(errorCode));
	//
	isConfigurationDataOk = isLastOk && Thruster::TEC_NoError == errorCode;
	isNodeIdNotFound = Thruster::TEC_NodeIdNotFound == errorCode;
	//
	setConfigurationControls();
	//
	updateGuiState();
	focusRestore();
	//
	if (Thruster::TEC_NoError != errorCode && Thruster::TEC_NodeIdNotFound != errorCode) {
		QString	errorMessage = thruster->errorMessage(errorCode);
		LOG_Error(loggerConfigurationRead, QString("Unable to read configuration: %1").arg(errorMessage));
		showWarning(QString("Unable to read the configurtion:\n\n%1").arg(errorMessage));
	} else if (!isLastOk) {
		LOG_Warn(loggerConfigurationRead, QString("Unable to find thruster with node ID of %1").arg(selectedNodeId));
		// Warn the user only once for the same node ID.
		if (lastBadNodeId != selectedNodeId) {
			lastBadNodeId = selectedNodeId;
			showWarning("Unable to read the configurtion:\n\nThruster not found");
		}
	}
}

//======================================================================================================================
/*! @brief Handle configuration write ending.
*/
void
MainWindowPrivate::configurationWriteEnd(
	int	errorCode	//!< Error code.
	)
{
	LOG_Trace(loggerConfigurationWrite, QString("configurationWriteEnd: %1").arg(errorCode));
	//
	updateGuiState();
	focusRestore();
	//
	if (Thruster::TEC_NoError != errorCode) {
		QString	errorMessage = thruster->errorMessage(errorCode);
		LOG_Error(loggerConfigurationWrite, QString("Unable to write configuration: %1").arg(errorMessage));
		showWarning(QString("Unable to write the configurtion:\n\n%1").arg(errorMessage));
	}
}

//======================================================================================================================
/*! @brief Query the time in milliseconds (0=permanent) to display error status messages.

@return the display time.
*/
uint
MainWindowPrivate::displayTimeError(void)
{
	return IniFile().INI_GET(CfgMiscStatusDurationError);
}

//======================================================================================================================
/*! @brief Query the time in milliseconds (0=permanent) to display non-error status messages.

@return the display time.
*/
uint
MainWindowPrivate::displayTimeNotError(void)
{
	return IniFile().INI_GET(CfgMiscStatusDurationNotError);
}

//======================================================================================================================
/*! @brief Query the time in milliseconds (0=permanent) to display permanent status messages.

@return the display time.
*/
uint
MainWindowPrivate::displayTimePermanent(void)
{
	return IniFile().INI_GET(CfgMiscStatusDurationPermanent);
}

//======================================================================================================================
/*! @brief Restore the saved focus widget.
*/
void
MainWindowPrivate::focusRestore(void)
{
	if (savedFocus) {
		savedFocus->setFocus();
		savedFocus = Q_NULLPTR;
	}
}

//======================================================================================================================
/*! @brief Save the focus widget.
*/
void
MainWindowPrivate::focusSave(
	QWidget	*widget	//!<[in] Widget that will get focus.
	)
{
	if (!savedFocus) {
		savedFocus = widget ? widget : focusWidget();
	}
}

//======================================================================================================================
/*! @brief Get the currently selected node ID.

If there is no currently selected nod ID, set @a selectedNodeId to -1 and return false.

@return true if there is a selected node.
*/
bool
MainWindowPrivate::haveNodeId(void)
{
	LOG_Trace(loggerHaveNodeId, "Getting the selected node ID");
	// Get the node ID.
	QString	groupIdText;
	QString	nodeIdText;
	QString	serialNumberText;
	if (nodeIdEnterRadioButton->isChecked()) {
		// Text from manually entered node ID.
		nodeIdText = nodeIdLineEdit->text().trimmed();
	} else {
		// Text from current selection.
		nodeIdText			= nodeIdComboBox->currentData().toString();
		groupIdText			= nodeIdText.mid(NodeIdWidth + 1, GroupIdWidth).trimmed();
		serialNumberText	= nodeIdText.mid(NodeIdWidth + GroupIdWidth + 2);
		nodeIdText			= nodeIdText.left(NodeIdWidth).trimmed();
	}
	// Handle no current selection.
	if (nodeIdText.isEmpty()) {
		LOG_Debug(loggerHaveNodeId, "No selected node");
		selectedNodeId = -1;
		selectedSerialNumber.clear();
		return false;
	}
	selectedNodeId			= nodeIdText.toInt();
	selectedGroupId			= groupIdText.toInt();
	selectedSerialNumber	= serialNumberText;
	LOG_Debug(loggerHaveNodeId,
				QString("Selected node ID %1, groupId=%2, SN=%3")
						.arg(selectedNodeId).arg(selectedGroupId).arg(selectedSerialNumber));
	return true;
}

//======================================================================================================================
/*! @brief Handle node ID change stop signals.
*/
void
MainWindowPrivate::idsChangeEnd(
	int	errorCode	//!< Error code.
)
{
	LOG_Trace(loggerIdsChange, QString("idsChangeEnd: %1").arg(errorCode));
	//
	if (Thruster::TEC_NoError == errorCode) {
		scanForThrusters();
	} else {
		updateGuiState();
		focusRestore();
		//
		QString	errorMessage = thruster->errorMessage(errorCode);
		LOG_Error(loggerIdsChange, QString("Unable to change the node ID: %1").arg(errorMessage));
		showWarning(QString("Unable to change the node ID:\n\n%1").arg(errorMessage));
	}
}

//======================================================================================================================
/*! @brief Invoke the test dialog.
*/
void
MainWindowPrivate::invokeTestDialog(void)
{
	LOG_Trace(loggerInvoke, "Invoking the test dialog");
	if (haveNodeId()) {
		TestDialog	testDialog(selectedNodeId, lastGroupId, lastMotorId);
		testDialog.invoke();
	}
	setValidatorLimits();
}

//======================================================================================================================
/*! @brief Get the new maximum power.

@return the new maximum power.
*/
int
MainWindowPrivate::newMaximumPower(
	bool	&isOkay		//!<[out] Is the new value valid.
)
{
	QString text = maximumPowerLineEdit->text();
	int pos = 0;
	isOkay = QValidator::Acceptable == maximumPowerValidator.validate(text, pos);
	int answer = text.toInt(isOkay ? &isOkay : Q_NULLPTR);
	LOG_Trace(loggerQuery, QString("New maximum power: %1, isOkay=%2").arg(answer).arg(isOkay));
	return answer;
}

//======================================================================================================================
/*! @brief Get the new motor ID.

@return the new motor ID.
*/
int
MainWindowPrivate::newMotorId(
	bool	&isOkay		//!<[out] Is the new value valid.
)
{
	QString text = motorIdLineEdit->text();
	int pos = 0;
	isOkay = QValidator::Acceptable == motorIdValidator.validate(text, pos);
	int answer = text.toInt(isOkay ? &isOkay : Q_NULLPTR);
	LOG_Trace(loggerQuery, QString("New motor ID: %1, isOkay=%2").arg(answer).arg(isOkay));
	return answer;
}

//======================================================================================================================
/*! @brief Handle triggering of the help-about menu item.
*/
void
MainWindowPrivate::on_aboutAction_triggered(void)
{
	LOG_Trace(logger, "The help-about menu item was triggered");
	AboutDialog aboutDialog;
	aboutDialog.invoke();
	updateGuiState();
	setValidatorLimits();
}

//======================================================================================================================
/*! @brief Handle clicking on the configuration read push button.
*/
void
MainWindowPrivate::on_changeIdsPushButton_clicked(void)
{
	LOG_Trace(loggerNodeIdUserInput, "The change IDs push button was clicked");
	setValidatorLimits();
	if (haveNodeId()) {
		ChangeIdsDialog changeNodeIdDialog(selectedNodeId, lastGroupId);
		if (QDialog::Accepted == changeNodeIdDialog.invoke()) {
			bool isGroupIdOkay = false;
			bool isNodeIdOkay = false;
			int	newGroupId = changeNodeIdDialog.newGroupId(isGroupIdOkay);
			int newNodeId = changeNodeIdDialog.newNodeId(isNodeIdOkay);
			if (isGroupIdOkay && isNodeIdOkay) {
				LOG_Debug(loggerSerialPort,
							QString("Change IDs: node: %1 to %2; group: %3 to %4")\
									.arg(selectedNodeId).arg(newNodeId).arg(lastGroupId).arg(newGroupId));
				focusSave();
				//
				if (!thruster->idsChange(lastSerialNumber, selectedNodeId, newNodeId, newGroupId)) {
					focusRestore();
					QString lastErrorMessage = thruster->lastErrorMessage();
					LOG_Error(loggerScanForThrusters, QString("Unable to change IDs: %1").arg(lastErrorMessage));
					showWarning(QString("Unable to change IDs:\n\n%1").arg(lastErrorMessage));
				}
			}
		}
	}
	updateGuiState();
	setValidatorLimits();
}

//======================================================================================================================
/*! @brief Handle triggering of the file-port menu item.
*/
void
MainWindowPrivate::on_changeSerialPortAction_triggered(void)
{
	LOG_Trace(loggerSerialPort, "The file-change port menu item was triggered");
	QString currentSerialPort = options->serialPort();
	PortSelectionDialog portSelectonDialog(currentSerialPort);
	if (QDialog::Accepted == portSelectonDialog.invoke()) {
		focusSave();
		newSerialPort = portSelectonDialog.newPort();
		LOG_Debug(loggerSerialPort,
					QString("Request to change serial port to '%1'").arg(newSerialPort));
		if (!thruster->serialPortChange(newSerialPort)) {
			focusRestore();
			QString lastErrorMessage = thruster->lastErrorMessage();
			LOG_Error(loggerSerialPort, QString("Unable to change serial port: %1").arg(lastErrorMessage));
			showWarning(QString("Unable to change the serial port:\n\n%1").arg(lastErrorMessage));
		}
	}
	updateGuiState();
	setValidatorLimits();
}

//======================================================================================================================
/*! @brief Hangle changes to the maximum power line edit.
*/
void
MainWindowPrivate::on_maximumPowerLineEdit_textEdited(void)
{
	LOG_Trace(logger, "The maximum power line edit was changed");
	updateGuiState();
	setValidatorLimits();
}

//======================================================================================================================
/*! @brief Hangle changes to the motor line edit.
*/
void
MainWindowPrivate::on_motorIdLineEdit_textEdited(void)
{
	LOG_Trace(logger, "The motor ID line edit was changed");
	updateGuiState();
	setValidatorLimits();
}

//======================================================================================================================
/*! @brief Handle changes to the manual node ID line edit control.
*/
void
MainWindowPrivate::on_nodeIdComboBox_currentIndexChanged(
	int	index	//!<[in] Index of new selection.
)
{
	Q_UNUSED(index);
	LOG_Trace(loggerNodeIdUserInput, "The selected node ID was changed");
	if (!isScanningForThrusters) {
		focusSave();
		readThrusterConfiguraton();
	}
}

//======================================================================================================================
/*! @brief Handle changing to manual entry of the node ID.
*/
void
MainWindowPrivate::on_nodeIdEnterRadioButton_clicked(
	bool	checked	//!<[in] Is the radio button now checked?
)
{
	LOG_Trace(loggerNodeIdUserInput,
				QString("The selection manual node ID radio button was toggled %1").arg(checked ? "on" : "off"));
	if (checked) {
		focusSave(nodeIdEnterRadioButton);
		readThrusterConfiguraton();
	}
}

//======================================================================================================================
/*! @brief Handle changes to the manual node ID line edit control.
*/
void
MainWindowPrivate::on_nodeIdLineEdit_editingFinished(void)
{
	bool	isModified = nodeIdLineEdit->isModified();
	LOG_Trace(loggerNodeIdUserInput,
				QString("The manual node ID was changed: '%1', modified=%2")
				.arg(nodeIdLineEdit->text()).arg(isModified));
	if (isModified) {
		nodeIdLineEdit->setModified(false);
		// Move focus to what will be the next widget after reading the configuration data.
		QWidget	*widget = focusWidget();
		focusSave(widget == scanPushButton ? motorIdLineEdit : widget);
		//
		readThrusterConfiguraton();
	}
}

//======================================================================================================================
/*! @brief Handle changing to list selection of the node ID.
*/
void
MainWindowPrivate::on_nodeIdSelectRadioButton_clicked(
	bool	checked	//!<[in] Is the radio button now checked?
)
{
	LOG_Trace(loggerNodeIdUserInput,
				QString("The selection list node ID radio button was toggled %1").arg(checked ? "on" : "off"));
	if (checked) {
		focusSave(nodeIdSelectRadioButton);
		readThrusterConfiguraton();
	}
}

//======================================================================================================================
/*! @brief Handle clicking on the configuration read push button.
*/
void
MainWindowPrivate::on_readPushButton_clicked(void)
{
	LOG_Trace(loggerConfigurationRead, "The read configuration push button was clicked");
	focusSave();
	readThrusterConfiguraton();
	setValidatorLimits();
}

//======================================================================================================================
/*! @brief Hangle changes rotation reversed check box.
*/
void
MainWindowPrivate::on_rotationReversedCheckBox_stateChanged(void)
{
	LOG_Trace(logger, "The rotation revereded check box was changed");
	updateGuiState();
	setValidatorLimits();
}

//======================================================================================================================
/*! @brief Handle clicking on the selection rescan push button.
*/
void
MainWindowPrivate::on_scanPushButton_clicked(void)
{
	LOG_Trace(loggerScanForThrusters, "The rescan push button was clicked");
	focusSave();
	scanForThrusters();
}

//======================================================================================================================
/*! @brief Handle triggering of the file-test menu item.
*/
void
MainWindowPrivate::on_testAction_triggered(void)
{
	LOG_Trace(loggerTesting, "The file-test menu item was triggered");
	invokeTestDialog();
}

//======================================================================================================================
/*! @brief Handle clicking on the configuration test push button.
*/
void
MainWindowPrivate::on_testPushButton_clicked(void)
{
	LOG_Trace(loggerTesting, "The test configuration push button was clicked");
	invokeTestDialog();
}

//======================================================================================================================
/*! @brief Handle clicking on the configuration write push button.
*/
void
MainWindowPrivate::on_writePushButton_clicked(void)
{
	LOG_Trace(loggerConfigurationWrite, "The write configuration push button was clicked");
	focusSave();
	writeThrusterConfiguraton();
	setValidatorLimits();
}

//======================================================================================================================
/*! @brief Read the thruster's current configuration.
*/
void
MainWindowPrivate::readThrusterConfiguraton(void)
{
	LOG_Trace(loggerConfigurationRead, "Read the thruster's configuration");
	// Do nothing if the thruster interface module is not idle.
	if (!thruster->isIdle()) {
		return;
	}
	//
	isLastOk				= false;
	isConfigurationDataOk	= false;
	isNodeIdNotFound		= false;
	lastGroupId				= 0;
	lastMotorId				= 0;
	lastMaximumPower		= 0;
	lastNodeId				= 0;
	lastRotationReversed	= false;
	lastCsrBlob.clear();
	lastSerialNumber.clear();
	//
	motorIdLineEdit			->clear();
	maximumPowerLineEdit	->clear();
	rotationReversedCheckBox->setChecked(lastRotationReversed);
	// Done if no node selected.
	if (!haveNodeId()) {
		updateGuiState();
		focusRestore();
		return;
	}
	// Special case if duplicate node IDs or old factory default node IDs.
	if (	thruster->isDuplicateIds()
		||	OldFactoryDefaultNodeId == selectedNodeId
		||	OldFactoryDefaultGroupId == selectedGroupId
		) {
		lastGroupId				= selectedGroupId;
		lastNodeId				= selectedNodeId;
		lastSerialNumber		= selectedSerialNumber;
		lastMotorId				= OldFactoryDefaultMotorId;
		isLastOk				= true;
		isConfigurationDataOk	= true;
		setConfigurationControls();
		updateGuiState();
		focusRestore();
		return;
	}
	//
	if (!thruster->configurationRead(selectedNodeId)) {
		QString lastErrorMessage = thruster->lastErrorMessage();
		LOG_Error(loggerConfigurationRead,
					QString("Unable to read configuration of %1\n\t\t%2")
							.arg(selectedNodeId).arg(lastErrorMessage));
		//
		setConfigurationControls();
		updateGuiState();
		focusRestore();
		//
		showWarning(QString("Unable to read the configurtion:\n\n%1").arg(lastErrorMessage));
	} else {
		updateGuiState();
	}
}

//======================================================================================================================
/*! @brief Restore the window state.
*/
void
MainWindowPrivate::restoreWindowState(void)
{
	LOG_Trace(loggerWindowState, "Restoring window state");
	//
	IniFile	iniFile;
	IniBytes	savedGeometry	= iniFile.INI_GET(CfgStateMainWindowGeometry);
	IniBytes	savedState		= iniFile.INI_GET(CfgStateMainWindowState);
	if (!savedGeometry.isEmpty() && !savedState.isEmpty()) {
		restoreGeometry(savedGeometry);
		restoreState(savedState);
	}
}

//======================================================================================================================
/*! @brief Save the window state.
*/
void
MainWindowPrivate::saveWindowState(void)
{
	LOG_Trace(loggerWindowState, "Saving window state");
	//
	IniFile	iniFile;
	iniFile.INI_SET(CfgStateMainWindowGeometry, saveGeometry());
	iniFile.INI_SET(CfgStateMainWindowState, saveState());
	iniFile.syncAndCheck();
}

//======================================================================================================================
/*! @brief Start a scan for thrusters.
*/
void
MainWindowPrivate::scanForThrusters(void)
{
	LOG_Trace(loggerScanForThrusters, "Starting scan for thrusters");
	isScanningForThrusters = true;
	nodeIdComboBox->clear();
	if (!thruster->scanForThrusters()) {
		isScanningForThrusters = false;
		isScanForThrustersOk = false;
		updateGuiState();
		focusRestore();
		QString lastErrorMessage = thruster->lastErrorMessage();
		LOG_Error(loggerScanForThrusters, QString("Unable to scan for thrusters: %1").arg(lastErrorMessage));
		showWarning(QString("Unable to scan for thrusters:\n\n%1").arg(lastErrorMessage));
	} else {
		updateGuiState();
	}
}

//======================================================================================================================
/*! @brief Handle scan for thrusters ending.
*/
void
MainWindowPrivate::scanForThrustersEnd(
	int	errorCode	//!< Error code.
)
{
	LOG_Trace(loggerScanForThrusters, QString("scanForThrustersEnd: %1").arg(errorCode));
	//
	isScanningForThrusters = false;
	//
	isScanForThrustersOk = Thruster::TEC_NoError == errorCode;
	if (isScanForThrustersOk) {
		if (nodeIdComboBox->count()) {
			nodeIdComboBox->setCurrentIndex(0);
		}
		readThrusterConfiguraton();
	} else {
		updateGuiState();
		focusRestore();
		//
		QString	errorMessage = thruster->errorMessage(errorCode);
		LOG_Error(loggerScanForThrusters, QString("Unable to scan for thrusters: %1").arg(errorMessage));
		showWarning(QString("Unable to scan for thrusters:\n\n%1").arg(errorMessage));
	}
}

//======================================================================================================================
/*! @brief Handle change serial port ending.
*/
void
MainWindowPrivate::serialPortChangeEnd(
	int	errorCode	//!< Error code.
)
{
	LOG_Trace(loggerSerialPort, QString("serialPortChangeEnd: %1").arg(errorCode));
	//
	if (Thruster::TEC_NoError == errorCode) {
		scanForThrusters();
	} else {
		updateGuiState();
		focusRestore();
		//
		QString	errorMessage = thruster->errorMessage(errorCode);
		LOG_Error(loggerSerialPort, QString("Unable to change the serial port: %1").arg(errorMessage));
		showWarning(QString("Unable to change the serial port:\n%1").arg(errorMessage));
	}
}

//======================================================================================================================
/*! @brief Updat the configuration controls with the current values.
*/
void
MainWindowPrivate::setConfigurationControls(void)
{
	LOG_Trace(loggerSetGuiControls, "Initialize the thruster's configuration edit controls");
	QString	groupIdText;
	QString	serialNumberText;
	if (isConfigurationDataOk) {
		serialNumberText = lastSerialNumber;
		groupIdText = QString::number(lastGroupId);
	} else if (isNodeIdNotFound) {
		serialNumberText = QApplication::translate("MainWindow", "Node not found");
		groupIdText = QApplication::translate("MainWindow", "Node not found");
	}
	serialNumberLabel		->setText(serialNumberText);
	groupIdLabel			->setText(groupIdText);
	//
	motorIdLineEdit			->setText(QString::number(lastMotorId));
	maximumPowerLineEdit	->setText(QString::number(lastMaximumPower));
	rotationReversedCheckBox->setChecked(lastRotationReversed);
}

//======================================================================================================================
/*! @brief Set the limits of the input validators.
*/
void
MainWindowPrivate::setValidatorLimits(void)
{
	LOG_Trace(loggerLimits, "Setting the input validator limits");
	//
	IniFile	iniFile;
	int maximumPowerMaximum	= iniFile.INI_GET(CfgValidatorsMaximumPowerMax);
	int maximumPowerMinimum	= iniFile.INI_GET(CfgValidatorsMaximumPowerMin);
	int motorIdMaximum		= iniFile.INI_GET(CfgValidatorsMotorIdMax);
	int motorIdMinimum		= iniFile.INI_GET(CfgValidatorsMotorIdMin);
	nodeIdMaximum			= iniFile.INI_GET(CfgValidatorsNodeIdMax);
	nodeIdMinimum			= iniFile.INI_GET(CfgValidatorsNodeIdMin);
	groupIdMaximum			= iniFile.INI_GET(CfgValidatorsGroupIdMax);
	groupIdMinimum			= iniFile.INI_GET(CfgValidatorsGroupIdMin);
	//
	maximumPowerValidator.setRange(maximumPowerMinimum, maximumPowerMaximum);
	motorIdValidator.setRange(motorIdMinimum, motorIdMaximum);
	nodeIdValidator.setRange(nodeIdMinimum, nodeIdMaximum);
	//
	motorIdLimitsLabel		->setText(QStringLiteral("%1-%2").arg(motorIdMinimum).arg(motorIdMaximum));
	maximumPowerLimitsLabel	->setText(QApplication::translate("MainWindow", "%1-%2 watts", Q_NULLPTR)
																.arg(maximumPowerMinimum).arg(maximumPowerMaximum));
}

//======================================================================================================================
/*! @brief Show the main window.
*/
void
MainWindow::show(void)
{
	LOG_Trace(loggerShow, "Showing MainWindow");
	d->show();
}

//======================================================================================================================
/*! @brief Write the thruster's configuration.
*/
void
MainWindowPrivate::showWarning(
	QString const	&message	//!<[in] Warning message.
)
{
	(void)QMessageBox::warning(this, "Error", message);
}

//======================================================================================================================
/*! @brief Display a status message.

Any prior status message will be replaced and its display time will be cancelled.
After @a displayTime milliseconds, the status message will be cleared.
if @a displayTime is DisplayUnlimited, the status message will displayed until it is replaced.
*/
void
MainWindowPrivate::statusMessage(
	QString const	&message,	//!<[in] Status message.
	uint			displayTime	//!<[in] Milliseconds to display the message.
)
{
	LOG_Trace(loggerStatus, QString("statusMessage: %1").arg(message));
	statusTimer->stop();
	statusLabel->setText(message);
	if (0 != displayTime) {
		statusTimer->start(displayTime);
	}
}

//======================================================================================================================
/*! @brief Update the state of the controls.
*/
void
MainWindowPrivate::updateGuiState(void)
{
	LOG_Debug(loggerGuiState, "Updating the status of the controls");
	// Determine the status.
	bool isIdle				= thruster->isIdle();
	bool isDupSNs			= thruster->isDuplicateSerials();
	bool scanOkay			= isIdle && isScanForThrustersOk;
	bool haveData			= scanOkay && haveNodeId() && isConfigurationDataOk;
	bool isMaximumPowerOkay	= false;
	bool isMotorIdOkay		= false;
	bool isConfigChanged	= lastRotationReversed	!= rotationReversedCheckBox	->isChecked();
	isConfigChanged			= lastMaximumPower		!= newMaximumPower(isMaximumPowerOkay)	|| isConfigChanged;
	isConfigChanged			= lastMotorId			!= newMotorId(isMotorIdOkay)			|| isConfigChanged;
	bool isDataOkay			= isMotorIdOkay && isMaximumPowerOkay;
	bool isIdsOkay			=	nodeIdMinimum <= selectedNodeId		&& selectedNodeId <= nodeIdMaximum
							&&	groupIdMinimum <= selectedGroupId	&& selectedGroupId <= groupIdMaximum;
	//
	bool isConfigEnabled	= haveData && isIdsOkay;
	bool isChangeIdsEnabled	= haveData && !isDupSNs;
	bool isReadEnabled		= isConfigEnabled && isConfigChanged;
	bool isWriteEnabled		= isConfigEnabled && isConfigChanged && isDataOkay;
	bool isTestEnabled		= isConfigEnabled && !isConfigChanged && isDataOkay;
	//
	// Update the status of the widgets.
	nodeIdSelectRadioButton	->setEnabled(scanOkay);
	nodeIdEnterRadioButton	->setEnabled(scanOkay);
	nodeIdComboBox			->setEnabled(scanOkay && nodeIdSelectRadioButton->isChecked());
	nodeIdLineEdit			->setEnabled(scanOkay && nodeIdEnterRadioButton->isChecked());
	scanPushButton			->setEnabled(isIdle);
	changeIdsPushButton		->setEnabled(isChangeIdsEnabled);
	configurationGroupBox	->setEnabled(isConfigEnabled);
	writePushButton			->setEnabled(isWriteEnabled);
	readPushButton			->setEnabled(isReadEnabled);
	testAction				->setEnabled(isTestEnabled);
	testPushButton			->setEnabled(isTestEnabled);
	changeSerialPortAction	->setEnabled(isIdle);
}

//======================================================================================================================
/*! @brief Write the thruster's configuration.
*/
void
MainWindowPrivate::writeThrusterConfiguraton(void)
{
	LOG_Trace(loggerConfigurationWrite, "Write the thruster's configuration");
	// Done if no node selected.
	if (!haveNodeId()) {
		updateGuiState();
		focusRestore();
		return;
	}
	//
	bool isMaximumPowerOkay	= false;
	bool isMotorIdOkay		= false;
	lastMotorId				= newMotorId(isMotorIdOkay);
	lastMaximumPower		= newMaximumPower(isMaximumPowerOkay);
	lastRotationReversed	= rotationReversedCheckBox	->isChecked();
	//
	// Abort if the data is not valid.
	if (!isMaximumPowerOkay || !isMotorIdOkay) {
		updateGuiState();
		return;
	}
	//
	if (!thruster->configurationWrite(selectedNodeId, lastMotorId, lastMaximumPower, lastRotationReversed,
										lastCsrBlob)) {
		QString lastErrorMessage = thruster->lastErrorMessage();
		LOG_Error(loggerConfigurationWrite,
					QString("Unable to write configuration of %1\n\t\t%2")
							.arg(selectedNodeId).arg(lastErrorMessage));
		//
		updateGuiState();
		focusRestore();
		//
		showWarning(QString("Unable to write the configurtion:\n\n%1").arg(lastErrorMessage));
	} else {
		updateGuiState();
	}
}

