#pragma once
#ifndef RPN_H
#ifndef DOXYGEN_SKIP
#define RPN_H
#endif
//######################################################################################################################
/*! @file
@brief Declare the interface to the RPN formula evaluator utility module.
*/
//######################################################################################################################

#include <QtCore/QScopedPointer>
#include <QtCore/QString>

//######################################################################################################################

class	RpnPrivate;

//######################################################################################################################
/*! @brief Encapsulates the RPN formula evaluator utility module.
*/
class Rpn
{
public:		// Constructors & destructors
	explicit	Rpn(void);
				~Rpn(void);

public:		// Functions
	bool		assignSymbol(QString const &name, double value);
	bool		declareSymbol(QString const &name, double value = 0.0);
	bool		evaluate(QString const &formula);
	bool		isSymbol(QString const &name);
	QString		lastError(void);
	double		lastValue(void);

private:	// Data
	QScopedPointer<RpnPrivate>	d;	//!< Implementation object.
};

//######################################################################################################################
#endif
