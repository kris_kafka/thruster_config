/*! @file
@brief Define the CSR byte array utility module.
*/
//######################################################################################################################

#include "app_com.h"

#include <QtCore/QMutex>
#include <QtCore/QMutexLocker>
#include <QtCore/QVector>

#include "csrBlob.h"
#include "csrBlob_log.h"
#include "csrInfo.h"

//######################################################################################################################
// Types

typedef	int		Index_t;		//!< Type of CSR base and offset.

//######################################################################################################################
// Constants.

static QByteArray const		BlobPrefix	= "Blob";
static QByteArray const		BlobDelim	= "bLOb";
static QByteArray const		BlobSuffix	= "bolB";

//######################################################################################################################
// Inline local functions.

/*! @brief Convert a Type to an integer.

@return the integer corresponding to @a type.
*/
inline constexpr int
operator+(
	CsrInfo::Type	type	//!<[in] type to convert.
)
{
	return static_cast<int>(type);
}

//######################################################################################################################
// Local functions.

static bool		checkBlob(QByteArray const &blob, int &bufSize, int &preSize, int &idxSize, int &dlmSize,
							int &sufSize, int &minSize);

//######################################################################################################################

//! @brief Floating point/integer value.
typedef enum VariableDataType_t {
	IsInteger = 0,
	IsFloat
} VariableFloatType;

//! @brief Floating point/integer value.
typedef enum VariableSignType_t {
	IsUnsigned = 0,
	IsSigned,
} VariableSignType;

//######################################################################################################################
/*! @brief Encapsulates the formula evaluator utility module.
*/
class CsrByteArrayPrivate
{
public:		// Constructors & destructors
	CsrByteArrayPrivate(QByteArray const buffer_, int base_);
	~CsrByteArrayPrivate(void);

public:		// Functions
	void	get(int varSize, VariableFloatType varFloatType, VariableSignType varSignType,
					CsrInfo::Type type, QString const &message);
	void	set(int valSize, VariableFloatType valFloatType, VariableSignType valSignType,
					CsrInfo::Type type, QString const &message, QString const &valText);

public:		// Data
	Index_t		base;		//!< Base offset of buffer.
	QByteArray	buffer;		//!< Buffer.
	double		fp;			//!< Floating point value.
	Index_t		offset;		//!< Offset into buffer.
	qint64		si;			//!< Signed integer value.
	quint64		ui;			//!< Unsigned integer value.
};

//######################################################################################################################
/*! @brief Create a CsrBlob object.
*/
CsrBlob::CsrBlob(void)
:
	d	(Q_NULLPTR)
{
	LOG_Trace(loggerCreate, "Creating an empty CsrBlob object");
	//
	d.reset(new (std::nothrow) CsrByteArrayPrivate(QByteArray(), 0));
	LOG_CHK_PTR_MSG(loggerCreate, d, "Unable to create CsrByteArrayPrivate object");
}

//----------------------------------------------------------------------------------------------------------------------
/*! @brief Create a CsrBlob object.
*/
CsrBlob::CsrBlob(
	void const	*buffer,	//!<[in] Pointer to the data buffer.
	int			size,		//!<[in] Length of the data buffer.
	int			base		//!<[in] Base offset of buffer.
)
:
	d	(Q_NULLPTR)
{
	LOG_Trace(loggerCreate, QString("Creating a CsrBlob object: size=%1, base=%2").arg(size).arg(base));
	//
	d.reset(new (std::nothrow) CsrByteArrayPrivate(QByteArray((char const *)buffer, 0 < size ? size : 0), base));
	LOG_CHK_PTR_MSG(loggerCreate, d, "Unable to create CsrByteArrayPrivate object");
}

//----------------------------------------------------------------------------------------------------------------------
/*! @brief Create a CsrBlob object.
*/
CsrBlob::CsrBlob(
	QByteArray const	&buffer,	//!<[in] Pointer to the data buffer.
	int					base		//!<[in] Base offset of buffer.
)
:
	d	(Q_NULLPTR)
{
	LOG_Trace(loggerCreate, QString("Creating a CsrBlob object: base=%1").arg(base));
	//
	int			bufSize, preSize, idxSize, dlmSize, sufSize, minSize;
	int			_base = base;
	QByteArray	_buffer = buffer;
	if (0 > base) {
		// Must be a CSR blob.
		LOG_ASSERT_MSG(loggerBlob, checkBlob(buffer, bufSize, preSize, idxSize, dlmSize, sufSize, minSize),
						"Invalid CSR blob");
		_buffer = buffer.mid(preSize + idxSize + dlmSize, bufSize - minSize);
		_base = *(Index_t *)(buffer.mid(dlmSize, idxSize).data());
	} else {
		// Most not be a CSR blob.
		LOG_ASSERT_MSG(loggerBlob, !checkBlob(buffer, bufSize, preSize, idxSize, dlmSize, sufSize, minSize),
						"Invalid CSR buffer");
	}
	//
	d.reset(new (std::nothrow) CsrByteArrayPrivate(_buffer, _base));
	LOG_CHK_PTR_MSG(loggerCreate, d, "Unable to create CsrByteArrayPrivate object");
}

//----------------------------------------------------------------------------------------------------------------------
/*! @brief Create a CsrBlobPrivate object.
*/
CsrByteArrayPrivate::CsrByteArrayPrivate(
	QByteArray const	buffer_,	//!<[in] Data buffer.
	int					base_	//!<[in] Base offset of buffer.
)
:
	base	(base_),
	buffer	(buffer_),
	fp		(0.0),
	offset	(0),
	si		(0),
	ui		(0)
{
	LOG_Trace(loggerCreate, QString("Creating a CsrByteArrayPrivate object: base=%1").arg(base));
}

//======================================================================================================================
/*! @brief Destroy a CsrBlob object.
*/
CsrBlob::~CsrBlob(void)
{
	LOG_Trace(loggerCreate, "Destroying a CsrBlob object");
}

//----------------------------------------------------------------------------------------------------------------------
/*! @brief Destroy a CsrByteArrayPrivate object.
*/
CsrByteArrayPrivate::~CsrByteArrayPrivate(void)
{
	LOG_Trace(loggerCreate, "Destroying a CsrByteArrayPrivate object");
}

//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
/*! @brief Get a blob.

@return the blob.
*/
QByteArray
CsrBlob::blob(void)
{
	LOG_Trace(loggerBlob, "Saving blob");
	return BlobPrefix + QByteArray((char const *)&d->base, sizeof(Index_t)) + BlobDelim + d->buffer + BlobSuffix;
}

//======================================================================================================================
/*! @brief Move to new blob.

@return a reference to this.
*/
CsrBlob &
CsrBlob::blob(
	QByteArray const	&buffer		//!<[in] CSR blob.
)
{
	LOG_Trace(loggerBlob, "Restoring blob");
	// Verify the blob.
	int	bufSize, preSize, idxSize, dlmSize, sufSize, minSize;
	LOG_ASSERT_MSG(loggerBlob, checkBlob(buffer, bufSize, preSize, idxSize, dlmSize, sufSize, minSize),
					"Invalid CSR blob");
	//
	d->buffer = buffer.mid(preSize + idxSize + dlmSize, bufSize - minSize);
	d->base = *(Index_t *)(buffer.mid(dlmSize, idxSize).data());
	return *this;
}


//======================================================================================================================
/*! @brief Get a copy of the CSR buffer.

@return the copy of the CSR buffer.
*/
QByteArray
CsrBlob::buffer(void)
{
	LOG_Trace(loggerBlob, "Return buffer");
	return d->buffer;
}

//======================================================================================================================
/*! @brief Move to new buffer and base.

@return a reference to this.
*/
CsrBlob &
CsrBlob::buffer(
	void const	*buffer,	//!<[in] Pointer to the data buffer.
	int			size,		//!<[in] Length of the data buffer.
	int			base		//!<[in] Base offset of buffer.
)
{
	LOG_Trace(loggerSeek, "Moving buffer");
	d->buffer = QByteArray((char const *)buffer, size);
	d->base = base;
	return *this;
}

//======================================================================================================================
/*! @brief Move to new buffer and base.

@return a reference to this.
*/
CsrBlob &
CsrBlob::buffer(
	QByteArray const	&buffer,	//!<[in] Pointer to the data buffer.
	int					base		//!<[in] Base offset of buffer.
)
{
	LOG_Trace(loggerSeek, "Moving buffer");
	int	bufSize, preSize, idxSize, dlmSize, sufSize, minSize;
	if (0 > base) {
		// Must be a CSR blob.
		LOG_ASSERT_MSG(loggerBlob, checkBlob(buffer, bufSize, preSize, idxSize, dlmSize, sufSize, minSize),
						"Invalid CSR blob");
		d->buffer = buffer.mid(preSize + idxSize + dlmSize, bufSize - minSize);
		d->base = *(Index_t *)(buffer.mid(dlmSize, idxSize).data());
	} else {
		// Most not be a CSR blob.
		LOG_ASSERT_MSG(loggerBlob, !checkBlob(buffer, bufSize, preSize, idxSize, dlmSize, sufSize, minSize),
						"Invalid CSR buffer");
		d->buffer = buffer;
		d->base = base;
	}
	return *this;
}

//======================================================================================================================
/*! @brief Check if a blob is valid.

@return true if the blob is okay else false.
*/
bool
checkBlob(
	QByteArray const	&blob,		//!<[in]  CSR blob to verify.
	int					&bufSize,	//!<[out] Size of the blob.
	int					&preSize,	//!<[out] Size of a blob prefix
	int					&idxSize,	//!<[out] Size of a blob index.
	int					&dlmSize,	//!<[out] Size of a blob delimiter
	int					&sufSize,	//!<[out] Size of a blob suffix.
	int					&minSize	//!<[out] Minimum size of a blob.

)
{
	bufSize = blob.size();
	preSize = BlobPrefix.size();
	idxSize = sizeof(Index_t);
	dlmSize = BlobDelim.size();
	sufSize = BlobSuffix.size();
	minSize = preSize + idxSize + dlmSize + sufSize;
	return bufSize >= minSize
		&& blob.startsWith(BlobPrefix)
		&& blob.endsWith(BlobSuffix)
		&& BlobDelim == blob.mid(preSize + idxSize, dlmSize);
}

//======================================================================================================================
/*! @brief Extract a value from the buffer.

@return a reference to this.
*/
CsrBlob &
CsrBlob::get(
	qint8			&value,	//!<[out] Returned value.
	CsrInfo::Type	type	//!<[in]  Data type.
)
{
	d->get(sizeof(value), IsInteger, IsSigned, type, "Get a %2-byte signed integer from a %1 field");
	value = (qint8)d->si;
	return *this;
}

//======================================================================================================================
/*! @brief Extract a value from the buffer.

@return a reference to this.
*/
CsrBlob &
CsrBlob::get(
	quint8			&value,	//!<[out] Returned value.
	CsrInfo::Type	type	//!<[in]  Data type.
)
{
	d->get(sizeof(value), IsInteger, IsUnsigned, type, "Get a %2-byte unsigned integer from a %1 field");
	value = (quint8)d->ui;
	return *this;
}

//======================================================================================================================
/*! @brief Extract a value from the buffer.

@return a reference to this.
*/
CsrBlob &
CsrBlob::get(
	qint16			&value,	//!<[out] Returned value.
	CsrInfo::Type	type	//!<[in]  Data type.
)
{
	d->get(sizeof(value), IsInteger, IsSigned, type, "Get a %2-byte signed integer from a %1 field");
	value = (qint16)d->si;
	return *this;
}

//======================================================================================================================
/*! @brief Extract a value from the buffer.

@return a reference to this.
*/
CsrBlob &
CsrBlob::get(
	quint16			&value,	//!<[out] Returned value.
	CsrInfo::Type	type	//!<[in]  Data type.
)
{
	d->get(sizeof(value), IsInteger, IsUnsigned, type, "Get a %2-byte unsigned integer from a %1 field");
	value = (quint16)d->ui;
	return *this;
}

//======================================================================================================================
/*! @brief Extract a value from the buffer.

@return a reference to this.
*/
CsrBlob &
CsrBlob::get(
	qint32			&value,	//!<[out] Returned value.
	CsrInfo::Type	type	//!<[in]  Data type.
)
{
	d->get(sizeof(value), IsInteger, IsSigned, type, "Get a %2-byte signed integer from a %1 field");
	value = (qint32)d->si;
	return *this;
}

//======================================================================================================================
/*! @brief Extract a value from the buffer.

@return a reference to this.
*/
CsrBlob &
CsrBlob::get(
	quint32			&value,	//!<[out] Returned value.
	CsrInfo::Type	type	//!<[in]  Data type.
)
{
	d->get(sizeof(value), IsInteger, IsUnsigned, type, "Get a %2-byte unsigned integer from a %1 field");
	value = (quint32)d->ui;
	return *this;
}

//======================================================================================================================
/*! @brief Extract a value from the buffer.

@return a reference to this.
*/
CsrBlob &
CsrBlob::get(
	qint64			&value,	//!<[out] Returned value.
	CsrInfo::Type	type	//!<[in]  Data type.
)
{
	d->get(sizeof(value), IsInteger, IsSigned, type, "Get a %2-byte signed integer from a %1 field");
	value = (qint64)d->si;
	return *this;
}

//======================================================================================================================
/*! @brief Extract a value from the buffer.

@return a reference to this.
*/
CsrBlob &
CsrBlob::get(
	quint64			&value,	//!<[out] Returned value.
	CsrInfo::Type	type	//!<[in]  Data type.
)
{
	d->get(sizeof(value), IsInteger, IsUnsigned, type, "Get a %2-byte unsigned integer from a %1 field");
	value = (quint64)d->ui;
	return *this;
}

//======================================================================================================================
/*! @brief Extract a value from the buffer.

@return a reference to this.
*/
CsrBlob &
CsrBlob::get(
	float			&value,	//!<[out] Returned value.
	CsrInfo::Type	type	//!<[in]  Data type.
)
{
	d->get(sizeof(value), IsFloat, IsSigned, type, "Get a %2-byte floating point from a %1 field");
	value = (float)d->fp;
	return *this;
}

//======================================================================================================================
/*! @brief Extract a value from the buffer.

@return a reference to this.
*/
CsrBlob &
CsrBlob::get(
	double			&value,	//!<[out] Returned value.
	CsrInfo::Type	type	//!<[in]  Data type.
)
{
	d->get(sizeof(value), IsFloat, IsSigned, type, "Get a %2-byte floating point from a %1 field");
	value = (double)d->fp;
	return *this;
}

//======================================================================================================================
/*! @brief Check destination varaiable information and extract a value from the buffer.

The extracted value is placed into one of the value variables (fp, si, ui).
*/
void
CsrByteArrayPrivate::get(
	int					varSize,		//!<[in] Size of the destination variable.
	VariableFloatType	varFloatType,	//!<[in] Determine if the destination variable is floating point.
	VariableSignType	varSignType,	//!<[in] Determine if the destination variable is signed.
	CsrInfo::Type		type,			//!<[in] Data type.
	QString const		&message		//!<[in] Log message format.
)
{
	LOG_Trace(loggerGet,
				QString("Get: size=%1, isFloat=%2, isSigned=%3, type=%4, messge='%5'")
						.arg(varSize).arg(varFloatType).arg(varSignType).arg(+type).arg(message));
	// Ensure type is valid.
	int		size;
	bool	isSigned;
	bool	isFloat;
	QString	name;
	bool	okay = CsrInfo::check(type, size, isSigned, isFloat, name);
	LOG_AssertMsg(loggerGet, okay, QString("Invalid CsrDataType: %1").arg(+type));
	// Ensure the variable is big enough for the buffered value.
	LOG_AssertMsg(loggerGet, varSize >= size,
					QString("Returned variable too small for %1").arg(name));
	// Ensure the variable is correct type: signed/unsigned.
	LOG_AssertMsg(loggerGet, (IsSigned == varSignType) == isSigned,
					QString("Returned variable is not %1").arg(isSigned ? "signed" : "unsigned"));
	// Ensure the variable is correct type integer/floating point.
	LOG_AssertMsg(loggerGet, (IsFloat == varFloatType) == isFloat,
					QString("Returned variable is not %1").arg(isFloat ? "float" : "integer"));
	// Extract the value from the buffer.
	int	index = offset - base;
	LOG_AssertMsg(loggerGet, buffer.size() >= (size + index),
					QString("Buffer overflow (get): offset=%1, base=%2, size=%3, buffer=%4")
							.arg(offset).arg(base).arg(size).arg(buffer.size()));
	// Record the operation.
	LOG_Trace(loggerGet, message.arg(name).arg(varSize));
	//
	void	*dataPtr = index + buffer.data();
	switch (type) {
	case CsrInfo::Type::SI1:	si = *(qint8 *)		dataPtr;	break;
	case CsrInfo::Type::UI1:	ui = *(quint8 *)	dataPtr;	break;
	case CsrInfo::Type::SI2:	si = *(qint16 *)	dataPtr;	break;
	case CsrInfo::Type::UI2:	ui = *(quint16 *)	dataPtr;	break;
	case CsrInfo::Type::SI4:	si = *(qint32 *)	dataPtr;	break;
	case CsrInfo::Type::UI4:	ui = *(quint32 *)	dataPtr;	break;
	case CsrInfo::Type::SI8:	si = *(qint64 *)	dataPtr;	break;
	case CsrInfo::Type::UI8:	ui = *(quint64 *)	dataPtr;	break;
	case CsrInfo::Type::FP4:	fp = *(float *)		dataPtr;	break;
	case CsrInfo::Type::FP8:	fp = *(double *)	dataPtr;	break;
	default:					LOG_Fatal(loggerGet, QString("Unknown CsrDataType=%1").arg(+type));
	}
	// Move the buffer offset forward.
	offset += size;
}

//======================================================================================================================
/*! @brief Move to offset.

@return a reference to this.
*/
CsrBlob &
CsrBlob::move(
	int	offset	//!<[in] New offset.
)
{
	LOG_Trace(loggerSeek, QString("Moving in buffer: offset=%1").arg(offset));
	d->offset = offset;
	return *this;
}

//======================================================================================================================
/*! @brief Adjust the buffer position by @a offset.

@return a reference to this.
*/
CsrBlob &
CsrBlob::seek(
	int	offset	//!<[in] Amount to adjust the buffer position.
)
{
	LOG_Trace(loggerSeek, QString("Seeking in buffer: offset=%1").arg(offset));
	d->offset += offset;;
	return *this;
}

//======================================================================================================================
/*! @brief Store a value into the buffer.

@return a reference to this.
*/
CsrBlob &
CsrBlob::set(
	qint8			value,	//!<[out] Value to insert into buffer.
	CsrInfo::Type	type	//!<[in]  Data type.
)
{
	d->si = value;
	d->set(sizeof(value), IsInteger, IsSigned, type,
			"Set a %1 field from %2-byte signed integer (%3)", QString::number(value));
	return *this;
}

//======================================================================================================================
/*! @brief Store a value into the buffer.

@return a reference to this.
*/
CsrBlob &
CsrBlob::set(
	quint8			value,	//!<[out] Value to insert into buffer.
	CsrInfo::Type	type	//!<[in]  Data type.
)
{
	d->ui = value;
	d->set(sizeof(value), IsInteger, IsUnsigned, type,
			"Set a %1 field from %2-byte unsigned integer (%3)", QString::number(value));
	return *this;
}

//======================================================================================================================
/*! @brief Store a value into the buffer.

@return a reference to this.
*/
CsrBlob &
CsrBlob::set(
	qint16			value,	//!<[out] Value to insert into buffer.
	CsrInfo::Type	type	//!<[in]  Data type.
)
{
	d->si = value;
	d->set(sizeof(value), IsInteger, IsSigned, type,
			"Set a %1 field from %2-byte signed integer (%3)", QString::number(value));
	return *this;
}

//======================================================================================================================
/*! @brief Store a value into the buffer.

@return a reference to this.
*/
CsrBlob &
CsrBlob::set(
	quint16			value,	//!<[out] Value to insert into buffer.
	CsrInfo::Type	type	//!<[in]  Data type.
)
{
	d->ui = value;
	d->set(sizeof(value), IsInteger, IsUnsigned, type,
			"Set a %1 field from %2-byte unsigned integer (%3)", QString::number(value));
	return *this;
}

//======================================================================================================================
/*! @brief Store a value into the buffer.

@return a reference to this.
*/
CsrBlob &
CsrBlob::set(
	qint32			value,	//!<[out] Value to insert into buffer.
	CsrInfo::Type	type	//!<[in]  Data type.
)
{
	d->si = value;
	d->set(sizeof(value), IsInteger, IsSigned, type,
			"Set a %1 field from %2-byte signed integer (%3)", QString::number(value));
	return *this;
}

//======================================================================================================================
/*! @brief Store a value into the buffer.

@return a reference to this.
*/
CsrBlob &
CsrBlob::set(
	quint32			value,	//!<[out] Value to insert into buffer.
	CsrInfo::Type	type	//!<[in]  Data type.
)
{
	d->ui = value;
	d->set(sizeof(value), IsInteger, IsUnsigned, type,
			"Set a %1 field from %2-byte unsigned integer (%3)", QString::number(value));
	return *this;
}

//======================================================================================================================
/*! @brief Store a value into the buffer.

@return a reference to this.
*/
CsrBlob &
CsrBlob::set(
	qint64			value,	//!<[out] Value to insert into buffer.
	CsrInfo::Type	type	//!<[in]  Data type.
)
{
	d->si = value;
	d->set(sizeof(value), IsInteger, IsSigned, type,
			"Set a %1 field from %2-byte signed integer (%3)", QString::number(value));
	return *this;
}

//======================================================================================================================
/*! @brief Store a value into the buffer.

@return a reference to this.
*/
CsrBlob &
CsrBlob::set(
	quint64			value,	//!<[out] Value to insert into buffer.
	CsrInfo::Type	type	//!<[in]  Data type.
)
{
	d->ui = value;
	d->set(sizeof(value), IsInteger, IsUnsigned, type,
			"Set a %1 field from %2-byte unsigned integer (%3)", QString::number(value));
	return *this;
}

//======================================================================================================================
/*! @brief Store a value into the buffer.

@return a reference to this.
*/
CsrBlob &
CsrBlob::set(
	float			value,	//!<[out] Value to insert into buffer.
	CsrInfo::Type	type	//!<[in]  Data type.
)
{
	d->fp = value;
	d->set(sizeof(value), IsFloat, IsSigned, type,
			"Set a %1 field from %2-byte floating point (%3)", QString::number(value));
	return *this;
}

//======================================================================================================================
/*! @brief Store a value into the buffer.

@return a reference to this.
*/
CsrBlob &
CsrBlob::set(
	double			value,	//!<[out] Value to insert into buffer.
	CsrInfo::Type	type	//!<[in]  Data type.
)
{
	d->fp = value;
	d->set(sizeof(value), IsFloat, IsSigned, type,
			"Set a %1 field from %2-byte floating point (%3)", QString::number(value));
	return *this;
}

//======================================================================================================================
/*! @brief Check source value information and insert a value into the buffer.

The value inserted into the buffer is obtained from one of the value variables (fp, si, ui).
*/
void
CsrByteArrayPrivate::set(
	int					valSize,		//!<[in] Size of the source value.
	VariableFloatType	valFloatType,	//!<[in] Determine if the source value is floating point.
	VariableSignType	valSignType,	//!<[in] Determine if the source value is signed.
	CsrInfo::Type		type,			//!<[in] Data type.
	QString const		&message,		//!<[in] Log message format.
	QString const		&valText		//!<[in] Log message text value.
)
{
	LOG_Trace(loggerGet,
				QString("Set: size=%1, isFloat=%2, isSigned=%3, type=%4, messge='%5', valText='%6'")
				.arg(valSize).arg(valFloatType).arg(valSignType).arg(+type).arg(message).arg(valText));
	int		size;
	bool	isSigned;
	bool	isFloat;
	QString	name;
	bool	okay = CsrInfo::check(type, size, isSigned, isFloat, name);
	LOG_AssertMsg(loggerGet, okay, QString("Invalid CsrDataType: %1").arg(+type));
	// Ensure the buffer data type is big enough for the value.
	LOG_AssertMsg(loggerSet, valSize <= size,
					QString("Source value too big for %1").arg(name));
	// Ensure the variable is correct type: signed/unsigned.
	LOG_AssertMsg(loggerSet, (IsSigned == valSignType) == isSigned,
					QString("Source value is not %1").arg(isSigned ? "signed" : "unsigned"));
	// Ensure the variable is correct type integer/floating point.
	LOG_AssertMsg(loggerSet, (IsFloat == valFloatType) == isFloat,
					QString("Source value is not %1").arg(isFloat ? "float" : "integer"));
	// Get index of the destination.
	int	index = offset - base;
	LOG_AssertMsg(loggerGet, buffer.size() >= (size + index),
					QString("Buffer overflow (set): offset=%1, base=%2, size=%3, buffer=%4")
							.arg(offset).arg(base).arg(size).arg(buffer.size()));
	// Record the operation.
	LOG_Trace(loggerSet, message.arg(name).arg(valSize).arg(valText));
	//
	switch (type) {
	case CsrInfo::Type::SI1:	buffer.replace(index, size, (char const *)&si, size);	break;
	case CsrInfo::Type::UI1:	buffer.replace(index, size, (char const *)&ui, size);	break;
	case CsrInfo::Type::SI2:	buffer.replace(index, size, (char const *)&si, size);	break;
	case CsrInfo::Type::UI2:	buffer.replace(index, size, (char const *)&ui, size);	break;
	case CsrInfo::Type::SI4:	buffer.replace(index, size, (char const *)&si, size);	break;
	case CsrInfo::Type::UI4:	buffer.replace(index, size, (char const *)&ui, size);	break;
	case CsrInfo::Type::SI8:	buffer.replace(index, size, (char const *)&si, size);	break;
	case CsrInfo::Type::UI8:	buffer.replace(index, size, (char const *)&ui, size);	break;
	case CsrInfo::Type::FP4: {
								float	f = fp;
								buffer.replace(index, size, (char const *)&f, size);	break;
	}
	case CsrInfo::Type::FP8:	buffer.replace(index, size, (char const *)&fp, size);	break;
	default:					LOG_Fatal(loggerSet, QString("Unknown CsrDataType=%1").arg(+type));
	}
	// Move the buffer forward.
	offset += size;
}
