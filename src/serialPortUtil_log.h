#pragma once
#if !defined(SERIALPORTUTIL_LOG_H) && !defined(DOXYGEN_SKIP)
#define SERIALPORTUTIL_LOG_H
//######################################################################################################################
/*! @file
@brief Declare the serial port utility module loggers.
*/
//######################################################################################################################

#include "log.h"

//######################################################################################################################
// Loggers

LOG_SETUP_START("Serial Port Utility Module");

// Top level module logger.
LOG_DEF_LOG(logger,	LOG_DL_INFO,	"SerialPortUtil");

LOG_SETUP_STOP();

//######################################################################################################################
#endif
