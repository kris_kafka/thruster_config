/*! @file
@brief Define the serial port utility module.
*/
//######################################################################################################################

#include "app_com.h"

#include <QtCore/QSysInfo>
#include <QtSerialPort/QSerialPortInfo>

#include "serialPortUtil.h"
#include "serialPortUtil_log.h"

//######################################################################################################################
/*! @brief Get the list of available serial ports.

@return String list of available port names.
*/
QStringList
SerialPortUtil::availablePorts(void)
{

	LOG_Trace(logger, "Query available serial ports");
	QStringList				list;
	QList<QSerialPortInfo>	ports = QSerialPortInfo::availablePorts();
	for (QSerialPortInfo const &port : ports) {
		if (!port.isBusy()) {
			list.append(port.portName());
		}
	}
	return list;
}
//======================================================================================================================
/*! @brief Convert serial port name to simple format.

@return serial port name.
*/
QString SerialPortUtil::portNameFromSystemLocation(
	const QString &portName	//!<[in] Serial port name to convert.
)
{
	if (QSysInfo::WV_None != QSysInfo::WindowsVersion) {
		return (portName.startsWith(QLatin1String("\\\\.\\")) || portName.startsWith(QLatin1String("//./")))
				? portName.mid(4)
				: portName;
	} else {
		return portName.startsWith(QLatin1String("/dev/"))
					? portName.mid(5)
					: portName;
	}
}

//======================================================================================================================
/*! @brief Convert serial port name to system format.

@return serial port name.
*/
QString SerialPortUtil::portNameToSystemLocation(
	const QString &portName	//!<[in] Serial port name to convert.
)
{
	if (QSysInfo::WV_None != QSysInfo::WindowsVersion) {
		return portName.startsWith(QLatin1String("COM"))
					? (QLatin1String("\\\\.\\") + portName)
					: portName;
	} else {
		return (portName.startsWith(QLatin1Char('/'))
					|| portName.startsWith(QLatin1String("./"))
					|| portName.startsWith(QLatin1String("../"))
				)
				? portName
				: (QLatin1String("/dev/") + portName);
	}
}

