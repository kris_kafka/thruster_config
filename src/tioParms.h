#pragma once
#ifndef TIOPARMS_H
#ifndef DOXYGEN_SKIP
#define TIOPARMS_H
#endif
//######################################################################################################################
/*! @file
@brief Declare the interface to the thruster I/O module.
*/
//######################################################################################################################

#include <stdint.h>

#include <boost/shared_array.hpp>

#include "videoray/applib/protocol_vr_csr.h"

#include "configCsr.h"
#include "iniFile.h"
#include "thruster.h"

//######################################################################################################################

class	QString;
class	TioParms;

//######################################################################################################################

//! Pointer to a reply handler member fuction.
typedef	void	(*PacketHandler)(TioParms *iop, Protocol_VRCSR_Packet const &packet, int deviceTypeSize);

//######################################################################################################################
/*! \brief Generic I/O parameters.
*/
class TioParms
{
public:		// Constructors & destructors
	TioParms(PacketHandler packetHandler_, QString const &name_);
	virtual ~TioParms(void);

public:		// functions
	void	initialize(IniFile &iniFile, IniKey const &group);
	void	setDefaults(void);
	void	update(IniFile &iniFile, IniKey const &group);

public:		// Data
	boost::shared_array<Byte>	
						commandBuffer;				//!< Command buffer.
	int					commandSize;				//!< Command buffer size.
	Thruster::Error		errorCode;					//!< Error code.
	GroupId_t			groupIdMapMask;				//!< Group ID mapping mask.
	bool				isAbortOnError;				//!< Abort any retries on an error.
	bool				isCheckReplyAddress;		//!< Reply packet address should be checked.
	bool				isCheckReplyFlags;			//!< Reply packet flags should be checked.
	bool				isCheckReplyLength;			//!< Reply packet length should be checked.
	bool				isCheckReplyNodeId;			//!< Reply packet node ID should be checked.
	bool				isImmediateRetryOnError;	//!< Immediate retry on an error.
	bool				isIoNeeded;					//!< Is I/O needed?
	bool				isReplyExpected;			//!< Is a reply expected?
	bool				isReplyHasDeviceType;		//!< Reply packet data has a device type prefix.
	bool				isReplySeen;				//!< Saw a valid reply.
	bool				isResendEveryRetry;			//!< Should the command be send on every retry attempt.
	QString const		name;						//!< Name of I/O packets.
	PacketHandler		packetHandler;				//!< Reply packet handler.
	int					replyValidAddress;			//!< Valid reply packet address value.
	int					replyValidFlags;			//!< Valid reply packet flags value.
	int					replyValidLength;			//!< Valid reply packet length value.
	int					replyValidNodeId;			//!< Valid reply packet node ID value.
	int					retryLimit;					//!< Maximum number of retry attempts.
	int					sleepDuration;				//!< How long to sleep for while waiting for a reply.
	int					timeoutDuration;			//!< Maximum time to wait for a reply.
	int					validReplyDelayDuration;	//!< Time to delay after command.
};

//######################################################################################################################
#endif
