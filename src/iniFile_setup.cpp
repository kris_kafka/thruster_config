/*! @file
@brief Define the INI file setup function.
*/
//######################################################################################################################

#include "app_com.h"

#include "videoray/applib/gizmos/motor_controller/CSR.h"

//! @hideinitializer @brief Setup mode control.
#define INI_SETUP 1
#include "iniFile.h"

//######################################################################################################################
/*! @brief Generate the setup function.
*/
void
IniFile::setup(
	IniFile	&iniFile	//!<[in] IniFile to receive the default configuration.
)
{
	iniFile.setupStart();

	#ifndef DOXYGEN_SKIP
		#include "iniFile_all.h"
	#endif

	iniFile.setupStop();
}
