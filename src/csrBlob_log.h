#pragma once
#if !defined(CSRBLOB_LOG_H) && !defined(DOXYGEN_SKIP)
#define CSRBLOB_LOG_H
//######################################################################################################################
/*! @file
@brief Declare the CSR blob module loggers.
*/
//######################################################################################################################

#include "log.h"

//######################################################################################################################
// Loggers

LOG_SETUP_START("CSR Blob Module");

// Top level module logger.
LOG_DEF_LOG(logger,				LOG_DL_INFO,	"CsrBlob");
// Blob logger.
LOG_DEF_LOG(loggerBlob,			LOG_DL_INFO,	"CsrBlob.Blob");
// Object creation/destruction logger.
LOG_DEF_LOG(loggerCreate,		LOG_DL_INFO,	"CsrBlob.Create");
// Get logger.
LOG_DEF_LOG(loggerGet,			LOG_DL_INFO,	"CsrBlob.Get");
// Seek logger.
LOG_DEF_LOG(loggerSeek,			LOG_DL_INFO,	"CsrBlob.Seek");
// Set logger.
LOG_DEF_LOG(loggerSet,			LOG_DL_INFO,	"CsrBlob.Set");

LOG_SETUP_STOP();

//######################################################################################################################
#endif
