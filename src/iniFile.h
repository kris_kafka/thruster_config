#pragma once
#ifndef INIFILE_H
#ifndef DOXYGEN_SKIP
#define INIFILE_H
#endif
//######################################################################################################################
/*! @file
@brief Declare the INI file module.
*/
//######################################################################################################################

#if !defined(INI_SETUP) && !defined(DOXYGEN_SKIP)
#define INI_SETUP 0
#endif

//######################################################################################################################

#include <cstdint>

#include <QtCore/QByteArray>
#include <QtCore/QtGlobal>
#include <QtCore/QSettings>
#include <QtCore/QString>
#include <QtCore/QVariant>

//######################################################################################################################

typedef qulonglong	IniBits;	//!< IniFile bits (unsigned integer) data type.
typedef	bool		IniBoolean;	//!< IniFile boolean data type.
typedef QByteArray	IniBytes;	//!< IniFile bytes data type.
typedef double		IniFloat;	//!< IniFile floating point data type.
typedef qlonglong	IniInteger;	//!< IniFile signed integer data type.
typedef QString		IniString;	//!< IniFile string data type.

typedef QString		IniKey;		//!< IniFile setting key data type.

//######################################################################################################################
/*! @brief Encapsulate the INI file module.
*/
class	IniFile
:
	public	QSettings
{
public:		// Constructors & destructors
	IniFile(void);
	IniFile(QString const &filePath);
	~IniFile(void);

public:		// Functions
	IniBoolean	get(IniBoolean def, IniKey const &key);
	IniBytes	get(IniBytes const &def, IniKey const &key);
	IniFloat	get(IniFloat def, IniKey const &key);
	IniInteger	get(IniInteger def, IniKey const &key);
	IniString	get(IniString const &def, IniKey const &key);
	IniBits		get(IniBits def, IniKey const &key);

	void		set(IniBoolean val, IniKey const &key);
	void		set(IniBytes const &val, IniKey const &key);
	void		set(IniFloat val, IniKey const &key);
	void		set(IniInteger val, IniKey const &key);
	void		set(IniString const &val, IniKey const &key);
	void		set(IniBits val, IniKey const &key);

	static void	setup(IniFile &iniFile);

	void		setupArrayAdd(IniKey const &array);
	void		setupArrayEnd(void);
	void		setupDefine(IniKey const &key, QVariant const &def, IniKey const &type);
	void		setupGroupAdd(IniKey const &group);
	void		setupGroupEnd(void);

	void		syncAndCheck(void);

private:	// Functions.
	void		setupEndArraysAndGroups(void);
	void		setupStart(void);
	void		setupStop(void);
};

//######################################################################################################################

//! @hideinitializer @brief Get an INI file setting's default value symbol.
#define INI_DEFAULT(base)	base ## _Default

//! @hideinitializer @brief Get an INI file group symbol.
#define INI_GROUP(base)		base ## _Group

//! @hideinitializer @brief Get an INI file setting's key symbol.
#define INI_KEY(base)		base ## _Key

//! @hideinitializer @brief Get an INI file setting's data type symbol.
#define INI_TYPE(base)		base ## _Type

//######################################################################################################################

//! @hideinitializer @brief datatype of setting or add to defined settings.
#if INI_SETUP
	#define	INI_DEFINE_TYPE(base, type)	iniFile.setupDefine(INI_KEY(base), INI_DEFAULT(base), # type)
#else
	#define	INI_DEFINE_TYPE(base, type)	typedef Ini ## type	INI_TYPE(base)
#endif

//! @hideinitializer @brief Define INI file array group symbol.
#if INI_SETUP
	#define	INI_DEFINE_ARRAY(base, key)	\
		static IniKey const	INI_GROUP(base) = key; \
		iniFile.setupArrayAdd(INI_GROUP(base))
#else
	#define INI_DEFINE_ARRAY(base, key)		static IniKey const	INI_GROUP(base) = key
#endif

//! @hideinitializer @brief Define INI file group symbol.
#if INI_SETUP
	#define INI_DEFINE_GROUP(base, key)	\
		static IniKey const	INI_GROUP(base) = key; \
		iniFile.setupGroupAdd(INI_GROUP(base))
#else
	#define INI_DEFINE_GROUP(base, key)		static IniKey const	INI_GROUP(base) = key
#endif

//! @hideinitializer @brief Mark the end of an INI file array group.
#if INI_SETUP
	#define	INI_END_ARRAY()		iniFile.setupArrayEnd()
#else
	#define INI_END_ARRAY()		namespace LogModule {}
#endif

//! @hideinitializer @brief Mark the end of an INI file array group.
#if INI_SETUP
	#define	INI_END_GROUP()		iniFile.setupGroupEnd()
#else
	#define INI_END_GROUP()		namespace LogModule {}
#endif

//! @hideinitializer @brief Mark the end of an INI file array group.
#if INI_SETUP
	#define	INI_TYPEDEF(name, type)
#else
	#define INI_TYPEDEF(name, type)	typedef type name;
#endif

//######################################################################################################################

//! @hideinitializer @brief Define the symbols for a bits (unsigned integer) setting.
#define INI_DEFINE_BITS(base, key, def) \
		static IniKey const		INI_KEY(base)		= key; \
		static IniBits const	INI_DEFAULT(base)	= def; \
		INI_DEFINE_TYPE(base, Bits)

//! @hideinitializer @brief Define the symbols for a boolean setting.
#define INI_DEFINE_BOOLEAN(base, key, def) \
		static IniKey const		INI_KEY(base)		= key; \
		static IniBoolean const	INI_DEFAULT(base)	= def; \
		INI_DEFINE_TYPE(base, Boolean)

//! @hideinitializer @brief Define the symbols for a bytes setting.
#define INI_DEFINE_BYTES(base, key, def) \
		static IniKey const		INI_KEY(base)		= key; \
		static IniBytes const	INI_DEFAULT(base)	= def; \
		INI_DEFINE_TYPE(base, Bytes)

//! @hideinitializer @brief Define the symbols for a floating point setting.
#define INI_DEFINE_FLOAT(base, key, def) \
		static IniKey const		INI_KEY(base)		= key; \
		static IniFloat const	INI_DEFAULT(base)	= def; \
		INI_DEFINE_TYPE(base, Float)

//! @hideinitializer @brief Define the symbols for an signed integer setting.
#define INI_DEFINE_INTEGER(base, key, def) \
		static IniKey const		INI_KEY(base)		= key; \
		static IniInteger const	INI_DEFAULT(base)	= def; \
		INI_DEFINE_TYPE(base, Integer)

//! @hideinitializer @brief Define the symbols for a string setting.
#define INI_DEFINE_STRING(base, key, def) \
		static IniKey const		INI_KEY(base)		= key; \
		static IniString const	INI_DEFAULT(base)	= def; \
		INI_DEFINE_TYPE(base, String)

//======================================================================================================================

//! @hideinitializer @brief Set an INI file setting's value.
#define INI_SET(base, var)				set((INI_TYPE(base))var, INI_KEY(base))

//! @hideinitializer @brief Get an INI file setting's value.
#define INI_GET(base)					get(INI_DEFAULT(base), INI_KEY(base))

//! @hideinitializer @brief Get an INI file setting's value.
#define INI_VALUE(base, defaultValue)	get((INI_TYPE(base))defaultValue, INI_KEY(base))

//######################################################################################################################
#endif
