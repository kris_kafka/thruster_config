/*! @file
@brief Define the thruster configuration read module.
*/
//######################################################################################################################

#include "app_com.h"

#include <cstddef>
#include <string>

#include <QtCore/QSettings>

#include "videoray/applib/std_device_types.h"

#include "configCsr.h"
#include "configGio.h"
#include "configThruster.h"
#include "csrBlob.h"
#include "thruster.h"
#include "tioConfigRead.h"
#include "tioConfigRead_log.h"
#include "tioImpl.h"
#include "tioParms.h"

//######################################################################################################################
// Local classes.

class	TioImplConfigRead;
class	TioParmsCsr;
class	TioParmsId;

//######################################################################################################################
// Local functions

static void		csrReply(TioParms *biop, Protocol_VRCSR_Packet const &packet, int deviceTypeSize);
static void		idsReply(TioParms *biop, Protocol_VRCSR_Packet const &packet, int deviceTypeSize);

//######################################################################################################################
/*! \brief Read Configuration CSR data implementation.
*/
class TioImplConfigRead
:
	public	TioImpl
{
public:		// Constructors & destructors
				TioImplConfigRead(std::string &portName, int nodeId, bool const volatile &halt);
	virtual		~TioImplConfigRead(void);

public:		// Functions
	Thruster::Error		csrGet(int &motorId, int &maximumPower, bool &rotationReverse, QByteArray &csrBlob);
	Thruster::Error		idsGet(int &nodeId, int &groupId, QString &serialNumber);
};

//======================================================================================================================
/*! \brief Read Configuration CSR data I/O parameters.
*/
class TioParmsCsr
:
	public	TioParms
{
public:		// Constructors & destructors
	TioParmsCsr(int nodeId, int &motorId_, int &maximumPower_, bool &rotationReverse_, QByteArray &csrBlob_);

public:		// Data
	int					motorControlFlagsOffset;	//!< CSR offset of motorControlFlags.
	CsrInfo::Type		motorControlFlagsType;		//!< CSR type of motorControlFlags.
	QByteArray			&csrBlob;					//!< CSR blob.
	int					&maximumPower;				//!< Maximum power read from thruster.
	int					maximumPowerOffset;			//!< CSR offset of maxPower.
	CsrInfo::Type		maximumPowerType;			//!< CSR type of maxPower.
	int					&motorId;					//!< Motor ID read from thruster.
	int					motorIdOffset;				//!< CSR offset of motorId.
	CsrInfo::Type		motorIdType;				//!< CSR type of motorId.
	bool				&rotationReverse;			//!< Rotation reversed read from thruster.
	MotorControlFlags_t	rotationReversedMask;		//!< Mask of motorControlFlags for rotation reversed flag.
	MotorControlFlags_t	rotationReversedTrue;		//!< True value of rotation reversed flag in motorControlFlags.
};

//======================================================================================================================
/*! \brief Read Configuration ID data I/O parameters.
*/
class TioParmsId
:
	public	TioParms
{
public:		// Constructors & destructors
	TioParmsId(int nodeId, int &nodeIdRead_, int &groupId_, QString &serialNumber_);

public:		// Data
	int			&nodeIdRead;	//!< Node ID read of the thruster.
	int			&groupId;		//!< Group ID read from thruster.
	QString		&serialNumber;	//!< Serial number read from thruster.
};

//######################################################################################################################
/*! @brief Create a TioConfigRead object.
*/
TioConfigRead::TioConfigRead(
	QString const		&serialPort,	//!<[in] Serial port name.
	int					nodeId,			//!<[in] Node ID.
	bool const volatile	&halt			//!<[in] Halt I/O.
)
:
	TioBase		(serialPort, nodeId, halt),
	connection	(Q_NULLPTR)
{
	LOG_Trace(loggerCreate,
				QString("Creating TioConfigRead object: serialPort='%1', nodeId=%2").arg(serialPort).arg(nodeId));
}

//======================================================================================================================
/*! @brief Destroy a TioConfigRead object.
*/
TioConfigRead::~TioConfigRead(void)
{
	LOG_Trace(loggerCreate,
				QString("Destroying TioConfigRead object: serialPort='%1', nodeId=%2").arg(serialPort).arg(nodeId));
}

//======================================================================================================================
/*! @brief Create a TioImplConfigRead object.
*/
TioImplConfigRead::TioImplConfigRead(
	std::string			&portName,	//!<[in] Serial port name.
	int					nodeId,		//!<[in] Node ID.
	bool const volatile	&halt		//!<[in] Halt I/O.
)
:
	TioImpl	(portName, nodeId, halt)
{
	LOG_Trace(loggerCreate,
				QString("Creating TioImplConfigRead object: serialPort='%1', nodeId=%2").arg(serialPort).arg(nodeId));
}

//======================================================================================================================
/*! @brief Destroy a TioImplConfigRead object.
*/
TioImplConfigRead::~TioImplConfigRead(void)
{
	LOG_Trace(loggerCreate,
				QString("Destroying TioImplConfigRead object: serialPort='%1', nodeId=%2").arg(serialPort).arg(nodeId));
}

//======================================================================================================================
/*! @brief Create a TioParmsCsr object.
*/
TioParmsCsr::TioParmsCsr(
	int			nodeId,				//!<[in]  Reply packet valid node ID.
	int			&motorId_,			//!<[out] Motor ID read from thruster.
	int			&maximumPower_,		//!<[out] Maximum power read from thruster.
	bool		&rotationReverse_,	//!<[out] Rotation reversed read from thruster.
	QByteArray	&csrBlob_			//!<[out] CSR blob.
)
:
	TioParms				(csrReply, "ConfigRead CSR"),
	motorControlFlagsOffset	(0),
	motorControlFlagsType	(CsrInfo::Type::Bad),
	csrBlob					(csrBlob_),
	maximumPower			(maximumPower_),
	maximumPowerOffset		(0),
	maximumPowerType		(CsrInfo::Type::Bad),
	motorId					(motorId_),
	motorIdOffset			(0),
	motorIdType				(CsrInfo::Type::Bad),
	rotationReverse			(rotationReverse_),
	rotationReversedMask	(0),
	rotationReversedTrue	(0)
{
	LOG_Trace(loggerCsrParameters, QString("Creating TioParmsCsr object: nodeId=%1").arg(nodeId));
	//
	IniFile	iniFile;
	initialize(iniFile, INI_GROUP(CfgGioConfRdCsr));
	//
	motorControlFlagsOffset	= iniFile.INI_GET(CfgCsrMotorControlFlagsOffset);
	maximumPowerOffset		= iniFile.INI_GET(CfgCsrMaximumPowerOffset);
	motorIdOffset			= iniFile.INI_GET(CfgCsrMotorIdOffset);
	rotationReversedMask	= iniFile.INI_GET(CfgCsrRotationReversedMask);
	rotationReversedTrue	= iniFile.INI_GET(CfgCsrRotationReversedTrue);
	motorControlFlagsType	= CsrInfo::type(iniFile.INI_GET(CfgCsrMotorControlFlagsType));
	maximumPowerType		= CsrInfo::type(iniFile.INI_GET(CfgCsrMaximumPowerType));
	motorIdType				= CsrInfo::type(iniFile.INI_GET(CfgCsrMotorIdType));
	//
	replyValidAddress	= qMin(motorControlFlagsOffset, maximumPowerOffset);
	replyValidAddress	= qMin(replyValidAddress, motorIdOffset);
	//
	int lastCsrAddress	= qMax(motorControlFlagsOffset + CsrInfo::size(motorControlFlagsType),
								maximumPowerOffset + CsrInfo::size(maximumPowerType));
	lastCsrAddress		= qMax(lastCsrAddress, motorIdOffset + CsrInfo::size(motorIdType));
	replyValidLength	= lastCsrAddress - replyValidAddress;
	//
	replyValidFlags		= RESPONSE_TYPE_FLAG;
	replyValidNodeId	= nodeId;
	//
	LOG_AssertMsg(loggerCsrParameters, 0 < replyValidLength,
					QString("Invalid reply length").arg(replyValidLength));
	//
	commandSize = protocol_vrcsr_calc_packet_size(0, false, PROTOCOL_VRCSR);
	ByteP	packetBuffer = new (std::nothrow) Byte[commandSize];
	LOG_CHK_PTR_MSG(loggerCsrParameters, packetBuffer, "Unable to create TioParmsCsr packetBuffer");
	commandBuffer.reset(packetBuffer);
	(void)protocol_vrcsr_build_request_inplace(nodeId,
												REQUEST_LEN(replyValidLength),
												replyValidAddress,
												0,
												commandBuffer.get(),
												PROTOCOL_VRCSR);
}

//======================================================================================================================
/*! @brief Create a TioParmsId object.
*/
TioParmsId::TioParmsId(
	int		nodeId,			//!<[in]  Reply packet valid node ID.
	int		&nodeIdRead_,	//!<[out] Node ID read from the thruster.
	int		&groupId_,		//!<[out] Group ID read from thruster.
	QString	&serialNumber_	//!<[out] Serial number read from thruster.
)
:
	TioParms		(idsReply, "ConfigRead IDs"),
	nodeIdRead		(nodeIdRead_),
	groupId			(groupId_),
	serialNumber	(serialNumber_)
{
	LOG_Trace(loggerIdsParameters, QString("Creating TioParmsId object: nodeId=%1").arg(nodeId));
	//
	IniFile	iniFile;
	initialize(iniFile, INI_GROUP(CfgGioConfRdIds));
	//
	replyValidAddress = ADDR_UTILITY;
	replyValidLength = 3 + BOOTLOADER_SN_SIZE;
	replyValidFlags = RESPONSE_ENNUMERATION_FLAG;
	replyValidNodeId = nodeId;
	//
	int	dataSize = iniFile.INI_GET(CfgThrusterRqstIdsPayloadSize);
	commandSize = protocol_vrcsr_calc_packet_size(dataSize, false, PROTOCOL_VRCSR);
	ByteP	packetBuffer = new (std::nothrow) Byte[commandSize];
	LOG_CHK_PTR_MSG(loggerIdsParameters, packetBuffer, "Unable to create TioParmsId packetBuffer");
	commandBuffer.reset(packetBuffer);
	int	cmdSize = protocol_vrcsr_request_id(nodeId,
											iniFile.INI_GET(CfgThrusterConfigReadIdUseCdma),
											commandBuffer.get(),
											commandSize,
											PROTOCOL_VRCSR);
	if (0 > cmdSize) {
		isIoNeeded = false;
		errorCode = Thruster::TEC_PacektError;
		LOG_Error(loggerIdsParameters,
					QString("Error building command packet: %1 (%2)").arg(cmdSize).arg(commandSize));
	}
}

//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
/*! @brief Read Configuration Read data.

@return the error code (Thruster::TEC_NoError if no errors).
*/
Thruster::Error
TioConfigRead::configurationRead(
	int			&nodeIdRead,		//!<[out] Node ID read from the thruster.
	int			&groupId,			//!<[out] Group ID read from thruster.
	QString		&serialNumber,		//!<[out] Serial number read from thruster.
	int			&motorId,			//!<[out] Motor ID read from thruster.
	int			&maximumPower,		//!<[out] Maximum power read from thruster.
	bool		&rotationReverse,	//!<[out] Rotation reversed read from thruster.
	QByteArray	&csrBlob			//!<[out] CSR blob.
)
{
	LOG_Trace(loggerMain, QString("Read configuration data: nodeId=%1").arg(nodeId));
	LOG_ASSERT_MSG(loggerMain, connection, "Not open");
	//
	nodeIdRead		= 0;
	groupId			= 0;
	motorId			= 0;
	maximumPower	= 0;
	rotationReverse	= false;
	serialNumber	= "";
	//
	Thruster::Error	errorCode;
	errorCode = connection->idsGet(nodeIdRead, groupId, serialNumber);
	if (Thruster::TEC_NoError != errorCode) {
		return errorCode;
	}
	errorCode = connection->csrGet(motorId, maximumPower, rotationReverse, csrBlob);
	if (Thruster::TEC_NoError != errorCode) {
		return errorCode;
	}
	return errorCode;
}

//======================================================================================================================
/*! @brief Get the configuration CSR data.

@return the error code (Thruster::TEC_NoError if no errors).
*/
Thruster::Error
TioImplConfigRead::csrGet(
	int			&motorId,			//!<[out] Motor ID read from thruster.
	int			&maximumPower,		//!<[out] Maximum power read from thruster.
	bool		&rotationReverse,	//!<[out] Rotation reversed read from thruster.
	QByteArray	&csrBlob			//!<[out] CSR blob.
)
{
	LOG_Trace(loggerCsrCommand, "Requesting configuration CSR data");
	TioParmsCsr		ciop(nodeId, motorId, maximumPower, rotationReverse, csrBlob);
	return genericIo(&ciop);
}

//======================================================================================================================
/*! @brief Process a configuration CSR data reply packet.
*/
void
csrReply(
	TioParms					*biop,			//!<[in,out] I/O parameters.
	Protocol_VRCSR_Packet const	&packet,		//!<[in] Reply packet.
	int							deviceTypeSize	//!<[in] Size of device type data prefix.
)
{
	LOG_Trace(loggerCsrReply, "Received configuration CSR data reply");
	TioParmsCsr	*iop = dynamic_cast<TioParmsCsr *>(biop);
	LOG_ASSERT_MSG(loggerCsrReply, iop, "csrReply: iop is invalid");
	// Extract the data from the payload.
	MotorControlFlags_t	motorControlFlags;
	MaximumPower_t	maximumPower;
	MotorId_t		motorId;
	CsrBlob	csr(packet.payload, packet.header->length, iop->replyValidAddress - deviceTypeSize);
	csr	.move(iop->motorControlFlagsOffset)	.get(motorControlFlags,	iop->motorControlFlagsType)
		.move(iop->maximumPowerOffset)	.get(maximumPower,	iop->maximumPowerType)
		.move(iop->motorIdOffset)		.get(motorId,		iop->motorIdType);
	iop->rotationReverse = iop->rotationReversedTrue == (iop->rotationReversedMask & motorControlFlags);
	iop->maximumPower	= maximumPower;
	iop->motorId		= motorId;
	iop->csrBlob		= csr.blob();
	iop->errorCode		= Thruster::TEC_NoError;
	iop->isReplySeen	= true;
}

//======================================================================================================================
/*! @brief Close the serial port.
*/
void
TioConfigRead::doClose(void)
{
	LOG_Trace(loggerOpen,
				QString("Closing connection: serialPort='%1', nodeId=%2").arg(serialPort).arg(nodeId));
	// Done if already closed.
	if (!connection.isNull()) {
		connection.reset();
	}
}

//======================================================================================================================
/*! @brief Open the serial port.

@return true if no errors occur else false.
*/
bool
TioConfigRead::doOpen(void)
{
	LOG_Trace(loggerOpen,
				QString("Opening connection: serialPort='%1', nodeId=%2").arg(serialPort).arg(nodeId));
	// Done if already open.
	if (!connection.isNull()) {
		return true;
	}
	std::string portName = serialPort.toStdString();
	connection.reset(new TioImplConfigRead(portName, nodeId, halt));
	return !connection.isNull() && connection->isOpen();
}

//======================================================================================================================
/*! @brief Get the configuration ID data.

@return the error code (Thruster::TEC_NoError if no errors).
*/
Thruster::Error
TioImplConfigRead::idsGet(
	int		&nodeIdRead,	//!< Node ID read from the thruster.
	int		&groupId,		//!< Group ID read from thruster.
	QString	&serialNumber	//!< Serial number read from thruster.
)
{
	LOG_Trace(loggerIdsCommand, "Requesting configuration IDs");
	TioParmsId	ciop(nodeId, nodeIdRead, groupId, serialNumber);
	return genericIo(&ciop);
}

//======================================================================================================================
/*! @brief Process a configuration ID reply packet.
*/
void
idsReply(
	TioParms					*biop,			//!<[in,out] I/O parameters.
	Protocol_VRCSR_Packet const	&packet,		//!<[in] Reply packet.
	int							deviceTypeSize	//!<[in] Size of device type data prefix.
)
{
	LOG_Trace(loggerIdsReply, "Received configuration IDs reply");
	Q_UNUSED(deviceTypeSize);
	TioParmsId	*iop = dynamic_cast<TioParmsId *>(biop);
	LOG_ASSERT_MSG(loggerIdsReply, iop, "idsReply: iop is invalid");
	// Extract the configuration data from the packet.
	ByteP	payload			= packet.payload;
	int		devTypePre		= *payload++;
	int		nodeIdRead		= *payload++;
	int		groupId			= *payload++;
	int		deviceType		= *payload++;
	QString	serialNumber	= QString((char *)payload);
	// Ensure the data is consistent.
	int hdrNodeId = packet.header->id;
	if (hdrNodeId != nodeIdRead) {
		iop->errorCode = Thruster::TEC_NodeIdDifferent;
		LOG_Error(loggerIdsReply,
					QString("Unexpected reply: Node ID mismatch in ID reply: header=%1, payload=%2")
							.arg(hdrNodeId).arg(nodeIdRead));
		return;
	}
	if (devTypePre != deviceType) {
		iop->errorCode = Thruster::TEC_DeviceTypeDifferent;
		LOG_Error(loggerIdsReply,
					QString("Unexpected reply: Device type mismatch in ID reply: prefix=%1, data=%2")
							.arg(devTypePre).arg(deviceType));
		return;
	}
	// Ensure the device is a thruster.
	if (DEVICE_M5_THRUSTER != deviceType) {
		iop->errorCode = Thruster::TEC_DeviceType;
		LOG_Error(loggerIdsReply,
					QString("Unexpected reply: Unexpected device type in ID reply: %1").arg(deviceType));
		return;
	}
	// Save the validated data.
	iop->nodeIdRead		= nodeIdRead;
	iop->groupId		= groupId & ~iop->groupIdMapMask;	// Remove group ID flag.
	iop->serialNumber	= serialNumber;
	iop->errorCode		= Thruster::TEC_NoError;
	iop->isReplySeen	= true;
}
