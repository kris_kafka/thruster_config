/*! @file
@brief Define the test dialog module.
*/
//######################################################################################################################

#include "app_com.h"

#include <algorithm>

#include <QtCore/QMetaMethod>
#include <QtCore/QRegularExpression>
#include <QtCore/QSettings>
#include <QtGui/QCloseEvent>
#include <QtWidgets/QMessageBox>

#include "configMisc.h"
#include "configState.h"
#include "main.h"
#include "strUtil.h"
#include "testDialog.h"
#include "testDialog_log.h"
#include "testDialogPrivate.h"
#include "thruster.h"

//######################################################################################################################

static double const	PercentageFactor	= 100.0;

//######################################################################################################################
/*! @brief Create a TestDialog object.
*/
TestDialog::TestDialog(
	int		nodeId,		//!<[in] Node ID of thruster.
	int		groupId,	//!<[in] Group ID of thruster.
	int		motorId,	//!<[in] Motor ID of thruster.
	QWidget	*parent		//!<[in] The parent object.
	)
:
	d	(Q_NULLPTR)
{
	LOG_Trace(loggerCreate, "Creating TestDialog");
	//
	d.reset(new (std::nothrow) TestDialogPrivate(nodeId, groupId, motorId, parent));
	LOG_CHK_PTR_MSG(loggerCreate, d, "Unable to create TestDialogPrivate object");
}

//----------------------------------------------------------------------------------------------------------------------
/*! @brief Create a TestDialogPrivate object.
*/
TestDialogPrivate::TestDialogPrivate(
	int		nodeId_,	//!<[in] Node ID of thruster.
	int		groupId_,	//!<[in] Group ID of thruster.
	int		motorId_,	//!<[in] Motor ID of thruster.
	QWidget	*parent		//!<[in] The parent object.
)
:
	QDialog		(parent),
	currentMode	(0),
	groupId		(groupId_),
	isCsr		(),
	nodeId		(nodeId_),
	motorId		(motorId_),
	thruster	(Q_NULLPTR)
{
	LOG_Trace(loggerCreate, "Creating TestDialogPrivate");
	//
	thruster.reset(new (std::nothrow) Thruster);
	LOG_CHK_PTR_MSG(loggerCreate, thruster, "Unable to create Thruster object");
	setupUi(this);
	setupSpeedControls();
	restoreWindowState();
	//
	initModes();
	//
	rpmValueLabel			->setText("N/A");
	voltageValueLabel		->setText("N/A");
	currentValueLabel		->setText("N/A");
	temperatureValueLabel	->setText("N/A");
	faultsValueLabel		->setText("N/A");
	//
	on_speedSlider_valueChanged(speedSlider->value());
	//
	LOG_CONNECT(loggerCreate,	thruster.data(),	&Thruster::testingModeNews,
								this,				&TestDialogPrivate::at_thruster_testModeNews);
	LOG_CONNECT(loggerCreate,	thruster.data(),	&Thruster::testingModeStart,
								this,				&TestDialogPrivate::at_thruster_testModeStart);
	LOG_CONNECT(loggerCreate,	thruster.data(),	&Thruster::testingModeStop,
								this,				&TestDialogPrivate::at_thruster_testModeStop);
	//
	updateGuiState();
}

//======================================================================================================================
/*! @brief Destroy a TestDialog object.
*/
TestDialog::~TestDialog(void)
{
	LOG_Trace(loggerCreate, "Destroying TestDialog");
}

//----------------------------------------------------------------------------------------------------------------------
/*! @brief Destroy a TestDialogPrivate object.
*/
TestDialogPrivate::~TestDialogPrivate(void)
{
	LOG_Trace(loggerCreate, "Destroying TestDialogPrivate");
	// Ensure testing mode is not active.
	if (thruster->isTestingMode()) {
		if (!QObject::disconnect(thruster.data(), QMetaMethod(), this, QMetaMethod())) {
			LOG_Error(loggerCreate, "Disconnect from thruster interface module failed");
		}
		if (!thruster->testingModeEnd()) {
			LOG_Error(logger, "Unable to stop testing mode");
		}
	}
}

//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
/*! @brief Update the status values.
*/
void
TestDialogPrivate::at_thruster_testModeNews(
	int		nodeId_,		//!< Node ID.
	double	rpm,			//!< Thruster current RPM.
	double	voltage,		//!< Bus voltage.
	double	current,		//!< Bus current.
	double	temperature,	//!< Thruster temperature.
	uint	faults			//!< Thruster fault flags.
)
{
	LOG_Trace(loggerStatus,
				QString("at_thruster_testModeNews: nodeId=%1, rpm=%2, voltage=%3, curent=%4, termperature=%5, "
								"faults=%6")
						.arg(nodeId).arg(rpm).arg(voltage).arg(current).arg(temperature)
						.arg(faults, 2, 16, QChar('0')));
	LOG_ASSERT_MSG(loggerStatus, nodeId == nodeId_, "Node ID different");
	int faultSize = isCsr[currentMode] ? 6 : 2;
	//
	rpmValueLabel			->setText(StrUtil::toString(rpm, 1));
	voltageValueLabel		->setText(StrUtil::toString(voltage, 2));
	currentValueLabel		->setText(StrUtil::toString(current, 3));
	temperatureValueLabel	->setText(StrUtil::toString(temperature, 1));
	faultsValueLabel		->setText(QString("0x%1").arg(faults, faultSize, 16, QChar('0')));
}

//======================================================================================================================
/*! @brief Invoke a TestDialog object.
*/
void
TestDialogPrivate::at_thruster_testModeStart(
	int	errorCode	//!< Error code (TEC_NoError if no errors).
)
{
	LOG_Trace(loggerStatus, QString("at_thruster_testModeStart: %1").arg(errorCode));
	//
	if (Thruster::TEC_NoError != errorCode) {
		QString	errorMessage = thruster->errorMessage(errorCode);
		LOG_Error(loggerStatus, QString("Unable to start testing mode: %1").arg(errorMessage));
		showErrorMessage(QString("Unable to start testing mode:\n\n%1").arg(errorMessage));
	}
	//
	updateGuiState();
}

//======================================================================================================================
/*! @brief Invoke a TestDialog object.
*/
void
TestDialogPrivate::at_thruster_testModeStop(
	int	errorCode	//!< Error code (TEC_NoError if no errors).
)
{
	LOG_Trace(loggerStatus, QString("at_thruster_testModeStop: %1").arg(errorCode));
	//
	if (Thruster::TEC_NoError != errorCode) {
		QString	errorMessage = thruster->errorMessage(errorCode);
		LOG_Error(loggerStatus, QString("Testing mode error: %1").arg(errorMessage));
		showErrorMessage(QString("Testing mode error:\n\n%1").arg(errorMessage));
	}
	//
	updateGuiState();
}

//======================================================================================================================
/*! @brief Restore the window state.
*/
void
TestDialogPrivate::closeEvent(
	QCloseEvent		*event	//![in,out] Event information.
)
{
	LOG_Trace(loggerCreate, "Closing window");
	//
	saveWindowState();
	QWidget::closeEvent(event);
}

//======================================================================================================================
/*! @brief Initialize the mode control.
*/
void
TestDialogPrivate::initModes(void)
{
	LOG_Trace(loggerCreate, "Initialize mode control");
	//
	IniFile		iniFile;
	QString		format		= iniFile.INI_GET(CfgMiscTestingModeFormat);
	QString		modeStr		= iniFile.INI_GET(CfgMiscTestingModes);
	int			modeDefault	= iniFile.INI_GET(CfgMiscTestingModeDefault);
	QStringList	modeList = modeStr.split(QRegularExpression("[^0-9]+"), QString::SkipEmptyParts);
	// Convert to numeric, keeping only the valid values.
	QVector<int>	modes;
	std::for_each(modeList.begin(),
					modeList.end(),
					[&modes](QString const &mode)
					{
						int num = mode.toInt();
						if (Thruster::TM_Min <= num && num < Thruster::TM_End && !modes.contains(num)) {
							modes.append(num);
						}
					});
	// Allow all modes if no available ones given.
	if (modes.isEmpty()) {
		for (int mode = Thruster::TM_Min; mode < Thruster::TM_End; ++mode) {
			modes.append(mode);
		}
	}
	// Populate the control.
	int defaultIndex = 0;	// Use the first available mode if the given default mode is not valid.
	modeComboBox->clear();
	isCsr.fill(false, Thruster::TM_End);
	std::for_each(modes.begin(),
					modes.end(),
					[this, &defaultIndex, &format, &modeDefault](int &mode)
					{
						Thruster::ThrusterIdType	idType;
						bool						isCheck, isPropulsion, isReply;
						QString						name;
						if (Thruster::testingModeInfo(mode, idType, isCheck, isPropulsion, isReply, name)) {
							// Set the index if this is the default mode.
							if (modeDefault == mode) {
								defaultIndex = modeComboBox->count();
							}
							//
							QString	text = QString(format)
												.replace("{name}", name, Qt::CaseInsensitive)
												.replace("{mode}", QString::number(1 + mode), Qt::CaseInsensitive);
							modeComboBox->addItem(text, QVariant(mode));
							isCsr[mode] = !isPropulsion;

						}
					});
	// Select the default mode.
	modeComboBox->setCurrentIndex(defaultIndex);
	currentMode = modeComboBox->currentData().toInt();
}

//======================================================================================================================
/*! @brief Invoke a TestDialog object.
*/
void
TestDialog::invoke(void)
{
	LOG_Trace(loggerInvoke, "Invoking TestDialog");
	d->exec();
}

//======================================================================================================================
/*! @brief Handle changes to the single mode check box.
*/
void
TestDialogPrivate::on_modeComboBox_currentIndexChanged(
	int	newValue	//!<[in] New value.
)
{
	LOG_Trace(loggerUserInput, "Mode changed");
	currentMode = modeComboBox->itemData(newValue).toInt();
}

//======================================================================================================================
/*! @brief Handle changes to the speed slider.
*/
void
TestDialogPrivate::on_speedSlider_valueChanged(
	int	newValue	//!<[in] New value.
)
{
	LOG_Trace(loggerUserInput, QStringLiteral("Speed slider changed: %1").arg(newValue));
	double newSpeed = newValue / sliderFactor;
	speedSpinBox->setValue(newSpeed);
	setSpeed(newSpeed);
}

//======================================================================================================================
/*! @brief Handle changes to the speed text.
*/
void
TestDialogPrivate::on_speedSpinBox_valueChanged(
	double	newValue	//!<[in] New value.
)
{
	LOG_Trace(loggerUserInput, QStringLiteral("Speed spin box changed").arg(newValue));
	int newSliderValue = sliderFactor * newValue;
	speedSlider->setValue(newSliderValue);
	setSpeed(newValue);
}

//======================================================================================================================
/*! @brief Start the thruster.
*/
void
TestDialogPrivate::on_startPushButton_clicked(void)
{
	LOG_Trace(loggerUserInput, "Start thruster testing mode");
	if (!thruster->testingModeBegin(nodeId, groupId, motorId, currentMode)) {
		QString lastErrorMessage = thruster->lastErrorMessage();
		LOG_Error(logger, QString("Unable to start testing mode: %1").arg(lastErrorMessage));
		showErrorMessage(QString("Unable to start testing mode:\n\n%1").arg(lastErrorMessage));
	}
	updateGuiState();
}

//======================================================================================================================
/*! @brief Stop the thruster.
*/
void
TestDialogPrivate::on_stopPushButton_clicked(void)
{
	LOG_Trace(loggerUserInput, "Stop thruster testing mode");
	if (!thruster->testingModeEnd()) {
		QString lastErrorMessage = thruster->lastErrorMessage();
		LOG_Error(logger, QString("Unable to stop testing mode: %1").arg(lastErrorMessage));
		showErrorMessage(QString("Unable to stop testing mode:\n\n%1").arg(lastErrorMessage));
	}
	updateGuiState();
}

//======================================================================================================================
/*! @brief Restore the window state.
*/
void
TestDialogPrivate::restoreWindowState(void)
{
	LOG_Trace(loggerWindowState, "Restoring window state");
	//
	IniFile	iniFile;
	IniBytes	savedGeometry	= iniFile.INI_GET(CfgStateTestModeDialogGeometry);
	if (!savedGeometry.isEmpty()) {
		restoreGeometry(savedGeometry);
	}
}

//======================================================================================================================
/*! @brief Save the window state.
*/
void
TestDialogPrivate::saveWindowState(void)
{
	LOG_Trace(loggerWindowState, "Saving window state");
	//
	IniFile	iniFile;
	iniFile.INI_SET(CfgStateTestModeDialogGeometry, saveGeometry());
	iniFile.syncAndCheck();
}

//======================================================================================================================
/*! @brief Handle changes to the speed text.
*/
void
TestDialogPrivate::setSpeed(
	double	newSpeed	//!<[in] New thruster testing mode speed.
)
{
	LOG_Trace(loggerUserInput, QString("Set the thruster testing mode speed: %1").arg(newSpeed));
	if (!thruster->testingModeSpeed(newSpeed)) {
		// Record the error and restore the control values.
		LOG_Error(logger, QString("Unable to set the thruster speed: %1").arg(newSpeed));
		// Restore the old values.
		double	oldSpeed	= thruster->testingModeSpeed();
		int		oldSlider	= sliderFactor * oldSpeed;
		speedSlider->setValue(oldSlider);
		speedSpinBox->setValue(oldSpeed);
	}
}

//======================================================================================================================
/*! @brief Configure the speed controls.
*/
void
TestDialogPrivate::setupSpeedControls(void)
{
	IniFile	iniFile;
	int sliderPageStep		= iniFile.INI_GET(CfgMiscTestingSpeedSliderPageStep);
	int sliderSingleStep	= iniFile.INI_GET(CfgMiscTestingSpeedSliderSingleStep);
	int	sliderSteps			= iniFile.INI_GET(CfgMiscTestingSpeedSliderSteps);
	int sliderTickInterval	= iniFile.INI_GET(CfgMiscTestingSpeedSliderTickInterval);
	double spinBoxStep		= iniFile.INI_GET(CfgMiscTestingSpeedSpinBoxStep);
	int spinBoxDecimals		= iniFile.INI_GET(CfgMiscTestingSpeedSpinBoxDecimalDigits);
	LOG_AssertMsg(loggerCreate,
					sliderPageStep > sliderSingleStep,
					QStringLiteral("Invalid speed slider steps: sliderSingleStep=%1, sliderPageStep=%2")
							.arg(sliderSingleStep).arg(sliderPageStep));
	LOG_AssertMsg(loggerCreate,
					sliderSteps > sliderPageStep,
					QStringLiteral("Invalid speed slider limit: sliderSteps=%1, sliderPageStep=%2")
							.arg(sliderSteps).arg(sliderPageStep));
	LOG_AssertMsg(loggerCreate,
					sliderTickInterval <= sliderSteps,
					QStringLiteral("Invalid speed slider tick interval: sliderTickInterval=%1, sliderSteps=%2")
							.arg(sliderTickInterval).arg(sliderSteps));
	LOG_Trace(loggerCreate,
				QStringLiteral("Configure speed controls: limit=%1, singleStep=%2, pageStep=%3, tickInterval=%4, "
								"spinBoxStep=%5")
						.arg(sliderSteps).arg(sliderSingleStep).arg(sliderPageStep).arg(sliderTickInterval)
						.arg(spinBoxStep));
	speedSlider->setMinimum(-sliderSteps);
	speedSlider->setMaximum(sliderSteps);
	speedSlider->setSingleStep(sliderSingleStep);
	speedSlider->setPageStep(sliderPageStep);
	if (sliderSteps == sliderTickInterval) {
		speedSlider->setTickPosition(QSlider::NoTicks);
	} else {
		speedSlider->setTickInterval(sliderTickInterval);
	}
	speedSpinBox->setSingleStep(spinBoxStep);
	speedSpinBox->setDecimals(spinBoxDecimals);
	sliderFactor = sliderSteps / PercentageFactor;
}

//======================================================================================================================
/*! @brief Display an error message.
*/
void
TestDialogPrivate::showErrorMessage(
	QString const	&message	//!< Error message.
)
{
	(void)QMessageBox::warning(this, "Error", message);
}

//======================================================================================================================
/*! @brief Update the control states.
*/
void
TestDialogPrivate::updateGuiState(void)
{
	LOG_Trace(loggerGuiState, "Update the control states");
	bool	isThrusterBusy = thruster->isBusy();
	bool	isTestingMode = thruster->isTestingMode();
	//
	modeComboBox	->setEnabled(!isTestingMode);
	startPushButton	->setEnabled(!isThrusterBusy && !isTestingMode);
	stopPushButton	->setEnabled(isTestingMode);
}
