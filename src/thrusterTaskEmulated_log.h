#pragma once
#if !defined(THRUSTERTASKEMULATED_LOG_H) && !defined(DOXYGEN_SKIP)
#define THRUSTERTASKEMULATED_LOG_H
//######################################################################################################################
/*! @file
@brief Declare the thruster interface background task emulation module loggers.
*/
//######################################################################################################################

#include "log.h"

//######################################################################################################################
// Loggers

LOG_SETUP_START("Thruster Interface Background Task Emulation Module");

// Top level module logger.
LOG_DEF_LOG(logger,							LOG_DL_TRACE,	"ThrusterTaskEmulated");
// Configuration read background task logger.
LOG_DEF_LOG(loggerConfigurationRead,		LOG_DL_TRACE,	"ThrusterTaskEmulated.ConfiguratioRead");
// Configuration background task logger.
LOG_DEF_LOG(loggerConfigurationWrite,		LOG_DL_TRACE,	"ThrusterTaskEmulated.ConfigurationWrite");
// Object creation/destruction logger.
LOG_DEF_LOG(loggerCreate,					LOG_DL_TRACE,	"ThrusterTaskEmulated.Create");
// Data logger.
LOG_DEF_LOG(loggerEmulationData,			LOG_DL_TRACE,	"ThrusterTaskEmulated.EmulationData");
// Object emulation configuration find logger.
LOG_DEF_LOG(loggerEmulationFind,			LOG_DL_TRACE,	"ThrusterTaskEmulated.EmulationFind");
// Node ID change background task logger.
LOG_DEF_LOG(loggerIdsChange,				LOG_DL_TRACE,	"ThrusterTaskEmulated.IdsChange");
// Query serial ports available logger.
LOG_DEF_LOG(loggerQuerySerialPorts,			LOG_DL_TRACE,	"ThrusterTaskEmulated.QuerySerialPorts");
// Rpn evaluation logger.
LOG_DEF_LOG(loggerRpn,						LOG_DL_TRACE,	"ThrusterTaskEmulated.Rpn");
// Scan for thrusters background task logger.
LOG_DEF_LOG(loggerScanForThrusters,			LOG_DL_TRACE,	"ThrusterTaskEmulated.ScanForThrusters");
// Module signals logger.
LOG_DEF_LOG(loggerSignals,					LOG_DL_TRACE,	"ThrusterTaskEmulated.Signals");
// Test thruster logger.
LOG_DEF_LOG(loggerTestingMode,				LOG_DL_TRACE,	"ThrusterTaskEmulated.TestingMode");

LOG_SETUP_STOP();

//######################################################################################################################
#endif
