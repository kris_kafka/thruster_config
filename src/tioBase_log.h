#pragma once
#if !defined(TIOBASE_LOG_H) && !defined(DOXYGEN_SKIP)
#define TIOBASE_LOG_H
//######################################################################################################################
/*! @file
@brief Declare the thruster I/O base module loggers.
*/
//######################################################################################################################

#include "log.h"

//######################################################################################################################
// Loggers

LOG_SETUP_START("Thruster Generic I/O Base Module");

// Base logger.
LOG_DEF_LOG(logger,				LOG_DL_INFO,	"TioBase");
// Create/destroy logger.
LOG_DEF_LOG(loggerCreate,		LOG_DL_INFO,	"TioBase.Create");
// Open logger.
LOG_DEF_LOG(loggerGenericIo,	LOG_DL_INFO,	"TioBase.GenericIo");
// Initialize logger.
LOG_DEF_LOG(loggerInitialize,	LOG_DL_INFO,	"TioBase.Initialize");
// Null packet logger.
LOG_DEF_LOG(loggerNullPacket,	LOG_DL_TRACE,	"TioBase.NullPacket");
// Open/close logger.
LOG_DEF_LOG(loggerOpen,			LOG_DL_INFO,	"TioBase.Open");
// Query logger.
LOG_DEF_LOG(loggerQuery,		LOG_DL_INFO,	"TioBase.Query");
// Reply logger.
LOG_DEF_LOG(loggerReply,		LOG_DL_TRACE,	"TioBase.Reply");
// Send command logger.
LOG_DEF_LOG(loggerSend,			LOG_DL_TRACE,	"TioBase.Send");
// Timer logger.
LOG_DEF_LOG(loggerTimer,		LOG_DL_INFO,	"TioBase.Timer");

LOG_SETUP_STOP();

//######################################################################################################################
#endif
