/*! @file
@brief Define the thruster I/O module.
*/
//######################################################################################################################

#include "app_com.h"

#include <cstddef>
#include <string>

#include <QtCore/QSettings>

#include "videoray/applib/std_device_types.h"

#include "configCsr.h"
#include "configGio.h"
#include "csrBlob.h"
#include "thruster.h"
#include "tioConfigWrite.h"
#include "tioConfigWrite_log.h"
#include "tioImpl.h"
#include "tioParms.h"

//######################################################################################################################
// Local classes.

class	TioImplConfigWrite;
class	TioParmsMaxPower;
class	TioParmsMotorId;
class	TioParmsRotation;
class	TioParmsSaveSettings;

//######################################################################################################################
// Local functions

static void		maxPowerReply(TioParms *biop, Protocol_VRCSR_Packet const &packet, int deviceTypeSize);
static void		motorIdReply(TioParms *biop, Protocol_VRCSR_Packet const &packet, int deviceTypeSize);
static void		rotationReply(TioParms *biop, Protocol_VRCSR_Packet const &packet, int deviceTypeSize);
static void		saveSettingsReply(TioParms *biop, Protocol_VRCSR_Packet const &packet, int deviceTypeSize);

//######################################################################################################################
/*! \brief Read Configuration CSR data implementation.
*/
class TioImplConfigWrite
:
	public	TioImpl
{
public:		// Constructors & destructors
				TioImplConfigWrite(std::string &portName, int nodeId, bool const volatile &halt);
	virtual		~TioImplConfigWrite(void);

public:		// Functions
	Thruster::Error		maxPower(int maximumPower, QByteArray const &csrBlob);
	Thruster::Error		motorId(int motorId, QByteArray const &csrBlob);
	Thruster::Error		rotation(bool rotationReverse, QByteArray const &csrBlob);
	Thruster::Error		saveSettings(bool isSaving);
};

//======================================================================================================================
/*! \brief Write Configuration CSR maximum power.
*/
class TioParmsMaxPower
:
	public	TioParms
{
public:		// Constructors & destructors
	TioParmsMaxPower(int nodeId, int maximumPower_, QByteArray const &csrBlob);

public:		// Data
	MaximumPower_t	maximumPower;			//!< New maximum power.
	int				maximumPowerOffset;		//!< CSR offset of maximum power.
	CsrInfo::Type	maximumPowerType;		//!< CSR type of maximum power.
};

//======================================================================================================================
/*! \brief Write Configuration CSR motor ID.
*/
class TioParmsMotorId
:
	public	TioParms
{
public:		// Constructors & destructors
	TioParmsMotorId(int nodeId, int motorId_, QByteArray const &csrBlob);

public:		// Data
	MotorId_t		motorId;			//!< New motor ID.
	int				motorIdOffset;		//!< CSR offset of motorId.
	CsrInfo::Type	motorIdType;		//!< CSR type of motorId.
};

//======================================================================================================================
/*! \brief Write Configuration CSR rotation.
*/
class TioParmsRotation
:
	public	TioParms
{
public:		// Constructors & destructors
	TioParmsRotation(int nodeId, bool rotationReverse_, QByteArray const &csrBlob);

public:		// Data
	int					motorControlFlagsOffset;	//!< CSR offset of motorControlFlags.
	CsrInfo::Type		motorControlFlagsType;		//!< CSR type of motorControlFlags.
	bool				rotationReverse;			//!< New rotation reversed.
	MotorControlFlags_t	rotationReversedFalse;		//!< False value of rotation reversed flag in motorControlFlags.
	MotorControlFlags_t	rotationReversedKeep;		//!< Keep bit mask of motorControlFlags for rotation reversed flag.
	MotorControlFlags_t	rotationReversedMask;		//!< Mask of motorControlFlags for rotation reversed flag.
	MotorControlFlags_t	rotationReversedTrue;		//!< True value of rotation reversed flag in motorControlFlags.
};

//======================================================================================================================
/*! \brief Write Configuration save settings.
*/
class TioParmsSaveSettings
:
	public	TioParms
{
public:		// Constructors & destructors
	TioParmsSaveSettings(int nodeId, bool isSaving_);

public:		// Data
	bool			isSaving;				//!< Is saving settings (or checking that they were saved).
	SaveSettings_t	saveSettingsValue;		//!< CSR save settings control field value.
	int				saveSettingsOffset;		//!< CSR offset of the save settings control field.
	CsrInfo::Type	saveSettingsType;		//!< CSR type of the save settings control field.
};

//######################################################################################################################
/*! @brief Create a TioConfigWrite object.
*/
TioConfigWrite::TioConfigWrite(
	QString const		&serialPort,	//!<[in] Serial port name.
	int					nodeId,			//!<[in] Node ID.
	bool const volatile	&halt			//!<[in] Halt I/O.
)
:
	TioBase		(serialPort, nodeId, halt),
	connection	(Q_NULLPTR)
{
	LOG_Trace(loggerCreate,
				QString("Creating TioConfigWrite object: serialPort='%1', nodeId=%2").arg(serialPort).arg(nodeId));
}

//======================================================================================================================
/*! @brief Destroy a TioConfigWrite object.
*/
TioConfigWrite::~TioConfigWrite(void)
{
	LOG_Trace(loggerCreate,
				QString("Destroying TioConfigWrite object: serialPort='%1', nodeId=%2").arg(serialPort).arg(nodeId));
}

//======================================================================================================================
/*! @brief Create a TioImplConfigWrite object.
*/
TioImplConfigWrite::TioImplConfigWrite(
	std::string			&portName,	//!<[in] Serial port name.
	int					nodeId,		//!<[in] Node ID.
	bool const volatile	&halt		//!<[in] Halt I/O.
)
:
	TioImpl	(portName, nodeId, halt)
{
	LOG_Trace(loggerCreate,
				QString("Creating TioImplConfigWrite object: serialPort='%1', nodeId=%2").arg(serialPort).arg(nodeId));
}

//======================================================================================================================
/*! @brief Destroy a TioImplConfigWrite object.
*/
TioImplConfigWrite::~TioImplConfigWrite(void)
{
	LOG_Trace(loggerCreate,
				QString("Destroying TioImplConfigWrite object: serialPort='%1', nodeId=%2").arg(serialPort).arg(nodeId));
}

//======================================================================================================================
/*! @brief Create a TioParmsMaxPower object.
*/
TioParmsMaxPower::TioParmsMaxPower(
	int					nodeId,			//!<[in] Reply packet valid node ID.
	int					maximumPower_,	//!<[in] Maximum power to write to the thruster.
	QByteArray const	&csrBlob		//!<[in] CSR blob.
)
:
	TioParms			(maxPowerReply, "ConfigWrite MaximumPower"),
	maximumPower		(maximumPower_),
	maximumPowerOffset	(0),
	maximumPowerType	(CsrInfo::Type::Bad)
{
	LOG_Trace(loggerMaxPowerParameters,
				QString("Creating TioParmsMaxPower object: nodeId=%1, maximumPower=%2")
						.arg(nodeId).arg(maximumPower));
	//
	IniFile	iniFile;
	initialize(iniFile, INI_GROUP(CfgGioConfWrMaxPower));
	//
	maximumPowerOffset	= iniFile.INI_GET(CfgCsrMaximumPowerOffset);
	maximumPowerType	= CsrInfo::type(iniFile.INI_GET(CfgCsrMaximumPowerType));
	//
	replyValidAddress	= maximumPowerOffset;
	int lastCsrAddress	= maximumPowerOffset + CsrInfo::size(maximumPowerType);
	replyValidLength	= lastCsrAddress - replyValidAddress;
	//
	replyValidFlags		= RESPONSE_TYPE_FLAG;
	replyValidNodeId	= nodeId;
	//
	LOG_AssertMsg(loggerMaxPowerParameters, 0 < replyValidLength,
					QString("Invalid reply length").arg(replyValidLength));
	// Do nothing if the maximum power has not changed.
	MaximumPower_t	csrMaximumPower;
	CsrBlob(csrBlob, -1)	.move(maximumPowerOffset)	.get(csrMaximumPower, maximumPowerType);
	if (csrMaximumPower == maximumPower) {
		isIoNeeded = false;
		LOG_Trace(loggerMaxPowerParameters,
					QString("I/O not needed: nodeId=%1, maximumPower=%2").arg(nodeId).arg(maximumPower));
		return;
	}
	// Generate the packet data.
	QByteArray	payload(replyValidLength, '\0');
	CsrBlob	csr(payload, replyValidAddress);
	csr	.move(maximumPowerOffset)	.set(maximumPower, maximumPowerType);
	payload = csr.buffer();
	//
	commandSize = protocol_vrcsr_calc_packet_size(payload.size(), false, PROTOCOL_VRCSR);
	ByteP	packetBuffer = new (std::nothrow) Byte[commandSize];
	LOG_CHK_PTR_MSG(loggerMaxPowerParameters, packetBuffer, "Unable to create TioParmsMaxPower packetBuffer");
	commandBuffer.reset(packetBuffer);
	//
	(void)protocol_vrcsr_insert_data((ByteP)payload.data(),
										payload.size(),
										false,
										commandBuffer.get(),
										PROTOCOL_VRCSR);
	(void)protocol_vrcsr_build_request_inplace(nodeId,
												REQUEST_LEN(replyValidLength),
												replyValidAddress,
												payload.size(),
												commandBuffer.get(),
												PROTOCOL_VRCSR);
}

//======================================================================================================================
/*! @brief Create a TioParmsMotorId object.
*/
TioParmsMotorId::TioParmsMotorId(
	int					nodeId,		//!<[in] Reply packet valid node ID.
	int					motorId_,	//!<[in] Motor ID to write to the thruster.
	QByteArray const	&csrBlob	//!<[in] CSR blob.
)
:
	TioParms			(motorIdReply, "ConfigWrite MotorId"),
	motorId				(motorId_),
	motorIdOffset		(0),
	motorIdType			(CsrInfo::Type::Bad)
{
	LOG_Trace(loggerMotorIdParameters,
				QString("Creating TioParmsMotorId object: nodeId=%1, motorId=%2").arg(nodeId).arg(motorId));
	//
	IniFile	iniFile;
	initialize(iniFile, INI_GROUP(CfgGioConfWrMotorId));
	//
	motorIdOffset	= iniFile.INI_GET(CfgCsrMotorIdOffset);
	motorIdType		= CsrInfo::type(iniFile.INI_GET(CfgCsrMotorIdType));
	//
	replyValidAddress	= motorIdOffset;
	int lastCsrAddress	= motorIdOffset + CsrInfo::size(motorIdType);
	replyValidLength	= lastCsrAddress - replyValidAddress;
	//
	replyValidFlags		= RESPONSE_TYPE_FLAG;
	replyValidNodeId	= nodeId;
	//
	LOG_AssertMsg(loggerMotorIdParameters, 0 < replyValidLength,
					QString("Invalid reply length").arg(replyValidLength));
	// Do nothing if the motor ID has not changed.
	MotorId_t	csrMotorId;
	CsrBlob(csrBlob, -1)	.move(motorIdOffset)	.get(csrMotorId, motorIdType);
	if (csrMotorId == motorId) {
		isIoNeeded = false;
		LOG_Trace(loggerMotorIdParameters,
					QString("I/O not needed: nodeId=%1, motorId=%2").arg(nodeId).arg(motorId));
		return;
	}
	// Generate the packet data.
	QByteArray	payload(replyValidLength, '\0');
	CsrBlob	csr(payload, replyValidAddress);
	csr	.move(motorIdOffset)	.set(motorId, motorIdType);
	payload = csr.buffer();
	//
	commandSize = protocol_vrcsr_calc_packet_size(payload.size(), false, PROTOCOL_VRCSR);
	ByteP	packetBuffer = new (std::nothrow) Byte[commandSize];
	LOG_CHK_PTR_MSG(loggerMotorIdParameters, packetBuffer, "Unable to create TioParmsMotorId packetBuffer");
	commandBuffer.reset(packetBuffer);
	//
	(void)protocol_vrcsr_insert_data((ByteP)payload.data(),
										payload.size(),
										false,
										commandBuffer.get(),
										PROTOCOL_VRCSR);
	(void)protocol_vrcsr_build_request_inplace(nodeId,
												REQUEST_LEN(replyValidLength),
												replyValidAddress,
												payload.size(),
												commandBuffer.get(),
												PROTOCOL_VRCSR);
}

//======================================================================================================================
/*! @brief Create a TioParmsRotation object.
*/
TioParmsRotation::TioParmsRotation(
	int					nodeId,				//!<[in] Reply packet valid node ID.
	bool				rotationReverse_,	//!<[in] Rotation reversed to write to the thruster.
	QByteArray const	&csrBlob			//!<[in] CSR blob.
)
:
	TioParms				(rotationReply, "ConfigWrite Rotation"),
	motorControlFlagsOffset	(0),
	motorControlFlagsType	(CsrInfo::Type::Bad),
	rotationReverse			(rotationReverse_),
	rotationReversedFalse	(0),
	rotationReversedKeep	(0),
	rotationReversedMask	(0),
	rotationReversedTrue	(0)
{
	LOG_Trace(loggerRotationParameters,
				QString("Creating TioParmsRotation object: nodeId=%1, rotationReverse=%2")
						.arg(nodeId).arg(rotationReverse));
	//
	IniFile	iniFile;
	initialize(iniFile, INI_GROUP(CfgGioConfWrRoration));
	//
	motorControlFlagsOffset	= iniFile.INI_GET(CfgCsrMotorControlFlagsOffset);
	rotationReversedFalse	= iniFile.INI_GET(CfgCsrRotationReversedFalse);
	rotationReversedKeep	= iniFile.INI_GET(CfgCsrRotationReversedKeep);
	rotationReversedMask	= iniFile.INI_GET(CfgCsrRotationReversedMask);
	rotationReversedTrue	= iniFile.INI_GET(CfgCsrRotationReversedTrue);
	motorControlFlagsType	= CsrInfo::type(iniFile.INI_GET(CfgCsrMotorControlFlagsType));
	//
	replyValidAddress	= motorControlFlagsOffset;
	int lastCsrAddress	= motorControlFlagsOffset + CsrInfo::size(motorControlFlagsType);
	replyValidLength	= lastCsrAddress - replyValidAddress;
	//
	replyValidFlags		= RESPONSE_TYPE_FLAG;
	replyValidNodeId	= nodeId;
	//
	LOG_AssertMsg(loggerRotationParameters, 0 < replyValidLength,
					QString("Invalid reply length").arg(replyValidLength));
	// Do nothing if the maximum power has not changed.
	MotorControlFlags_t	csrMotorControlFlags;
	CsrBlob(csrBlob, -1)	.move(motorControlFlagsOffset)	.get(csrMotorControlFlags, motorControlFlagsType);
	MotorControlFlags_t	newControlFlags
			= (csrMotorControlFlags & rotationReversedKeep)
			| (rotationReversedMask & (rotationReverse ? rotationReversedTrue : rotationReversedFalse));
	if (csrMotorControlFlags == newControlFlags) {
		isIoNeeded = false;
		LOG_Trace(loggerRotationParameters,
					QString("I/O not needed: nodeId=%1, rotationReverse=%2").arg(nodeId).arg(rotationReverse));
		return;
	}
	// Generate the packet data.
	QByteArray	payload(replyValidLength, '\0');
	CsrBlob	csr(payload, replyValidAddress);
	csr	.move(motorControlFlagsOffset)	.set(newControlFlags, motorControlFlagsType);
	payload = csr.buffer();
	//
	commandSize = protocol_vrcsr_calc_packet_size(payload.size(), false, PROTOCOL_VRCSR);
	ByteP	packetBuffer = new (std::nothrow) Byte[commandSize];
	LOG_CHK_PTR_MSG(loggerRotationParameters, packetBuffer, "Unable to create TioParmsRotation packetBuffer");
	commandBuffer.reset(packetBuffer);
	//
	(void)protocol_vrcsr_insert_data((ByteP)payload.data(),
										payload.size(),
										false,
										commandBuffer.get(),
										PROTOCOL_VRCSR);
	(void)protocol_vrcsr_build_request_inplace(nodeId,
												REQUEST_LEN(replyValidLength),
												replyValidAddress,
												payload.size(),
												commandBuffer.get(),
												PROTOCOL_VRCSR);
}

//======================================================================================================================
/*! @brief Create a TioParmsSaveSettings object.
*/
TioParmsSaveSettings::TioParmsSaveSettings(
	int		nodeId,		//!<[in] Reply packet valid node ID.
	bool	isSaving_	//!<[in] Is saving settings (or checking that they were saved).
)
:
	TioParms			(saveSettingsReply, isSaving_ ? "SaveSettings" : "SaveCheck"),
	isSaving			(isSaving_),
	saveSettingsValue	(isSaving_ ? CSR_SAVE_SETTINGS_PASSWORD : CSR_SAVE_SETTINGS_COMPLETE),
	saveSettingsOffset	(0),
	saveSettingsType	(CsrInfo::Type::Bad)
{
	LOG_Trace(loggerSaveSettingsParameters,
				QString("Creating TioParmsSaveSettings object: nodeId=%1, isSaving=%2")
						.arg(nodeId).arg(isSaving));
	//
	IniFile	iniFile;
	initialize(iniFile,
				isSaving ? INI_GROUP(CfgGioSaveSettingStart) : INI_GROUP(CfgGioSaveSettingCheck));
	//
	saveSettingsOffset	= iniFile.INI_GET(CfgCsrSaveSettingsOffset);
	saveSettingsType	= CsrInfo::type(iniFile.INI_GET(CfgCsrSaveSettingsType));
	//
	replyValidAddress	= saveSettingsOffset;
	int lastCsrAddress	= saveSettingsOffset + CsrInfo::size(saveSettingsType);
	replyValidLength	= lastCsrAddress - replyValidAddress;
	//
	replyValidFlags		= RESPONSE_TYPE_FLAG;
	replyValidNodeId	= nodeId;
	//
	LOG_AssertMsg(loggerSaveSettingsParameters, 0 < replyValidLength,
					QString("Invalid reply length").arg(replyValidLength));
	// Generate the payload.
	QByteArray	payload;
	if (isSaving) {
		payload.fill('\0', replyValidLength);
		SaveSettings_t	newSaveSettings = saveSettingsValue;
		CsrBlob	csr(payload, replyValidAddress);
		csr	.move(saveSettingsOffset)	.set(newSaveSettings, saveSettingsType);
		payload = csr.buffer();
	}
	// Generate the packet data.
	commandSize = protocol_vrcsr_calc_packet_size(payload.size(), false, PROTOCOL_VRCSR);
	ByteP	packetBuffer = new (std::nothrow) Byte[commandSize];
	LOG_CHK_PTR_MSG(loggerSaveSettingsParameters, packetBuffer, "Unable to create TioParmsSaveSettings packetBuffer");
	commandBuffer.reset(packetBuffer);
	//
	(void)protocol_vrcsr_insert_data((ByteP)payload.data(),
										payload.size(),
										false,
										commandBuffer.get(),
										PROTOCOL_VRCSR);
	(void)protocol_vrcsr_build_request_inplace(nodeId,
												REQUEST_LEN(replyValidLength),
												replyValidAddress,
												payload.size(),
												commandBuffer.get(),
												PROTOCOL_VRCSR);
}

//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
/*! @brief Write the configuration data.

@return the error code (Thruster::TEC_NoError if no errors).
*/
Thruster::Error
TioConfigWrite::configurationWrite(
	int					motorId,			//!<[in] New motor ID.
	int					maximumPower,		//!<[in] New maximum power level.
	bool				rotationReverse,	//!<[in] New rotation reversed.
	QByteArray const	&csrBlob			//!<[in] CSR blob.
)
{
	LOG_Trace(loggerMain,
				QString("Write configuration data: nodeId=%1, motorId=%2, maximumPower=%3, rotationReverse=%4")
						.arg(nodeId).arg(motorId).arg(maximumPower).arg(rotationReverse));
	LOG_ASSERT_MSG(loggerMain, connection, "Not open");
	//
	Thruster::Error	errorCode;
	errorCode = connection->maxPower(maximumPower, csrBlob);
	if (Thruster::TEC_NoError != errorCode) {
		return errorCode;
	}
	errorCode = connection->motorId(motorId, csrBlob);
	if (Thruster::TEC_NoError != errorCode) {
		return errorCode;
	}
	errorCode = connection->rotation(rotationReverse, csrBlob);
	if (Thruster::TEC_NoError != errorCode) {
		return errorCode;
	}
	errorCode = connection->saveSettings(true);
	if (Thruster::TEC_NoError != errorCode) {
		return errorCode;
	}
	errorCode = connection->saveSettings(false);
	if (Thruster::TEC_NoError != errorCode) {
		return errorCode;
	}
	return errorCode;
}

//======================================================================================================================
/*! @brief Close the serial port.
*/
void
TioConfigWrite::doClose(void)
{
	LOG_Trace(loggerOpen,
				QString("Closing connection: serialPort='%1', nodeId=%2").arg(serialPort).arg(nodeId));
	// Done if already closed.
	if (!connection.isNull()) {
		connection.reset();
	}
}

//======================================================================================================================
/*! @brief Open the serial port.

@return true if no errors occur else false.
*/
bool
TioConfigWrite::doOpen(void)
{
	LOG_Trace(loggerOpen,
				QString("Opening connection: serialPort='%1', nodeId=%2").arg(serialPort).arg(nodeId));
	// Done if already open.
	if (!connection.isNull()) {
		return true;
	}
	std::string portName = serialPort.toStdString();
	connection.reset(new TioImplConfigWrite(portName, nodeId, halt));
	return !connection.isNull() && connection->isOpen();
}

//======================================================================================================================
/*! @brief Write the configuration maximum power.

@return the error code (Thruster::TEC_NoError if no errors).
*/
Thruster::Error
TioImplConfigWrite::maxPower(
	int					maximumPower,	//!<[in] Maximum power to write to the thruster.
	QByteArray const	&csrBlob		//!<[in] CSR blob.
)
{
	LOG_Trace(loggerMaxPowerCommand, QString("Writing configuration CSR maximum power: %1").arg(maximumPower));
	TioParmsMaxPower	ciop(nodeId, maximumPower, csrBlob);
	return genericIo(&ciop);
}

//======================================================================================================================
/*! @brief Process a configuration CSR data reply packet.
*/
void
maxPowerReply(
	TioParms					*biop,			//!<[in,out] I/O parameters.
	Protocol_VRCSR_Packet const	&packet,		//!<[in] Reply packet.
	int							deviceTypeSize	//!<[in] Size of device type data prefix.
)
{
	LOG_Trace(loggerMaxPowerReply, "Received write configuration CSR maximum power reply");
	TioParmsMaxPower	*iop = dynamic_cast<TioParmsMaxPower *>(biop);
	LOG_ASSERT_MSG(loggerMaxPowerReply, iop, "maxPowerReply: iop is invalid");
	// Extract the data from the payload.
	MaximumPower_t	csrMaximumPower;
	CsrBlob(packet.payload, packet.header->length, iop->replyValidAddress - deviceTypeSize)
		.move(iop->maximumPowerOffset)
		.get(csrMaximumPower, iop->maximumPowerType);
	// Update the status.
	if (iop->maximumPower == csrMaximumPower) {
		iop->errorCode = Thruster::TEC_NoError;
		iop->isReplySeen = true;
	} else {
		iop->errorCode = Thruster::TEC_WriteError;
		LOG_Error(loggerMaxPowerReply,
					QString("Unexpected reply: Data mismatch (maximum power): expected=%1, payload=%2")
							.arg(iop->maximumPower).arg(csrMaximumPower));
	}
}

//======================================================================================================================
/*! @brief Write the configuration motor ID.

@return the error code (Thruster::TEC_NoError if no errors).
*/
Thruster::Error
TioImplConfigWrite::motorId(
	int					motorId,	//!<[in] Motor ID to write to the thruster.
	QByteArray const	&csrBlob	//!<[in] CSR blob.
)
{
	LOG_Trace(loggerMotorIdCommand, QString("Writing configuraton CSR motor ID: %1").arg(motorId));
	TioParmsMotorId	ciop(nodeId, motorId, csrBlob);
	return genericIo(&ciop);
}

//======================================================================================================================
/*! @brief Process a configuration CSR data reply packet.
*/
void
motorIdReply(
	TioParms					*biop,			//!<[in,out] I/O parameters.
	Protocol_VRCSR_Packet const	&packet,		//!<[in] Reply packet.
	int							deviceTypeSize	//!<[in] Size of device type data prefix.
)
{
	LOG_Trace(loggerMotorIdReply, "Received write configuration CSR motor ID reply");
	TioParmsMotorId	*iop = dynamic_cast<TioParmsMotorId *>(biop);
	LOG_ASSERT_MSG(loggerMotorIdReply, iop, "motorIdReply: iop is invalid");
	// Extract the data from the payload.
	MotorId_t	csrMotorId;
	CsrBlob(packet.payload, packet.header->length, iop->replyValidAddress - deviceTypeSize)
		.move(iop->motorIdOffset)
		.get(csrMotorId, iop->motorIdType);
	// Update the status.
	if (iop->motorId == csrMotorId) {
		iop->errorCode = Thruster::TEC_NoError;
		iop->isReplySeen = true;
	} else {
		iop->errorCode = Thruster::TEC_WriteError;
		LOG_Error(loggerMotorIdReply,
					QString("Unexpected reply: Data mismatch (motor ID): expected=%1, payload=%2")
							.arg(iop->motorId).arg(csrMotorId));
	}
}

//======================================================================================================================
/*! @brief Write the configuration rotation.

@return the error code (Thruster::TEC_NoError if no errors).
*/
Thruster::Error
TioImplConfigWrite::rotation(
	bool				rotationReverse,	//!<[in] Rotation reversed to write to the thruster.
	QByteArray const	&csrBlob			//!<[in] CSR blob.
)
{
	LOG_Trace(loggerRotationCommand, QString("Writing configuration CSR maximum power: %1").arg(rotationReverse));
	TioParmsRotation	ciop(nodeId, rotationReverse, csrBlob);
	return genericIo(&ciop);
}

//======================================================================================================================
/*! @brief Process a configuration CSR data reply packet.
*/
void
rotationReply(
	TioParms					*biop,			//!<[in,out] I/O parameters.
	Protocol_VRCSR_Packet const	&packet,		//!<[in] Reply packet.
	int							deviceTypeSize	//!<[in] Size of device type data prefix.
)
{
	LOG_Trace(loggerRotationReply, "Received write configuration CSR maximum power reply");
	TioParmsRotation	*iop = dynamic_cast<TioParmsRotation *>(biop);
	LOG_ASSERT_MSG(loggerRotationReply, iop, "rotationReply: iop is invalid");
	// Extract the data from the payload.
	MotorControlFlags_t	motorControlFlags;
	CsrBlob(packet.payload, packet.header->length, iop->replyValidAddress - deviceTypeSize)
		.move(iop->motorControlFlagsOffset)
		.get(motorControlFlags, iop->motorControlFlagsType);
	// Update the status.
	bool	csrRotationReverse = iop->rotationReversedTrue == (iop->rotationReversedMask & motorControlFlags);
	if (iop->rotationReverse == csrRotationReverse) {
		iop->errorCode = Thruster::TEC_NoError;
		iop->isReplySeen = true;
	} else {
		iop->errorCode = Thruster::TEC_WriteError;
		LOG_Error(loggerRotationReply,
					QString("Unexpected reply: Data mismatch (rotation reverse): expected=%1, payload=%2")
							.arg(iop->rotationReverse).arg(csrRotationReverse));
	}
}

//======================================================================================================================
/*! @brief Read after optionally writing the CSR save settings control field.

@return the error code (Thruster::TEC_NoError if no errors).
*/
Thruster::Error
TioImplConfigWrite::saveSettings(
	bool	isSaving	//!< Maximum power to write to the thruster.
)
{
	LOG_Trace(loggerSaveSettingsCommand, QString("Save settings: %1").arg(isSaving ? "start" : "check"));
	TioParmsSaveSettings	ciop(nodeId, isSaving);
	return genericIo(&ciop);
}

//======================================================================================================================
/*! @brief Process a save settings reply packet.
*/
void
saveSettingsReply(
	TioParms					*biop,			//!<[in,out] I/O parameters.
	Protocol_VRCSR_Packet const	&packet,		//!<[in] Reply packet.
	int							deviceTypeSize	//!<[in] Size of device type data prefix.
)
{
	TioParmsSaveSettings	*iop = dynamic_cast<TioParmsSaveSettings *>(biop);
	LOG_ASSERT_MSG(loggerSaveSettingsReply, iop, "saveSettingsReply: iop is invalid");
	LOG_Trace(loggerSaveSettingsReply,
				QString("Received save settings %1 reply").arg(iop->isSaving ? "start" : "check"));
	// Extract the data from the payload.
	SaveSettings_t	csrSaveSettings;
	CsrBlob(packet.payload, packet.header->length, iop->replyValidAddress - deviceTypeSize)
		.move(iop->saveSettingsOffset)
		.get(csrSaveSettings, iop->saveSettingsType);
	// Update the status.
	if (iop->saveSettingsValue == csrSaveSettings) {
		iop->errorCode = Thruster::TEC_NoError;
		iop->isReplySeen = true;
	} else {
		iop->errorCode = Thruster::TEC_WriteError;
		LOG_Error(loggerSaveSettingsReply,
					QString("Unexpected reply: Data mismatch (save settings): expected=%1, payload=%2")
							.arg(iop->saveSettingsValue).arg(csrSaveSettings));
	}
}
