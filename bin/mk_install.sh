prjDir=${ProjectsDir:-~/Projects}
isDebug=${dbgIsDebug:-0}

# Application related source directories.
appName=thruster_config
AppScript=${appName}
AppScriptSh=${appName}.sh
ReadMe=README.md
appPrjDir=$prjDir/${appName}
appSrcDir=${appPrjDir}/src

# Debug/release related values.
case $isDebug in
0)
    echo Building release version.
    debugSuffix=
    appBldDir=${appPrjDir}/release
    ;;
*)
    echo Building debug version.
    debugSuffix=d
    appBldDir=${appPrjDir}/debug
    ;;
esac

# Qt related source directories.
qtSrcDir=/usr/local/qt/5.9.1/5.9.1/gcc_64
qtLibSrcDir=${qtSrcDir}/lib
qtPlatformsSrcDir=${qtSrcDir}/plugins/platforms
qtIconEnginesSrcDir=${qtSrcDir}/plugins/iconengines
qtImageFormatsSrcDir=${qtSrcDir}/plugins/imageformats

# Standard library source directories.
stdSrcDir=/usr/lib/x86_64-linux-gnu

# Destination directories.
appDstDir=${appPrjDir}/thruster_config
qtPlatformsDstDir=${appDstDir}/platforms
qtIconEnginesDstDir=${appDstDir}/iconengines
qtImageFormatsDstDir=${appDstDir}/imageformats

# Ensure all of the destiantion directories exist.
mkdir -p ${appDstDir}
mkdir -p ${qtPlatformsDstDir}
mkdir -p ${qtIconEnginesDstDir}
mkdir -p ${qtImageFormatsDstDir}

# Copy the application files.
cp ${appBldDir}/${appName}${debugSuffix} ${appDstDir}/${appName}.pgm
cp ${appSrcDir}/${ReadMe}                ${appDstDir}
cp ${appSrcDir}/qt.conf                  ${appDstDir}
cp ${appSrcDir}/${AppScript}             ${appDstDir}/${AppScript}
cp ${appSrcDir}/${AppScriptSh}           ${appDstDir}/${AppScriptSh}

# Copy the Qt libraries.
cp ${qtLibSrcDir}/libicudata.so.56      ${appDstDir}
cp ${qtLibSrcDir}/libicui18n.so.56      ${appDstDir}
cp ${qtLibSrcDir}/libicuuc.so.56        ${appDstDir}
cp ${qtLibSrcDir}/libQt5Core.so.5       ${appDstDir}
cp ${qtLibSrcDir}/libQt5DBus.so.5       ${appDstDir}
cp ${qtLibSrcDir}/libQt5Gui.so.5        ${appDstDir}
cp ${qtLibSrcDir}/libQt5SerialPort.so.5 ${appDstDir}
cp ${qtLibSrcDir}/libQt5Widgets.so.5    ${appDstDir}
cp ${qtLibSrcDir}/libQt5XcbQpa.so.5     ${appDstDir}

# Copy the Qt plug-ins.
cp ${qtPlatformsSrcDir}/libqxcb.so       ${qtPlatformsDstDir}
cp ${qtIconEnginesSrcDir}/libqsvgicon.so ${qtIconEnginesDstDir}
cp ${qtImageFormatsSrcDir}/libqgif.so    ${qtImageFormatsDstDir}
cp ${qtImageFormatsSrcDir}/libqicns.so   ${qtImageFormatsDstDir}
cp ${qtImageFormatsSrcDir}/libqico.so    ${qtImageFormatsDstDir}
cp ${qtImageFormatsSrcDir}/libqjp2.so    ${qtImageFormatsDstDir}
cp ${qtImageFormatsSrcDir}/libqjpeg.so   ${qtImageFormatsDstDir}
cp ${qtImageFormatsSrcDir}/libqsvg.so    ${qtImageFormatsDstDir}
cp ${qtImageFormatsSrcDir}/libqtga.so    ${qtImageFormatsDstDir}
cp ${qtImageFormatsSrcDir}/libqtiff.so   ${qtImageFormatsDstDir}
cp ${qtImageFormatsSrcDir}/libqwbmp.so   ${qtImageFormatsDstDir}
cp ${qtImageFormatsSrcDir}/libqwebp.so   ${qtImageFormatsDstDir}

# Copy the standard libraries.
cp ${stdSrcDir}/libstdc++.so.6 ${appDstDir}/libstdc++.so.6

# Generate the default control files.
${appDstDir}/${AppScript} --setup_ini=${appDstDir}/default.ini --setup_log=${appDstDir}/default.xml --minimum_level=warn

